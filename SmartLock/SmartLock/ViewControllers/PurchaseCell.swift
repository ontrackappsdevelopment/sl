//
//  PurchaseCell.swift
//  SmartLock
//
//  Created by Bogachev on 5/31/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol PurchaseCellDelegate {
	var price: String? { get }
	var period: String? { get }
	var description: String? { get }
	var disabled: Bool { get }
}

class PurchaseCell: UITableViewCell {

	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var periodLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var cellContentView: UIView!
	
	var delegate: PurchaseCellDelegate?
	
	func configurate(with delegate: PurchaseCellDelegate) {
		self.delegate = delegate
		
		priceLabel.text = delegate.price
		periodLabel.text = delegate.period
		descriptionLabel.text = delegate.description
		
		cellContentView.alpha = delegate.disabled ? 0.5 : 1
	}
	
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }

}
