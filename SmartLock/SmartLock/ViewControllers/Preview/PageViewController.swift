//
//  PageViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/22/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol PageViewControllerDelegate {
	func pageViewController(pageViewController: UIPageViewController, currentPage: PreviewViewController, pageIndexChanged index: Int, count: Int)
	func movePageViewController(pageViewController: UIPageViewController)
}

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
	
	var objects = [NSManagedObject]()
	
	var directory: File {
		return FileService.sharedInstance.currentDirectory
	}
	
	var currentPage: PreviewViewController! {
		didSet {
			PreviewViewController.index = currentPage.currentIndex
			if pageViewControllerDelegate != nil {
				pageViewControllerDelegate?.pageViewController(pageViewController: self, currentPage: currentPage, pageIndexChanged: PreviewViewController.index + 1, count: objects.count)
			}
		}
	}
	
	var pageViewControllerDelegate: PageViewControllerDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		objects = FileService.fetchAlbumFiles(relativePath: directory.relativePath)
		
		delegate = self
		dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if currentPage == nil {
			
			let index = PreviewViewController.index
			let object = objects[index]
			let relativePath = object.value(forKey: "mobPath") as? String ?? ""
			let file = File(relativePath: relativePath)
			
			if let page = viewControllersForFile(file: file, index: index) {
				currentPage = page
				setViewControllers([currentPage], direction: .forward, animated: true, completion: nil)
			}
		}
		
	}
	
	func getFileByIndex(index: Int) -> File {
		let object = objects[index]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		return File(relativePath: relativePath)
	}
	
	func setViewControllerByIndex(index: Int, completion: @escaping () -> ()) {
		
		let object = objects[index]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let file = File(relativePath: relativePath)
		
		currentPage = viewControllersForFile(file: file, index: index)!
		
		self.setViewControllers([self.currentPage], direction: .forward, animated: true) { (finished) in
			
			DispatchQueue.main.async {
				
				self.setViewControllers([self.currentPage], direction: .forward, animated: false, completion: nil)
				completion()
				
			}
			
		}
		
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		return getPrevious()
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		return getNext()
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
		if pageViewControllerDelegate != nil {
			pageViewControllerDelegate?.movePageViewController(pageViewController: self)
		}
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
		currentPage = pageViewController.viewControllers?.first as! PreviewViewController!
	}
	
	func viewControllersForFile(file: File, index: Int) -> PreviewViewController? {
		
		if file.fileType == .image {
			
			let contr = storyboard?.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
			
			contr.imageURL = file.path
			contr.currentIndex = index
			
			return contr
			
		}
		
		if file.fileType == .video {
			
			let contr = storyboard?.instantiateViewController(withIdentifier: "VideoPreviewViewController") as! VideoPreviewViewController
			
			contr.movieURL = file.url
			contr.currentIndex = index
			var rect = view.frame
			rect.size.height -= 64
			contr.previewImage = file.previewImageForVideo(rect: view.frame)
			
			return contr
			
		}
		
		return nil
		
	}
	
	func getNext() -> UIViewController {
		
		var nextIndex = currentPage.currentIndex + 1
		
		if nextIndex == objects.count {
			nextIndex = 0
		}
		
		let object = objects[nextIndex]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let nextFile = File(relativePath: relativePath)
		
		return viewControllersForFile(file: nextFile, index: nextIndex)!
	}
	
	func getPrevious() -> UIViewController {
		
		var previousIndex = currentPage.currentIndex - 1
		
		if previousIndex < 0 {
			
			previousIndex = objects.count - 1
			
		}
		
		let object = objects[previousIndex]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let previousFile = File(relativePath: relativePath)
		
		return viewControllersForFile(file: previousFile, index: previousIndex)!
	}
	
	func play() {
		let file = getFileByIndex(index: PreviewViewController.index)
		if file.fileType == .video {
			(currentPage as! VideoPreviewViewController).play()
		}
	}
	
	func stop() {
		let file = getFileByIndex(index: PreviewViewController.index)
		if file.fileType == .video {
			(currentPage as! VideoPreviewViewController).stop()
		}
	}
	
}
