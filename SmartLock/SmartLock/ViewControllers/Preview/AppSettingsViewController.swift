//
//  AppSettingsViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import UserNotifications
import SwiftyStoreKit
import YandexMobileMetrica
import MessageUI

extension SettingsViewControllerDelegate {
	func configureNavigationBar(navigationBarView: NavigationBarView, target: UIViewController) { }
}

class AppSettingsViewControllerDelegate: NSObject, SettingsViewControllerDelegate, MFMailComposeViewControllerDelegate {
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		print("\result")
		controller.dismiss(animated: true, completion: nil)
	}
	
	func configureNavigationBar(navigationBarView: NavigationBarView, target: UIViewController) {
		navigationBarView.setupNoButton(button: navigationBarView.leftTopBarButton)
		navigationBarView.setupRightButton(image: nil, title: "Close", target: target, action: #selector(SettingsViewController.onFinish))
	}
	
	var appStatusText: String {
		
		switch UserService.sharedService.user.appUsingType {
		case .freeTrial:
			return UserService.sharedService.user.currentAppStatus
		case .locked:
			return "limited version"
		default: break
		}
		
		if UserService.sharedService.user.canBackup == true {
			
			let size = Float64(FileService.sharedInstance.currentDirectory.getSize())  / 1024 / 1024 / 1024
			
			if size < 25 {
				return String(format: "Stored in cloud: %.2f of 25 GB available", size)
			} else {
				return "out of \nbackup space"
			}
			
		}
		
		return ""
		
	}
	
	var title: String {
		return "Settings"
	}
	
	var settings: Settings {
		
		return Settings(headers: [""], cells: [
			
			[Setting(title: "Passcode", info: nil, badge: "", icon: nil, type: .detailsAndDisclosure, isExpanded: true, value: LockService.sharedService.user?.mobLockType, action: { (value, compleation: (() -> ())?) in
				
				let controller: BaseViewController = value as! BaseViewController
				
				controller.unlockScreen(alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, canOpenDecoy: false, canCancel: true, success: {
					
					SettingsViewController.inistantiate(delegate: PasscodeSettingsViewControllerDelegate()).pushFrom(viewController: value as! UIViewController, transitionFade: UIDevice.current.isIpad())
					
				})
				
			}),
			 Setting(title: "Recovery email", info: nil, badge: "", icon: nil, type: .disclosure, isExpanded: true, value: nil, action: { (value, compleation: (() -> ())?) in
				
				if let email = LockService.sharedService.user?.mobEmail, email != "" {
					EmailViewController.change().pushFrom(viewController: value as! UIViewController, transitionFade: false)
				} else {
					EmailViewController.register().pushFrom(viewController: value as! UIViewController, transitionFade: false)
				}
				
			}),
			 Setting(title: "Backup over cellular data", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: UserService.sharedService.user.canBackup, value: LockService.sharedService.user?.mobAllowUseMobileInternet, action: { (value, compleation: (() -> ())?) in
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						localUser.mobAllowUseMobileInternet = value as! Bool
					}
				})
				
			}) ,
			 Setting(title: "FAQ", info: nil, badge: "", icon: nil, type: .disclosure, isExpanded: true, value: nil, action: { (value, compleation: (() -> ())?) in
				YMMYandexMetrica.reportEvent("FAQ", parameters: nil, onFailure: nil)
				StoryboardScene.Main.instantiateTutorialScene().push(from: value as! UIViewController)
			}),
			Setting(title: "Privacy Policy", info: nil, badge: "", icon: nil, type: .disclosure, isExpanded: true, value: nil, action: { (value, compleation: (() -> ())?) in
								
				WebViewController.instantiate(url: URL(string:"http://chatkeepersave.me/privacy.html")!, titleText: "Privacy Policy").pushFrom(viewController: value as! UIViewController, transitionFade: false)
				
			}),
			 
			 Setting(title: "Terms of Use", info: nil, badge: "", icon: nil, type: .disclosure, isExpanded: true, value: nil, action: { (value, compleation: (() -> ())?) in
				
				WebViewController.instantiate(url: URL(string:"http://chatkeepersave.me/tos.html")!, titleText: "Terms of Use").pushFrom(viewController: value as! UIViewController, transitionFade: false)
				
			})]])
	}
	
}

class PasscodeSettingsViewControllerDelegate: SettingsViewControllerDelegate {
	
	var title: String {
		return "Passcode"
	}
	
	var lockType: LockType {
		return LockType(rawValue: Int(LockService.sharedService.user!.mobLockType))!
	}
	
	var appPasscodeEnabled: Bool {
		return lockType != .undefined
	}
	
	var isNumpad4Enabled: Bool {
		return lockType == .numpad4
	}
	
	var isNumpad6Enabled: Bool {
		return lockType == .numpad6
	}
	
	var isDotLockEnabled: Bool {
		return lockType == .dotlock
	}
	
	var isAplhanumericEnabled: Bool {
		return lockType == .aplhanum
	}
	
	var decoyType: DecoyType {
		return DecoyType(rawValue: Int(LockService.sharedService.user!.mobDecoyPasscodeType))!
	}
	
	var decoyPasscodeEnabled: Bool {
		return decoyType != .decoyOff || decoyPasscodeIsExpanded
	}
	
	var decoy3AttempsEnabled: Bool {
		return decoyType == .after3attepms
	}
	
	var decoy5AttempsEnabled: Bool {
		return decoyType == .after5attepms
	}
	
	var decoy10AttempsEnabled: Bool {
		return decoyType == .after10attepms
	}
	
	var appPasscodeIsExpanded: Bool = false
	var decoyPasscodeIsExpanded: Bool = false
	
	
	var settings: Settings {
		
		return Settings(headers: [""], cells: [[
			
			Setting(title: "App Passcode", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: appPasscodeEnabled || appPasscodeIsExpanded, action: { (value, compleation: (() -> ())?) in
			
			self.appPasscodeIsExpanded = (value as! Bool)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					if (value as! Bool) == false {
						localUser.mobLockType = Int16(LockType.undefined.rawValue)
						localUser.mobTouchID = false
						localUser.mobShakeToLock = false
						localUser.mobDecoyPasscodeType = Int16(DecoyType.decoyOff.rawValue)
					}
				}
			})
			
			compleation!()
			
		}),
			
			Setting(title: "  4-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: appPasscodeEnabled || appPasscodeIsExpanded, value: isNumpad4Enabled , action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad4, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				let lockType: LockType = .numpad4
				
				if LockService.sharedService.user == nil {
					
					let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
					
				} else {
					
					
				 let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobLockType = Int16(lockType.rawValue)
							localUser.mobPasscode = passcode
						}
					})
				}
				
				presenterController.popToViewController(viewController: presenterController, transitionFade: UIDevice.current.isIpad())
				
				compleation!()
				
			})
			
		}),
			
			Setting(title: "  6-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: appPasscodeEnabled || appPasscodeIsExpanded, value: isNumpad6Enabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad6, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				let lockType: LockType = .numpad6
				
				if LockService.sharedService.user == nil {
					
					let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
					
				} else {
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobLockType = Int16(lockType.rawValue)
							localUser.mobPasscode = passcode
						}
					})
					
				}
				
				presenterController.popToViewController(viewController: presenterController, transitionFade: UIDevice.current.isIpad())
				
				compleation!()
				
			})
			
		}),
			
			Setting(title: "  DotLock Pattern", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: appPasscodeEnabled || appPasscodeIsExpanded, value: isDotLockEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .dotlock, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				let lockType: LockType = .dotlock
				
				if LockService.sharedService.user == nil {
					
					let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
					
				} else {
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobLockType = Int16(lockType.rawValue)
							localUser.mobPasscode = passcode
						}
					})
					
				}
				
				presenterController.popToViewController(viewController: presenterController, transitionFade: UIDevice.current.isIpad())
				
				compleation!()
				
			})
			
		}),
			Setting(title: "  Alphanumeric", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: appPasscodeEnabled || appPasscodeIsExpanded, value: isAplhanumericEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .aplhanum, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				let lockType: LockType = .aplhanum
				
				if LockService.sharedService.user == nil {
					
					let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
					
				} else {
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobLockType = Int16(lockType.rawValue)
							localUser.mobPasscode = passcode
						}
					})
					
				}
				
				presenterController.popToViewController(viewController: presenterController, transitionFade: UIDevice.current.isIpad())
				
				compleation!()
				
			})
			
		}),
		    Setting(title: "Touch ID", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: appPasscodeEnabled, value: LockService.sharedService.user?.mobTouchID, action: { (value, compleation: (() -> ())?) in
			
			if value as! Bool == true {
				YMMYandexMetrica.reportEvent("TouchID_on", onFailure: nil)
			}
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					localUser.mobTouchID = value as! Bool
				}
			})
			
		}),
		    Setting(title: "Shake to lock", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: appPasscodeEnabled, value: LockService.sharedService.user?.mobShakeToLock, action: { (value, compleation: (() -> ())?) in
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					localUser.mobShakeToLock = value as! Bool
				}
			})
			
		})]])
	}
}

class SlideShowSettingsViewControllerDelegate: SettingsViewControllerDelegate {
	
	var title: String {
		return "Slideshow"
	}
	
	var slideShowInterval: SlideShowTimeInterval {
		return SlideShowTimeInterval(rawValue: Int(LockService.sharedService.user!.mobSlideShowInterval))!
	}
	
	var slideShowIntervalEnabled: Bool {
		return slideShowInterval != .undefined || intervalIsExpanded
	}
	
	var oneSecInterval : Bool {
		return slideShowInterval == .one
	}
	
	var threeSecInterval : Bool {
		return slideShowInterval == .three
	}
	
	var fiveSecInterval : Bool {
		return slideShowInterval == .five
	}
	
	var sevenSecInterval : Bool {
		return slideShowInterval == .seven
	}
	
	var slideShowTransition: SlideShowTransition {
		return SlideShowTransition(rawValue: Int(LockService.sharedService.user!.mobSlideShowTransition))!
	}
	
	var slideShowTransitionEnabled: Bool {
		return slideShowTransition != .undefined || transitionIsExpanded
	}
	
	var fadeIn: Bool {
		return slideShowTransition == .fadein
	}
	
	var fadeOut: Bool {
		return slideShowTransition == .fadeout
	}
	
	var dissolve: Bool {
		return slideShowTransition == .dissolve
	}
	
	var intervalIsExpanded: Bool = false
	var transitionIsExpanded: Bool = false
	
	var settings: Settings {
		
		return Settings(headers: ["", "PLAYBACK", "TRANSITION"], cells: [[
			Setting(title: "Interval", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: slideShowIntervalEnabled, action: { (value, compleation: (() -> ())?) in
				
				self.intervalIsExpanded = (value as! Bool)
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						if (value as! Bool) == false {
							localUser.mobSlideShowInterval = Int16(SlideShowTimeInterval.undefined.rawValue)
						}
					}
				})
				compleation!()
				
			}), Setting(title: "  1 sec", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowIntervalEnabled, value: oneSecInterval, action: { (value, compleation: (() -> ())?) in
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						localUser.mobSlideShowInterval = Int16(SlideShowTimeInterval.one.rawValue)
					}
				})
					
				compleation!()
				
			}), Setting(title: "  3 sec", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowIntervalEnabled, value: threeSecInterval, action: { (value, compleation: (() -> ())?) in
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						localUser.mobSlideShowInterval = Int16(SlideShowTimeInterval.three.rawValue)
					}
				})
				
				compleation!()
				
			}), Setting(title: "  5 sec", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowIntervalEnabled, value: fiveSecInterval, action: { (value, compleation: (() -> ())?) in
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						localUser.mobSlideShowInterval = Int16(SlideShowTimeInterval.five.rawValue)
					}
				})

				compleation!()
				
			}), Setting(title: "  7 sec", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowIntervalEnabled, value: sevenSecInterval, action: { (value, compleation: (() -> ())?) in

				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						localUser.mobSlideShowInterval = Int16(SlideShowTimeInterval.seven.rawValue)
					}
				})
				
				compleation!()
				
			})],[
				
				Setting(title: "Shuffle", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: LockService.sharedService.user?.mobSlideShaffle, action: { (value, compleation: (() -> ())?) in
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobSlideShaffle = (value as! Bool)
						}
					})
					
				}),
				Setting(title: "Repeat", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: LockService.sharedService.user?.mobSlideRepeate, action: { (value, compleation: (() -> ())?) in
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobSlideRepeate = (value as! Bool)
						}
					})
					
					compleation!()
					
				})
				
			],[
				
				Setting(title: "Animate transition", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: slideShowTransitionEnabled, action: { (value, compleation: (() -> ())?) in
					
					self.transitionIsExpanded = (value as! Bool)
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							if (value as! Bool) == false {
								localUser.mobSlideShowTransition = Int16(SlideShowTransition.undefined.rawValue)
								
							}
						}
					})
					
					compleation!()
				}),
				
				Setting(title: "Fade in", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowTransitionEnabled, value: fadeIn, action: { (value, compleation: (() -> ())?) in
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobSlideShowTransition = Int16(SlideShowTransition.fadein.rawValue)
						}
					})
					
					compleation!()
					
				}),
				Setting(title: "Fade out", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowTransitionEnabled, value: fadeOut, action: { (value, compleation: (() -> ())?) in
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobSlideShowTransition = Int16(SlideShowTransition.fadeout.rawValue)
						}
					})
					
					compleation!()
		
				}),
				Setting(title: "Dissolve", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: slideShowTransitionEnabled, value: dissolve, action: { (value, compleation: (() -> ())?) in
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
							localUser.mobSlideShowTransition = Int16(SlideShowTransition.dissolve.rawValue)
						}
					})
					compleation!()
					
				})
				
			]])
	}
	
}

class BreakInAttempsSettingsViewControllerDelegate: SettingsViewControllerDelegate {
	
	var alert: UIAlertController!
	
	var settings: Settings {
		
		return Settings(headers: ["SECURITY"], cells: [[
			
			Setting(title: "Show all attempts", info: nil, badge: MBreakIn.вreakInCount, icon: nil, type: .badge, isExpanded: true, value: nil, action: { (value, compleation: (() -> ())?) in
				
				let presenterController: UIViewController = value! as! UIViewController
				
				StoryboardScene.instantiateBreakInLogScene().pushFrom(viewController: presenterController, transitionFade: UIDevice.current.isIpad())
				
			}),
			
			Setting(title: "Break-in tracking", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: LockService.sharedService.user?.mobBreakInTracking, action: { (value, compleation: (() -> ())?) in
				
				if (value as! Bool) == true {
					LocationManager.sharedInstance.requestWhenInUseAuthorization()
				}
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					
						if value as! Bool == false {
							localUser.mobPhotoOnLogin = false
							localUser.mobShowOnIcon = false
						}
						
						localUser.mobBreakInTracking = value as! Bool
					}
				})
				
				compleation!()
				
			}),
			Setting(title: "Photo on login", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: LockService.sharedService.user!.mobBreakInTracking, value: LockService.sharedService.user?.mobPhotoOnLogin, action: { (value, compleation: (() -> ())?) in
				
				if (value as! Bool) == true {
					let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
					switch authStatus {
					case .denied, .restricted:
						
						self.alert = UIAlertController(title: "", message: "Camera access required for this option!", preferredStyle: .alert)
						
						self.alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (_) in
							UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
						}))
						
						self.alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
						}))
						
						DispatchQueue.main.async {
							
							if let visibleViewController = BaseViewController.visibleViewController {
								
								if UIDevice.current.isIpad() == true {
									
									if let popoverPresentationController = self.alert.popoverPresentationController {
										popoverPresentationController.sourceView = visibleViewController.view
										popoverPresentationController.sourceRect = CGRect(x: visibleViewController.view.bounds.size.width / 2.0,
										                                                  y: visibleViewController.view.bounds.size.height / 2.0,
										                                                  width: visibleViewController.view.bounds.size.width / 2.0,
										                                                  height: visibleViewController.view.bounds.size.height / 2.0)
									}
									
								}
								
								visibleViewController.present(self.alert, animated: true, completion: nil)
								
							}
							
						}
						
					case .notDetermined, .authorized:
						if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
							BaseViewController.avoidHidingWhenResignActive = true
							AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (_) in }
						}
						
					default: break
						
					}
				}
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						if value as! Bool == true {
							localUser.mobBreakInTracking = true
						}
						localUser.mobPhotoOnLogin = value as! Bool
					}
				})
				
				compleation!()
				
			}) ,
			Setting(title: "Show on icon", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: LockService.sharedService.user!.mobBreakInTracking, value: LockService.sharedService.user?.mobShowOnIcon, action: { (value, compleation: (() -> ())?) in
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
						if value as! Bool == true {
							localUser.mobBreakInTracking = true
						}
						localUser.mobShowOnIcon = value as! Bool
					}
				})
				
				BaseViewController.avoidHidingWhenResignActive = true
				UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: .badge, categories: nil))
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
					BaseViewController.avoidHidingWhenResignActive = false
				})
				compleation!()
				
			})]])
		
	}
	
	var title: String {
		
		return "Break-in attemps"
		
	}
}

class AppSettingsViewController: NSObject {
	
	static func inistantiate() -> SettingsViewController {
		
		let controller = SettingsViewController.inistantiate(delegate: AppSettingsViewControllerDelegate(), showHeader: false)
		
		return controller
		
	}
	
}
