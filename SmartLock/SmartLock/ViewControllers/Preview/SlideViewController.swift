//
//  SlideViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/22/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import MessageUI
import AVFoundation
import YandexMobileMetrica

class SlideViewController: BaseViewController, PageViewControllerDelegate, MFMailComposeViewControllerDelegate, UIDocumentPickerDelegate, VideoPreviewViewControllerDelegate {
	
	@IBOutlet weak var toolBarHeight: NSLayoutConstraint!
	@IBOutlet weak var toolBarView: UIView!
	@IBOutlet weak var tabBarBottom: NSLayoutConstraint!
	@IBOutlet weak var navigationBarTop: NSLayoutConstraint!
	
	@IBOutlet var videoPlayerToolsView: UIView!
	@IBOutlet var previewToolsView: UIView!
	
	@IBOutlet weak var slider: UISlider!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var durationLabel: UILabel!
	@IBOutlet weak var playPauseButton: UIButton!
	
	@IBOutlet weak var deleteButton: UIButton!
		
	static var isHidden = false
	
	var videoTimer: Timer?
	
	var pageViewController: PageViewController?
	
	var exportMenuSheets: CustomizableActionSheet?
	var cloudMenuSheets: CustomizableActionSheet?
	var progressView: ProgressView?
	
//	var deleteAction: (() -> ())?
	
	var isSeeking: Bool = false
	
	var hideToolBars: Bool = true
	
	var directory: File {
		return FileService.sharedInstance.currentDirectory
	}
	
	var videoViewController: VideoPreviewViewController? {
		if pageViewController?.currentPage is VideoPreviewViewController {
			return pageViewController!.currentPage as? VideoPreviewViewController
		}
		return nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		objects = FileService.fetchAlbumFiles(relativePath: directory.relativePath)
		
		BackupService.sharedInstance.previewingPath = FileService.sharedInstance.currentDirectory.relativePath
		
		BackupService.sharedInstance.suspendBackup()
		
		slider.setThumbImage(#imageLiteral(resourceName: "ico_dotsmall"), for: .normal)
		
	}
	
	deinit {
		BackupService.sharedInstance.previewingPath = nil
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		toolBarView.layoutIfNeeded()
		previewToolsView.frame = toolBarView.bounds
		toolBarView.addSubview(previewToolsView)
		
		previewToolsView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		previewToolsView.translatesAutoresizingMaskIntoConstraints = true
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(didPlayVideoPreview(notification:)), name: PreviewViewController.playVideoNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didStopVideoPreview(notification:)), name: PreviewViewController.stopVideoNotification, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(onTapPreview(notification:)), name: PreviewViewController.tapNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(onDoubleTapPreview(notification:)), name: PreviewViewController.doubeTapNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(onZoomedPreview(notification:)), name: PreviewViewController.zoomedNotification, object: nil)
		
		if SlideViewController.isHidden == true {
			setToolBarHidden(hidden: false)
		}
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: PreviewViewController.tapNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: PreviewViewController.doubeTapNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: PreviewViewController.zoomedNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: PreviewViewController.playVideoNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: PreviewViewController.stopVideoNotification, object: nil)
		
		let value = UIInterfaceOrientation.portrait.rawValue
		UIDevice.current.setValue(value, forKey: "orientation")
	}
	
	override func willMove(toParentViewController parent: UIViewController?) {
		super.willMove(toParentViewController: parent)
		
		if parent == nil {
			BackupService.sharedInstance.previewingPath = nil
		}
	}
	
	override func didEnterBackground(notification: NSNotification) {
		super.didEnterBackground(notification: notification)
		
		if videoViewController != nil {
			videoViewController?.stop()
		}
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = title
	}
	
	func setToolBarHidden(hidden: Bool, completion: (() -> ())? = nil) {
		
		SlideViewController.isHidden = hidden
		
		setNeedsStatusBarAppearanceUpdate()
		
		UIView.animate(withDuration: 0.2, animations: {
			
			if hidden {
				
				self.navigationBarTop.constant = -64
				self.tabBarBottom.constant = -64
				
			} else {
				
				self.navigationBarTop.constant = 0
				self.tabBarBottom.constant = 0
				
			}
			
			self.view.layoutIfNeeded()
			
		}) { (_) in
			if let completion = completion {
				completion()
			}
		}
		
	}
	
	fileprivate func showCloudMenu() {
	}
	
	fileprivate func showWiFiTransferView() {
		
		if let port = WebUploaderService.sharedInstance.start(typeService: .upload) {
			
			let _ = WiFiTransferView.showWiFiTransferView(inView: view, port: port, cancel: {
				
				WebUploaderService.sharedInstance.stop()
				
				DispatchQueue.main.async {
					
					FileService.sharedInstance.fetchCurrentDirectoryContent()
					
				}
				
			})
			
		}
		
	}
	
	fileprivate func showSlideshowMenu() {
		
		BackupService.sharedInstance.suspendBackup()
		
		let viewToRemove = toolBarView.subviews.first
		
		guard viewToRemove != previewToolsView else { return }
		
		previewToolsView.frame = toolBarView.bounds
		
		previewToolsView.alpha = 0.0
		toolBarView.addSubview(previewToolsView)
		
		previewToolsView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		previewToolsView.translatesAutoresizingMaskIntoConstraints = true
		
		UIView.animate(withDuration: 1.0, animations: {
			self.previewToolsView.alpha = 1.0
			if let view = viewToRemove {
				view.alpha = 0.0
			}
		}) { (_) in
			
			if let view = viewToRemove {
				view.removeFromSuperview()
				view.alpha = 1.0
			}
			
		}
		
	}
	
	fileprivate func showVideoToolsMenu() {
		
		BackupService.sharedInstance.resumeBackup()
		
		let viewToRemove = toolBarView.subviews.first
		
		guard viewToRemove != videoPlayerToolsView else { return }
		
		videoPlayerToolsView.frame = toolBarView.bounds
		videoPlayerToolsView.alpha = 0.0
		toolBarView.addSubview(videoPlayerToolsView)
		
		videoPlayerToolsView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		videoPlayerToolsView.translatesAutoresizingMaskIntoConstraints = true
		
		UIView.animate(withDuration: 1.0, animations: {
			self.videoPlayerToolsView.alpha = 1.0
			if let view = viewToRemove {
				view.alpha = 0.0
			}
		}) { (_) in
			
			if let view = viewToRemove {
				view.removeFromSuperview()
				view.alpha = 1.0
			}
			
		}
		
	}
	
	private func popWithAnimation() {
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionReveal
		transition.subtype = kCATransitionFromBottom
		self.navigationController?.view.layer.add(transition, forKey: kCATransition)
		navigationController?.popViewController(animated: false)
	}
	
	func getFileByIndex(index: Int) -> File {
		let object = objects[index]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		return File(relativePath: relativePath)
	}
	
	//MARK: - MFMailComposeViewControllerDelegate
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		
		self.dismiss(animated: true, completion: nil)
		
	}
	
	//MARK: - PageViewControllerDelegate
	
	func pageViewController(pageViewController: UIPageViewController, currentPage: PreviewViewController, pageIndexChanged index: Int, count: Int) {
		
//		DispatchQueue.main.async {
//			self.deleteButton.isEnabled = true
//		}
		
		PreviewViewController.index = index - 1
		
		if let navigationBarView = navigationBarView {
			navigationBarView.setupRightButton(image: nil, title: "\(index) of \(objects.count)", target: nil, action: nil)
		}
		
		if currentPage is VideoPreviewViewController {
			(currentPage as! VideoPreviewViewController).delegate = self
		}
		
	}
	
	func movePageViewController(pageViewController: UIPageViewController) {
		
		if hideToolBars == true {
			
			setToolBarHidden(hidden: true)
			
			hideToolBars = false
			
		} else {
			
			showSlideshowMenu()
			
		}
		
	}
	
	//MARK: - UIDocumentPickerDelegate
	
	func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
		
	}
	
	func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
		
	}
	
	//MARK: - VideoPreviewViewControllerDelegate
	
	func startVideo(in view: VideoPreviewViewController, duration: CMTime) {

		if SlideViewController.isHidden == false {
			setToolBarHidden(hidden: true)
		}
		
		durationLabel.text = duration.toString()
		slider.maximumValue = Float(duration.toSeconds())
		
		showVideoToolsMenu()
		
		playPauseButton.isSelected = true
	}
	
	func playVideo(in view: VideoPreviewViewController) {
		playPauseButton.isSelected = true
	}
	
	func stopVideo(in view: VideoPreviewViewController) {
		playPauseButton.isSelected = false
	}
	
	func finishVideo(in view: VideoPreviewViewController) {
		playPauseButton.isSelected = false
		//showSlideshowMenu()
	}
	
	func seek(in view: VideoPreviewViewController, time: CMTime, duration: CMTime) {
		
		guard isSeeking == false else { return }
		
		timeLabel.text = time.toString()
		
		print("seek:\(time.toSeconds())")
		
		if time.toSeconds() == 0 {
			self.slider.setValue(Float(time.toSeconds()), animated: false)
		} else {
			
			UIView.animate(withDuration: 0.2) {
				self.slider.setValue(Float(time.toSeconds()), animated: true)
			}
			
		}
		
		durationLabel.text = duration.toString()
		slider.maximumValue = Float(duration.toSeconds())
		
	}
	
	//MARK: -
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.destination is PageViewController {
			
			pageViewController = segue.destination as? PageViewController
			
			pageViewController?.pageViewControllerDelegate = self
			
		}
		
	}
	
	override var prefersStatusBarHidden: Bool {
		return SlideViewController.isHidden
	}
	
	//MARK: - Action
	
	func didPlayVideoPreview(notification: Notification) {
		
	}
	
	func didStopVideoPreview(notification: Notification) {
		
	}
	
	func onTapPreview(notification: Notification) {
		
		if videoViewController != nil {
			
			if toolBarView.subviews.first != videoPlayerToolsView {
				
				showVideoToolsMenu()
				
			} else {
				
				setToolBarHidden(hidden: !SlideViewController.isHidden)
				
			}
			
		} else {
			
			setToolBarHidden(hidden: !SlideViewController.isHidden)
			
		}
		
	}
	
	func onDoubleTapPreview(notification: Notification) {
		
		if SlideViewController.isHidden == false {
			setToolBarHidden(hidden: !SlideViewController.isHidden)
		}
		
	}
	
	func onZoomedPreview(notification: Notification) {
		
		if SlideViewController.isHidden == false {
			setToolBarHidden(hidden: !SlideViewController.isHidden)
		}
		
	}
	
	@IBAction func onShare(_ sender: UIButton) {
		
		exportMenuSheets = ExportMenuView.showExportMenu(inView: view, delegate: self, cancel: { })
		
	}
	
	@IBAction func onPlay(_ sender: UIButton) {
		
		YMMYandexMetrica.reportEvent("Tap_on_play", onFailure: nil)
		
		setToolBarHidden(hidden: true, completion: {
			
			DispatchQueue.main.async {
				
				let file = self.getFileByIndex(index: PreviewViewController.index)
			
				switch file.fileType {
					
				case .video:
					let previewViewController = self.storyboard?.instantiateViewController(withIdentifier: "VideoPreviewViewController") as! VideoPreviewViewController
					previewViewController.slideShow = true
					previewViewController.movieURL = file.url
					previewViewController.previewImage = file.getFileThumbnailSourceImage()
					previewViewController.objects = self.objects

					
					PreviewViewController.finishShow = { (lastIndex) in
						self.pageViewController?.setViewControllerByIndex(index: lastIndex, completion: {})
					}
					
					self.navigationController?.pushViewController(previewViewController, animated: false)
					
				case .image:
					let previewViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
					previewViewController.slideShow = true
					previewViewController.imageURL = file.url.path
					previewViewController.objects = self.objects
					
					PreviewViewController.finishShow = { (lastIndex) in
						self.pageViewController?.setViewControllerByIndex(index: lastIndex, completion: {})
					}
					
					self.navigationController?.pushViewController(previewViewController, animated: false)
					
				default: break
					
				}
				
			}
		
		})
		
	}
	
	@IBAction func onDelete(_ sender: UIButton) {
		
		sender.isUserInteractionEnabled = false
		
//		if let action = deleteAction {
//			action()
//		}
		
		let file = self.getFileByIndex(index: PreviewViewController.index)
		FileService.sharedInstance.deleteFiles(files: [file])
		
		objects = FileService.fetchAlbumFiles(relativePath: directory.relativePath)
		
		BackupService.sharedInstance.tryBackupAgainWhenBackupSpaceLimitException()
		
		if objects.count == 0 {
			
			_ = navigationController?.popViewController(animated: true)
			
		} else {
			
			var newIndex = PreviewViewController.index
			
			if newIndex >= objects.count {
				
				newIndex = objects.count - 1
				
			}
			
			self.pageViewController?.objects = objects
			
			self.pageViewController?.setViewControllerByIndex(index: newIndex, completion: {
			
				sender.isUserInteractionEnabled = true
				
			})
			
		}
		
	}
	
	@IBAction func onPlayPause(_ sender: UIButton) {
		
		guard videoViewController != nil else { return }
		
		if sender.isSelected == true {
			videoViewController!.stop()
		} else {
			videoViewController!.play()
		}
		
	}
	
	@IBAction func onSkipForward(_ sender: UIButton) {
		
		guard videoViewController != nil else { return }
		
		videoViewController?.skipForward()
		
	}
	
	@IBAction func onSkipBackward(_ sender: UIButton) {
		
		guard videoViewController != nil else { return }
		
		videoViewController?.skipBackward()
		
	}
	
	@IBAction func onTouchUp(_ sender: UISlider) {
		guard videoViewController != nil else { return }
		isSeeking = false
		videoViewController!.play()
	}
	
	@IBAction func onChangeValue(_ sender: UISlider) {
		guard videoViewController != nil else { return }
		isSeeking = true
		videoViewController!.stop()
		videoViewController!.seek(to: sender.value / 1000)
	}
	
	@IBAction func onSwipeDown(_ sender: UISwipeGestureRecognizer) {
		popWithAnimation()
	}
	
}

//MARK: - ExportMenuViewDelegate

extension SlideViewController: ExportMenuViewDelegate {
	
	func openCameraRoll(exportView: ExportMenuView) {
		
//		var filesToUpload = [File]()
//		
//		let file = self.getFileByIndex(index: PreviewViewController.index)
//		
//		if file.fileType == .directory {
//			
//			filesToUpload.append(contentsOf: file.getFiles())
//			
//		} else {
//			
//			filesToUpload.append(file)
//			
//		}
//		
//		
//		exportMenuSheets?.dismiss {
//			
//		}
//		
//		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .items, cancel: {
//			
//			DispatchQueue.main.async {
//				
//				self.progressView?.close()
//				
//			}
//			
//		})
//		
//		FileService.sharedInstance.exportToPhotoRoll(files: filesToUpload, progress: { (written, total) in
//			
//			DispatchQueue.main.async {
//				
//				self.progressView?.setProgressLabel(progress: Float(written) / Float(total), written: written, total: total)
//				
//			}
//			
//		}) {
//			
//			DispatchQueue.main.async {
//				
//				self.progressView?.close()
//				
//			}
//			
//		}
		
	}
	
	func chooseCloud(exportView: ExportMenuView) {
		
		exportMenuSheets!.dismiss {
			
			self.showCloudMenu()
			
		}
		
	}
	
	func wifiTransfer(exportView: ExportMenuView) {
		
		let file = self.getFileByIndex(index: PreviewViewController.index)
		
		PhotosViewController.filesToUpload = [file]
		
		exportMenuSheets!.dismiss {
			
			self.showWiFiTransferView()
			
		}
		
	}
	
	func onAirDrop(exportView: ExportMenuView) {
		
		let file = self.getFileByIndex(index: PreviewViewController.index)
		
		PhotosViewController.filesToUpload = [file]
		
		exportMenuSheets!.dismiss {
			
			var urls = [URL]()
			for file in PhotosViewController.filesToUpload {
				
				switch file.fileType {
				case .video, .image:
					urls.append(file.url)
				case .directory:
					urls.append(contentsOf: file.getFiles().map( { $0.url } ))
				default: break
				}
				
			}
			
			let controller = UIActivityViewController(activityItems: urls, applicationActivities: nil)
			
			controller.presentBy(viewController: self)
			
		}
	}
	
	func onMail(exportView: ExportMenuView) {
		
		let file = self.getFileByIndex(index: PreviewViewController.index)
		PhotosViewController.filesToUpload = [file]
		
		exportMenuSheets!.dismiss {
			
			if( MFMailComposeViewController.canSendMail() ) {
				
				let mailComposer = MFMailComposeViewController()
				mailComposer.mailComposeDelegate = self
				
				
				mailComposer.setSubject("Smark Lock")
				mailComposer.setMessageBody("", isHTML: false)
				
				
				var urls = [URL]()
				for file in PhotosViewController.filesToUpload {
					
					switch file.fileType {
					case .video, .image:
						urls.append(file.url)
					case .directory:
						urls.append(contentsOf: file.getFiles().map( { $0.url } ))
					default: break
					}
					
				}
				
				for url in urls {
					
					if let fileData = NSData(contentsOfFile: url.path) {
						
						let fileName = url.lastPathComponent
						let mimeType = url.mimeType()
						mailComposer.addAttachmentData(fileData as Data, mimeType: mimeType, fileName: fileName)
					}
					
				}
				
				mailComposer.presentBy(viewController: self)
				
			}
			
			
		}
		
	}
	
}

