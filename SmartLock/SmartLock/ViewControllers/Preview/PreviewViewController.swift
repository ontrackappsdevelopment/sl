//
//  PreviewViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/13/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SwiftRandom
import AVFoundation

enum SlideShowTimeInterval: Int {
	case undefined = 0
	case one = 1
	case three = 3
	case five = 5
	case seven = 7
}

enum SlideShowTransition: Int {
	case undefined
	case fadein
	case fadeout
	case dissolve
}

class PreviewViewController: UIViewController, UINavigationControllerDelegate {
	
	static let tapNotification = Notification.Name(rawValue: "OneTap")
	static let doubeTapNotification = Notification.Name(rawValue: "DoubleTap")
	static let zoomedNotification = Notification.Name(rawValue: "Zoomed")
	static let stopVideoNotification = Notification.Name(rawValue: "StopVideoPreview")
	static let playVideoNotification = Notification.Name(rawValue: "PlayVideoPreview")
	
	static let fullScreenMode: Bool = false
	
	static var file: File?
	static var index: Int = 0
	static var finishShow: ((_ lastIndex: Int) -> ())?
	
	var currentIndex: Int = 0
	
	var slideShow: Bool = false
	
	var timer: Timer?
	
//	var files = [MFile]()
	var objects = [NSManagedObject]()
	
	var slideShowTransition: SlideShowTransition {
		return SlideShowTransition(rawValue: Int(LockService.sharedService.user!.mobSlideShowTransition))!
	}
	
	var slideShowInterval: SlideShowTimeInterval {
		return SlideShowTimeInterval(rawValue: Int(LockService.sharedService.user!.mobSlideShowInterval))!
	}
	
	@IBAction func onSwipeDown(_ sender: UISwipeGestureRecognizer) {
		popWithAnimation()
	}
	
	private func popWithAnimation() {
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionReveal
		transition.subtype = kCATransitionFromBottom
		self.navigationController?.view.layer.add(transition, forKey: kCATransition)
		navigationController?.popToViewController(ChatViewController.chatViewController, animated: false)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let recognizer = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeDown(_:)))
		recognizer.direction = .down
		self.view.addGestureRecognizer(recognizer)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		
		if timer != nil {
			timer?.invalidate()
			timer = nil
		}
		
	}
	
	func getNextFileToShow() -> File? {
		
		if LockService.sharedService.user?.mobSlideShaffle == true {
			
			let nextIndex = Int.random(0, objects.count - 1)
			
			if PreviewViewController.index == nextIndex {
				
				return getNextFileToShow()
				
			} else {
				
				PreviewViewController.index = nextIndex
				
				let fileObject = objects[PreviewViewController.index]
				let relativePath = fileObject.value(forKey: "mobPath") as? String ?? ""
				let file = File(relativePath: relativePath)
				
				return file
				
			}
			
		} else {
			
			var nextIndex = PreviewViewController.index + 1
			
			if nextIndex >= objects.count {
				
				if LockService.sharedService.user?.mobSlideRepeate == false {
					return nil
				}
				
				nextIndex = 0
				
			}
			
			PreviewViewController.index = nextIndex
			
			let fileObject = objects[PreviewViewController.index]
			let relativePath = fileObject.value(forKey: "mobPath") as? String ?? ""
			let file = File(relativePath: relativePath)
			
			return file
			
		}
		
	}
	
	func playNextSlide() {
		
		guard AppDelegate.appIsLocked == false else { return }
		
		guard let file = getNextFileToShow() else { return }
		
		switch file.fileType {
		case .video:
			let previewViewController = self.storyboard?.instantiateViewController(withIdentifier: "VideoPreviewViewController") as! VideoPreviewViewController
			previewViewController.slideShow = true
			previewViewController.movieURL = file.url
			previewViewController.videoGravity = AVLayerVideoGravityResizeAspectFill
			previewViewController.previewImage = file.getFileThumbnailSourceImage()
			previewViewController.objects = self.objects
			
			if let navigationController = self.navigationController {
				var viewControllers = navigationController.viewControllers
				viewControllers[viewControllers.count - 1] = previewViewController
				navigationController.setViewControllers(viewControllers, animated: true)

			}
				
		case .image:
			let previewViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
			previewViewController.slideShow = true
			previewViewController.imageURL = file.url.path
			previewViewController.objects = self.objects
			
			if let navigationController = self.navigationController {
				var viewControllers = navigationController.viewControllers
				viewControllers[viewControllers.count - 1] = previewViewController
				navigationController.setViewControllers(viewControllers, animated: true)
			}
			
		default: break
		}
		
	}
	
	func onTapAction() {
		
		NotificationCenter.default.post(name: PreviewViewController.tapNotification, object: nil)
		
		guard slideShow == true else { return }
		
		if let action = PreviewViewController.finishShow {
			action(PreviewViewController.index)
		}
		
		let _ = navigationController?.popViewController(animated: false)
		
	}
	
	func onDoubleTapAction() {
		NotificationCenter.default.post(name: PreviewViewController.doubeTapNotification, object: nil)
	}
	
	func onZoomAction() {
		NotificationCenter.default.post(name: PreviewViewController.zoomedNotification, object: nil)
	}
	
}
