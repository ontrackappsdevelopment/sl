//
//  ChatTableViewCellViewModel.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class ChatTableViewCellViewModel: ChatTableViewCellDelegate {
	
	var chat: Chat?
	
	init(chat: Chat) {
		self.chat = chat
	}
	
	var backupStatusHidden: Bool {
		if let backuped = self.chat?.backuped {
			return !backuped
		}
		
		return true
	}
	
	var iconHighlighted: Bool {
		guard let type = self.chat?.type else { return false }
		return type == .multiple ? true : false
	}
	
	var status: String {
		return self.chat?.status ?? ""
	}
	
	var title: String {
		return self.chat?.name ?? ""
	}
	
	var time: String {
		guard let date = chat?.lastUpdateDate else { return "" }
		
		if Calendar.current.isDateInYesterday(date) {
			return "yesterday"
		}
		
		if Calendar.current.isDateInToday(date) {
			let dateFormatter = DateFormatter()
			dateFormatter.dateStyle = .none
			dateFormatter.timeStyle = .short
			dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
			return dateFormatter.string(from: date)
		}
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .none
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		return dateFormatter.string(from: date)
	}
	
	var importedTime: String {
		guard let importedDate = self.chat?.importedDate else { return "" }
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .short
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		let dateString = dateFormatter.string(from: importedDate)
		return "Imported \(dateString)"
	}
}
