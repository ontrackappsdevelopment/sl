//
//  HeaderMessagesTableViewCell.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol HeaderMessageTableViewCellDelegate {
	var day: String  { get }
	var borderColor: UIColor { get }
	var backgroundColor: UIColor { get }
}

class HeaderMessagesTableViewCell: UITableViewCell, ConfigurableCell {
	static var identifier: String = "HeaderMessagesTableViewCell"
	
	@IBOutlet weak var dateLabel: UILabel! {
		didSet {
			dateLabel.layer.borderWidth = 2
			dateLabel.layer.cornerRadius = 16
		}
	}
	
	typealias T = HeaderMessageTableViewCellDelegate
	var viewModel: HeaderMessageTableViewCellDelegate?
	
	func configureWithViewModel(_ viewModel: HeaderMessageTableViewCellDelegate) {
		self.viewModel = viewModel
		dateLabel.text = viewModel.day
		dateLabel.layer.borderColor = viewModel.borderColor.cgColor
		dateLabel.layer.backgroundColor = viewModel.backgroundColor.cgColor
	}
}
