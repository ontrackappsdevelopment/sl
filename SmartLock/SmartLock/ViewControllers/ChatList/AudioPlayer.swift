//
//  AudioPlayer.swift
//  ChatKepper
//
//  Created by Bogachev on 9/28/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol AudioPlayerDelegate {
	func didChangeMediaPlayerTime(time: VLCTime, length: VLCTime)
	func didChangeMediaPlayerState()
	func stopPlaying()
}

class AudioPlayer: NSObject, VLCMediaPlayerDelegate {
	static let sharedPlayer = AudioPlayer()
	
	var delegate: AudioPlayerDelegate?
	var mediaPlayer = VLCMediaPlayer()
	
	private var audioPath: String = ""
	
	var isPlaying: Bool {
		return mediaPlayer.isPlaying
	}
	
	func play(delegate: AudioPlayerDelegate? = nil,audioURL: URL) {
		self.delegate?.stopPlaying()
		self.delegate = delegate
		if self.audioPath == audioURL.path {
			if !mediaPlayer.isPlaying {
				mediaPlayer.play()
			}
		} else {
			self.stop()
			self.audioPath = audioURL.path
			let media = VLCMedia(url: audioURL)
			mediaPlayer.media = media
			mediaPlayer.play()
		}
	}

	func pause() {
		if mediaPlayer.isPlaying {
			mediaPlayer.pause()
		}
	}
	
	func stop() {
		audioPath = ""
		mediaPlayer.stop()
	}
	
	func pause(completion: (_ remainingTime: VLCTime, _ time: VLCTime) -> ()) {
		if mediaPlayer.isPlaying {
			mediaPlayer.pause()
			completion(mediaPlayer.remainingTime, mediaPlayer.time)
		}
	}
	
	func seekTo(value: Int32) {
		if value < 0 {
			mediaPlayer.jumpBackward(-value)
		} else {
			mediaPlayer.jumpForward(value)
		}
	}
	
	var currentTime: VLCTime {
		return mediaPlayer.time
	}
	
	override init() {
		super.init()
		self.mediaPlayer.delegate = self
	}
	
	func didChangeMediaPlayerTime(time: VLCTime) {
		
	}
	
	func mediaPlayerTimeChanged(_ aNotification: Notification!) {
		if let player = aNotification.object as? VLCMediaPlayer {
			self.delegate?.didChangeMediaPlayerTime(time: player.time, length: player.media.length)
		}
	}
	
	func mediaPlayerStateChanged(_ aNotification: Notification!) {
		if let player = aNotification.object as? VLCMediaPlayer {
			self.delegate?.didChangeMediaPlayerState()
		}
	}
}

//class AudioPlayer: NSObject {
//	
//	static let sharedPlayer = AudioPlayer()
//	private lazy var player = AVPlayer()
//	private var audioPath: String?
//	
//	var currentItem: AVPlayerItem? {
//		willSet {
//			currentItem?.removeObserver(self, forKeyPath: "status", context: nil)
//		}
//		didSet {
//			player.replaceCurrentItem(with: currentItem)
//			if let item = currentItem {
//				item.addObserver(self, forKeyPath: "status", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
//			}
//		}
//	}
//	
//	var currentAudioURL: URL?
//	
//	var timeObserverToken: Any?
//	
//	var audioSession: AVAudioSession {
//		return AVAudioSession.sharedInstance()
//	}
//	
//	var isPlaying: Bool {
//		return player.rate != 0
//	}
//	
//	var currentTime: CMTime {
//		return player.currentTime()
//	}
//	
//	override init() {
//		super.init()
////		if #available(iOS 10.0, *) {
////			player.automaticallyWaitsToMinimizeStalling = false
////		} else {
////			// Fallback on earlier versions
////		}
//
//		try! self.audioSession.setCategory(AVAudioSessionCategoryPlayback)
//		try! self.audioSession.setActive(true)
//		
//		timeObserverToken = player.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 1), queue: nil, using: timeObserverAction(time:))
//		NotificationCenter.default.addObserver(self, selector: #selector(didPlayToEnd(notifiction:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
//	}
//	
//	deinit {
//		player.removeTimeObserver(timeObserverToken!)
//	}
//	
//	func play(audioURL: URL) {
//		if let currentAudioURL = currentAudioURL, currentAudioURL.path == audioURL.path {
//			if isPlaying == false {
//				play()
//			}
//		} else {
//			if isPlaying == true {
//				pause()
//			}
//			
//			currentItem = AVPlayerItem(url: audioURL)
//			currentAudioURL = audioURL
//			play()
//		}
//	}
//	
//	func play() {
//		player.play()
//	}
//	
//	func pause() {
//		player.pause()
//	}
//	
//	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//		
//		guard let keyPath = keyPath else { return }
//		switch keyPath {
//		case "status":
//			
//			if let newValue = change?[.newKey], let oldValue = change?[.oldKey] {
//				didChangeCurrentItemStatus(status: AVPlayerItemStatus(rawValue: (newValue as! NSNumber).intValue)!)
//				print("\(newValue)")
//				print("\(oldValue)")
//			}
//			
//		default: break
//		}
//		
//	}
//	
//	func didChangeCurrentItemStatus(status: AVPlayerItemStatus) {
//		
//	}
//	
//	@objc private func didPlayToEnd(notifiction: Notification) {
//		didPlayToEndAction()
//	}
//	
//	func timeObserverAction(time: CMTime) {
//		print("timeObserverAction")
//	}
//
//	func didPlayToEndAction() {
//		print("didPlayToEndAction")
//	}
//	
////	func seekToTime(position: Float) {
////		if let duration = duration {
////
////			let time = CMTimeMakeWithSeconds(Float64(CMTimeGetSeconds(duration)) * Float64(position), 1);
////			player.seek(to: time)
////
////			updateNowPlayingInfoElapsedTime(time: time)
////
////		}
////	}
////
////	func seekToTime(time: CMTime) {
////		player.seek(to: time)
////
////		updateNowPlayingInfoElapsedTime(time: time)
////	}
////
////	func seekForward() {
////		let time = CMTimeAdd(player.currentTime(),CMTimeMake(Int64(seekTimeInterval), 1))
////		player.seek(to: time)
////
////		updateNowPlayingInfoElapsedTime(time: time)
////	}
////
////	func seekBackward() {
////		let time = CMTimeSubtract(player.currentTime(),CMTimeMake(Int64(seekTimeInterval), 1))
////		player.seek(to: time)
////	}
//
//}

