//
//  ChatListViewController.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import MediaPlayer

class ChatListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, ChatTableViewCellDragAndDropDelegate {

	static var chatListViewController: ChatListViewController?
	
	@IBOutlet weak var chatListTableView: UITableView!
	@IBOutlet weak var emptyLabel: UILabel! {
		didSet {
			self.emptyLabel.isHidden = true
		}
	}
	
	private var chatList = [Chat]()
	private var filteredChatList = [Chat]()
	private var filterIsActive: Bool = false
	private var draggingChat: Chat?
	private var targetCell: ChatTableViewCell?
	private var tutorialWasShown: Bool = false
	private var canShowReview: Bool = false
	
	var needSyncData: Bool = false {
		didSet {
			if isVisible == true && needSyncData == true {
				refreshData()
			}
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		ChatListViewController.chatListViewController = self
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if BackupService.sharedInstance.backupStatus == nil {
			hideBackupStatus()
		}
		
		if let status = BackupService.sharedInstance.backupStatus {
			showBackupStatus(text: status)
		}

		refreshData()
	
		if tutorialWasShown {
			tutorialWasShown = false
			showEmailAlert()
		}
	}
	
	func showBackupStatus(text: String) {
		if let navigationBarView = navigationBarView {
			navigationBarView.showBackupActiveStatus()
		}
		
		if BackupService.sharedInstance.backupSpaceLimitException == .waiting && wasShownBackupException == false {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
				self.wasShownBackupException = true
				_ = ErrorView.shows(inViewController: self, text: "You have reached 25GB limit of backup storage space.")
			}
		}
	}
	
	var wasShownBackupException: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "canShowBackupException")
		}
		set(newValue) {
			UserDefaults.standard.set(newValue, forKey: "canShowBackupException")
			UserDefaults.standard.synchronize()
		}
	}
	
	func hideBackupStatus() {
		if let navigationBarView = navigationBarView {
			navigationBarView.hideBackupActiveStatus()
		}
	}
	
	override func didBecomeActive(notification: NSNotification) {
		super.didBecomeActive(notification: notification)
		
		guard AppDelegate.appIsLocked == false else { return }
		guard isVisible == true else { return }
		
		if let status = BackupService.sharedInstance.backupStatus {
			showBackupStatus(text: status)
		}
	}

	override func didUnlock(notification: Notification) {
		super.didUnlock(notification: notification)
		if let status = BackupService.sharedInstance.backupStatus {
			showBackupStatus(text: status)
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		let _ = showTutorialIfNeeded()

		if canShowReview == true {
			showReview()
		}
	}
	
	private func showReview() {
		guard AppDelegate.appIsLocked == false else { return }
		guard self.isVisible == true else { return }
		_ = ReviewService.showReviewControllerIfNeeded(presenter: self)
	}
	
	func showEmailAlert() {
		LockService.sharedService.checkForEmail {
			
			guard AppDelegate.appIsLocked == false else { return }
			guard isVisible == true else { return }
			
			_ = AlertView.shows(inViewController: self, text: "Set your email in order to have backup feature activated.", actionTitle: "Set", cancelTitle: "Cancel", action: {
				
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
					guard AppDelegate.appIsLocked == false else { return }
					guard self.isVisible == true else { return }
					EmailViewController.register().pushFrom(viewController: self)
				})
				
			}, cancel: {

			})
		}
	}
	
	private func showTutorialIfNeeded() -> Bool {
		if UserDefaults.standard.bool(forKey: "TutorialWasShown") {
			return false
		}

		UserDefaults.standard.set(true, forKey: "TutorialWasShown")
		StoryboardScene.Main.instantiateTutorialScene().push(from: self)
		tutorialWasShown = true
		return true
	}

	override func didFinishUploadExtentionFiles() {
		super.didFinishUploadExtentionFiles()
		refreshData()
	}
			
	override var canUploadExtentionFiles: Bool {
		return true
	}
	
	override var canShowBackupStatus: Bool {
		return true
	}
	
	private func refreshData() {
		ChatService.fetchChatList { (chatList) in
			self.chatList = chatList
			self.chatListTableView.reloadData()
			
			self.emptyLabel.isHidden = !(self.filterIsActive == false && self.chatList.count == 0)
		}
	}
	
	private func getChatName(indexPath: IndexPath) -> String {
		return filterIsActive ? filteredChatList[indexPath.row].albumName : chatList[indexPath.row].albumName
	}
	
	private func getChat(indexPath: IndexPath) -> Chat {
		return filterIsActive ? filteredChatList[indexPath.row] : chatList[indexPath.row]
	}
	
	//MARK: - NavigationBarView
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		hideBackupStatus()
		
		navigationBarView.topBarLabel.text = "Secured Chats"
		navigationBarView.setupSettingsButton(button: navigationBarView.leftTopBarButton)
	}
	
	override func onLeft(view: NavigationBarView, button: UIButton) {
		AppSettingsViewController.inistantiate().pushFrom(viewController: self, transitionFade: UIDevice.current.isIpad())
	}
	
	override func search(by string: String) {
		filterIsActive = !string.isEmpty
		if filterIsActive == false {
			filteredChatList.removeAll()
		} else {
			filteredChatList = chatList.filter({ $0.name.lowercased().contains(string.lowercased()) })
		}
		self.chatListTableView.reloadData()
	}
	
	//MARK: - UITableViewDelegate
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0.0001
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.0001
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 85
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		guard let chatCell = cell as? ChatTableViewCell else { return }
		let chat = getChat(indexPath: indexPath)
		chatCell.configureWithViewModel(ChatTableViewCellViewModel(chat: chat))
	}
	
	//MARK: - UITableViewDataSource
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filterIsActive ? filteredChatList.count : chatList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: ChatTableViewCell.identifier, for: indexPath)
		if let chatListCell = cell as? ChatTableViewCell {
			chatListCell.tableView = tableView
			chatListCell.dragAndDropDelegate = self
		}
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let chatName = getChatName(indexPath: indexPath)
		showChat(name: chatName)
		tableView.deselectRow(at: indexPath, animated: false)
		
		canShowReview = true
	}
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
	func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
		return .delete
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if (editingStyle == UITableViewCellEditingStyle.delete) {
			let chatName = getChatName(indexPath: indexPath)
			ChatService.deleteChat(chatName: chatName, completion: {
				DispatchQueue.main.async {
					self.refreshData()
				}
			})
		}
	}
	
	private func showChat(name: String) {
		guard let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: name) else { return }
		
		let controller = StoryboardScene.Chat.instantiateChatScene()
		controller.viewModel = ChatViewControllerViewModel(album: album)
		controller.push(from: self)
	}
	
	func dragBegun(cell: ChatTableViewCell) {
		if let indexPath = chatListTableView.indexPath(for: cell) {
			draggingChat = chatList[indexPath.row]
		}
	}

	func scrollDown(point: CGPoint) {
		if let indexPath = chatListTableView.indexPathForRow(at: point) {
			
			print("\(indexPath)")
			
			let newRow = indexPath.row + 1
			let newSection = indexPath.section
			let newIndexPath = IndexPath(row: newRow, section: newSection)
			
			if newRow < chatList.count {
				chatListTableView.scrollToRow(at: newIndexPath, at: .bottom, animated: false)
			}
		}
	}
	
	func scrollUp(point: CGPoint) {
		if let indexPath = chatListTableView.indexPathForRow(at: point) {
			
			print("\(indexPath)")
			
			let newRow = indexPath.row - 1
			let newSection = indexPath.section
			let newIndexPath = IndexPath(row: newRow, section: newSection)
			
			if newRow >= 0 {
				chatListTableView.scrollToRow(at: newIndexPath, at: .top, animated: false)
			}
		}
	}
	
	func drag(toPoint: CGPoint) {
		if let indexPath = chatListTableView.indexPathForRow(at: toPoint), let cell = chatListTableView.cellForRow(at: indexPath) as? ChatTableViewCell {
			if targetCell != cell {
				targetCell?.selectAsTarget(select: false)
				targetCell = cell
				targetCell?.selectAsTarget(select: true)
			}
		}
	}
	
	func cancel() {
		targetCell?.selectAsTarget(select: false)
		targetCell = nil
	}
	
	func drop(toPoint: CGPoint) {
		if let indexPath = chatListTableView.indexPathForRow(at: toPoint) {
			let goalChat = chatList[indexPath.row]

			if let draggingChat = draggingChat, goalChat.albumName != draggingChat.albumName {
				_ = AlertView.shows(inViewController: self, text: "Conversations will be merged", actionTitle: "Continue", cancelTitle: "Cancel", action: {
					ChatService.moveChat(movedAlbumName: draggingChat.albumName, destinationAlbumName: goalChat.albumName, completion: {
						self.refreshData()
					})
				}, cancel: {
					self.refreshData()
				})
			}

			draggingChat = nil
			
			targetCell?.selectAsTarget(select: false)
			targetCell = nil
		}
	}
	
}
