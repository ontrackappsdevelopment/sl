//
//  ChatViewController.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol ChatViewControllerDelegate {
	var chatName: String { get }
	var albumName: String { get }
	var type: ChatType { get }
}

class ChatViewControllerViewModel: ChatViewControllerDelegate {
	var album: MAlbum
	
	init(album: MAlbum) {
		self.album = album
	}
	
	var chatName: String {
		return album.mobChatName ?? ""
	}

	var albumName: String {
		return album.mobName ?? ""
	}
	
	var type: ChatType {
		return album.mobParticipants > 2 ? .multiple : .single
	}
}

class ChatViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	static var chatViewController: ChatViewController!
	var viewModel: ChatViewControllerDelegate?
	private var messages = [Message]()
	private var filteredMessages = [Message]()
	private var days = [String]()
	private var participants = [String]()
	private var myNickName = ""
	private var chooseNickNameWasShown = false
	
	@IBOutlet weak var chatTableView: UITableView! {
		didSet {
			chatTableView.rowHeight = UITableViewAutomaticDimension
			chatTableView.estimatedRowHeight = 65
			chatTableView.backgroundView = UIView()
			chatTableView.backgroundView?.backgroundColor = UIColor.clear
		}
	}
	
	let searchController = UISearchController(searchResultsController: nil)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		chatTableView.tableHeaderView = searchController.searchBar
		definesPresentationContext = true
		searchController.dimsBackgroundDuringPresentation = false
		
		// Setup the Scope Bar
//		searchController.searchBar.scopeButtonTitles = ["All", "Last Added"]
		searchController.searchBar.delegate = self
		searchController.delegate = self
		
		searchController.searchBar.isTranslucent = true
		searchController.searchBar.backgroundImage = UIImage()
		searchController.searchBar.barTintColor = UIColor.clear
		searchController.searchBar.tintColor = UIColor.black
		
		searchController.searchBar.translatesAutoresizingMaskIntoConstraints = true
		
		if let view = searchController.searchBar.subviews.first {
			view.subviews.forEach({ (view) in
				if let imageView = view as? UIImageView {
					imageView.image = nil
					imageView.backgroundColor = UIColor.clear
				} else if let textField = view as? UITextField {
					textField.borderStyle = .roundedRect
					textField.backgroundColor = UIColor(white: 1.0, alpha: 0.7)
					textField.font = UIFont(name: "SFUIDisplay-Light", size: 14.0)
					
					var frame = textField.frame
					frame.size.height = 30
					textField.frame = frame

					
				} else if let button = view as? UIButton {
					button.setTitleColor(UIColor.black, for: .normal)
				}
			})
			view.backgroundColor = UIColor.clear
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		ChatViewController.chatViewController = self
		refreshData()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		scrollToBottom()
		
		if  chooseNickNameWasShown == false {
			chooseNickNameWasShown = true
			
			if let viewModel = viewModel, let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: viewModel.albumName),
				let mobMyNick = album.mobMyNick, mobMyNick.isEmpty == true {
				openChooseNickName()
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		AudioPlayer.sharedPlayer.stop()
	}

	override func didEnterBackground(notification: NSNotification) {
		super.didEnterBackground(notification: notification)
		AudioPlayer.sharedPlayer.stop()
	}

	override func didFinishUploadExtentionFiles() {
		super.didFinishUploadExtentionFiles()
		refreshData()
	}
	
	override var canShowBackupStatus: Bool {
		return true
	}
	
	override var canUploadExtentionFiles: Bool {
		return true
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		guard let viewModel = viewModel else { return }
		
		navigationBarView.topBarLabel.text = viewModel.chatName
		navigationBarView.setupRightButton(image: nil, title: "Me", target: self, action: #selector(onMe(sender:)))
	}
	
	@objc private func onMe(sender: UIButton) {
		openChooseNickName()
	}
	
	private func openChooseNickName() {
		let controller = StoryboardScene.Chat.instantiateChooseNickName()
		controller.list = self.participants
		controller.chatName = viewModel?.chatName ?? ""
		controller.albumName = viewModel?.albumName ?? ""
		controller.push(from: self)
	}
	
	private func refreshData() {
		guard let viewModel = viewModel else { return }
		
		ChatService.fetchChat(name: viewModel.albumName, completion: { (myNickName, messages, days, participants) in
			DispatchQueue.main.async {
				self.myNickName = myNickName
				self.participants = participants
				self.messages = messages
				self.days = days
				self.chatTableView.reloadData()
			}
		})
	}
	
	private func scrollToBottom() {
		let lastSection = days.count
		if lastSection > 0 {
			let lastRow = messages.filter({ $0.date == days[lastSection - 1] }).count
			let indexPath = IndexPath(row: lastRow, section: lastSection)
			if let _ = chatTableView.cellForRow(at: indexPath) {
				chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
			}
		}
	}
	
	private func getMessageByIndexPath(indexPath: IndexPath) -> Message {
		var message: Message
		if isFiltering() {
			message = filteredMessages[indexPath.row]
		} else {
			message = messages.filter({ $0.date == days[indexPath.section] })[indexPath.row]
		}
		return message
	}
	
	//MARK: -
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return isFiltering() ? 0.0001 : 50
	}
	
//	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//		let message = getMessageByIndexPath(indexPath: indexPath)
//		if message.filePath != nil {
//			return 180
//		} else {
//			return UITableViewAutomaticDimension
//		}
//	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		if isFiltering() {
			return 1
		}
		
		return days.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if isFiltering() {
			return filteredMessages.count
		}
		
		return messages.filter({ $0.date == days[section] }).count
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if isFiltering() {
			return nil
		}
		
		let cell = tableView.dequeueReusableCell(withIdentifier: HeaderMessagesTableViewCell.identifier) as? HeaderMessagesTableViewCell
		cell?.configureWithViewModel(HeaderMessageTableViewCellViewModel(dateString: days[section]))
		return cell
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let message = getMessageByIndexPath(indexPath: indexPath)
		var file: File?
		if let filePath	= message.filePath {
			file = File(relativePath: filePath)
			if let file = file {
				if file.fileType == .audio {
					let cell = tableView.dequeueReusableCell(withIdentifier: AudioTableViewCell.identifier) as! AudioTableViewCell
					cell.configureWithViewModel(AudioTableViewCellViewModel(message: message, myNickName: myNickName, chatName: viewModel?.albumName ?? ""))
					return cell
				}
			}

			let cell = tableView.dequeueReusableCell(withIdentifier: FileTableViewCell.identifier) as! FileTableViewCell
			cell.configureWithViewModel(FileTableViewCellViewModel(message: message, myNickName: myNickName, chatName: viewModel?.albumName ?? ""))
			return cell
		}
		
		var cellIdentifier =  MessageTableViewCell.identifier
		if let viewModel = viewModel, viewModel.type == .single {
			cellIdentifier = MessageTableViewCell.identifierWithOutName
		}
		
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MessageTableViewCell
		cell.configureWithViewModel(MessageTableViewCellViewModel(message: message, myNickName: myNickName))
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let message = getMessageByIndexPath(indexPath: indexPath)
		var file: File?
		if let filePath	= message.filePath {
			file = File(relativePath: filePath)
			if let file = file {
				if file.fileType == .image || file.fileType == .video {
					
					let controller = StoryboardScene.Main.instantiateSlideScene()
					controller.title = viewModel?.chatName
				
					if let viewModel = viewModel {
					
						if let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: viewModel.albumName), let albumPath = album.mobPath {
							if let indexedObject = FileService.fetchAlbumFiles(relativePath: albumPath).enumerated().first(where: { $0.element.mobPath == file.relativePath }) {
								FileService.sharedInstance.currentDirectory = File(relativePath: albumPath)
								PreviewViewController.index = indexedObject.offset
								controller.pushFrom(viewController: self)
	//							showSwipeHint = true
							}
						}
					}
				}
			}
		}
	}
	
	//MARK: -
	
	func filterContentForSearchText(_ searchText: String, scope: String = "All") {
		filteredMessages = messages.filter({ (message) -> Bool in
			let doesCategoryMatch = (scope == "All")
			
			if searchBarIsEmpty() {
				return doesCategoryMatch
			} else {
				return doesCategoryMatch && message.message.lowercased().contains(searchText.lowercased())
			}
		})
		
		chatTableView.reloadData()
	}
	
	func searchBarIsEmpty() -> Bool {
		return searchController.searchBar.text?.isEmpty ?? true
	}
	
	func isFiltering() -> Bool {
		let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
		return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
	}
	
	//MARK: -
	
}

extension ChatViewController: UISearchControllerDelegate {
	
	func willPresentSearchController(_ searchController: UISearchController) {
		if let view = searchController.searchBar.subviews.first {
			view.subviews.forEach({ (view) in
				if let textField = view as? UITextField {
					textField.backgroundColor = UIColor.white
				}
			})
		}
		
		if let navigationBarView = navigationBarView {
			navigationBarView.rightTopBarButton.isHidden = true
		}
		
		UIView.animate(withDuration: 0.3) {
			self.chatTableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: 0, right: 0)
		}
	}
	
	func didPresentSearchController(_ searchController: UISearchController) {
//		self.view.layoutIfNeeded()
//		UIView.animate(withDuration: 0.1) {
			if let view = searchController.searchBar.subviews.first {
				view.subviews.forEach({ (view) in
					if let imageView = view as? UIImageView {
						imageView.image = nil
						imageView.backgroundColor = UIColor.clear
					} else if let textField = view as? UITextField {
						var frame = textField.frame
						frame.origin.y = 14
						frame.size.height -= 7
						textField.frame = frame
					} else if let button = view as? UIButton {
						var frame = button.frame
						frame.origin.y = 14
						button.frame = frame
					}
				})
			}
//		}
	}
	
	func willDismissSearchController(_ searchController: UISearchController) {
		if let view = searchController.searchBar.subviews.first {
			view.subviews.forEach({ (view) in
				if let textField = view as? UITextField {
					textField.backgroundColor = UIColor(white: 1.0, alpha: 0.7)
				}
			})
		}
		
		if let navigationBarView = navigationBarView {
			navigationBarView.rightTopBarButton.isHidden = false
		}
		
		UIView.animate(withDuration: 0.3) {
			self.chatTableView.contentInset = UIEdgeInsets.zero			
			self.chatTableView.contentOffset = CGPoint(x: 0, y: 0)
		}
	}
}

extension ChatViewController: UISearchBarDelegate {	
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
		filterContentForSearchText(searchBar.text!)
	}
}

extension ChatViewController: UISearchResultsUpdating {
	// MARK: - UISearchResultsUpdating Delegate
	func updateSearchResults(for searchController: UISearchController) {
		let searchBar = searchController.searchBar
//		let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
		filterContentForSearchText(searchController.searchBar.text!)
	}
}
