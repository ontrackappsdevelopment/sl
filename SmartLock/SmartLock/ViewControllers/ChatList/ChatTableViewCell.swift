//
//  ChatTableViewCell.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import AudioToolbox

protocol ChatTableViewCellDelegate {
	var iconHighlighted: Bool { get }
	var title: String { get }
	var status: String { get }
	var backupStatusHidden: Bool { get }
	var time: String { get }
	var importedTime: String { get }
}

protocol ChatTableViewCellDragAndDropDelegate {
	func dragBegun(cell: ChatTableViewCell)
	func drag(toPoint: CGPoint)
	func scrollUp(point: CGPoint)
	func scrollDown(point: CGPoint)
	func drop(toPoint: CGPoint)
	func cancel()
}

class ChatTableViewCell: UITableViewCell, ConfigurableCell {
	static var identifier: String = "ChatTableViewCell"
	typealias T = ChatTableViewCellDelegate
	
	@IBOutlet weak var iconImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var backupStatusImageView: UIImageView!
	@IBOutlet weak var importedTimeLabel: UILabel!
	@IBOutlet weak var dragAndDropImageView: UIImageView!
	
   var fakeImageView: UIImageView!

	
	var tableView: UITableView!
	var dragAndDropDelegate: ChatTableViewCellDragAndDropDelegate?
	
	var longPressRecognizer: UILongPressGestureRecognizer?
	
	var viewModel: ChatTableViewCellDelegate?
	
	func configureWithViewModel(_ viewModel: ChatTableViewCellDelegate) {
		self.viewModel = viewModel
		iconImageView.isHighlighted = viewModel.iconHighlighted
		titleLabel.text = viewModel.title
		statusLabel.text = viewModel.status
		backupStatusImageView.isHidden = viewModel.backupStatusHidden
		timeLabel.text = viewModel.time
		importedTimeLabel.text = viewModel.importedTime
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(sender:)))
		
		if let longPressRecognizer = longPressRecognizer {
			dragAndDropImageView.addGestureRecognizer(longPressRecognizer)
		}
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}
	
	@objc private func longPress(sender: UILongPressGestureRecognizer) {
		
		switch sender.state {
		case .began:
			
			if UIDevice.current.isIpad() {
				AudioServicesPlaySystemSound(1103) //1103, 1104, 1105
			} else {
				AudioServicesPlaySystemSound(1519)
			}
			
//			dragAndDropImageView.layer.borderWidth = 1
//			dragAndDropImageView.layer.borderColor = UIColor.gray.cgColor
//			dragAndDropImageView.image = self.makeScreenshot
			
			fakeImageView = UIImageView(image: self.makeScreenshot)
			
			tableView.addSubview(fakeImageView)
			tableView.bringSubview(toFront: fakeImageView)
			let point = sender.location(in: tableView)
			fakeImageView.center = point
			dragAndDropDelegate?.dragBegun(cell: self)
		case .changed:
			let point = sender.location(in: tableView)
			
			if point.y - tableView.bounds.minY < dragAndDropImageView.bounds.height && tableView.bounds.minY > 0 {
				dragAndDropDelegate?.scrollUp(point: point)
			}
			
			if (tableView.bounds.maxY - point.y) < dragAndDropImageView.bounds.height {
				dragAndDropDelegate?.scrollDown(point: point)
			}
			
			if let indexPath = self.tableView.indexPathForRow(at: point) {
				print("\(indexPath)")
			} else {
				print("НЕТУ")
			}
			
			fakeImageView.center = point
			self.backgroundColor = UIColor.white
			
			dragAndDropDelegate?.drag(toPoint: point)
		case .cancelled:
//			self.addSubview(dragAndDropImageView)
			
			fakeImageView.removeFromSuperview()
			
//			dragAndDropImageView.image = nil
//			dragAndDropImageView.backgroundColor = UIColor.red
//			dragAndDropImageView.layer.borderColor = UIColor.clear.cgColor
			
			UIView.animate(withDuration: 0.3, animations: {
//				self.center = CGPoint(x: self.superview!.bounds.size.width / 2, y: self.superview!.bounds.size.height / 2)
			})
			
			dragAndDropDelegate?.cancel()
			
		case .ended:
			let point = sender.location(in: tableView)
	
			fakeImageView.removeFromSuperview()
//			self.addSubview(dragAndDropImageView)
//			dragAndDropImageView.image = nil
//			dragAndDropImageView.backgroundColor = UIColor.red
//			dragAndDropImageView.layer.borderColor = UIColor.clear.cgColor
			
			UIView.animate(withDuration: 0.3, animations: {
//				self.dragAndDropImageView.center = CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2)
			})
			
			dragAndDropDelegate?.drop(toPoint: point)
			
		case .failed:
//			self.addSubview(dragAndDropImageView)
//			dragAndDropImageView.image = nil
//			dragAndDropImageView.backgroundColor = UIColor.red
//			dragAndDropImageView.layer.borderColor = UIColor.clear.cgColor
			
			fakeImageView.removeFromSuperview()
			
			UIView.animate(withDuration: 0.3, animations: {
//				self.dragAndDropImageView.center = CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2)
			})
			
			dragAndDropDelegate?.cancel()
			
		case .possible:
			break
		}
		
	}

	func selectAsTarget(select: Bool) {
		dragAndDropImageView.backgroundColor = select ? UIColor(white: 1.0, alpha: 0.7) : UIColor.clear
	}
	
}

extension UIView{
	
	var makeScreenshot: UIImage?{
		
		UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
		let context = UIGraphicsGetCurrentContext();
		self.layer.render(in: context!)
		let screenShot = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		return screenShot
	}
}

//class DragableImageView: UIImageView {
//
//	var longPressRecognizer: UILongPressGestureRecognizer?
//
//	override func awakeFromNib() {
//		super.awakeFromNib()
//		longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(sender:)))
//
//		if let longPressRecognizer = longPressRecognizer {
//			self.addGestureRecognizer(longPressRecognizer)
//		}
//	}
//

//
//extension UIView{
//
//	var makeScreenshot: UIImage?{
//
//		UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
//		let context = UIGraphicsGetCurrentContext();
//		self.layer.render(in: context!)
//		let screenShot = UIGraphicsGetImageFromCurrentImageContext();
//		UIGraphicsEndImageContext();
//		return screenShot
//}

