//
//  ChooseNickNameViewController.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class ChooseNickNameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	
	var list = [String]()
	var chatName: String?
	var albumName: String?
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return list.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cellIdentifier = "cell"
		let nickName = list[indexPath.row]
		if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
			cell.textLabel?.text = nickName
			return cell
		} else {
			let cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
			cell.textLabel?.text = nickName
			cell.textLabel?.textAlignment = .center
			
			cell.textLabel?.font = UIFont(name: "SFUIDisplay-Regular", size: 16)
			
			cell.backgroundColor = UIColor.clear
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let albumName = albumName else { return }
		
		let nickName = list[indexPath.row]
		
		ChatService.saveMyNickForChat(albumName: albumName, nickName: nickName) {
			self.pop()
		}
	}
	
	@IBAction func onClose(_ sender: UIButton) {
		self.pop()
	}
}
