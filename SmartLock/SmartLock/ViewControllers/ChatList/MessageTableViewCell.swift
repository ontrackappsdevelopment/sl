//
//  MessageTableViewCell.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import ActiveLabel

enum MessageHorizontalPosition {
	case left
	case right
}

protocol MessageTableViewCellDelegate {
	var messageHorizontalPosition: MessageHorizontalPosition { get }
	var messageText: String { get }
	var senderText: String { get }
	var timeText: String { get }
	var borderColor: UIColor { get }
	var backgroundColor: UIColor { get }
	var senderTextColor: UIColor { get }
}

class MessageTableViewCell: UITableViewCell, ConfigurableCell {
	typealias T = MessageTableViewCellDelegate
	
	static var identifier: String = "MessageTableViewCell"
	static var identifierWithOutName: String = "MessageWithOutNameTableViewCell"
	
	var viewModel: MessageTableViewCellDelegate?

	@IBOutlet weak var bubbleView: UIView!
	@IBOutlet weak var messageLabel: ActiveLabel! {
		didSet {
			messageLabel.urlMaximumLength = 31
			messageLabel.handleURLTap {
				if let url = URL(string: $0.absoluteString) {
					if UIApplication.shared.canOpenURL(url) {
						UIApplication.shared.openURL(url)
					}
				}
			}
		}
	}
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var senderLabel: UILabel!
	
	@IBOutlet var leftSideConstraint: NSLayoutConstraint!
	@IBOutlet var rightSideConstraint: NSLayoutConstraint!
	@IBOutlet var leftSideFreeConstraint: NSLayoutConstraint!
	@IBOutlet var rightSideFreeConstraint: NSLayoutConstraint!
	
	func configureWithViewModel(_ viewModel: MessageTableViewCellDelegate) {
		self.viewModel = viewModel
		
		switch viewModel.messageHorizontalPosition {
		case .left: leftSideMessage()
		case .right: rightSideMessage()
		}
		
		messageLabel.text = viewModel.messageText
		timeLabel.text = viewModel.timeText
		if let senderLabel = senderLabel {
			senderLabel.text = viewModel.senderText
			senderLabel.textColor = viewModel.senderTextColor
		}
		
		bubbleView.layer.backgroundColor = viewModel.backgroundColor.cgColor
		bubbleView.layer.borderColor = viewModel.borderColor.cgColor
		
//		messageLabel.layoutIfNeeded()
//		timeLabel.layoutIfNeeded()
	}
	
	private func leftSideMessage() {
		leftSideConstraint.isActive = true
		rightSideConstraint.isActive = false

		leftSideFreeConstraint.isActive = false
		rightSideFreeConstraint.isActive = true
		
		messageLabel.textAlignment = .left
		timeLabel.textAlignment = .right
		
		if let senderLabel = senderLabel {
			senderLabel.textAlignment = .left
		}
	}
	
	private func rightSideMessage() {
		leftSideConstraint.isActive = false
		rightSideConstraint.isActive = true
		
		leftSideFreeConstraint.isActive = true
		rightSideFreeConstraint.isActive = false
		
		messageLabel.textAlignment = .right
		timeLabel.textAlignment = .right
		
		if let senderLabel = senderLabel {
			senderLabel.textAlignment = .right
		}
	}
}
