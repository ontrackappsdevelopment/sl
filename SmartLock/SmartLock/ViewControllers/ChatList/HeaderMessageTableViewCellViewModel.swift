//
//  HeaderMessageTableViewCellViewModel.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class HeaderMessageTableViewCellViewModel: HeaderMessageTableViewCellDelegate {
	var dateString: String?
	
	init(dateString: String) {
		self.dateString = dateString
	}
	
	var day: String {
		guard let dateString = dateString else { return "" }
				
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .none
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		
		if let date = dateFormatter.date(from: dateString) {
			if Calendar.current.isDateInYesterday(date) {
				return "yesterday"
			}
			
			if Calendar.current.isDateInToday(date) {
				return "today"
			}
		}
		
		return dateString
	}
	
	var borderColor: UIColor {
		return UIColor(red: 187.0/255.0, green: 204.0/255.0, blue: 254.0/255.0, alpha: 1.0)
	}
	
	var backgroundColor: UIColor {
		return UIColor(red: 210.0/255.0, green: 220.0/255.0, blue: 250.0/255.0, alpha: 1.0)
	}
}
