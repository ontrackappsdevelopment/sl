//
//  AudioTableViewCellViewModel.swift
//  ChatKepper
//
//  Created by Bogachev on 9/28/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class AudioTableViewCellViewModel: AudioTableViewCellDelegate {
	
	var message: Message
	var myNickName: String
	var chatName: String
	var times = [String: VLCTime]()
	
	init(message: Message, myNickName: String, chatName: String) {
		self.message = message
		self.myNickName = myNickName
		self.chatName = chatName
	}
	
	var audioURL: URL? {
		if let filePath = message.filePath {
			let file = File(relativePath: filePath)
			return file.url
		}
		return nil
	}
	
	var messageHorizontalPosition: MessageHorizontalPosition {
		return isMyMessage ? .right : .left
	}
	
	var isMyMessage: Bool {
		return message.sender == myNickName
	}
	
	var senderText: String {
		return message.sender
	}
	
	var timeText: String {
		return message.time
	}
	
	var pauseTime: VLCTime? {
		guard let audioURL = audioURL else { return nil }
		return times[audioURL.path]
	}
	
	var borderColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
	
	var backgroundColor: UIColor {
		return isMyMessage ?
			UIColor(red: 225.0/255.0, green: 247.0/255.0, blue: 202.0/255.0, alpha: 1.0) :
			UIColor(red: 253.0/255.0, green: 243.0/255.0, blue: 197.0/255.0, alpha: 1.0)
	}
	
	var senderTextColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
	
	func saveTime(time: VLCTime) {
		guard let audioURL = audioURL else { return }
		times[audioURL.path] = time
	}
	
//	var duration: VLCTime {
//		let mediaPlayer = VLCMediaPlayer()
//		let media = VLCMedia(url: audioURL)
//		mediaPlayer.media = media
//		mediaPlayer.audio.isMuted = true
//		mediaPlayer.play()
//		mediaPlayer.pause()
//		return mediaPlayer.media.length
//	}
}
