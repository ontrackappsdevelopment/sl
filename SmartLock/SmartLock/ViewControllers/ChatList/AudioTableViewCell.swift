//
//  AudioTableViewCell.swift
//  ChatKepper
//
//  Created by Bogachev on 9/28/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class Slider: UISlider {
	
	var stopSync: Bool = false
	
	@IBInspectable open var trackWidth:CGFloat = 2 {
		didSet {setNeedsDisplay()}
	}
	
	override open func trackRect(forBounds bounds: CGRect) -> CGRect {
		let defaultBounds = super.trackRect(forBounds: bounds)
		return CGRect(
			x: defaultBounds.origin.x,
			y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
			width: defaultBounds.size.width,
			height: trackWidth
		)
	}
}

protocol AudioTableViewCellDelegate {
	var messageHorizontalPosition: MessageHorizontalPosition { get }
	var audioURL: URL? { get }
	var senderText: String { get }
	var timeText: String { get }
	var pauseTime: VLCTime? { get }
	var borderColor: UIColor { get }
	var backgroundColor: UIColor { get }
	var senderTextColor: UIColor { get }
	
	func saveTime(time: VLCTime)
}

class AudioTableViewCell: UITableViewCell, ConfigurableCell, AudioPlayerDelegate {
	
	@IBOutlet weak var bubbleView: UIView!
	@IBOutlet weak var currentTimeLabel: UILabel!
	@IBOutlet weak var totalTimeLabel: UILabel!
	@IBOutlet weak var playButton: UIButton!
	
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var senderLabel: UILabel!
	
	@IBOutlet var leftSideConstraint: NSLayoutConstraint!
	@IBOutlet var rightSideConstraint: NSLayoutConstraint!
	@IBOutlet weak var seekBar: Slider!{
		didSet {
			seekBar.setThumbImage(UIImage(named: "ico_circle"), for: .normal)
			
			seekBar.addTarget(self, action: #selector(didtTouchUpSeekBarSlider(sender:)), for: .touchUpInside)
			seekBar.addTarget(self, action: #selector(didtTouchUpSeekBarSlider(sender:)), for: .touchUpOutside)
			seekBar.addTarget(self, action: #selector(didtTouchDownSeekBarSlider(sender:)), for: .touchDown)
		}
	}
	
	typealias T = AudioTableViewCellDelegate
	static var identifier: String = "AudioTableViewCell"
	var viewModel: AudioTableViewCellDelegate?
	func configureWithViewModel(_ viewModel: AudioTableViewCellDelegate) {
		self.viewModel = viewModel
		
		switch viewModel.messageHorizontalPosition {
		case .left: leftSideMessage()
		case .right: rightSideMessage()
		}
		
		senderLabel.text = viewModel.senderText
		timeLabel.text = viewModel.timeText
		
		seekBar.value = 0
		seekBar.minimumValue = 0
		seekBar.maximumValue = 0
		
		seekBar.isEnabled = false
		
		if let senderLabel = senderLabel {
			senderLabel.text = viewModel.senderText
			senderLabel.textColor = viewModel.senderTextColor
		}
		
		bubbleView.layer.backgroundColor = viewModel.backgroundColor.cgColor
		bubbleView.layer.borderColor = viewModel.borderColor.cgColor
	}
	
	private func leftSideMessage() {
		leftSideConstraint.isActive = true
		rightSideConstraint.isActive = false
		
		timeLabel.textAlignment = .right
		senderLabel.textAlignment = .left
	}
	
	private func rightSideMessage() {
		leftSideConstraint.isActive = false
		rightSideConstraint.isActive = true
		
		timeLabel.textAlignment = .right
		senderLabel.textAlignment = .right
	}
	
	@objc private func didtTouchUpSeekBarSlider(sender: Slider) {
		let interval = sender.value - AudioPlayer.sharedPlayer.currentTime.value.floatValue
		
		currentTimeLabel.text = "00:00"
		AudioPlayer.sharedPlayer.seekTo(value: Int32(interval/1000))
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
			sender.stopSync = false
		})
	}
	
	@objc private func didtTouchDownSeekBarSlider(sender: Slider) {
		sender.stopSync = true
	}
	
	@IBAction func onPlay(_ sender: UIButton) {
		guard let viewModel = viewModel else { return }
		guard let audioURL = viewModel.audioURL else { return }
		
		sender.isSelected = !sender.isSelected
		if sender.isSelected {
			AudioPlayer.sharedPlayer.play(delegate: self, audioURL: audioURL)
		} else {								
			AudioPlayer.sharedPlayer.pause(completion: { (remainingTime, time) in
				viewModel.saveTime(time: time)				
			})
		}
	}
	
	func didChangeMediaPlayerTime(time: VLCTime, length: VLCTime) {
		guard seekBar.stopSync == false else { return }
		
		seekBar.value = Float(time.intValue)
		seekBar.maximumValue = Float(length.intValue)
		
		currentTimeLabel.text = time.description
		totalTimeLabel.text = length.description
		
		if length.intValue > 0 {
			seekBar.isEnabled = true
		}
	}
	
	func didChangeMediaPlayerState() {		
		playButton.isSelected = AudioPlayer.sharedPlayer.isPlaying
	}
	
	func stopPlaying() {
		playButton.isSelected = false
	}
}
