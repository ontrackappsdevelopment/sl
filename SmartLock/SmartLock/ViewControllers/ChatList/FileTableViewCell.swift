//
//  FileTableViewCell.swift
//  ChatKepper
//
//  Created by Bogachev on 9/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol FileTableViewCellDelegate {
	var messageHorizontalPosition: MessageHorizontalPosition { get }
	var senderText: String { get }
	var timeText: String { get }
	var senderTextColor: UIColor { get }
	var preview: UIImage? { get }
	
	var borderColor: UIColor { get }
	var backgroundColor: UIColor { get }
}

class FileTableViewCell: UITableViewCell, ConfigurableCell {
	static var identifier: String = "FileTableViewCell"
	
	typealias T = FileTableViewCellDelegate
	
	var viewModel: FileTableViewCellDelegate?
	
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var senderLabel: UILabel!
	@IBOutlet weak var previewImageView: UIImageView!
	@IBOutlet var leftSideConstraint: NSLayoutConstraint!
	@IBOutlet var rightSideConstraint: NSLayoutConstraint!
	@IBOutlet weak var bubbleView: UIView!
	
	func configureWithViewModel(_ viewModel: FileTableViewCellDelegate) {
		self.viewModel = viewModel
		
		switch viewModel.messageHorizontalPosition {
		case .left: leftSideMessage()
		case .right: rightSideMessage()
		}
		
		senderLabel.text = viewModel.senderText
		timeLabel.text = viewModel.timeText		
		previewImageView.image = viewModel.preview
		
		if let senderLabel = senderLabel {
			senderLabel.text = viewModel.senderText
			senderLabel.textColor = viewModel.senderTextColor
		}
		
		bubbleView.layer.backgroundColor = viewModel.backgroundColor.cgColor
		bubbleView.layer.borderColor = viewModel.borderColor.cgColor

	}
	
	private func leftSideMessage() {
		leftSideConstraint.isActive = true
		rightSideConstraint.isActive = false
				
		timeLabel.textAlignment = .right
		senderLabel.textAlignment = .left
	}
	
	private func rightSideMessage() {
		leftSideConstraint.isActive = false
		rightSideConstraint.isActive = true
		
		timeLabel.textAlignment = .right
		senderLabel.textAlignment = .right
	}

}
