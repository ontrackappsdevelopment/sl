//
//  MessageTableViewCellViewModel.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class MessageTableViewCellViewModel: MessageTableViewCellDelegate {
	var message: Message
	var myNickName: String
	
	init(message: Message, myNickName: String) {
		self.message = message
		self.myNickName = myNickName
	}
	
	var messageHorizontalPosition: MessageHorizontalPosition {
		return isMyMessage ? .right : .left
	}
	
	var messageText: String {
		return message.message
	}
	
	var senderText: String {
		return message.sender
	}
	
	var timeText: String {
		return message.time
	}
	
	var isMyMessage: Bool {
		return message.sender == myNickName
	}
	
	var borderColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
	
	var backgroundColor: UIColor {
		return isMyMessage ?
			UIColor(red: 225.0/255.0, green: 247.0/255.0, blue: 202.0/255.0, alpha: 1.0) :
			UIColor(red: 253.0/255.0, green: 243.0/255.0, blue: 197.0/255.0, alpha: 1.0)
	}
	
	var senderTextColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
}
