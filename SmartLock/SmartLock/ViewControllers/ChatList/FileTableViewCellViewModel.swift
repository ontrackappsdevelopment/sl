//
//  FileTableViewCellViewModel.swift
//  ChatKepper
//
//  Created by Bogachev on 9/28/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class FileTableViewCellViewModel: FileTableViewCellDelegate {
	
	var message: Message
	var myNickName: String
	var chatName: String
	
	init(message: Message, myNickName: String, chatName: String) {
		self.message = message
		self.myNickName = myNickName
		self.chatName = chatName
	}
	
	var messageHorizontalPosition: MessageHorizontalPosition {
		return isMyMessage ? .right : .left
	}
	
	var senderText: String {
		return message.sender
	}
	
	var timeText: String {
		return message.time
	}
	
	var isMyMessage: Bool {
		return message.sender == myNickName
	}
	
	var borderColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
	
	var backgroundColor: UIColor {
		return isMyMessage ?
			UIColor(red: 225.0/255.0, green: 247.0/255.0, blue: 202.0/255.0, alpha: 1.0) :
			UIColor(red: 253.0/255.0, green: 243.0/255.0, blue: 197.0/255.0, alpha: 1.0)
	}
	
	var senderTextColor: UIColor {
		return isMyMessage ?
			UIColor(red: 174.0/255.0, green: 226.0/255.0, blue: 118.0/255.0, alpha: 1.0) :
			UIColor(red: 223.0/255.0, green: 202.0/255.0, blue: 103.0/255.0, alpha: 1.0)
	}
	
	var preview: UIImage? {
		if let filePath = message.filePath {
			let file = File(relativePath: filePath)
			if let savedFile = MFile.mr_findFirst(byAttribute: "mobPath", withValue: filePath), let thumbnail = savedFile.mobThumbnail {
				switch file.fileType {
				case .image: return file.getPhotoThumbnailImage(name: thumbnail, size: 150)
				case .video: return file.getVideoThumbnailImage(name: thumbnail, size: 150)
				default: break
				}
			}
		}
		
		return nil
	}
}
