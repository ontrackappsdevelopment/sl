//
//  AboutSubscriptionViewController.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class AboutSubscriptionViewController: UIViewController {
	
	@IBOutlet weak var contentView: UIView! {
		didSet {
			contentView.layer.cornerRadius = 5
			contentView.masksToBounds = true
		}
	}
	@IBOutlet weak var detailTextView: UITextView!
	
	override func viewDidLayoutSubviews() {
		self.detailTextView.setContentOffset(.zero, animated: false)
	}
	
	@IBAction func onClose(_ sender: UIButton) {
		navigationController?.popViewController(animated: true)
	}	
}
