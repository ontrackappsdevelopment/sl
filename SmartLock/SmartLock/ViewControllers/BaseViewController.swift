//
//  BaseViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/7/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController, NavigationBarViewDelegate {
	
	func search(by: String) {
	
	}
	
	var canShowBackupStatus: Bool {
		return false
	}
	
	static var contextImage: UIImage?
	static var uploadExtentionInProgress = false
	
	var objects = [NSManagedObject]()
	
	@IBOutlet weak var currentContextImageView: UIImageView!
	
	@IBOutlet weak var topBarView: UIView! {
		didSet {
			navigationBarView = NavigationBarView.instantiateAndAddToView(parentView: topBarView, style: preferedNavigationBarViewStyle)
			navigationBarView?.delegate = self
			
			if let navigationBarView = navigationBarView {
				didSetNavigationBarView(navigationBarView: navigationBarView)
			}
		}
	}
	
	var navigationBarView: NavigationBarView?
	
	var topBarLabel: UILabel? {
		guard let navigationBarView = navigationBarView else { return nil }
		return navigationBarView.topBarLabel
	}
	
	var leftTopBarButton: UIButton? {
		guard let navigationBarView = navigationBarView else { return nil }
		return navigationBarView.leftTopBarButton
	}
	
	var rightTopBarButton: UIButton? {
		guard let navigationBarView = navigationBarView else { return nil }
		return navigationBarView.rightTopBarButton
	}
	
	var preferedNavigationBarViewStyle: NavigationBarViewStyle {
		return .colored
	}
	
	func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		
		
	}
	
	static var visibleViewController: BaseViewController?
	
	var inactiveView: UIView?
	var isVisible: Bool = false
	
	var lockController: LockViewController?
	var unlockController: LockViewController?
	var unlockAlbumController: LockViewController?
	
	var didUnlockAction: (() -> ())?
	
	static var avoidHidingWhenResignActive: Bool = false
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .default
	}
	
	override var prefersStatusBarHidden: Bool {
		return false
	}
	
	var canUploadExtentionFiles: Bool {
		return false
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let navigationController = self.navigationController {
			navigationController.setNavigationBarHidden(true, animated: false)
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		checkAndUploadExtentionFiles()

		
		setNeedsStatusBarAppearanceUpdate()
		
		NotificationCenter.default.addObserver(self, selector: #selector(didUnlock(notification:)), name: NSNotification.Name(rawValue: "DidUnlockNotification"), object: nil)
		
		setNeedsStatusBarAppearanceUpdate()
		
		if UIDevice.current.isIpad() {
			if let currentContextImageView = currentContextImageView {
				currentContextImageView.image = BaseViewController.contextImage
			}
		}
		
		setNeedsStatusBarAppearanceUpdate()
			
		isVisible = true
		
		BaseViewController.visibleViewController = self
		
		NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(willResignActive(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DidUnlockNotification"), object: nil)
		
		isVisible = false
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
		
		if inactiveView != nil {
			inactiveView?.removeFromSuperview()
			inactiveView = nil
		}
	}
	
	@objc func didUnlock(notification: Notification) {
		setNeedsStatusBarAppearanceUpdate()
		
		checkAndUploadExtentionFiles()
	}
	
	func unlockScreen(withTouchID: Bool = false, alpha: CGFloat = 1.0, canOpenDecoy: Bool = false, canCancel: Bool = false, success: @escaping () -> ()) {
		
		guard let user = LockService.sharedService.user else { return }
		
		view.endEditing(true)
		
		unlockController = getLockControllerBy(lockType: LockType(rawValue: Int(user.mobLockType))!, modal: alpha == 0.0)
		
		if unlockController == nil {
			
			success()
			
			return
			
		}
		
		if unlockController != nil {
			unlockController!.canCancel = canCancel
			unlockController!.canOpenDecoy = canOpenDecoy
			unlockController!.passcode = user.mobPasscode
			unlockController!.lockMode = .unlock
			
			unlockController?.success = { (pin: String) in
				
				if LockService.sharedService.isDecoyEnabled == true {
					
					LockService.sharedService.isDecoyEnabled = false
					FileService.sharedInstance.isDecoyEnabled = false
					
					self.unlockController!.view.removeFromSuperview()
					self.unlockController!.removeFromParentViewController()
					
					let controller = StoryboardScene.Main.instantiatePhotosScene()
					
					self.navigationController?.setViewControllers([controller], animated: false)
					
				} else {
					
					UIView.animate(withDuration: 0.3, animations: {
						
						self.unlockController!.view.alpha = 0
						
					}, completion: { (_) in
						
						self.unlockController!.view.removeFromSuperview()
						self.unlockController!.removeFromParentViewController()
						
//						self.navigationController?.setNavigationBarHidden(false, animated: false)
						
						success()
						
					})
					
				}
				
			}
			
			unlockController?.failure = {
				
				if DecoyType(rawValue: Int(LockService.sharedService.user!.mobDecoyPasscodeType)) != .decoyOff {
					
					LockService.sharedService.isDecoyEnabled = true
					FileService.sharedInstance.isDecoyEnabled = true
					
					self.unlockController?.view.removeFromSuperview()
					self.unlockController?.removeFromParentViewController()
					
					let controller = StoryboardScene.Main.instantiatePhotosScene()
					
					self.navigationController?.setViewControllers([controller], animated: false)
					
				}
				
			}
			
			unlockController?.cancel = {
				
//				self.navigationController?.setNavigationBarHidden(false, animated: false)
				
			}
			
//			self.navigationController?.setNavigationBarHidden(true, animated: false)
			
			BaseViewController.visibleViewController?.addViewControllerAsSubView(viewController: unlockController!)
			
		}
		
	}
	
	func unlockAlbum(file: File, presenter: UIViewController, success: @escaping () -> (), cancel: @escaping () -> ()) {
		
		view.endEditing(true)
		
		if let album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: file.relativePath) {
			
			let lockType = LockType(rawValue: Int(album.mobLockType))!
			
			if lockType != .undefined {
				
				LockService.sharedService.lockAlbum = album
				LockService.sharedService.lockAlbumFile = file
				
				unlockAlbumController = getLockControllerBy(lockType: lockType, modal: true)
				
				if unlockAlbumController == nil {
					
					success()
					
					return
					
				}
				
				if unlockAlbumController != nil {
					
					unlockAlbumController!.canCancel = true
					unlockAlbumController!.canOpenDecoy = false
					unlockAlbumController!.passcode = album.mobPasscode
					unlockAlbumController!.lockMode = .unlock
					
					unlockAlbumController!.success = { (pin: String) in
						
						LockService.sharedService.lockAlbum = nil
						
//						self.navigationController?.setNavigationBarHidden(false, animated: false)
						
						self.unlockAlbumController?.view.removeFromSuperview()
						self.unlockAlbumController?.removeFromParentViewController()
						
						success()
					}
					
					unlockAlbumController?.cancel = {
						
						LockService.sharedService.lockAlbum = nil
						
//						self.navigationController?.setNavigationBarHidden(false, animated: false)
						
						cancel()
						
					}
				
					
					presenter.addViewControllerAsSubView(viewController: unlockAlbumController!)
					
				}
				
			} else {
				
				success()
				
			}
			
		}
		
	}
	
	func removeLock(animated: Bool) {
		
		if lockController != nil {
			lockController?.view.removeFromSuperview()
			lockController?.removeFromParentViewController()
			
//			self.navigationController?.setNavigationBarHidden(false, animated: false)
		}
		
	}
	
	func removeUnlock(animated: Bool) {
		
		if unlockController != nil {
			unlockController?.view.removeFromSuperview()
			unlockController?.removeFromParentViewController()
			
//			self.navigationController?.setNavigationBarHidden(false, animated: false)
		}
		
	}
	
	func removeUnlockAlbum(animated: Bool) {
		
		if unlockAlbumController != nil {
			unlockAlbumController?.view.removeFromSuperview()
			unlockAlbumController?.removeFromParentViewController()
			
//			self.navigationController?.setNavigationBarHidden(false, animated: false)
		}
		
	}
	
	func getLockControllerBy(lockType: LockType, modal: Bool = false) -> LockViewController? {
		
		var controller: LockViewController?
		
		switch lockType {
			
		case .numpad4:
			
			controller = StoryboardScene.instantiateNumpadLockScene(modal: modal)
			(controller as! NumpadLockViewController).pinLenght = 4
			
		case .numpad6:
			
			controller = StoryboardScene.instantiateNumpadLockScene(modal: modal)
			(controller as! NumpadLockViewController).pinLenght = 6
			
		case .dotlock:
			
			controller = StoryboardScene.instantiatePatternLockScene(modal: modal)
			
		case .aplhanum:
			
			controller = StoryboardScene.instantiateAlphanumericLockScene(modal: modal)
			
		default:
			
			break
			
		}
		
		return controller
		
	}
	
	func willResignActive(notification: NSNotification) {
	}
	
	func didEnterBackground(notification: NSNotification) {
		self.view.endEditing(true)
	}
	
	func didBecomeActive(notification: NSNotification) {
		if AppDelegate.appIsLocked == false {
			checkAndUploadExtentionFiles()
		}
	}
	
	override func becomeFirstResponder() -> Bool {
		return true
	}
	
	override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
		
		if motion == .motionShake {
			
			guard LockService.sharedService.user?.mobShakeToLock == true else { return }

			if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
				appDelegate.lockScreen()
			}
						
		}
		
	}

	func checkAndUploadExtentionFiles() {
		guard AppDelegate.appIsLocked == false else { return }
		guard canUploadExtentionFiles == true else { return }
		guard BaseViewController.uploadExtentionInProgress == false else { return }
		BaseViewController.uploadExtentionInProgress = true
		
		FileService.sharedInstance.checkAndUploadExtentionFiles(start: {
			SVProgressHUD.showProgress(0, status: "Unzipping...")
		}, progress: { (progress) in
			SVProgressHUD.showProgress(progress, status: "Unzipping...")
		}, competion: {
			SVProgressHUD.show(withStatus: "Saving...")
			ChatService.addUnackedChats(completion: {
				BaseViewController.uploadExtentionInProgress = false
				SVProgressHUD.dismiss()
				self.didFinishUploadExtentionFiles()
			})
		})
	}
	
	func didFinishUploadExtentionFiles() {
		
	}
	
	//NavigationBarViewDelegate
	
	func onLeft(view: NavigationBarView, button: UIButton) {
		onBack(button)
	}
	
	func onRight(view: NavigationBarView, button: UIButton) {
		
	}
	
	//Actions
	
	@IBAction func onBack(_ sender: UIButton) {
		if let navigationController = navigationController {
			navigationController.popViewController(animated: true)
		}
	}
	
}
