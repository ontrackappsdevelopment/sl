//
//  SettingsViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {
	@IBOutlet weak var switcher: UISwitch!
	var switchedAction: ((Bool) -> ())!
	@IBAction func onChanged(_ sender: UISwitch) {
		switchedAction(sender.isOn)
	}
}

class BadgeTableViewCell: UITableViewCell {
	@IBOutlet weak var badgeLable: UILabel! {
		didSet {
			badgeText = ""
		}
	}
	var badgeText: String? {
		didSet {
			if let badgeText = badgeText {
				badgeLable.text = "  \(badgeText)  "
				badgeLable.sizeToFit()
				badgeLable.isHidden = badgeText.isEmpty == true				
			}
		}
	}
}

class InfoTableViewCell: UITableViewCell {
	@IBOutlet weak var title: UILabel!
	@IBOutlet weak var info: UILabel!
}

enum TypeSetting {
	
	case checkmark
	case switcher
	case disclosure
	case detailsAndDisclosure
	case badge
	case info
	
}

struct Setting {
	
	var title: String?
	var info: String?
	var badge: String = ""
	var icon: UIImage?
	var type: TypeSetting?
	var isExpanded: Bool = true
	var value: Any?
	var action: ((Any?, (() ->())?) -> ())?
	
}

struct Settings {
	
	var headers: [String]
	var cells: [[Setting]]
	
}

protocol SettingsViewControllerDelegate {
	
	var settings:Settings { get }
	var title:String { get }
	func configureNavigationBar(navigationBarView: NavigationBarView, target: UIViewController)
	
}

class SettingsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	static func inistantiate(delegate: SettingsViewControllerDelegate, showHeader: Bool = false) -> SettingsViewController {
		let controller = StoryboardScene.instantiateSettingsScene()
		controller.delegate = delegate
		controller.showHeader = showHeader
		return controller
	}
		
	var delegate: SettingsViewControllerDelegate!
	
	private var showHeader: Bool!
	
	@IBOutlet weak var tableView: UITableView!
	
	override var preferedNavigationBarViewStyle: NavigationBarViewStyle {
		return UIDevice.current.isIpad() ? .light : .colored
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)			
		tableView.reloadData()
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = delegate.title
		if let delegate = delegate {
			delegate.configureNavigationBar(navigationBarView: navigationBarView, target: self)
		}
	}
	
	override func didUnlock(notification: Notification) {
		super.didUnlock(notification: notification)
		tableView.reloadData()
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return delegate.settings.headers.count
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.0001
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0.0001
	}
	
//	func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
//		return 0.0001
//	}
	
//	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//		if(section == 0) && showHeader == true {
//			let view = UIView()
//			let label = UILabel(frame: view.bounds)
//			label.font = UIFont(name: "SFUIDisplay-Medium", size: 18.0)
//			label.text = delegate.settings.headers[0].capitalized
//			label.textColor = UIColor.blueBackgroundColor
//			label.textAlignment = .center
//			label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//			label.translatesAutoresizingMaskIntoConstraints = true
//			view.addSubview(label)
//			return view
//		}
//
//		return nil
//	}
	
//	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//		return delegate.settings.headers[section]
//	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return delegate.settings.cells[section].count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		switch setting.type! {
			
		case .checkmark:
			
			var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
			if cell == nil {
				cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
			}
			
			cell?.textLabel?.text = setting.title
			cell?.textLabel?.font = UIFont.systemFont(ofSize: 17)
			cell?.accessoryType = (setting.value as! Bool) == true ? .checkmark : .none
			cell?.imageView?.image = setting.icon
			
			cell?.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell!
			
		case .disclosure:
			
			var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
			if cell == nil {
				cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
			}
			
			cell?.textLabel?.text = setting.title
			cell?.textLabel?.font = UIFont.systemFont(ofSize: 17)
			cell?.accessoryType = .disclosureIndicator
			cell?.imageView?.image = setting.icon
			
			cell?.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell!
			
		case .switcher:
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell") as! SwitchTableViewCell
			
			cell.textLabel?.text = setting.title
			cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
			cell.imageView?.image = setting.icon
			cell.switcher.isOn = (setting.value as! Bool)
			cell.switchedAction = { (onStatus) in
				setting.action!(onStatus) {
					self.tableView.reloadData()
				}
			}
			
			cell.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell
			
		case .detailsAndDisclosure:
			
			var cell = tableView.dequeueReusableCell(withIdentifier: "cell1")
			
			if cell == nil {
				cell = UITableViewCell(style: .value1, reuseIdentifier: "cell1")
			}
			
			cell?.textLabel?.text = setting.title
			cell?.textLabel?.font = UIFont.systemFont(ofSize: 17)
			cell?.accessoryType = .disclosureIndicator
			cell?.imageView?.image = setting.icon
			
			cell?.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell!
			
		case .badge:
			
			let cell: BadgeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BadgeTableViewCell") as! BadgeTableViewCell
			
			cell.textLabel?.text = setting.title
			cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
			cell.badgeText = setting.badge 
			
			cell.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell
			
		case .info:

			let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as! InfoTableViewCell
			
			cell.title.text = setting.title
			cell.info.text = setting.info

			cell.title?.font = UIFont.systemFont(ofSize: 17)
			cell.info?.font = UIFont.systemFont(ofSize: 17)
			
			cell.contentView.alpha = setting.isExpanded ? 1.0 : 0.0
			
			return cell
			
		}
		
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		switch setting.type! {
			
		case .checkmark:
			
			setting.action!(self) {
				self.tableView.reloadData()
			}
			
		case .detailsAndDisclosure, .disclosure, .badge: setting.action!(self, nil)
			
		default: break
			
		}
		
		tableView.deselectRow(at: indexPath, animated: false)
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		return setting.isExpanded ? 42 : 0.0001
	}
	
	func onFinish() {
		pop(transitionFade: UIDevice.current.isIpad())		
	}
	
	override func onBack(_ sender: UIButton) {
		pop(transitionFade: UIDevice.current.isIpad())
	}
	
}
