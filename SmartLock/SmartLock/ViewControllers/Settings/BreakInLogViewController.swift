//
//  BreakInLogViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class BreakInLogCell: UITableViewCell {
	@IBOutlet weak var cellImageView: UIImageView!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var gpsCoordinate: UILabel!
	
	var coordinate:CLLocationCoordinate2D?
	
	var deleteAction: (() -> ())?
	
	@IBAction func onDelete(_ sender: UIButton) {
		
		if let action = deleteAction {
			
			action()
			
		}
		
	}
	
	@IBAction func onMap(_ sender: UIButton) {
		
		guard coordinate != nil else { return }
		
		let region = MKCoordinateRegionMake(coordinate!, MKCoordinateSpanMake(0.01, 0.02))
		let placemark = MKPlacemark(coordinate: coordinate!, addressDictionary: nil)
		let mapItem = MKMapItem(placemark: placemark)
		let options = [
			MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
			MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
		mapItem.name = ""
		mapItem.openInMaps(launchOptions: options)
		
	}
	
}

class BreakInLogViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet weak var tableView: UITableView!
	
	var logs = [MBreakIn]()
		
	func onClear(sender: UIBarButtonItem) {
		
		logs.removeAll()
		
		tableView.reloadData()
		
		FileService.sharedInstance.clearBreakInFolder()
		
		LockService.sharedService.deleteAllBreakInAttempt()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		logs = MBreakIn.mr_findAllSorted(by: "mobTime", ascending: false) as! [MBreakIn]
		
		tableView.reloadData()
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = ""
		navigationBarView.setupRightButton(image: nil, title: "Clear All", target: self, action: #selector(onClear(sender:)))
	}
	
	//  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
	//    return .portrait
	//  }
	
	//MARK: - UITableViewDelegate, UITableViewDataSource
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return logs.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let breakIn = logs[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "BreakInLogCell") as! BreakInLogCell
		cell.cellImageView.image = nil
		cell.dateLabel.text = ""
		cell.gpsCoordinate.text = ""
		cell.deleteAction = {
			
			LockService.sharedService.deleteBreakInAttempt(attemp: breakIn)
			
			if let imageFileName = breakIn.mobImageFileName {
				let photoPath = FileService.sharedInstance.breakInDirectoryURL.appendingPathComponent(imageFileName).path
				FileManager.default.delete(paths: [photoPath])
			}
			
			self.logs.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)
			tableView.reloadData()
			
		}
		
		if let imageFileName = breakIn.mobImageFileName,
			let image = UIImage(contentsOfFile: FileService.sharedInstance.breakInDirectoryURL.appendingPathComponent(imageFileName).path) {
			cell.cellImageView.image = image
		}
		
		if let time = breakIn.mobTime as? Date {
			cell.dateLabel.text = time.toString()
		}
		
		if breakIn.mobLatitude != 0 || breakIn.mobLongitude != 0 {
			
			cell.coordinate = CLLocationCoordinate2DMake(breakIn.mobLatitude, breakIn.mobLongitude)
			
			cell.gpsCoordinate.text = "\(breakIn.mobLatitude):\(breakIn.mobLongitude)"
			
		}
		
		return cell
	}
	
	override func onBack(_ sender: UIButton) {
		pop(transitionFade: UIDevice.current.isIpad())
	}
	
}
