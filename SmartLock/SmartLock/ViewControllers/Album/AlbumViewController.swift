//
//  AlbumViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/23/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import YandexMobileMetrica

class CreateAlbumSettingsViewControllerDelegate: AlbumSettingsViewControllerDelegate {
	
	var album: MAlbum?
	
	var passcodeType: LockType = .undefined
	var passcode: String = ""
	var passcodeChanged = false
	
	func configureNavigationBar(navigationBarView: NavigationBarView, target: UIViewController) {
		navigationBarView.setupLeftButton(image: nil, title: "Cancel", target: target, action: #selector(AlbumViewController.onCancel))
		navigationBarView.setupRightButton(image: nil, title: "Create", target: target, action: #selector(AlbumViewController.onCreate))
	}
	
	func saveAlbum(file: File, compleation: @escaping () -> ()) {
		
		guard let album = album else { return }
		
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in
			
			if let localAlbum = album.mr_(in: localContext) {
								
				localAlbum.mobThumbnail = "\(UUID().uuidString).jpg"
				localAlbum.mobDate = Date().timeIntervalSince1970
				localAlbum.mobName = file.name
				localAlbum.mobPath = file.relativePath
				localAlbum.mobAlbumPath = file.relativePath.deletingLastPathComponent()
					
				localAlbum.mobPasscode = self.passcode
				localAlbum.mobLockType = Int16(self.passcodeType.hashValue)
				
				_ = MLog.createDirectory(with: file.relativePath, context: localContext)
				_ = MLog.addAlbumPasscode(path: file.relativePath, lockType: self.passcodeType, passcode: self.passcode, context: localContext)
				
			}
			
		}, completion: { (success, error) in
			compleation()
		})
		
		if passcodeType != .undefined {
			YMMYandexMetrica.reportEvent("Album Pasccode", parameters: ["event": "\(passcodeType.metricaName)"], onFailure: nil)
		}
		
	}
	
	var title: String {
		return "Create Album"
	}
	
	init() {
		
		album = MAlbum.mr_createEntity()
		
		album?.mobDate = Date().timeIntervalSince1970
		
	}
	
	var lockType: LockType {
		return passcodeType//LockType(rawValue: Int(album!.mobLockType))!
	}
	
	var albumPasscodeEnabled: Bool {
		return lockType != .undefined || isExpanded
	}
	
	var isNumpad4Enabled: Bool {
		return lockType == .numpad4
	}
	
	var isNumpad6Enabled: Bool {
		return lockType == .numpad6
	}
	
	var isDotLockEnabled: Bool {
		return lockType == .dotlock
	}
	
	var isAplhanumericEnabled: Bool {
		return lockType == .aplhanum
	}
	
	var isExpanded: Bool = false
	
	var settings: Settings {
		
		return Settings(headers: [""], cells: [[Setting(title: "Album Passcode", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: albumPasscodeEnabled, action: { (value, compleation: (() -> ())?) in
			
			self.isExpanded = value as! Bool
			
			if (value as! Bool) == false {
				
				self.passcodeType = LockType.undefined
				self.passcode = ""
				self.passcodeChanged = false
				
			}
			
			compleation!()
			
		}), Setting(title: "  4-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isNumpad4Enabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad4, presenter: presenterController, showAnimated: true, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.numpad4
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  6-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isNumpad6Enabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad6, presenter: presenterController, showAnimated: true, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.numpad6
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  DotLock Pattern", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isDotLockEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .dotlock, presenter: presenterController, showAnimated: true, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.dotlock
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  Alphanumeric", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isAplhanumericEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .aplhanum, presenter: presenterController, showAnimated: true, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.aplhanum
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		})
			]])
		
	}
	
}

class EditAlbumSettingsViewControllerDelegate: AlbumSettingsViewControllerDelegate {
	
	var album: MAlbum?
	var isExpanded: Bool = false
	
	func configureNavigationBar(navigationBarView: NavigationBarView, target: UIViewController) {
		navigationBarView.setupLeftButton(image: nil, title: "Cancel", target: target, action: #selector(AlbumViewController.onCancel))
		navigationBarView.setupRightButton(image: nil, title: "OK", target: target, action: #selector(AlbumViewController.onSave))
	}		
	
	func saveAlbum(file: File, compleation: @escaping () -> ()) {
		
		guard let album = album else { return }
		
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in

			if let localAlbum = album.mr_(in: localContext) {
				
				if self.passcodeChanged == true {
					
					if self.passcodeType == LockType.undefined {
						
						_ = MLog.deleteAlbumPasscode(path: file.relativePath, uuid: localAlbum.mobUUID, context: localContext)
						
					} else {
						
						_ = MLog.addAlbumPasscode(path: file.relativePath, lockType: self.passcodeType, passcode: self.passcode, context: localContext)
						
					}
					
				}
				
				localAlbum.mobName = file.name
				localAlbum.mobPath = file.relativePath
				localAlbum.mobAlbumPath = file.relativePath.deletingLastPathComponent()
				localAlbum.mobPasscode = self.passcode
				localAlbum.mobLockType = Int16(self.passcodeType.hashValue)
				
			}
			
		}, completion: { (success, error) in
			compleation()
		})
		
		
		if passcodeType != .undefined {
			YMMYandexMetrica.reportEvent("Album Pasccode", parameters: ["event": "\(passcodeType.metricaName)"], onFailure: nil)
		}
		
	}
	
	var passcodeType: LockType = .undefined
	
	var passcode: String = ""
	
	var passcodeChanged = false
	
	var name: String = ""
	
	var file:File?
	
	init(file: File) {
		self.file = file
		
		album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: file.relativePath)
		
		passcodeType = LockType(rawValue: Int((album?.mobLockType)!))!
		passcode = album?.mobPasscode ?? ""
	}
	
	init(album: MAlbum) {
		self.album = album
	}
	
	var title: String {
		return "Edit Album"
	}
	
	var lockType: LockType {
		return passcodeType
	}
	
	var albumPasscodeEnabled: Bool {
		return lockType != .undefined || isExpanded
	}
	
	var isNumpad4Enabled: Bool {
		return lockType == .numpad4
	}
	
	var isNumpad6Enabled: Bool {
		return lockType == .numpad6
	}
	
	var isDotLockEnabled: Bool {
		return lockType == .dotlock
	}
	
	var isAplhanumericEnabled: Bool {
		return lockType == .aplhanum
	}
	
	var items: Int {
		if let file = file {
			return FileService.fetchAlbumContent(relativePath: file.relativePath).count
		}
		
		return 0
	}
	
	var created: String {
		
		if let album = album {
			let dateFormatter = DateFormatter()
			dateFormatter.timeStyle = .short
			dateFormatter.dateStyle = .medium
			dateFormatter.timeZone = TimeZone.current
			return dateFormatter.string(from: Date(timeIntervalSince1970: album.mobDate))
		}
		
		return ""
		
	}
	
	var settings: Settings {
		
		return Settings(headers: ["", ""], cells: [[Setting(title: "Album Passcode", info: nil, badge: "", icon: nil, type: .switcher, isExpanded: true, value: albumPasscodeEnabled, action: { (value, compleation: (() -> ())?) in
			
			self.isExpanded = (value as! Bool)
			
			if (value as! Bool) == false {
				
				if self.album?.mobLockType != Int16(LockType.undefined.rawValue) {
					
					self.passcodeChanged = true
					
				}
				
				self.passcodeType = LockType.undefined
				
			}
			
			compleation!()
			
		}), Setting(title: "  4-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isNumpad4Enabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad4, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.numpad4
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  6-Digits Passcode", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isNumpad6Enabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .numpad6, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.numpad6
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  DotLock Pattern", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isDotLockEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .dotlock, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.dotlock
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		}), Setting(title: "  Alphanumeric", info: nil, badge: "", icon: nil, type: .checkmark, isExpanded: albumPasscodeEnabled, value: isAplhanumericEnabled, action: { (value, compleation: (() -> ())?) in
			
			let presenterController: UIViewController = value! as! UIViewController
			
			LockService.sharedService.setLockScreen(lockType: .aplhanum, presenter: presenterController, alpha: UIDevice.current.isIpad() ? 0.0 : 0.9, remove: true, completion: { passcode in
				
				self.passcodeType = LockType.aplhanum
				self.passcode = passcode
				self.passcodeChanged = true
				
				compleation!()
				
			})
			
		})
			
			], [Setting(title: "Items stored:", info: "\(items)", badge: "", icon: nil, type: .info, isExpanded: true, value: albumPasscodeEnabled, action: { (value, compleation: (() -> ())?) in
				
				self.isExpanded = (value as! Bool)
				
				if (value as! Bool) == false {
					
					if self.album?.mobLockType != Int16(LockType.undefined.rawValue) {
						
						self.passcodeChanged = true
						
					}
					
					self.passcodeType = LockType.undefined
					
				}
				
				compleation!()
				
			}), Setting(title: "Created time:", info: created, badge: "", icon: nil, type: .info, isExpanded: true, value: albumPasscodeEnabled, action: { (value, compleation: (() -> ())?) in
				
				self.isExpanded = (value as! Bool)
				
				if (value as! Bool) == false {
					
					if self.album?.mobLockType != Int16(LockType.undefined.rawValue) {
						
						self.passcodeChanged = true
						
					}
					
					self.passcodeType = LockType.undefined
					
				}
				
				compleation!()
				
			})
				
			]

			])
		
	}
	
}

protocol AlbumSettingsViewControllerDelegate: SettingsViewControllerDelegate {
	
	var album: MAlbum? { get }
	var passcodeChanged: Bool { get set }
	func saveAlbum(file: File, compleation: @escaping () -> ())
	
}

enum AlbumViewControllerMode {
	case create
	case edit
}

class AlbumViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
	
	@IBOutlet weak var albumNameTextField: UITextField!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var albumNameButton: UIButton!
	@IBOutlet weak var headerHeightEditAlbum: NSLayoutConstraint!
	@IBOutlet weak var headerHeightCreateAlbum: NSLayoutConstraint!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var logoImageView: UIImageView!
	
	var directoryWasCreated: (() -> ())?
	var cancelAction: (() -> ())?
	
	var mode: AlbumViewControllerMode?
	var file: File?
	var passcodeChanged = false
	
	var delegate: AlbumSettingsViewControllerDelegate!
	
	static func createAlbum(delegate: CreateAlbumSettingsViewControllerDelegate) -> AlbumViewController {
		
		let controller = StoryboardScene.instantiateAlbumSettingsScene()
		controller.mode = .create
		controller.delegate = delegate
		return controller
		
	}
	
	static func editAlbum(delegate: EditAlbumSettingsViewControllerDelegate) -> AlbumViewController {
				
		let controller = StoryboardScene.instantiateAlbumSettingsScene()
		controller.mode = .edit
		controller.file = delegate.file
		controller.delegate = delegate
		return controller
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		BackupService.sharedInstance.suspendBackup()
		
		assert(headerHeightEditAlbum != nil, "Must be set")
		assert(headerHeightCreateAlbum != nil, "Must be set")
		
		if let file = file, let firstObject = FileService.fetchAlbumFiles(relativePath: file.relativePath).first {
			let relativePath = firstObject.value(forKey: "mobPath") as? String ?? ""
			let firstFile = File(relativePath: relativePath)
			imageView.image = firstFile.getFileThumbnailSourceImage()
			logoImageView.image = nil
		} else {
			if let navigationBarView = navigationBarView {
//				navigationBarView.backgroundImageView.isHidden = true
			}
		}
		
		switch mode! {
		case .create:
			if let navigationBarView = navigationBarView {
//				navigationBarView.backgroundImageView.isHidden = true
			}
			albumNameTextField.text = FileService.sharedInstance.getNewDirectoryName()
		case .edit:
			albumNameTextField.text = file?.name
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = delegate.title
		if let delegate = delegate {
			delegate.configureNavigationBar(navigationBarView: navigationBarView, target: self)
		}
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		if LockService.sharedService.isDecoyEnabled {
			return 0
		}
		
		return delegate.settings.headers.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if LockService.sharedService.isDecoyEnabled {
			return 0
		}
		
		return delegate.settings.cells[section].count
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return delegate.settings.headers[section]
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		switch setting.type! {
			
		case .checkmark:
			
			var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
			if cell == nil {
				cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
			}
			
			cell?.textLabel?.text = setting.title
			cell?.accessoryType = (setting.value as! Bool) == true ? .checkmark : .none
			
			return cell!
			
		case .switcher:
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell") as! SwitchTableViewCell
			
			cell.textLabel?.text = setting.title
			cell.switcher.isOn = (setting.value as! Bool)
			cell.switchedAction = { (onStatus) in
				setting.action!(onStatus) {
					self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
				}
			}
			return cell
			
		case .info:
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as! InfoTableViewCell
			
			cell.title.text = setting.title
			cell.info.text = setting.info
			
			return cell
			
		default: return UITableViewCell()
			
		}
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		return setting.isExpanded ? 42 : 0
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let setting = delegate.settings.cells[indexPath.section][indexPath.row]
		
		switch setting.type! {
			
		case .checkmark:
			
			setting.action!(self) {
				self.tableView.reloadData()
			}
			
		case .detailsAndDisclosure, .disclosure: setting.action!(self, nil)
			
		default: break
			
		}
		
		tableView.deselectRow(at: indexPath, animated: false)
		
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		textField.text = ""
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		
		switch textField {
		case albumNameTextField:
			if let newName = textField.text {
				albumNameTextField.text = FileService.sharedInstance.getNewDirectoryName(name: newName)
			}
			albumNameTextField.isEnabled = false
		default: break
		}
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		if textField.text == "" {
			
			textField.text = file?.name
			
		}
		
		textField.resignFirstResponder()
		return true
	}
	
	func onCreate() {
		
		albumNameTextField.resignFirstResponder()
		
		guard let file = FileService.sharedInstance.createDirectory(name: albumNameTextField.text!) else { return }
		
		delegate.saveAlbum(file: file, compleation: {
		
//			BackupService.sharedInstance.resumeBackup()
			
			self.pop(transitionFade: UIDevice.current.isIpad())
			
			if let action = self.directoryWasCreated {
				action()
			}
		
		})
		
	}
	
	func onSave() {
		
		albumNameTextField.resignFirstResponder()
		
		guard let file = file, file.isBlocked == false else { return }
		
		if albumNameTextField.text != file.name {
			
			file.rename(name: albumNameTextField.text!)
			
			NSManagedObjectContext.mr_default().mr_save(blockAndWait: { (localContext) in

				if let album = self.delegate.album?.mr_(in: localContext) {
					album.rename(newPath: file.relativePath, context: localContext)
				}
				
			})
			
		}
		
		delegate.saveAlbum(file: file, compleation: {
		
			BackupService.sharedInstance.resumeBackup()
			
			self.pop(transitionFade: UIDevice.current.isIpad())
			
			if let action = self.directoryWasCreated {
				action()
			}
		
		})
		
	}
	
	func onCancel() {
		
		if delegate is CreateAlbumSettingsViewControllerDelegate {
			BackupService.sharedInstance.resumeBackup()
		}
		
		pop(transitionFade: UIDevice.current.isIpad())
		
		if let action = cancelAction {
			action()
		}
		
	}
	
	@IBAction func onNameEdit(_ sender: UIButton) {
		
		albumNameTextField.isEnabled = true
		albumNameTextField.becomeFirstResponder()
		
	}
	
}
