//
//  WebViewController.swift
//  SmartLock
//
//  Created by Bogachev on 6/19/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController, WKNavigationDelegate {

	@IBOutlet weak var contentView: UIView!
	
	private var webView: WKWebView!{
		didSet {
			webView.navigationDelegate = self
		}
	}
	
	private var url: URL!
	private var titleText: String!
	
	static func instantiate(url: URL, titleText: String) -> WebViewController {
		let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
		
		controller.url = url
		controller.titleText = titleText
		
		return controller
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		webView = WKWebView(frame:contentView.frame)
		contentView.addSubview(webView)
		constrainView(view: webView, toView: contentView)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		let request = URLRequest(url: url)
		webView.load(request)
		
		LoaderView.show(in: self.view)
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = titleText
	}
	
	func constrainView(view:UIView, toView contentView:UIView) {
		view.translatesAutoresizingMaskIntoConstraints = false
		view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
		view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
		view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
		view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
	}

	//MARK: - WKNavigationDelegate
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		LoaderView.close()
	}
	
	func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
		LoaderView.close()
	}
}
