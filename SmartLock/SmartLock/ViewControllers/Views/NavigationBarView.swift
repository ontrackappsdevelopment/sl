//
//  NavigationBarView.swift
//  SmartLock
//
//  Created by Bogachev on 8/12/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

enum NavigationBarViewStyle {
	case colored
	case light
}

protocol NavigationBarViewDelegate {
	func onLeft(view: NavigationBarView, button: UIButton)
	func onRight(view: NavigationBarView, button: UIButton)
	var canShowBackupStatus: Bool { get }
	func search(by: String)
}

class NavigationBarView: UIView, UITextFieldDelegate {
	
	@IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
	@IBOutlet weak var titleRightConstraint: NSLayoutConstraint!
	@IBOutlet weak var backupIcon: UIImageView! {
		didSet {
			backupIcon.alpha = 0
		}
	}
	@IBOutlet weak var searchTextField: UITextField!{
		didSet {
			searchTextField.delegate = self
			searchTextField.isHidden = true
		}
	}
	@IBOutlet weak var topBarLabel: UILabel!
	@IBOutlet weak var leftTopBarButton: UIButton! {
		didSet {
			setupBackButton()
			leftTopBarButton.addTarget(self, action: #selector(onLeft(button:)), for: .touchUpInside)
		}
	}
	@IBOutlet weak var rightTopBarButton: UIButton! {
		didSet {
			setupNoButton(button: rightTopBarButton)
			rightTopBarButton.addTarget(self, action: #selector(onRight(button:)), for: .touchUpInside)
		}
	}
	
	var delegate: NavigationBarViewDelegate?
	var timer: Timer?

	static func instantiateFromXib(style: NavigationBarViewStyle) -> NavigationBarView? {
		
		let nibName = "NavigationBarColoredView"
		
		if let view = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)?.first as? NavigationBarView {
			return view
		}
		
		return nil
	}
	
	static func instantiateAndAddToView(parentView: UIView, style: NavigationBarViewStyle) -> NavigationBarView?  {
		if let view = instantiateFromXib(style: style) {
			view.frame = parentView.bounds
			view.autoresizingMask = [.flexibleWidth]
			view.translatesAutoresizingMaskIntoConstraints = true
			parentView.addSubview(view)
			return view
		}
		return nil
	}
	
	func showBackupActiveStatus() {
		guard let delegate = delegate, delegate.canShowBackupStatus else { return }
		
		if timer == nil {
			timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animateCloudIcon), userInfo: nil, repeats: true)
		}
	}
	
	func hideBackupActiveStatus() {
		if timer != nil {
			timer?.invalidate()
			timer = nil
		}
		
		UIView.animate(withDuration: 0.1) {
			self.backupIcon.alpha = 0.0
		}
	}
	
	func animateCloudIcon() {
		UIView.animate(withDuration: 0.1) {
			self.backupIcon.alpha = self.backupIcon.alpha == 1 ? 0 : 1
		}
	}
	
	@objc private func onLeft(button: UIButton) {
		if let delegate = delegate {
			delegate.onLeft(view: self, button: button)
		}
	}
	
	@objc private func onRight(button: UIButton) {
		if let delegate = delegate {
			delegate.onRight(view: self, button: button)
		}
	}
	
	func setupLeftButton(image: UIImage?, title: String?, target: Any?, action: Selector?) {
		setupButton(button: leftTopBarButton, image: image, title: title, target: target, action: action)
	}
	
	func setupRightButton(image: UIImage?, title: String?, target: Any?, action: Selector?) {
		setupButton(button: rightTopBarButton, image: image, title: title, target: target, action: action)
	}

	func setupButton(button: UIButton, image: UIImage?, title: String?, enabled: Bool = true, target: Any?, action: Selector?) {
		if let image = image?.withRenderingMode(.alwaysTemplate) {
			button.setImage(image, for: .normal)
			button.tintColor = UIColor.black
		}
		button.setTitle(title, for: .normal)
		button.isEnabled = enabled
		button.removeTarget(nil, action: nil, for: .touchUpInside)
		if let target = target, let action = action {
			button.addTarget(target, action: action, for: .touchUpInside)
		}
	}
	
	func setupEditButton(button: UIButton) {
		if let image = UIImage(named: "ico_edit")?.withRenderingMode(.alwaysTemplate) {
			button.setImage(image, for: .normal)
			button.tintColor = UIColor.black
		}
		button.setTitle(nil, for: .normal)
		button.isEnabled = true
	}
	
	func setupSettingsButton(button: UIButton) {
		if let image = UIImage(named: "ico_settings")?.withRenderingMode(.alwaysTemplate) {
			button.setImage(image, for: .normal)
			button.tintColor = UIColor.black
		}
		button.setTitle(nil, for: .normal)
		button.isEnabled = true
	}
	
	func setupSearchButton(button: UIButton) {
		if let image = UIImage(named: "ico_search")?.withRenderingMode(.alwaysTemplate) {
			button.setImage(image, for: .normal)
			button.tintColor = UIColor.black
		}
		
		if let image = UIImage(named: "ico_close")?.withRenderingMode(.alwaysTemplate) {
			button.setImage(image, for: .selected)
			button.tintColor = UIColor.black
		}
		
		button.setTitle(nil, for: .normal)
		button.isEnabled = true
		button.addTarget(self, action: #selector(onSearch(_:)), for: .touchUpInside)
	}
	
	func setupBackButton() {
		if let image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate) {
			leftTopBarButton.setImage(image, for: .normal)
			leftTopBarButton.tintColor = UIColor.black
		}
		leftTopBarButton.setTitle(nil, for: .normal)
		leftTopBarButton.isEnabled = true
	}

	func setupNoButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle(nil, for: .normal)
		button.isEnabled = false
	}

	func setupSelectAllButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Select All", for: .normal)
		button.isEnabled = true
	}
	
	func setupDeselectAllButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Deselect All", for: .normal)
		button.isEnabled = true
	}

	func setupCancelButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Cancel", for: .normal)
		button.isEnabled = true
	}
	
	func setupDownloadButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Download", for: .normal)
		button.isEnabled = true
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let textFieldText: NSString = (textField.text ?? "") as NSString
		let textAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
		delegate?.search(by: textAfterUpdate)		
		return true
	}
	
	@IBAction func onSearch(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
		let angle: CGFloat = sender.isSelected ? -CGFloat(Double.pi * 1/3) : CGFloat(Double.pi * 1/3)
		sender.transform = CGAffineTransform(rotationAngle: angle)
		UIView.animate(withDuration: 0.3) {
			sender.transform = CGAffineTransform.identity
			self.titleLeftConstraint.constant = sender.isSelected ? 0 : 85
			self.titleRightConstraint.constant = sender.isSelected ? 0 : 85
			self.topBarLabel.isHidden = sender.isSelected
			self.searchTextField.isHidden = !sender.isSelected
		}
		
		if sender.isSelected {
			self.searchTextField.becomeFirstResponder()
		} else {
			self.searchTextField.text = ""
			self.searchTextField.resignFirstResponder()
			delegate?.search(by: "")
		}
	}
}
