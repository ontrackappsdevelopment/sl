//
//  VideoCollectionViewCell.swift
//  CameraTest
//
//  Created by Bogachev on 1/29/17.
//  Copyright © 2017 SBogachev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class VideoCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var selectedView: UIView!
	@IBOutlet weak var videoIconImageView: UIImageView!
	@IBOutlet weak var draggedView: UIView!
	@IBOutlet weak var progressContainerView: UIView!
	private var progressIndicatorView: NVActivityIndicatorView?
	
	var path: String?
	
	var isDragged: Bool = false {
		didSet {
			draggedView.isHidden = !isDragged
		}
	}
	
	var isBlocked: Bool = false {
		didSet {
			if isBlocked == true {
				
				if progressIndicatorView == nil {
					progressIndicatorView = NVActivityIndicatorView(frame: progressContainerView.bounds, type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white)
					progressContainerView.addSubview(progressIndicatorView!)
					progressIndicatorView?.startAnimating()
				}
				
			} else {
				
				if progressIndicatorView != nil {
					progressIndicatorView?.stopAnimating()
					progressIndicatorView?.removeFromSuperview()
					progressIndicatorView = nil
				}
				
			}
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override var isSelected: Bool {
		didSet {
			selectedView.isHidden = !isSelected
			videoIconImageView.isHidden = isSelected
		}
	}
	
}
