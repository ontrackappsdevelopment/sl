//
//  ConfirmCodeViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import YandexMobileMetrica

enum ConfirmCodeViewControllerMode {
	case register
	case change
	case restore
	case restoreAlbum
}

class ConfirmCodeViewController: BaseViewController {
	
	var mode: ConfirmCodeViewControllerMode?
	var email: String?
	var code: String?
	
	var previousNavigationBarState: Bool?
	
	@IBOutlet weak var codeTextField: UITextField!
	
	@IBOutlet weak var centerViewConstrant: NSLayoutConstraint!
	
	static func instantiate(mode: ConfirmCodeViewControllerMode, email: String) -> ConfirmCodeViewController {
		let controller = StoryboardScene.instantiateConfirmCodeScene()
		controller.mode = mode
		controller.email = email
		return controller
	}
	
	static func instantiateRecoveryPasscode() -> ConfirmCodeViewController {
		let controller = StoryboardScene.instantiatePasscodeRecoveryScene()
		controller.mode = .restore
		return controller
	}
	
	static func instantiateRecoveryAlbum() -> ConfirmCodeViewController {
		let controller = StoryboardScene.instantiatePasscodeRecoveryScene()
		controller.mode = .restoreAlbum
		return controller
	}
	
	static func register(email: String) -> ConfirmCodeViewController {
		return instantiate(mode: .register, email: email)
	}
	
	static func change(email: String) -> ConfirmCodeViewController {
		return instantiate(mode: .change, email: email)
	}
	
	static func restore() -> ConfirmCodeViewController {
		YMMYandexMetrica.reportEvent("Forgot passcode: Tap_on_recovery", onFailure: nil)
		
		return instantiateRecoveryPasscode()
	}
	
	static func restoreAlbum() -> ConfirmCodeViewController {
		YMMYandexMetrica.reportEvent("Forgot passcode album: Tap_on_recovery", onFailure: nil)
		
		return instantiateRecoveryAlbum()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		switch mode! {
		case .restore, .restoreAlbum:
			
			LoaderView.show(in: view)
			
			NetworkManager.Requests.changePassword(device_id: DataManager.device!.mobID!, compleation: { (restoreCode, success, errorMessage) in
				
				self.codeTextField.becomeFirstResponder()
				
				self.code = restoreCode
				
				LoaderView.close()
				
			})
			
			break
		default: break
		}
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(didShowKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		UIView.setAnimationsEnabled(false)
		
		switch mode! {
		case .restore:
			title = "Passcode Recovery"
		default:
			break
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		LoaderView.close()
	}
	
	@objc private func didShowKeyboard(notification: Notification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if let centerViewConstrant = centerViewConstrant {
				centerViewConstrant.constant = -(keyboardSize.height / 2)
			}
		}
		
		UIView.setAnimationsEnabled(true)
	}
	
	@objc private func willHideKeyboard(notification: Notification) {
		if let centerViewConstrant = centerViewConstrant {
			centerViewConstrant.constant = 0
		}
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = ""
	}
	
	func saveEmailAndPop() {
		DataManager.registerUser(email: self.email!)
		
		BackupService.sharedInstance.prepareToBackup()
		
		if let chatListViewController = ChatListViewController.chatListViewController {
			self.popToViewController(viewController: chatListViewController, transitionFade: UIDevice.current.isIpad())
		} else {
			if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
				appDelegate.pushMainViewControler()
			}
		}
	}
	
	@IBAction func onConfirm(_ sender: UIButton) {
		
		if codeTextField.text?.isEmpty == true {
			
			_ = ErrorView.shows(inViewController: self, text:"Confirmation code should not be empty.")
			
			return
		}
		
		
		switch mode! {
		case .register:
			
			LoaderView.show(in: view)
			
			NetworkManager.Requests.registerConfirm(deviceToken: codeTextField.text!, compleation: { (success, errorMessage) in
				
				LoaderView.close()
				
				if success == true {
					
					self.saveEmailAndPop()
					
					YMMYandexMetrica.reportEvent("is_set_email", onFailure: nil)
					
				} else {
					
					_ = ErrorView.shows(inViewController: self, text: errorMessage!)
					
				}
				
			})
			
		case .change:
			
			LoaderView.show(in: view)
						
			NetworkManager.Requests.changeEmailConfirm(deviceToken: codeTextField.text!, compleation: { (success, errorMessage) in
				
				LoaderView.close()
				
				if success == true {
					
					self.saveEmailAndPop()
					
				} else {
					
					_ = ErrorView.shows(inViewController: self, text: errorMessage!)
					
				}
				
			})
			
		case .restore:
			
			if codeTextField.text == code {
				
				let controller = UIStoryboard(name: "Lock", bundle: nil).instantiateViewController(withIdentifier: "ChoosePasscodeTypeViewController") as! ChoosePasscodeTypeViewController
				
				controller.mode = self.mode == .restore ? ChoosePasscodeTypeViewControllerMode.restore : ChoosePasscodeTypeViewControllerMode.restoreAlbum
				
				controller.pushFrom(viewController: self)
				
			} else {
				
				_ = ErrorView.shows(inViewController: self, text:"Confirmation code does not match.")
				
			}
			
		case .restoreAlbum:
			
			guard codeTextField.text == code else {
				_ = ErrorView.shows(inViewController: self, text:"Confirmation code does not match.")
				return
			}
			
			let baseViewController = navigationController?.viewControllers.reversed().first(where: { (controller) -> Bool in
				
				if controller is PhotosViewController { return true }
				
				return false
				
			}) as! PhotosViewController			
			
			self.popToViewController(viewController: baseViewController, transitionFade: UIDevice.current.isIpad())
			
			baseViewController.removeUnlockAlbum(animated: false)
			
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					if let lockAlbum = LockService.sharedService.lockAlbum?.mr_(in: localContext) {
						lockAlbum.mobLockType = Int16(LockType.undefined.hashValue)
						lockAlbum.mobPasscode = nil
					}
				})
								
				_ = AlertView.shows(inViewController: baseViewController, text: "Passcode has been removed. Set new passcode.", actionTitle: "SET NOW", action: {
					
					let controller = AlbumViewController.editAlbum(delegate: EditAlbumSettingsViewControllerDelegate(file: LockService.sharedService.lockAlbumFile!))
					
					controller.directoryWasCreated = {
						DispatchQueue.main.async {
							baseViewController.refreshDataAndReload()
							LockService.sharedService.lockAlbum = nil
							LockService.sharedService.lockAlbumFile = nil
						}
					}
					
					controller.cancelAction = {
						DispatchQueue.main.async {
							baseViewController.refreshDataAndReload()
							LockService.sharedService.lockAlbum = nil
							LockService.sharedService.lockAlbumFile = nil
						}
					}
					
					controller.pushFrom(viewController: baseViewController)
					
				}, cancel: {
					DispatchQueue.main.async {
						LockService.sharedService.lockAlbum = nil
						LockService.sharedService.lockAlbumFile = nil
					}
				})
				
				baseViewController.refreshDataAndReload()
				
			})
			
		}
		
	}
	
	@IBAction func onResend(_ sender: UIButton) {
		
		LoaderView.show(in: view)
		
		switch mode! {
		case .change:
			NetworkManager.Requests.changeEmail(email: email!, device_id: DataManager.device!.mobID!) { (success, message) in
				
				LoaderView.close()
				
				if success == false {
					
					DispatchQueue.main.async {
						
						_ = ErrorView.shows(inViewController: self, text: message!)
						
					}
					
				}
				
			}
		case .register:
			NetworkManager.Requests.register(email: email!, device_id: DataManager.device!.mobID!) { (success, message) in
				
				LoaderView.close()
				
				if success == false {
					
					DispatchQueue.main.async {
						
						_ = ErrorView.shows(inViewController: self, text: message!)
						
					}
					
				}
				
			}
		case .restore, .restoreAlbum:
			
			LoaderView.show(in: view)
			
			NetworkManager.Requests.changePassword(device_id: DataManager.device!.mobID!, compleation: { (restoreCode, success, errorMessage) in
				
				self.code = restoreCode
				
				LoaderView.close()
				
			})
		}
		
	}
	
}
