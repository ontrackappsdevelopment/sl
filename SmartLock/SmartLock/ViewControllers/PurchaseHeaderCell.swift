//
//  PurchaseHeaderCell.swift
//  SmartLock
//
//  Created by Bogachev on 5/31/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

protocol PurchaseHeaderCellDelegate {
	var message: String? { get }
}

class PurchaseHeaderCell: UITableViewCell {

	@IBOutlet weak var headerLabel: UILabel!
	
	var delegate: PurchaseHeaderCellDelegate?
	
	func configurate(with delegate: PurchaseHeaderCellDelegate) {
		self.delegate = delegate
		
		headerLabel.text = delegate.message
	}
}
