//
//  FolderTableViewCell.swift
//  SmartLock
//
//  Created by Bogachev on 3/2/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class FolderTableViewCell: UITableViewCell {
  
  @IBOutlet weak var folderName: UILabel!
  @IBOutlet weak var folderIcon: UIImageView!
  @IBOutlet weak var leftPosition: NSLayoutConstraint!
  
}
