//
//  PhotosViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import MessageUI
import MobileCoreServices
import Photos
import DragDropiOS
import AudioToolbox
import YandexMobileMetrica

enum CellIdentifier: String {
	case photo = "PhotoCell"
	case video = "VideoCell"
	case folder = "FolderCell"
}

enum ViewMode {
	case view
	case edit
}

class PhotosViewController: BaseViewController,
	UINavigationControllerDelegate,
	UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, DragDropCollectionViewDelegate,
	UIDocumentPickerDelegate, MFMailComposeViewControllerDelegate, UIScrollViewDelegate, CollectionPushAndPoppable {
	
	static var currentPhotosViewController: PhotosViewController!
	static var filesToUpload = [File]()
	
	@IBOutlet weak var collectionView: DragDropCollectionView!
	@IBOutlet weak var addMenuButton: UIButton!
	@IBOutlet weak var toolBarView: UIView!
	@IBOutlet weak var toolBarViewHeight: NSLayoutConstraint!
	
	private var updateTitleTimer: Timer?
	private var backupStatusText: String?
	
	var addMenuSheets: CustomizableActionSheet?
	var exportMenuSheets: CustomizableActionSheet?
	var cloudMenuSheets: CustomizableActionSheet?
	var sourceCell: UICollectionViewCell?
	var progressView: ProgressView?
	var loadImageAsync: Bool = false
	var needScrollToEnd: Bool = false
	
	var context: NSManagedObjectContext?
	lazy var cellSize = CGSize(width: (UIScreen.main.bounds.width - (PhotosViewController.columnCount - 1)) / PhotosViewController.columnCount, height: (UIScreen.main.bounds.width - (PhotosViewController.columnCount - 1)) / PhotosViewController.columnCount)
	var needUpdate = true
	
	var mode: ViewMode = .view
	lazy var directory = FileService.sharedInstance.currentDirectory!
	
	var dragDropManager: DragDropManager!
	var isDragged: Bool = false
	var draggingPath: IndexPath?
	var showSwipeHint = false
	var showDragHint = false
	
	var needSyncData: Bool = false{
		didSet {
			if isVisible == true && needSyncData == true {
				refreshDataAndReload()
			}
		}
	}
	
	var folderPreviewSize = CGSize.zero
	
	var slideShowTransition: SlideShowTransition {
		return SlideShowTransition(rawValue: Int(LockService.sharedService.user!.mobSlideShowTransition))!
	}
	
	override var prefersStatusBarHidden: Bool {
		return false
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override var canShowBackupStatus: Bool {
		return true
	}
	
	//MARK: - View Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setNeedsStatusBarAppearanceUpdate()
		
		dragDropManager = DragDropManager(canvas: self.collectionView, views: [collectionView])
		context = NSManagedObjectContext.mr_newMainQueue()
		context?.persistentStoreCoordinator = NSPersistentStoreCoordinator.mr_default()
		toolBarViewHeight.constant = 0
		collectionView.allowsSelection = true
		collectionView.allowsMultipleSelection = true
		collectionView.dragDropDelegate = self
		registeCollectionViewNib()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		updateTitle()
		
		if BackupService.sharedInstance.backupStatus == nil {
			hideBackupStatus()
		}
		
		if LockService.sharedService.isDecoyEnabled == false {
		
			if UserService.sharedService.user.appUsingType == .freeTrial {
				_ = User.freeTrialDayLeft
			}
			
			if let status = BackupService.sharedInstance.backupStatus {
				showBackupStatus(text: status)
			}
			
		}
		
		PhotosViewController.currentPhotosViewController = self
		
		FileService.sharedInstance.currentDirectory = directory
		
		navigationController?.delegate = self
				
//		if (needUpdate == true && (sourceCell == nil)) || needSyncData == true {
//			
//			needSyncData = false
		
		if needUpdate == true {
		
			self.refreshDataAndReload()
			
		}
		
			if needScrollToEnd == true {
				self.scrollToEnd()
			}
			
//		}
		
		needScrollToEnd = false		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
				
//		hideBackupStatus()
		
		if LockService.sharedService.firstLaunch {
			LockService.sharedService.firstLaunchCompleted()
		}
		
		showEmailAlert()
		
		sourceCell = nil
		
		if showSwipeHint && wasShownSwipeHint == false {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
				guard AppDelegate.appIsLocked == false else { return }
				guard self.isVisible == true else { return }
				
				self.wasShownSwipeHint = true
				_ = HintView.shows(inViewController: self, image: #imageLiteral(resourceName: "ic_hint_swipe"), text: "Hot key hint!\nSwipe down to exit photo viewer")
			}
		}
		
		showReview()

	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		needUpdate = true
		
//		DispatchQueue.global().async {
//			self.directory.removefolderThambnail()
//		}
	}
	
	override func didEnterBackground(notification: NSNotification) {
		super.didEnterBackground(notification: notification)
		
		collectionView.dataSource = nil
		collectionView.delegate = nil
	}
	
	override func didBecomeActive(notification: NSNotification) {
		super.didBecomeActive(notification: notification)
		
		guard AppDelegate.appIsLocked == false else { return }
		guard isVisible == true else { return }
		
		collectionView.dataSource = self
		collectionView.delegate = self
		
		showEmailAlert()
		
		if let status = BackupService.sharedInstance.backupStatus {
			showBackupStatus(text: status)
		}

		showReview()
	}
	
	override func didUnlock(notification: Notification) {
		super.didUnlock(notification: notification)
		self.refreshDataAndReload()
		
		collectionView.dataSource = self
		collectionView.delegate = self
		
		showEmailAlert()
		
		if let status = BackupService.sharedInstance.backupStatus {
			showBackupStatus(text: status)
		}
		
		showReview()
	}
	
	//MARK: - NavigationBarView
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.font = UIFont.titleBold
		
		hideBackupStatus()
		
		if LockService.sharedService.isDecoyEnabled == true {
			
			if directory.url == FileService.sharedInstance.rootMediaDirectory?.url {
			
				if let leftTopBarButton = leftTopBarButton {
					navigationBarView.setupNoButton(button: leftTopBarButton)
				}
				
				if let rightTopBarButton = rightTopBarButton {
					navigationBarView.setupNoButton(button: rightTopBarButton)
				}
				
			} else {

				if let rightTopBarButton = rightTopBarButton {
					navigationBarView.setupNoButton(button: rightTopBarButton)
				}
				
			}
			
		} else {

			if directory.url == FileService.sharedInstance.rootMediaDirectory?.url {
				viewModeBarItems()
			} else {
				viewSubFolderModeBarItems()
			}
			
		}
		
	}
	
	override func onLeft(view: NavigationBarView, button: UIButton) {
		
		switch mode {
		case .view:
			
			if let navigationController = navigationController {
				if navigationController.viewControllers.count > 1 {
					onBack(button)
				} else {
					onSettings()
				}
			}
		case .edit:

			onFinishEdit()
			
		}
	}
	
	override func onBack(_ sender: UIButton) {
		guard let navigationController = navigationController else { return }
		
		if directory.url.path != FileService.sharedInstance.mainDirectoryURL.path {
			
			if let album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: directory.relativePath) {
				if let thumbnail = album.mobThumbnail {
					
					DispatchQueue.global().async {
						_ = self.directory.getAlbumThumbnailImage(name: thumbnail, size: CGSize.zero, update: true)
						
						DispatchQueue.main.async {
							
							if let photosViewController = navigationController.viewControllers.last as? PhotosViewController {
								
								photosViewController.reloadCollectionView()
								
							}
							
						}
						
					}
					
				}
				
			}
			
		}
		
		super.onBack(sender)
	}
	
	override func onRight(view: NavigationBarView, button: UIButton) {
		
		switch mode {
		case .view: onEdit()
		case .edit:
			if let navigationBarView = navigationBarView {
				
				if let indexPaths = collectionView.indexPathsForSelectedItems, indexPaths.count > 0 {
					
					navigationBarView.setupSelectAllButton(button: navigationBarView.rightTopBarButton)
					onUnselectAll()
					
				} else {
					
					navigationBarView.setupDeselectAllButton(button: navigationBarView.rightTopBarButton)
					onSelectAll()
					
				}
				
			}
			
		}
	}
	
	// MARK: - Internal
	
	func showAddMenu() {
		
//		if LockService.sharedService.isDecoyEnabled == true {
//			addMenuSheets = AddMenuView.showDecoyAddMenu(inView: view, delegate: self, cancel: {
//				self.hideAddMenuButton(hide: false)
//			})
//		} else {
//			addMenuSheets = AddMenuView.showAddMenu(inView: view, delegate: self, cancel: {
//				if self.mode == .view {
//					self.hideAddMenuButton(hide: false)
//				}
//			})
//		}
	}
	
	func showCloudMenu() {

	}
	
	func showExportMenu() {
//		exportMenuSheets = ExportMenuView.showExportMenu(inView: view, delegate: self, cancel: { })
	}
	
	func showWiFiTransferView() {
		if let port = WebUploaderService.sharedInstance.start(typeService: mode == .view ? .download : .upload) {
			
			let _ = WiFiTransferView.showWiFiTransferView(inView: view, port: port, cancel: {
				self.hideAddMenuButton(hide: false)
				
				WebUploaderService.sharedInstance.stop()
				
				DispatchQueue.main.async {
					
					self.onFinishEdit()
					self.refreshDataAndReload()
					
				}
			})
		}
	}
	
	func hideAddMenuButton(hide: Bool) {
		
		UIView.animate(withDuration: 0.3) {
			self.addMenuButton.alpha = hide ? 0.0 : 1.0
		}
		
	}
	
	func showEmailAlert() {
		
		guard LockService.sharedService.isDecoyEnabled == false else { return }
		
		LockService.sharedService.checkForEmail {
			
			guard AppDelegate.appIsLocked == false else { return }
			guard isVisible == true else { return }
			
			_ = AlertView.shows(inViewController: self, text: "Secure yourself. Set recovery email in order to be able to reset your passcode.", action: {
				
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
					
					guard AppDelegate.appIsLocked == false else { return }
					guard self.isVisible == true else { return }
					
					EmailViewController.register().pushFrom(viewController: self)
					
				})
				
			}, cancel: {
				
				
				
			})
			
		}
	}
	
	func showReview() {
		guard LockService.sharedService.isDecoyEnabled == false else { return }
		
		guard AppDelegate.appIsLocked == false else { return }
		guard self.isVisible == true else { return }		
		_ = ReviewService.showReviewControllerIfNeeded(presenter: self)
	}
	
	func viewModeBarItems() {
		
		guard LockService.sharedService.isDecoyEnabled == false else  { return }
		guard let navigationController = navigationController else { return }
		guard let navigationBarView = navigationBarView else { return }
		
		if navigationController.viewControllers.filter({ $0 is PhotosViewController }).count > 1 {
			navigationBarView.setupBackButton()
		} else {
			navigationBarView.setupSettingsButton(button: navigationBarView.leftTopBarButton)
		}
		
		navigationBarView.setupEditButton(button: navigationBarView.rightTopBarButton)

	}
	
	func viewSubFolderModeBarItems() {
		guard let navigationBarView = navigationBarView else { return }
		navigationBarView.setupEditButton(button: navigationBarView.rightTopBarButton)
	}
	
	func editModeBarItems() {
		guard let navigationBarView = navigationBarView else { return }
		
		if BackupService.sharedInstance.backupStatus == nil {
			navigationBarView.setupSelectAllButton(button: navigationBarView.rightTopBarButton)
		} else {
			navigationBarView.setupNoButton(button: navigationBarView.rightTopBarButton)
		}
		
		navigationBarView.setupCancelButton(button: navigationBarView.leftTopBarButton)
	}
	
	func registeCollectionViewNib() {
		collectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: .photo)
		collectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: .video)
		collectionView.register(UINib(nibName: "FolderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: .folder)
	}
	
	func selectedFiles() -> [File] {
		
		var files = [File]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems! {
			
			let object = objects[indexPath.row]
			let relativePath = object.value(forKey: "mobPath") as? String ?? ""
			let file = File(relativePath: relativePath)
			
			files.append(file)
			
		}
		
		return files
		
	}
	
	func showBackupStatus(text: String) {
		
		if let navigationBarView = navigationBarView {
			navigationBarView.showBackupActiveStatus()
		}
		
		backupStatusText = text
		updateTitle()
//		if updateTitleTimer == nil {
//			updateTitleTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(updateTitle), userInfo: nil, repeats: true)
//		}
		
		if BackupService.sharedInstance.backupSpaceLimitException == .waiting && wasShownBackupException == false {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
				self.wasShownBackupException = true
				_ = ErrorView.shows(inViewController: self, text: "You have reached 25GB limit of backup storage space.")
			}
		}
	}
	
	func hideBackupStatus() {
		if let navigationBarView = navigationBarView {
			navigationBarView.hideBackupActiveStatus()
		}
		
		guard let topBarLabel = topBarLabel else { return }

//		if updateTitleTimer != nil {
//			updateTitleTimer?.invalidate()
//			updateTitleTimer = nil
//		}
		
		if directory.name == "albums" {
			
			if topBarLabel.text != "My Photos" {
				topBarLabel.text = "My Photos"
			}
			
		} else {
			
			if topBarLabel.text != directory.name {
				topBarLabel.text = directory.name
			}
			
		}
		
	}
	
	func reloadCollectionView() {
		
		loadImageAsync = false
		collectionView.reloadData()
		
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
			self.loadImageAsync = true
		}
		
	}
	
	func refreshDataAndReload(reload: Bool = true) {
		
		needSyncData = false
		
		print("refreshDataAndReload: \(directory.relativePath)")
		
		objects = FileService.fetchAlbumContent(relativePath: directory.relativePath)
		
		DispatchQueue.global().async {
			FileService.sharedInstance.fetchCurrentDirectoryContent()
		}
		
		if reload {
			self.reloadCollectionView()
		}
		
	}
	
	func loadImage(action: @escaping () -> ()) {
		
		if loadImageAsync == true {
			
			DispatchQueue.main.async {
				
				action()
				
			}
			
		} else {
			
			action()
			
		}
		
	}
	
	func scrollToEnd() {
		if objects.count > 0 {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { 
				self.collectionView.scrollToItem(at: IndexPath(row: self.objects.count - 1, section: 0), at: .bottom, animated: false)
			})
		}
	}
	
	func checkPhotoPermission(_ handler: @escaping (_ granted: Bool) -> Void) {
		func hasPhotoPermission() -> Bool {
			return PHPhotoLibrary.authorizationStatus() == .authorized
		}
		
		func needsToRequestPhotoPermission() -> Bool {
			return PHPhotoLibrary.authorizationStatus() == .notDetermined
		}
		
		hasPhotoPermission() ? handler(true) : (needsToRequestPhotoPermission() ?
			PHPhotoLibrary.requestAuthorization({ status in
				DispatchQueue.main.async(execute: { () in
					hasPhotoPermission() ? handler(true) : handler(false)
				})
			}) : handler(false))
	}
	
	@objc private func updateTitle() {
		guard let topBarLabel = topBarLabel else { return }
		
		if directory.name == "albums" {
			
			if topBarLabel.text != "My Photos" {
				topBarLabel.text = "My Photos"
			} else {
				if let backupStatusText = backupStatusText, backupStatusText.isEmpty == false {
					topBarLabel.text = backupStatusText
				}
			}
			
		} else {
			
			if topBarLabel.text != directory.name {
				topBarLabel.text = directory.name
			} else {
				if let backupStatusText = backupStatusText, backupStatusText.isEmpty == false {
					topBarLabel.text = backupStatusText
				}
			}
			
		}
		
	}
		
	//  //MARK: - UINavigationControllerDelegate
	
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		
		objects = FileService.fetchAlbumContent(relativePath: directory.relativePath)
		
		if objects.isEmpty == true {
			self.reloadCollectionView()
			return nil
		}
		
		if fromVC is PhotosViewController || toVC is PhotosViewController {
			
			guard sourceCell != nil else { return nil }
			
			if toVC is PhotosViewController {
				
				let fileObjects = FileService.fetchAlbumFiles(relativePath: directory.relativePath)
				
				if fileObjects.count > PreviewViewController.index {
				
					let fileObject = fileObjects[PreviewViewController.index]
					let relativePath = fileObject.value(forKey: "mobPath") as? String ?? ""
					let file = File(relativePath: relativePath)
					
					let index = objects.index(where: { (item) -> Bool in
						
						if let itemFile = item as? MFile {
							return itemFile.mobPath == file.relativePath
						}
						
						return false
					})
					
					if let index = index {
						self.sourceCell = self.collectionView.cellForItem(at: IndexPath(row: index, section: 0))						
					}
				}
				
			}
			
			needUpdate = false
			
			return PopInAndOutAnimator(operation: operation, copmpleationAcion: {
				
				self.reloadCollectionView()
				
			})
			
		}
		
		
		if (fromVC is PreviewViewController && toVC is PreviewViewController) {
			
			switch slideShowTransition {
			case .fadein: return FadeInAnimatedTransitioning()
			case .fadeout: return FadeOutAnimatedTransitioning()
			case .dissolve: return DissolveAnimatedTransitioning()
			default: return nil
			}
			
		}
		
		return nil
		
	}
	
	//MARK: - UICollectionViewDataSource
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return objects.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
//		print("cellForItemAt:\(indexPath)")
		
		let object = objects[indexPath.row]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let file = File(relativePath: relativePath)
		
		switch file.fileType {
			
		case .image:
			let fileObject = object as? MFile
			let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: .photo, for: indexPath) as! PhotoCollectionViewCell
			photoCell.isDragged = false

			if photoCell.path == nil || photoCell.path != file.path {
				
				loadImage {
					
					if let fileObject = fileObject, let thumbnail = fileObject.mobThumbnail {
						let size = rint(self.cellSize.width)
						photoCell.imageView.image = file.getPhotoThumbnailImage(name: thumbnail, size: size)
					}
					
					photoCell.path = file.path
				}
				
			}
			
			if photoCell.isSelected == true {
				photoCell.isDragged = isDragged
			}
			
			return photoCell
			
		case .video:
			let fileObject = object as? MFile
			let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: .video, for: indexPath) as! VideoCollectionViewCell
			videoCell.isDragged = false

			if videoCell.path == nil || videoCell.path != file.path {
				loadImage {
					
					if let fileObject = fileObject, let thumbnail = fileObject.mobThumbnail {
						let size = rint(self.cellSize.width)
						videoCell.imageView.image = file.getVideoThumbnailImage(name: thumbnail, size: size)
					}
					
					videoCell.path = file.path
				}
			}

			if videoCell.isSelected == true {
				videoCell.isDragged = isDragged
			}
			
			return videoCell
			
		case .directory:
			let albumObject = object as? MAlbum
			let folderCell = collectionView.dequeueReusableCell(withReuseIdentifier: .folder, for: indexPath) as! FolderCollectionViewCell
			folderCell.mode = self.mode == .view ? .view : .edit
			folderCell.nameLabel.text = file.name
			folderCell.isDragged = false
			folderCell.isOver = false
			
			folderPreviewSize = folderCell.preview.bounds.size
			
			if let album = albumObject {
				
				let isSecret = LockType(rawValue: Int(album.mobLockType)) != .undefined
				folderCell.secretAlbum = isSecret
				
				loadImage {
					if isSecret == true {
						folderCell.preview.image = nil
					} else {
						
						if let thumbnail = album.mobThumbnail {
							folderCell.preview.image = file.getAlbumThumbnailImage(name: thumbnail, size: folderCell.preview.bounds.size)
						}
					}
				}
				
			}
			
			folderCell.openSettingsAction = {
				
				BaseViewController.contextImage = self.view.screenshot
				
				self.unlockAlbum(file: file, presenter: self, success: {
					let controlller = AlbumViewController.editAlbum(delegate: EditAlbumSettingsViewControllerDelegate(file: file))
					controlller.directoryWasCreated = {
						
						DispatchQueue.main.async {
							self.refreshDataAndReload()
							self.onFinishEdit()
						}
						
					}
					controlller.pushFrom(viewController: self, transitionFade: UIDevice.current.isIpad())
					
				}, cancel: {
					
					DispatchQueue.main.async {
						self.refreshDataAndReload()
						self.onFinishEdit()
					}
					
				})
				
			}
			
			if folderCell.isSelected == true {
				folderCell.isDragged = isDragged
			}
			
			return folderCell
			
		default:
			return UICollectionViewCell()
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return cellSize
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 1.0
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 1.0
	}
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
		
		if mode == .edit {
			if let navigationBarView = navigationBarView {
				if let indexPaths = collectionView.indexPathsForSelectedItems, indexPaths.count == 0 {
					navigationBarView.setupSelectAllButton(button: navigationBarView.rightTopBarButton)
				}
			}
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		let object = objects[indexPath.row]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let file = File(relativePath: relativePath)
		
		if mode == .edit {
			
			if file.isBlocked == true {
				collectionView.deselectItem(at: indexPath, animated: true)
			}
			
			if let navigationBarView = navigationBarView {
				if let indexPaths = collectionView.indexPathsForSelectedItems, indexPaths.count > 0 {
					navigationBarView.setupDeselectAllButton(button: navigationBarView.rightTopBarButton)
				}
			}			
			
			return
		}
		
		switch file.fileType {
			
		case .image, .video:
			
			sourceCell = collectionView.cellForItem(at: indexPath)
			
			let controller = storyboard?.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
			
			controller.title = directory.name
			
			if let indexedObject = FileService.fetchAlbumFiles(relativePath: directory.relativePath).enumerated().first(where: { $0.element.mobPath == relativePath }) {
				PreviewViewController.index = indexedObject.offset
				controller.pushFrom(viewController: self)
//				needUpdate = true
				showSwipeHint = true
			}
			
//			controller.deleteAction = {
//				self.needUpdate = true
//			}
			
		case .directory:
			
//			self.needUpdate = true
			
			self.unlockAlbum(file: file, presenter: self, success: {
				
				self.sourceCell = nil
				
				let controller = StoryboardScene.Main.instantiatePhotosScene()
				
				controller.directory = file
				
				controller.pushFrom(viewController: self)
				
			}, cancel: {
				
			})
			
		default: return
			
		}
		
		collectionView.deselectItem(at: indexPath, animated: true)
		
	}
	
	//MARK: - DragDropCollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, touchBeginAtIndexPath indexPath: IndexPath) {
		
		if UIDevice.current.isIpad() {
			AudioServicesPlaySystemSound(1103) //1103, 1104, 1105
		} else {
			AudioServicesPlaySystemSound(1519)
		}
		
		
		BackupService.sharedInstance.suspendBackup()
		
		isDragged = true
		draggingPath = indexPath
		
		let cell = collectionView.cellForItem(at: indexPath)
		switch cell {
		case is FolderCollectionViewCell:
			(cell as! FolderCollectionViewCell).isDragged = true
			
		case is PhotoCollectionViewCell:
			(cell as! PhotoCollectionViewCell).isDragged = true
			
		case is VideoCollectionViewCell:
			(cell as! VideoCollectionViewCell).isDragged = true
			
		default: break
		}
		
		setCellsDraggedStatus()
	}
	
	func collectionView(_ collectionView: UICollectionView, representationImageAtIndexPath indexPath: IndexPath) -> UIImage? {
		if let cell = collectionView.cellForItem(at: indexPath) {
			UIGraphicsBeginImageContextWithOptions(cell.bounds.size, false, 0)
			cell.layer.render(in: UIGraphicsGetCurrentContext()!)
			let img = UIGraphicsGetImageFromCurrentImageContext()
			UIGraphicsEndImageContext()
			return img
		}
		
		return nil
	}
	
	func collectionView(_ collectionView: UICollectionView, canDragAtIndexPath indexPath: IndexPath) -> Bool {
		
		guard objects.count > indexPath.row else { return false }
		
		let object = objects[indexPath.row]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let file = File(relativePath: relativePath)
		
		if file.isBlocked{
			return false
		}
				
		return true
	}
	
	func collectionView(_ collectionView: UICollectionView, dragInfoForIndexPath indexPath: IndexPath) -> AnyObject {
		return NSString(string: "Hello")
	}
	
	func collectionView(_ collectionView: UICollectionView, canDropWithDragInfo info: AnyObject, AtIndexPath indexPath: IndexPath) -> Bool {
		
		clearCellsOverStatus()
		
		var draggingPaths = [IndexPath]()
		
		if let paths = collectionView.indexPathsForSelectedItems {
			draggingPaths.append(contentsOf: paths)
		}
		
		if let path = self.draggingPath {
			draggingPaths.append(path)
		}
		
		if draggingPaths.contains(where: { $0 == indexPath }) == true {
			return false
		}
		
		if let cell = collectionView.cellForItem(at: indexPath) {
			if cell is FolderCollectionViewCell {
				
				(cell as! FolderCollectionViewCell).isOver = true
				
				return true
			}
		}
		
		return false
	}
	
	func collectionView(_ collectionView: UICollectionView, dragCompleteWithDragInfo dragInfo: AnyObject, atDragIndexPath dragIndexPath: IndexPath, withDropInfo dropInfo: AnyObject?) {
		clearCellsDraggedStatus()
	}
	
	func collectionView(_ collectionView: UICollectionView, dropCompleteWithDragInfo dragInfo: AnyObject, atDragIndexPath dragIndexPath: IndexPath?, withDropInfo dropInfo: AnyObject?, atDropIndexPath dropIndexPath: IndexPath) {
		clearCellsDraggedStatus()
		
		var draggingPaths = [IndexPath]()
		
		if let paths = collectionView.indexPathsForSelectedItems {
			draggingPaths.append(contentsOf: paths)
		}
		
		if let path = self.draggingPath {
			draggingPaths.append(path)
		}
		
		let object = objects[dropIndexPath.row]
		let relativePath = object.value(forKey: "mobPath") as? String ?? ""
		let destinationFolder = File(relativePath: relativePath)
		
		var draggedFiles = selectedFiles()
		
		if let path = self.draggingPath {
			
			let object = objects[path.row]
			let relativePath = object.value(forKey: "mobPath") as? String ?? ""
			let draggingFolder = File(relativePath: relativePath)
			
			if !draggedFiles.contains(where: { $0.url.path == draggingFolder.url.path} ) {
				draggedFiles.append(draggingFolder)
			}
		}
		
		if draggedFiles.count >= 50 {
			LoaderView.show(in: self.view)
		}
		
		DispatchQueue.global().async {
			FileService.sharedInstance.move(files: draggedFiles.filter({ $0.isBlocked == false }), to: destinationFolder)
			
			DispatchQueue.main.async {
				LoaderView.close()
				self.refreshDataAndReload()
			}
			
			if let album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: destinationFolder.relativePath) {
				if let thumbnail = album.mobThumbnail {
					_ = destinationFolder.getAlbumThumbnailImage(name: thumbnail, size: self.folderPreviewSize, update: true)
					
					DispatchQueue.main.async {
						LoaderView.close()
						self.reloadCollectionView()
					}
					
				}
			}
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, dropOutsideWithDragInfo info: AnyObject) {
		clearCellsDraggedStatus()
	}
	
	func collectionViewStopDragging(_ collectionView: UICollectionView) {
		isDragged = false		
		
		if let indexPath = draggingPath {
		
			let cell = collectionView.cellForItem(at: indexPath)
			switch cell {
			case is FolderCollectionViewCell:
				(cell as! FolderCollectionViewCell).isDragged = false
				
			case is PhotoCollectionViewCell:
				(cell as! PhotoCollectionViewCell).isDragged = false
				
			case is VideoCollectionViewCell:
				(cell as! VideoCollectionViewCell).isDragged = false
				
			default: break
			}
			
			draggingPath = nil
			
		}
		
		clearCellsDraggedStatus()
	}
	
	func collectionViewStopDropping(_ collectionView: UICollectionView) {
		clearCellsDraggedStatus()
	}
	
	func setCellsDraggedStatus() {
		
		for cell in collectionView.visibleCells {
			guard cell.isSelected == true else { continue }
			
			switch cell {
			case is FolderCollectionViewCell:
				(cell as! FolderCollectionViewCell).isDragged = true
				
			case is PhotoCollectionViewCell:
				(cell as! PhotoCollectionViewCell).isDragged = true
				
			case is VideoCollectionViewCell:
				(cell as! VideoCollectionViewCell).isDragged = true
				
			default: break
			}
			
		}
		
	}

	func clearCellsDraggedStatus() {
		
		for cell in collectionView.visibleCells {
			
			switch cell {
			case is FolderCollectionViewCell:
				(cell as! FolderCollectionViewCell).isDragged = false
				
			case is PhotoCollectionViewCell:
				(cell as! PhotoCollectionViewCell).isDragged = false
				
			case is VideoCollectionViewCell:
				(cell as! VideoCollectionViewCell).isDragged = false
				
			default: break
			}
			
		}
		
	}

	func clearCellsOverStatus() {
		
		for cell in collectionView.visibleCells {
			
			switch cell {
			case is FolderCollectionViewCell:
				(cell as! FolderCollectionViewCell).isOver = false
	
			default: break
			}
			
		}
		
	}
	
	//MARK: - UIDocumentPickerDelegate
	
	func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
		
		let uniqueName = FileService.sharedInstance.verifyFileName(name: url.lastPathComponent)
		let newURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
		
		if newURL.isImage || newURL.isVideo {
		
			do {
				try FileManager.default.moveItem(at: url, to: newURL)
				let file = File(url: newURL)
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					_ = MFile.create(with: file.relativePath, context: localContext)
				})
				
			} catch let error as NSError {
				print("\(error.description)")
			}
			
		}
		
		refreshDataAndReload()
		
		onFinishEdit()
		scrollToEnd()
	}
	
	func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
		reloadCollectionView()
		onFinishEdit()
	}
	
	//MARK: - MFMailComposeViewControllerDelegate
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		
		self.dismiss(animated: true, completion: nil)
		
		if error == nil {
			onFinishEdit()
		}
		
	}
	
	//MARK: - @IBAction
	
	@IBAction func onShowAddMenu(_ sender: UIButton) {
		
		BaseViewController.contextImage = view.screenshot
		
		if mode == .view {
			
			if UserService.sharedService.user.appUsingType == .locked {
				
				if LockService.sharedService.isDecoyEnabled == true {
					showAddMenu()
					hideAddMenuButton(hide: true)
				} else {
					self.storyboard?.instantiateViewController(withIdentifier: "PurchasesViewController").pushFrom(viewController: self)
				}
				
			} else {
				showAddMenu()
				hideAddMenuButton(hide: true)
			}
			
		}
		
		if mode == .edit {
			
			showExportMenu()
			hideAddMenuButton(hide: true)
	
		}
		
	}
	
	func onSettings() {
		BaseViewController.contextImage = view.screenshot
		AppSettingsViewController.inistantiate().pushFrom(viewController: self, transitionFade: UIDevice.current.isIpad())
	}
	
	func onEdit() {
		
		mode = .edit
		
		UIView.animate(withDuration: 0.3) {
			self.toolBarViewHeight.constant = 50
			self.view.layoutSubviews()
		}
		
		hideAddMenuButton(hide: true)
		
		editModeBarItems()
		
		collectionView.visibleCells.forEach { (cell) in
			if cell is FolderCollectionViewCell {
				(cell as! FolderCollectionViewCell).mode = .edit
			}
		}
		
		BackupService.sharedInstance.suspendBackup()
	}
	
	func onFinishEdit() {
				
		BackupService.sharedInstance.resumeBackup()
		
		mode = .view
		
		UIView.animate(withDuration: 0.3) {
			self.toolBarViewHeight.constant = 0
			self.view.layoutSubviews()
		}
		
		hideAddMenuButton(hide: false)
		
		viewModeBarItems()
		
		PhotosViewController.filesToUpload.removeAll()
		
		collectionView.selectItem(at: nil, animated: true, scrollPosition: .top)
		
		collectionView.visibleCells.forEach { (cell) in
			if cell is FolderCollectionViewCell {
				(cell as! FolderCollectionViewCell).mode = .view
			}
		}
	}
	
	func onSelectAll() {
		for row in 0..<collectionView!.numberOfItems(inSection: 0) {
			collectionView!.selectItem(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: .top)
		}
	}
	
	func onUnselectAll() {
		collectionView.selectItem(at: nil, animated: true, scrollPosition: .top)
	}
	
	@IBAction func onShare(_ sender: UIButton) {
		
		PhotosViewController.filesToUpload = selectedFiles()
		
		showExportMenu()
		
	}
	
	@IBAction func onDelete(_ sender: UIButton) {
				
		let files = selectedFiles().filter({ $0.isBlocked == false })
		
		FileService.sharedInstance.deleteFiles(files: files)
		
		BackupService.sharedInstance.tryBackupAgainWhenBackupSpaceLimitException()
		
		refreshDataAndReload()
		
		onFinishEdit()
		
	}
	
	@IBAction func onMove(_ sender: UIButton) {
		
		PhotosViewController.filesToUpload = selectedFiles()
		
		let controller = StoryboardScene.instantiateMoveToScene()
		
		controller.compleationAction = {
			self.onFinishEdit()
		}
		
		controller.pushFrom(viewController: self, transitionFade: UIDevice.current.isIpad())
		
	}
	
}

extension PhotosViewController {
	
	static var columnCount: CGFloat {
		switch UIDevice.current.deviceType! {
		case .iPhone35, .iPhone40:
			return 3
		case .iPad:
			return 7
		default:
			return 4
		}
	}
	
	var wasShownSwipeHint: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "canShowSwipeHint")
		}
		set(newValue) {
			UserDefaults.standard.set(newValue, forKey: "canShowSwipeHint")
			UserDefaults.standard.synchronize()
		}
	}
	
	var wasShownDragHint: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "canShowDrageHint")
		}
		set(newValue) {
			UserDefaults.standard.set(newValue, forKey: "canShowDrageHint")
			UserDefaults.standard.synchronize()
		}
	}
	
	var wasShownBackupException: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "canShowBackupException")
		}
		set(newValue) {
			UserDefaults.standard.set(newValue, forKey: "canShowBackupException")
			UserDefaults.standard.synchronize()
		}
	}
}

