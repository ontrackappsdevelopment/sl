//
//  SplashViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class SplashViewController: BaseViewController, UINavigationControllerDelegate {
	
	private var reachabilityManager: NetworkReachabilityManager?
	private var progressIndicatorView: NVActivityIndicatorView?
	
	@IBOutlet weak var progressContainerView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.configurateTransparentNavigationBar()
		navigationController?.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		progressIndicatorView = NVActivityIndicatorView(frame: progressContainerView.bounds, type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white)
		progressContainerView.addSubview(progressIndicatorView!)
		progressIndicatorView?.startAnimating()
				
		if LockService.sharedService.firstLaunch {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
				let controller = StoryboardScene.instantiateChoosePasscodeTypeScene()
				self.navigationController?.setViewControllers([controller], animated: false)
			})
		} else {
			User.verifyReceipt { (result) in
				if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
					appDelegate.lockScreen()
				}
			}
		}
	}
}


