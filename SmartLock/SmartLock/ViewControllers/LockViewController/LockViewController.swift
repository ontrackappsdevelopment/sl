//
//  LockViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/6/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import LocalAuthentication
import AVFoundation

enum LockMode {
	case set
	case unlock
}

class LockViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
	
	var lockMode: LockMode = .set
	var passcode: String?
	var showedTouchID: Bool = false
	
	var success: ((_ pin: String) -> ())?
	var failure: (() -> ())?
	var cancel: (() -> ())?
	
	var session: AVCaptureSession?
	var input: AVCaptureDeviceInput?
	var videoOutput:AVCaptureVideoDataOutput?
	var videoDataOutputQueue:DispatchQueue?
	
	var photoOnLogin: Bool = false
	var makePhotoCompleation: ((_ image: UIImage) -> ())!
	
	var breakInAttempCount: Int = 0
	
	var canCancel: Bool = false
	var canOpenDecoy: Bool = false
	
	var alowedCamera = false
	
	@IBOutlet weak var cameraButton: UIButton! {
		didSet {
			self.cameraButton.isHidden = true
		}
	}
	
	@IBOutlet weak var cancelButton: UIButton! {
		didSet {
			cancelButton.isHidden = !canCancel
		}
	}
	
	@IBOutlet weak var forgotButton: UIButton! {
		didSet {
			forgotButton.isHidden = lockMode == .set
			
			if lockMode == .unlock {
				
				if let user = LockService.sharedService.user, let _ = user.mobEmail {
					forgotButton.isHidden = false
				} else {
					forgotButton.isHidden = true
				}
				
			}
		}
	}
	
	static func getLockControllerBy(lockType: LockType) -> LockViewController? {
		
		var controller: LockViewController?
		switch lockType {
			
		case .numpad4:
			
			controller = StoryboardScene.instantiateNumpadLockScene(modal: false)
			(controller as! NumpadLockViewController).pinLenght = 4
			
		case .numpad6:
			
			controller = StoryboardScene.instantiateNumpadLockScene(modal: false)
			(controller as! NumpadLockViewController).pinLenght = 6
			
		case .dotlock:
			
			controller = StoryboardScene.instantiatePatternLockScene(modal: false)
			
		case .aplhanum:
			
			controller = StoryboardScene.instantiateAlphanumericLockScene(modal: false)
			
		default:
			break
		}
		
		return controller
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .default
	}
	
	override var prefersStatusBarHidden: Bool {
		return false
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
		
		if let navigationController = self.navigationController, navigationController.viewControllers.contains(where: { $0 is ChatListViewController }) == false {
			touchIDLock()
		}
	}
	
	func viewWillAppear() {
		if canCancel {
			cancelButton?.isHidden = true
		} else {
			
//			if let user = MUser.mr_findFirst(), user.mobPhotoOnLogin == true {

				if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized {
				
					self.alowedCamera = true
					
					if let cameraButton = self.cameraButton {
						cameraButton.isHidden = true
					}
					
				}
				
//			askUserForCameraPermission { (alowed) in
//			}
				
//			}
		}
	}
	
	func viewDidDisappear() {
		guard canCancel == false else { return }
		
		DispatchQueue.global().async {
			
			if let session = self.session {
				session.stopRunning()
			}
			
		}
		
		if LockService.sharedService.isDecoyEnabled == false {
			UIApplication.shared.applicationIconBadgeNumber = 0
		}
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .portrait
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
	}
	
	func remove() {
		willMove(toParentViewController: nil)
		view.removeFromSuperview()
		removeFromParentViewController()
		viewDidDisappear()
		
		setNeedsStatusBarAppearanceUpdate()
	}
	
	func touchIDLock() {
		guard LockService.sharedService.user?.mobTouchID == true else { return }
		guard showedTouchID == false else { return }
		
		showedTouchID = true
		
		let myContext = LAContext()
		let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
		let myLocalizedReasonString = "Access \(appName)"
		
		var authError: NSError? = nil
		
		if myContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
			self.view.isUserInteractionEnabled = false
			myContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { (success, evaluateError) in
				DispatchQueue.main.async {
					self.view.isUserInteractionEnabled = true
					if (success) {
						NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
						
						self.showedTouchID = false
						
						if let action = self.success {
							DispatchQueue.main.async {
								action(LockService.sharedService.user?.mobPasscode ?? "")
							}
						}
						
					} else {
						// User did not authenticate successfully, look at error and take appropriate action
					}
				}
			}
		} else {
			// Could not evaluate policy; look at authError and present an appropriate message to user
		}
		
		print("\(authError?.localizedDescription)")
	}
	
	func breakInAttempt() {
		
		breakInAttempCount += 1
		
		if let user = LockService.sharedService.user, user.mobBreakInTracking == true {
			
			if user.mobPhotoOnLogin == true {
				
				makePhotoOnLogin(compleation: { (image) in
					
					let fileName = FileService.sharedInstance.saveBreakInPhoto(image: image)
					self.saveBreakInAttempt(imagePath: fileName)
					
				})
				
			} else {
				
				saveBreakInAttempt(imagePath: nil)
				
			}
			
			if user.mobShowOnIcon == true {
				
				UIApplication.shared.applicationIconBadgeNumber += 1
				
			}
			
		}
		
	}
	
	func saveBreakInAttempt(imagePath: String?) {
		
		LocationManager.sharedInstance.currentLocation { (location) in
			
			DispatchQueue.main.async {
				LockService.sharedService.saveBreakInAttempt(imageFileName: imagePath, location: location, time: Date.localDate)
				
			}
		}
	}
	
	func initCameraForBreakInPhoto() {
		
		if let user = LockService.sharedService.user, user.mobBreakInTracking == true {
			
			if user.mobPhotoOnLogin == true {
				
				videoDataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue")
				
				session = AVCaptureSession()
				session?.sessionPreset = AVCaptureSessionPresetMedium
				
				guard let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo), devices.count > 0 else {
					return
				}
				
				let frontDevices = devices.filter( { ($0 as! AVCaptureDevice).position == AVCaptureDevicePosition.front } )
				
				guard let device: AVCaptureDevice = frontDevices.first as? AVCaptureDevice else {
					return
				}
				
				do {
					input = try AVCaptureDeviceInput(device: device)
				} catch {
					
				}
				
				session?.addInput(input)
				
				videoOutput = AVCaptureVideoDataOutput()
				videoOutput!.alwaysDiscardsLateVideoFrames=true;
				videoOutput!.videoSettings = [kCVPixelBufferPixelFormatTypeKey as AnyHashable:Int(kCVPixelFormatType_32BGRA)]
				videoOutput!.setSampleBufferDelegate(self, queue:self.videoDataOutputQueue)
				session!.addOutput(videoOutput!)
				
				videoOutput!.connection(withMediaType: AVMediaTypeVideo).isEnabled = true
				
			}
			
		}
		
	}
	
	func makePhotoOnLogin(compleation: @escaping (_ image: UIImage) -> ()) {
		
		makePhotoCompleation = compleation
		
		photoOnLogin = true
		
	}
	
	open func askUserForCameraPermission(_ completion: @escaping (Bool) -> Void) {
		AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (alowedAccess) -> Void in
			
			DispatchQueue.main.sync(execute: { () -> Void in
				completion(alowedAccess)
			})
			
		})
	}
	
	//MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
	
	func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
		
		guard photoOnLogin == true else { return }
		
		photoOnLogin = false
		
		let image = sampleBuffer.imageFromSampleBuffer()
		
		makePhotoCompleation(image)
		
	}
	
	//MARK: - NSNotifications
	
	func didBecomeActive(notification:NSNotification) {

		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
		
		if let alphanumericLockViewController = self as? AlphanumericLockViewController {
			alphanumericLockViewController.passcodeTextField.becomeFirstResponder()
		}
		
		if canCancel == false {
			if lockMode == .unlock {
				initCameraForBreakInPhoto()
				if let user = LockService.sharedService.user, user.mobPhotoOnLogin == true {
					session!.startRunning()
				}
			}
		}
		
		touchIDLock()
	}
	
	func didEnterBackground(notification:NSNotification) {
		if let session = session {
			session.stopRunning()
			self.session = nil
		}
		
//		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
	}
	
	//MARK: Action
	
	@IBAction func onCamera(_ sender: UIButton) {
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
		view.removeFromSuperview()
		removeFromParentViewController()
		
		if let action = cancel {
			action()
		}
		
	}
	
	@IBAction func onForgot(_ sender: UIButton) {
		
		if LockService.sharedService.lockAlbum == nil {
			ConfirmCodeViewController.restore().pushFrom(viewController: self)
		} else {
			ConfirmCodeViewController.restoreAlbum().pushFrom(viewController: self)
		}
		
	}
	
}
