//
//  ChoosePasscodeTypeViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import YandexMobileMetrica

enum ChoosePasscodeTypeViewControllerMode {
	case new
	case restore
	case restoreAlbum
}

class ChoosePasscodeTypeViewController: BaseViewController, UINavigationControllerDelegate, CAAnimationDelegate {
	
	var mode: ChoosePasscodeTypeViewControllerMode = .new
	
	@IBOutlet weak var gradientView: UIView!
	
	let gradient = CAGradientLayer()
	var gradientSet = [[CGColor]]()
	var currentGradient: Int = 0
	
	let black = UIColor.black.cgColor
	let blue = UIColor(red: 108/255, green: 0/255, blue: 255/255, alpha: 1.0).cgColor
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.delegate = self
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		guard let leftTopBarButton = leftTopBarButton else { return }
		guard let rightTopBarButton = rightTopBarButton else { return }
		
		navigationBarView.topBarLabel.text = "Choose Passcode Type"
		navigationBarView.topBarLabel.font = UIFont.titleMedium
		navigationBarView.setupNoButton(button: leftTopBarButton)
		navigationBarView.setupNoButton(button: rightTopBarButton)
	}
	
	func saveLockAndClose(lockType: LockType, passcode: String?) {
		guard let navigationController = navigationController else { return }
		
		var controller: UIViewController = StoryboardScene.Chat.instantiateChatListScene()
		
		switch mode {
		case .new, .restore:
			
			AppDelegate.appIsLocked = false
			
			if LockService.sharedService.user == nil {
				
				if mode == .new {
					controller = StoryboardScene.instantiatePurchasesScene()
				}

				if LockService.sharedService.firstLaunch {
					LockService.sharedService.firstLaunchCompleted()
				}
				
				let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
				navigationController.setViewControllers([controller], animated: true)
				
			} else {
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
				
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					
						localUser.mobLockType = Int16(lockType.hashValue)
						localUser.mobPasscode = passcode
						
						if lockType == .undefined {
							localUser.mobTouchID = false
							localUser.mobShakeToLock = false
							localUser.mobDecoyPasscodeType = Int16(DecoyType.decoyOff.rawValue)
						}
						
					}
					
				})
				
//				}
				
				//go to
				
				var viewControllers = navigationController.viewControllers
				viewControllers.removeLast()
				viewControllers.removeLast()
				
				navigationController.setViewControllers(viewControllers: viewControllers, animated: true, completion: {
					if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let lockViewController = appDelegate.lockViewController {
						
//						if let success = lockViewController.success {
//							success("")
//						}
						
						appDelegate.removeLockAndPushPhotos()
					}
				})
				
//				} else {
					
//				}
				
			}
			
		default: break
		}
		
		if mode == .new && lockType == .undefined {
			DispatchQueue.main.async {
				_ = ErrorView.shows(inViewController: controller, text: "Your personal photos and videos will not be protected. We recommend to use passcode.")
			}
		}
		
	}
	
//	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//		return FadeOutAnimatedTransitioning()
//	}
	
	//MARK: - Action
	
	@IBAction func onNumPad4(_ sender: UIButton) {
		LockService.sharedService.setLockScreen(lockType: .numpad4, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { (passcode) in
			self.saveLockAndClose(lockType: .numpad4, passcode: passcode)
		}
	}
	
	@IBAction func onNumPad6(_ sender: UIButton) {
		LockService.sharedService.setLockScreen(lockType: .numpad6, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .numpad6, passcode: passcode)
		}
	}
	
	@IBAction func onDotLock(_ sender: UIButton) {
		LockService.sharedService.setLockScreen(lockType: .dotlock, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .dotlock, passcode: passcode)
		}
	}
	
	@IBAction func onAlphanumeric(_ sender: UIButton) {
		LockService.sharedService.setLockScreen(lockType: .aplhanum, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .aplhanum, passcode: passcode)
		}
	}
	
	@IBAction func onSkip(_ sender: UIButton) {
		self.saveLockAndClose(lockType: .undefined, passcode: nil)
	}

}
