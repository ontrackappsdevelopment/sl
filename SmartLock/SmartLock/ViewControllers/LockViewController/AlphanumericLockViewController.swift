//
//  AlphanumericLockViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import AudioToolbox


class AlphanumericLockViewController: LockViewController, UITextFieldDelegate {
	
	@IBOutlet weak var passcodeTextField: UITextField!
	@IBOutlet var bottomConstraint: [NSLayoutConstraint]!
	@IBOutlet weak var messageLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		cameraButton.isHidden = true
		cancelButton?.isHidden = !canCancel
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		passcodeTextField.becomeFirstResponder()
		
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	func keyboardWasShown(notification: Notification) {
		
		guard bottomConstraint != nil else { return }
		
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			
			bottomConstraint.forEach( { $0.constant =  keyboardSize.height + 10 } )
			
			UIView.animate(withDuration: 0.0, animations: {
				self.view.layoutSubviews()
				self.view.layoutIfNeeded()
			})
			
		}
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		if textField.text?.characters.count == 0 {
			return false
		}
		
		if lockMode == .set {
			
			if passcode == nil {
				
				passcode = textField.text
				
				messageLabel.text = "Re-enter your new passcode"
				
			} else {
				
				if passcode! == textField.text! {
					
					if let action = success {
						action(passcode!)
					}
					
				} else {
					
					passcode = nil
					self.messageLabel.text = "Passcode did not match"
					
					AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
					
					textField.frame.origin.x -= 20
					
					UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseInOut, animations: {
						
						textField.frame.origin.x += 20
						
					}) { _ in
						
						textField.text = ""
						
					}
					
				}
				
			}
			
		}
		
		if lockMode == .unlock {
			
			if passcode == textField.text {
				
				if let action = success {
					action(passcode!)
				}
				
			} else {
				
				//        if canOpenDecoy == true {
				
				breakInAttempt()
				
				//        }
				
												
				messageLabel.text = "Passcode did not match"
				
				AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
				
				textField.frame.origin.x -= 20
				
				UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseInOut, animations: {
					
					textField.frame.origin.x += 20
					
				}) { _ in
					
					textField.text = ""
					
				}
				
				
				
			}
			
		}
		
		textField.text = ""
		
		return false
		
	}
	
}
