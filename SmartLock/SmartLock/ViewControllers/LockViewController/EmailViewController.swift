//
//  EmailViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

enum EmailViewControllerMode {
	case register
	case change
}

class EmailViewController: BaseViewController {
	
	var mode: EmailViewControllerMode?
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var emailLabel: UILabel!
	
	@IBOutlet weak var centerViewConstrant: NSLayoutConstraint!
	
	static func instantiate(mode: EmailViewControllerMode) -> EmailViewController {
		let controller: EmailViewController?
		
		switch mode {
		case .register: controller = StoryboardScene.instantiateEmailScene()
		case .change: controller = StoryboardScene.instantiateChangeEmailScene()
		}
		
		controller!.mode = mode
		return controller!
	}
	
	static func register() -> EmailViewController {
		return instantiate(mode: .register)
	}
	
	static func change() -> EmailViewController {
		return instantiate(mode: .change)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if mode! == .change {
			emailLabel.text = "Your passcode recovery email:\n\(LockService.sharedService.user!.mobEmail!)"
		}
				
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self, selector: #selector(didShowKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		
		UIView.setAnimationsEnabled(false)
		emailTextField.becomeFirstResponder()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	@objc private func didShowKeyboard(notification: Notification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
			if let centerViewConstrant = centerViewConstrant {
				centerViewConstrant.constant = -(keyboardSize.height / 2)
			}
		}
		
		UIView.setAnimationsEnabled(true)
	}
	
	@objc private func willHideKeyboard(notification: Notification) {
		if let centerViewConstrant = centerViewConstrant {
			centerViewConstrant.constant = 0
		}
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = ""
	}
	
	@IBAction func onConfirm(_ sender: UIButton) {
		
		let email = emailTextField.text!
		
		if email.isEmpty == true {
			
			return
		}
		
		if email.isEmail != true {
			
			return
		}
		
		LoaderView.show(in: view)
		
		switch mode! {
		case .change:
			NetworkManager.Requests.changeEmail(email: email, device_id: DataManager.device!.mobID!) { (success, message) in
				
				LoaderView.close()
				
				if success == true {
					
					DispatchQueue.main.async {
						
						ConfirmCodeViewController.change(email: email).pushFrom(viewController: self)
						
					}
					
				} else {
					
					DispatchQueue.main.async {
						
						if message == "this_is_your_email" {
							
							self.emailLabel.text = "Your passcode recovery email:\n\(LockService.sharedService.user!.mobEmail!)"
							_ = ErrorView.shows(inViewController: self, text: "This email is already in use by your account.")
							
						} else {
							
							_ = ErrorView.shows(inViewController: self, text: message!)
							
						}
						
					}
					
				}
				
			}
		case .register:
			NetworkManager.Requests.register(email: email, device_id: DataManager.device!.mobID!) { (success, message) in
				
				LoaderView.close()
				
				if success == true {
					
					DispatchQueue.main.async {
						
						ConfirmCodeViewController.register(email: email).pushFrom(viewController: self)
						
					}
					
				} else {
					
					DispatchQueue.main.async {
						
						_ = ErrorView.shows(inViewController: self, text: message!)
						
					}
					
				}
				
			}
		}
		
	}
	
//	override func onBack(_ sender: UIButton) {
//		pop(transitionFade: UIDevice.current.isIpad())
//	}
}
