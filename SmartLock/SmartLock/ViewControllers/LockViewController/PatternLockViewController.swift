//
//  AlphanumericLockViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import AudioToolbox

class PatternLockViewController: LockViewController {
	
	@IBOutlet var label: UILabel!
	@IBOutlet var lockView: HUIPatternLockView!
	@IBOutlet weak var messageLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configuareLockViewWithImages()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
}

// MARK: - Custom LockView with images
extension PatternLockViewController {
	internal func configuareLockViewWithImages() {
		let defaultLineColor = HUIPatternLockView.defaultLineColor
		
		let normalImage = UIImage(named: "dot_normal")
		let highlightedImage = UIImage(named: "dot_highlighted")
		
		lockView.didDrawPatternPassword = { (lockView, count, password) -> Void in
			guard count > 0 else {
				return
			}
			
			switch self.lockMode {
			case .set:
				
				if self.passcode == nil {
					
					self.passcode = password
					
					self.messageLabel.text = "Re-enter your new passcode"
					
					DispatchQueue.main.async {
						
						lockView.resetDotsState()
						lockView.lineColor = defaultLineColor
						lockView.normalDotImage = normalImage
						lockView.highlightedDotImage = highlightedImage
						
					}
					
				} else {
					
					if self.passcode == password {
						
						if let action = self.success {
							action(password!)
						}
						
					} else {
						
						self.passcode = nil
						self.messageLabel.text = "Passcode did not match"
						
						AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
						
						lockView.frame.origin.x -= 20
						
						UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseInOut, animations: {
							
							lockView.frame.origin.x += 20
							
						}) { _ in
							
							lockView.resetDotsState()
							lockView.lineColor = defaultLineColor
							lockView.normalDotImage = normalImage
							lockView.highlightedDotImage = highlightedImage
							
						}
						
					}
					
				}
				
			case .unlock:
				
				if self.passcode == password {
					
					if let action = self.success {
						action(password!)
					}
					
				} else {
					
					
					self.breakInAttempt()
					
					self.messageLabel.text = "Passcode did not match"
					
					AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
					
					lockView.frame.origin.x -= 20
					
					UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseInOut, animations: {
						
						lockView.frame.origin.x += 20
						
					}) { _ in
						
						lockView.resetDotsState()
						lockView.lineColor = defaultLineColor
						lockView.normalDotImage = normalImage
						lockView.highlightedDotImage = highlightedImage
						
					}
					
					//          }
					
				}
				
			}
			
		}
		
	}
	
}


