//
//  NumpadLockViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class NumpadLockViewController: LockViewController, ABPadLockScreenViewControllerDelegate, ABPadLockScreenSetupViewControllerDelegate {
	
	@IBOutlet weak var containerView: UIView!
	
	var lockViewController:ABPadLockScreenViewController!
	var setLockViewController:ABPadLockScreenSetupViewController!
	
	var pinLenght: Int = 4
	
	override func viewDidLoad() {
		super.viewDidLoad()
			
		ABPadLockScreenView.appearance().labelColor = UIColor.white
		
		let buttonLineColor = UIColor.white
		ABPadButton.appearance().backgroundColor = UIColor.clear
		ABPadButton.appearance().borderColor = UIColor(red: 21.0/255.0, green:164.0/255.0, blue:250.0/255.0, alpha:0.3)
//			UIColor(red: 221.00/255.00, green: 220.00/255.00, blue: 216.00/255.00, alpha: 1.0)
		//UIColor(red: 76/255, green: 72/255, blue: 239/255, alpha: 1)
		ABPadButton.appearance().selectedColor = buttonLineColor
		ABPinSelectionView.appearance().selectedColor = buttonLineColor
		ABPinSelectionView.appearance().borderColor = UIColor(red: 21.0/255.0, green:164.0/255.0, blue:250.0/255.0, alpha:0.3)
//			UIColor(red: 221.00/255.00, green: 220.00/255.00, blue: 216.00/255.00, alpha: 1.0)
//			UIColor(red: 76/255, green: 72/255, blue: 239/255, alpha: 1)
		
		switch lockMode {
		case .set:
			
			setLockViewController = ABPadLockScreenSetupViewController(delegate: self, length: pinLenght)
			
			setLockViewController!.view.frame = containerView.bounds
			containerView.addSubview(setLockViewController!.view)
			
			setLockViewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
			setLockViewController.view.translatesAutoresizingMaskIntoConstraints = true
			
			addChildViewController(setLockViewController)
			
			setLockViewController.modalPresentationStyle = UIModalPresentationStyle.fullScreen
			setLockViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
			
			//      setLockViewController.tapSoundEnabled = true
			setLockViewController.errorVibrateEnabled = true
			
		case .unlock:
			
			lockViewController = ABPadLockScreenViewController(delegate: self, complexPin: false, length: pinLenght)
			
			lockViewController!.view.frame = containerView.bounds
			containerView.addSubview(lockViewController!.view)
			
			lockViewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
			lockViewController.view.translatesAutoresizingMaskIntoConstraints = true
			
			addChildViewController(lockViewController)
			
			lockViewController.modalPresentationStyle = UIModalPresentationStyle.fullScreen
			lockViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
			
			lockViewController.setAllowedAttempts(3)
			
			lockViewController.errorVibrateEnabled = true
			
			lockViewController.cameraButtonHidden(canCancel)
			if UserService.sharedService.user.appUsingType == .locked {
				lockViewController.cameraButtonHidden(true)
			}
			
			self.cameraButton = lockViewController.photoButton;
			
		}
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		//    forgotButton.isHidden = lockMode == .set
		cancelButton?.isHidden = !canCancel
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)		
	}
	
	func padLockScreenViewController(_ padLockScreenViewController: ABPadLockScreenViewController!, validatePin pin: String!) -> Bool {
		
		if passcode == pin {
			
			if let action = success {
				action(pin)
			}
			
		} else {
			
//			if canOpenDecoy == true {
			
				breakInAttempt()
				
//			}			
			
		}
		
		return passcode == pin
	}
	
	func unlockWasSuccessful(for padLockScreenViewController: ABPadLockScreenViewController!) {
		
	}
	
	func unlockWasCancelled(for padLockScreenViewController: ABPadLockScreenViewController!) {
		
	}
	
	func unlockWasUnsuccessful(_ falsePin: String!, afterAttemptNumber attemptNumber: Int, padLockScreenViewController: ABPadLockScreenViewController!) {
		
	}
	
	func pinSet(_ pin: String!, padLockScreenSetupViewController padLockScreenViewController: ABPadLockScreenSetupViewController!) {
		
		if let action = success {
			action(pin)
		}
		
	}
	
	func unlockWasCancelled(forSetupViewController padLockScreenViewController: ABPadLockScreenAbstractViewController!) {
		
	}
	
}
