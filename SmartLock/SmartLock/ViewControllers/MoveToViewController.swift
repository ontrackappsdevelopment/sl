//
//  MoveToViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/1/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class MoveToViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	var folders = [(file: File, expanded: Bool)]()
	var forbiddenFolder = [File]()
	
	var compleationAction: (() -> ())?
	
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		BackupService.sharedInstance.suspendBackup()
		
		title = "Move To"
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(onCancel))
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(onMove))
		
		folders = FileService.sharedInstance.rootMediaDirectory!.getFolders().map( { ($0, true) } )
		
		forbiddenFolder = PhotosViewController.filesToUpload.filter( { $0.fileType == .directory } )
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = "Move To"
		navigationBarView.setupLeftButton(image: nil, title: "Cancel", target: self, action: #selector(onCancel))
		navigationBarView.setupRightButton(image: nil, title: "OK", target: self, action: #selector(onMove))		
	}
	
	func onCancel() {
		pop(transitionFade: UIDevice.current.isIpad())
	}
	
	func onMove() {
		
		if let indexPaht = tableView.indexPathForSelectedRow {
			
			let destinationFolder = folders[indexPaht.row]
			
			let filesToUpload = PhotosViewController.filesToUpload.filter({ $0.isBlocked == false })
			
			FileService.sharedInstance.move(files: filesToUpload, to: destinationFolder.file)
			
			BackupService.sharedInstance.resumeBackup()
			
			pop(transitionFade: UIDevice.current.isIpad())
			
			if let action = compleationAction {
				action()
			}
			
		}
		
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		
		return 1
		
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return folders.filter( { $0.expanded == true } ).count
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell") as! FolderTableViewCell
  
		let folder = folders.filter( { $0.expanded == true } )[indexPath.row].file
		
		cell.folderName.text = folder.name
		
		cell.folderIcon.image = UIImage(named: "ico_light_folder")
		
		return cell
		
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		
		let folderCell = cell as! FolderTableViewCell
		
		let folder = folders.filter( { $0.expanded == true } )[indexPath.row].file
		
		let filter = forbiddenFolder.filter { (file) -> Bool in
			
			if file.level == folder.level && folder.path == file.path {
				return true
			}
			
			if folder.level > file.level && folder.path.contains("\(file.path)/") {
				return true
			}
			
			if folder.level == file.level - 1 {
				return true
			}
			
			return false
		}
		
		if filter.count > 0 {
			
			folderCell.folderName.textColor = UIColor.lightGray
			folderCell.folderIcon.alpha = 0.5
			folderCell.selectionStyle = .none
			
		}
		
		if folder.path == FileService.sharedInstance.currentDirectory.path {
			
			folderCell.folderName.textColor = UIColor.lightGray
			folderCell.folderIcon.alpha = 0.5
			folderCell.selectionStyle = .none
			
		}
		
		if folder.isBlocked == true {
			
			folderCell.folderName.textColor = UIColor.lightGray
			folderCell.folderIcon.alpha = 0.5
			folderCell.selectionStyle = .none
			
		}
		
		let level = folder.level - rootFolderLevel
		
		UIView.animate(withDuration: 0) {
			
			folderCell.leftPosition.constant = 5 + CGFloat(level * 20)
			
			folderCell.folderIcon.layoutIfNeeded()
			
		}
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 45
	}
	
}
