//
//  AlertView.swift
//  SmartLock
//
//  Created by Bogachev on 3/30/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class AlertView: UIView {
	
	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var alertView: UIView!
	@IBOutlet weak var actionButton: UIButton!
	@IBOutlet weak var cancelButton: UIButton!
	
	
	@IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
	
	weak var parentViewController: UIViewController?
	
	var action: (() -> ())?
	var cancel: (() -> ())?
	
	static func instantiateFromXib() -> AlertView {
		
		let view = Bundle.main.loadNibNamed("AlertView", owner: nil, options: nil)?.first as! AlertView
		
		return view
		
	}
	
	static func shows(inViewController controller: UIViewController, text: String, actionTitle: String = "OK", cancelTitle: String = "CANCEL", action: @escaping () -> (), cancel: @escaping () -> ()) -> AlertView {
		
		let view = instantiateFromXib()
		view.frame = UIScreen.main.bounds
		controller.view.addSubview(view)
		view.label.text = text
		
		if UIDevice.current.isIpad() == true {
			NSLayoutConstraint.activate([view.errorViewWidthConstraint])
			NSLayoutConstraint.deactivate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		} else {
			NSLayoutConstraint.deactivate([view.errorViewWidthConstraint])
			NSLayoutConstraint.activate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		}
		
		view.parentViewController = controller
		view.action = action
		view.cancel = cancel
		view.actionButton.setTitle(actionTitle, for: .normal)
		view.cancelButton.setTitle(cancelTitle, for: .normal)
		
		if controller.navigationController != nil {
			
//			controller.navigationController?.setNavigationBarHidden(true, animated: false)
			
		}
		
		view.alertView.transform = CGAffineTransform(scaleX: 0, y: 0)
		
		UIView.animate(withDuration: 0.3) {
			
			view.alertView.transform = CGAffineTransform.identity
			
		}
		
		return view
		
	}
	
	
	@IBAction func action(_ sender: UIButton) {
		
//		parentViewController?.navigationController?.setNavigationBarHidden(false, animated: false)
		removeFromSuperview()
		
		if action != nil {
			action!()
		}
		
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
		
//		parentViewController?.navigationController?.setNavigationBarHidden(false, animated: false)
		removeFromSuperview()
		
		if cancel != nil {
			cancel!()
		}
	}
	
}
