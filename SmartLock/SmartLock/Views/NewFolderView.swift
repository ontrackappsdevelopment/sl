//
//  NewFolderView.swift
//  SmartLock
//
//  Created by Bogachev on 2/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class NewFolderView: UIView {

  @IBOutlet weak var newFolderName: UITextField!
  @IBOutlet weak var newFolderView: UIView!
  
  @IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
  
  var okAction: ((String) -> ())?
  var cancelAction: (() -> ())?
  
  static func instantiateFromXib() -> NewFolderView {
    
    let view = Bundle.main.loadNibNamed("NewFolderView", owner: nil, options: nil)?.first as! NewFolderView
    return view
    
  }
  
  static func showsView(inView view: UIView, ok: @escaping (_ folderName: String) -> (), cancel: @escaping () -> ()) -> NewFolderView {
    
    let newFolderView = instantiateFromXib()
    newFolderView.frame = UIScreen.main.bounds
    newFolderView.okAction = ok
    newFolderView.cancelAction = cancel
    
    if UIDevice.current.isIpad() == true {
      NSLayoutConstraint.activate([newFolderView.errorViewWidthConstraint])
      NSLayoutConstraint.deactivate([newFolderView.errorViewLeadingConstraint, newFolderView.errorViewTrailingConstraint])
    } else {
      NSLayoutConstraint.deactivate([newFolderView.errorViewWidthConstraint])
      NSLayoutConstraint.activate([newFolderView.errorViewLeadingConstraint, newFolderView.errorViewTrailingConstraint])
    }
    
    view.addSubview(newFolderView)
    
    newFolderView.newFolderView.transform = CGAffineTransform(scaleX: 0, y: 0)
    
    UIView.animate(withDuration: 0.3) {
      
      newFolderView.newFolderView.transform = CGAffineTransform.identity
      
    }
    
    return newFolderView
  }
  
  func close() {
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.newFolderView.frame = CGRect(x: self.bounds.width, y: 0, width: 0, height: 0)
      
    }) { (compleat) in
      
      self.removeFromSuperview()
      
    }
    
  }

  @IBAction func onOk(_ sender: UIButton) {
    okAction!(newFolderName.text!)
    close()
  }
  
  @IBAction func onCancel(_ sender: UIButton) {
    close()
  }
}
