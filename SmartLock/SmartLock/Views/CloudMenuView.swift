//
//  CloudMenuView.swift
//  SmartLock
//
//  Created by Bogachev on 2/10/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

@objc protocol CloudMenuViewDelegate {
  func openGoogleDrive(view: CloudMenuView)
  func openDropBox(view: CloudMenuView)
  func openiCloud(view: CloudMenuView)
  func openYandexDisk(view: CloudMenuView)
  func openBox(view: CloudMenuView)
  func openOneDrive(view: CloudMenuView)
}

class CloudMenuView: UIView {
  
  static func showCloudMenu(inView view: UIView, delegate: CloudMenuViewDelegate, cancel: @escaping () -> ()) -> CustomizableActionSheet {
    
    var items = [CustomizableActionSheetItem]()
    
    if let cloudMenuView = UINib(nibName: "CloudMenuView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? CloudMenuView {
      let sampleViewItem = CustomizableActionSheetItem()
      sampleViewItem.type = .view
      sampleViewItem.view = cloudMenuView
      sampleViewItem.height = 260
      items.append(sampleViewItem)
      cloudMenuView.delegate = delegate
    }
    
    let closeItem = CustomizableActionSheetItem()
    closeItem.type = .button
    closeItem.label = "Cancel"
    closeItem.backgroundColor = UIColor.clear
    closeItem.backgroundImage = UIImage(named: "menu_cancel_background")
    closeItem.textColor = UIColor.textColor
    closeItem.font = UIFont(name: "SFUIDisplay-Medium", size: 24.0)
    closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
      actionSheet.dismiss {
        cancel()
      }
    }
    
    items.append(closeItem)
    
    let cloudMenuSheets = CustomizableActionSheet()
    
    cloudMenuSheets.showInView(view, items: items) {
      cancel()
    }
    
    return cloudMenuSheets
    
  }
  
  weak var delegate: CloudMenuViewDelegate?
  
  @IBAction func onGoogleDrive(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.openGoogleDrive(view: self)
    }
  }
  
  @IBAction func onDropbox(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.openDropBox(view: self)
    }
  }
  
  @IBAction func oniCloud(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.openiCloud(view: self)
    }
  }
  
  @IBAction func onYandexDisk(_ sender: UIButton) {

    if let delegate = delegate {
      delegate.openYandexDisk(view: self)
    }
  }
  
  @IBAction func onBox(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.openBox(view: self)
    }
  }
  
  @IBAction func onOneDrive(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.openOneDrive(view: self)
    }
  }
  
}
