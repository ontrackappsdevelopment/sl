//
//  LoaderView.swift
//  SmartLock
//
//  Created by Bogachev on 2/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoaderView: UIView {
	
	@IBOutlet weak var containerView: UIView!
	
	static let view: LoaderView = instantiateFromXib()
	
	private var activityIndicatorView: NVActivityIndicatorView?
	
	static func instantiateFromXib() -> LoaderView {
		let view = Bundle.main.loadNibNamed("LoaderView", owner: nil, options: nil)?.first as! LoaderView
		view.frame = UIScreen.main.bounds
		view.configurate()
		return view
	}
	
	func configurate() {
		activityIndicatorView = NVActivityIndicatorView(frame: containerView.bounds, type: .lineScalePulseOut, color: UIColor.white, padding: 20)
		containerView.addSubview(activityIndicatorView!)
	}
	
	static func show(in view: UIView) {
		LoaderView.view.alpha = 0.0
		view.addSubview(LoaderView.view)
		UIView.animate(withDuration: 0.3, animations: {
			LoaderView.view.alpha = 1.0
		})
		
		LoaderView.view.activityIndicatorView?.startAnimating()
	}
	
	static func close() {
		
		UIView.animate(withDuration: 0.3, animations: {
			LoaderView.view.alpha = 0.0
		}) { (compleat) in
			LoaderView.view.removeFromSuperview()
		}
		
		LoaderView.view.activityIndicatorView?.stopAnimating()
		
	}
	
}
