//
//  HintView.swift
//  SmartLock
//
//  Created by Bogachev on 3/30/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class HintView: UIView {
	
	@IBOutlet weak var hintView: UIView!
	@IBOutlet weak var actionButton: UIButton!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var labelText: UILabel!
	
	@IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
	
	weak var parentViewController: UIViewController?
	
	static func instantiateFromXib() -> HintView {
		let view = Bundle.main.loadNibNamed("HintView", owner: nil, options: nil)?.first as! HintView
		return view
	}
	
	static func shows(inViewController controller: UIViewController, image: UIImage, text: String, actionTitle: String = "OK") -> HintView {
		
		let view = instantiateFromXib()
		view.frame = UIScreen.main.bounds
		controller.view.addSubview(view)
		
		if UIDevice.current.isIpad() == true {
			NSLayoutConstraint.activate([view.errorViewWidthConstraint])
			NSLayoutConstraint.deactivate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		} else {
			NSLayoutConstraint.deactivate([view.errorViewWidthConstraint])
			NSLayoutConstraint.activate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		}
		
		view.parentViewController = controller
		view.actionButton.setTitle(actionTitle, for: .normal)
		view.imageView.image = image
		view.labelText.text = text
		
//		if controller.navigationController != nil {
//			controller.navigationController?.setNavigationBarHidden(true, animated: false)
//		}
		
		view.hintView.transform = CGAffineTransform(scaleX: 0, y: 0)
		
		UIView.animate(withDuration: 0.3) {
			view.hintView.transform = CGAffineTransform.identity
		}
		
		return view
		
	}
	
	
	@IBAction func action(_ sender: UIButton) {
//		parentViewController?.navigationController?.setNavigationBarHidden(false, animated: false)
		removeFromSuperview()
	}
}
