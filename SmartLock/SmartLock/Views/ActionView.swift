//
//  ActionView.swift
//  SmartLock
//
//  Created by Bogachev on 3/30/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class ActionView: UIView {
	
	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var actionView: UIView!
	@IBOutlet weak var actionButton: UIButton!
	
	
	@IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
	
	weak var parentViewController: UIViewController?
	
	var action: (() -> ())?
	
	static func instantiateFromXib() -> ActionView {
		
		let view = Bundle.main.loadNibNamed("ActionView", owner: nil, options: nil)?.first as! ActionView
		
		return view
		
	}
	
	static func shows(inViewController controller: UIViewController, text: String, actionTitle: String = "OK", action: @escaping () -> ()) -> ActionView {
		
		let view = instantiateFromXib()
		view.frame = UIScreen.main.bounds
		controller.view.addSubview(view)
		view.label.text = text
		
		if UIDevice.current.isIpad() == true {
			NSLayoutConstraint.activate([view.errorViewWidthConstraint])
			NSLayoutConstraint.deactivate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		} else {
			NSLayoutConstraint.deactivate([view.errorViewWidthConstraint])
			NSLayoutConstraint.activate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
		}
		
		view.parentViewController = controller
		view.action = action
		view.actionButton.setTitle(actionTitle, for: .normal)
		
//		if controller.navigationController != nil {
//			
//			controller.navigationController?.setNavigationBarHidden(true, animated: false)
//			
//		}
		
		view.actionView.transform = CGAffineTransform(scaleX: 0, y: 0)
		
		UIView.animate(withDuration: 0.3) {
			
			view.actionView.transform = CGAffineTransform.identity
			
		}
		
		return view
		
	}
	
	
	@IBAction func action(_ sender: UIButton) {
		
//		parentViewController?.navigationController?.setNavigationBarHidden(false, animated: false)
		removeFromSuperview()
		
		if action != nil {
			action!()
		}
		
	}
	
}
