//
//  ExportMenuView.swift
//  SmartLock
//
//  Created by Bogachev on 2/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Alamofire
import YandexMobileMetrica

@objc protocol ExportMenuViewDelegate {
  func openCameraRoll(exportView: ExportMenuView)
  func chooseCloud(exportView: ExportMenuView)
  func wifiTransfer(exportView: ExportMenuView)
  func onAirDrop(exportView: ExportMenuView)
  func onMail(exportView: ExportMenuView)
}

class ExportMenuView: UIView {
  
  static func showExportMenu(inView view: UIView, delegate: ExportMenuViewDelegate, cancel: @escaping () -> ()) -> CustomizableActionSheet {
    
    var items = [CustomizableActionSheetItem]()
    
    if let exportMenuView = UINib(nibName: "ExportMenuView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? ExportMenuView {
      let sampleViewItem = CustomizableActionSheetItem()
      sampleViewItem.type = .view
      sampleViewItem.view = exportMenuView
      sampleViewItem.height = 260
      items.append(sampleViewItem)
      exportMenuView.delegate = delegate
      
      exportMenuView.startNetworkReachabilityObserver()
      
    }
    
    let closeItem = CustomizableActionSheetItem()
    closeItem.type = .button
    closeItem.label = "Cancel"
    closeItem.backgroundColor = UIColor.clear
    closeItem.backgroundImage = UIImage(named: "menu_cancel_background")
    closeItem.textColor = UIColor.textColor
    closeItem.font = UIFont(name: "SFUIDisplay-Medium", size: 24.0)
    closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
      actionSheet.dismiss {
        cancel()
      }
    }
    
    items.append(closeItem)
    
    let exportMenuSheets = CustomizableActionSheet()
    
    exportMenuSheets.showInView(view, items: items) {
      cancel()
    }
    
    return exportMenuSheets
    
  }
  
  weak var delegate: ExportMenuViewDelegate?
  
  @IBOutlet weak var photosBotton: UIButton!
  
  @IBOutlet weak var topButtonLeftPosition: NSLayoutConstraint!
  @IBOutlet weak var topButtonRightPosition: NSLayoutConstraint!
  @IBOutlet weak var topButtonImageView: UIImageView!
  
  @IBOutlet weak var wifiStatusImageView: UIImageView!
  @IBOutlet weak var wifiStatusLabel: UILabel!
  
  @IBOutlet weak var wifiButton: UIButton!
  
  func startNetworkReachabilityObserver() {
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")!
    
    if reachabilityManager.networkReachabilityStatus != .reachable(.ethernetOrWiFi) {
      wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
      wifiStatusLabel.text = "Wi-Fi Transfer \n(no network)"
      wifiButton.isEnabled = false
    }
    
    reachabilityManager.listener = {[weak self] status in
      
      guard let strongSelf = self else { return }
      
      switch status {
      case .reachable(.ethernetOrWiFi):
        strongSelf.wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
        strongSelf.wifiStatusLabel.text = "Wi-Fi Transfer \n(network detected)"
        strongSelf.wifiButton.isEnabled = true
        
      default:
        strongSelf.wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
        strongSelf.wifiStatusLabel.text = "Wi-Fi Transfer \n(no network)"
        strongSelf.wifiButton.isEnabled = false
      }
      
      reachabilityManager.startListening()
      
    }
  }
  
  @IBAction func onPhotos(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Photoroll"], onFailure: nil)

    if let delegate = delegate {
      delegate.openCameraRoll(exportView: self)
    }
  }
  
  @IBAction func onClouds(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.chooseCloud(exportView: self)
    }
  }
  
  @IBAction func onWiFi(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Wi-fi"], onFailure: nil)
	
    if let delegate = delegate {
      delegate.wifiTransfer(exportView: self)
    }
  }
  
  @IBAction func onAirDrop(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Airdrop"], onFailure: nil)
    if let delegate = delegate {
      delegate.onAirDrop(exportView: self)
    }
  }
  
  @IBAction func onMail(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Email"], onFailure: nil)
    if let delegate = delegate {
      delegate.onMail(exportView: self)
    }
  }
  
}
