//
//  UIViewController.swift
//  MotionVibe
//
//  Created by Bogachev on 12/30/16.
//  Copyright © 2016 Sergei Bogachev. All rights reserved.
//

import Foundation

extension UIViewController {

	func pushFrom(viewController: UIViewController, transitionFade: Bool = false) {
		
		navigationController?.view.layer.removeAllAnimations()
		
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionFade
		
		if transitionFade {
			viewController.navigationController?.view.layer.add(transition, forKey:nil)
		}
		
		viewController.navigationController?.pushViewController(self, animated: !transitionFade)
	}

	func pop(transitionFade: Bool = false) {
		
		navigationController?.view.layer.removeAllAnimations()
		
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionFade
		
		if transitionFade {
			navigationController?.view.layer.add(transition, forKey:nil)
		}
		
		navigationController?.popViewController(animated: !transitionFade)
	}
	
	func popToViewController(viewController: UIViewController, transitionFade: Bool = false) {
		
		navigationController?.view.layer.removeAllAnimations()
		
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionFade
		
		if transitionFade {
			navigationController?.view.layer.add(transition, forKey:nil)
		}
		
		navigationController?.popToViewController(viewController, animated: !transitionFade)
	}
	
	func setViewControllers(viewControllers: [UIViewController], transitionFade: Bool = false) {
		
		navigationController?.view.layer.removeAllAnimations()
		
		let transition = CATransition()
		transition.duration = 0.5
		transition.type = kCATransitionFade
		
		if transitionFade {
			navigationController?.view.layer.add(transition, forKey:nil)
		}

		navigationController?.setViewControllers(viewControllers, animated: !transitionFade)
	}

	func presentBy(viewController: UIViewController) {
		viewController.present(self, animated: true, completion: nil)
	}

//	func addViewControllerAsSubView(viewController controller: UIViewController, showAnimated: Bool = false, alpha: CGFloat = 1.0) {
	func addViewControllerAsSubView(viewController controller: UIViewController, showAnimated: Bool = false) {	
		let frame = view.bounds
		controller.view.frame = frame
//		controller.view.backgroundColor = UIColor(white: 0.0, alpha: alpha)
		controller.view.alpha = showAnimated ? 0 : 1
		
		addChildViewController(controller)
		view.addSubview(controller.view)
		view.bringSubview(toFront: controller.view)
		controller.didMove(toParentViewController: self)
		
		if showAnimated {
			
			UIView.animate(withDuration: 0.3, animations: {
				controller.view.alpha = 1.0
			})
			
		}
		
	}
	
}
