//
//  BackupMoveLog.swift
//  SmartLock
//
//  Created by Bogachev on 4/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import ObjectMapper

class BackupMoveLog: Mappable {
	
	var id: String?
	var oldPath: String?
	var newPath: String?
	var type: String?
	var dateCreated: String?
	var dateDevice: String?
	var email: String?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		id <- map["_id"]
		oldPath <- map["old_path"]
		newPath <- map["new_path"]
		type <- map["type"]
		dateCreated <- map["date_created"]
		dateDevice <- map["date_device"]
		email <- map["email"]
	}
}
