//
//  ReviewObject.swift
//  SmartLock
//
//  Created by Bogachev on 6/18/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import ObjectMapper

class ReviewObject: Mappable {
	
	var id: String?
	var v: String?
	var isActive: Bool?
	var date_created: String?
	var typeUser: String?
	var cancel_button: String?
	var ok_button: String?
	var message: String?
	var title: String?
	var locale: String?
	
	required init?(map: Map) {
		
	}
	
	func mapping(map: Map) {
		id <- map["_id"]
		v <- map["__v"]
		isActive <- map["isActive"]
		date_created <- map["date_created"]
		typeUser <- map["typeUser"]
		cancel_button <- map["cancel_button"]
		ok_button <- map["ok_button"]
		message <- map["message"]
		title <- map["title"]
		locale <- map["locale"]
	}
}
