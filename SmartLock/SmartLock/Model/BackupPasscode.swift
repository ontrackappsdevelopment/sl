//
//  BackupPasscode.swift
//  SmartLock
//
//  Created by Bogachev on 4/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import ObjectMapper

class BackupPasscode: Mappable {
	
	var id: String?
	var hash: String?
	var path: String?
	var dateModified: String?
	var uuid: String?
	var dateDevice: String?
	var removed: Bool?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		id <- map["_id"]
		hash <- map["password_hash"]
		path <- map["folder_path"]
		dateModified <- map["date_modified"]
		uuid <- map["uuid"]
		dateDevice <- map["date_device"]
		removed <- map["removed"]
	}
	
}
