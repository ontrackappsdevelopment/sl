//
//  User.swift
//  SmartLock
//
//  Created by Bogachev on 6/3/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import SwiftyStoreKit
import StoreKit

enum UserType: String {
	case newValue = "newValue"
	case oldValue = "oldValue"
}

enum AppUsingType {
	case freeTrial
	case purchased
	case subscribed
	case locked
}

class User {
	
	static let verifyReceiptURLType = AppleReceiptValidator.VerifyReceiptURLType.production
	
	private static var freeTrialPeriod: Double {		
		get {
			let value = UserDefaults.standard.integer(forKey: "FreeTrialPeriod")
			return Double(value == 0 ? 2 : value) * 60 * 60 * 24
		}
	}
	
	static var daysOfFreeTrialPeriod: Int {
		get {
			return UserDefaults.standard.integer(forKey: "FreeTrialPeriod")
		}
	}
	
	var currentAppStatus: String {
		
		switch appUsingType {
		case .freeTrial:
			let count = User.freeTrialDayLeft
			let days = count > 1 ? "days" : "day"
			return "\(count) \(days) of your premium version left"
		case .locked:
			return "Premium trial period has expired. Select a plan"
		case .purchased:
			
			if canBackup == true {
				return "You have signed up for backup plan"
			} else {
				return "Select a plan for backup option"
			}
			
		case .subscribed:
			
			if canBackup == true {
				return "You have signed up for backup plan"
			} else {
				return "You have signed up for all features, no backup"
			}
			
		}
		
	}
	
	var appUsingType = AppUsingType.locked
	var canBackup = false
	var expiryDate: Date?
	var subscribedProduct: RegisteredPurchase?
	var purchasedProduct: RegisteredPurchase?
	var reviewData = [ReviewObject]()
	
	init() {
	}
	
	static var freeTrialExpired: Bool {
		return UserService.sharedService.user.appUsingType == .locked
	}
	
	static var freeTrialDayLeft: Int {
		
		if let firstLaunchDate = KeychainService.firstLaunchDate {
			let dayLef = Int((freeTrialPeriod - Date().timeIntervalSince(firstLaunchDate)) / 60 / 60 / 24)
			
			if dayLef < 1 && UserService.sharedService.user.appUsingType == .freeTrial {
				UserService.sharedService.user.appUsingType = .locked
			}
			
			return dayLef + 1
		}
		
		return 7
		
	}
	
	static var currentReceipt: ReceiptInfo? {
		get{
			guard let receiptData = KeychainService.loadData(service: "ChatKeeperAppReceipt") else { return nil }
			let receiptInfo = NSKeyedUnarchiver.unarchiveObject(with: receiptData) as! ReceiptInfo
			return receiptInfo
		}
		
		set(newValue){
			
			guard let receiptInfo = newValue  else {
				return
			}
			
			let receiptData = NSKeyedArchiver.archivedData(withRootObject: receiptInfo)
			KeychainService.save(service: "ChatKeeperAppReceipt", data: receiptData)
			
		}
	}
	
	static func verifyNewReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
		let appleValidator = AppleReceiptValidator(service: User.verifyReceiptURLType, sharedSecret: sharedSecret)
		
		SwiftyStoreKit.verifyReceipt(using: appleValidator) { (result) in
			
			switch result {
			case .success(receipt: let receiptInfo):
				readReceiptInfo(receiptInfo: receiptInfo)
				
			case .error(error: let receiptError):
				print("\(receiptError)")
			}
			
			completion(result)
		}
		
	}
	
	static func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
		let appleValidator = AppleReceiptValidator(service: User.verifyReceiptURLType, sharedSecret: sharedSecret)
		
		guard currentReceipt != nil else {
			completion(.error(error: .noReceiptData))
			return
		}
		
		SwiftyStoreKit.verifyReceipt(using: appleValidator) { (result) in
			
			switch result {
			case .success(receipt: let receiptInfo):
				readReceiptInfo(receiptInfo: receiptInfo)
				completion(result)
				
			case .error(error: let receiptError):
				completion(.error(error: receiptError))
			}
			
		}
		
	}
	
	static func readReceiptInfo(receiptInfo: ReceiptInfo) {
		currentReceipt = receiptInfo
		
		verifySubscription(subscription: .appSubscription, receiptInfo: receiptInfo, purchased: {
			UserService.sharedService.user.appUsingType = .subscribed
			UserService.sharedService.user.canBackup = true
			UserService.sharedService.user.subscribedProduct = .appSubscription
		})
	}
	
	static func verifyPurchase(purchase: RegisteredPurchase, receiptInfo: ReceiptInfo, purchased: () -> ()) {
		let purchaseResult = SwiftyStoreKit.verifyPurchase(
			productId: purchase.rawValue,
			inReceipt: receiptInfo
		)
		
		switch purchaseResult {
		case .purchased(item: let receiptItem):
			print("\(receiptItem)")
			purchased()
		case .notPurchased:
			break
		}
	}
	
	static func verifySubscription(subscription: RegisteredPurchase, receiptInfo: ReceiptInfo, purchased: () -> ()) {
		let subscriptionResult = SwiftyStoreKit.verifySubscription(
			type: .autoRenewable,
			productId: subscription.rawValue,
			inReceipt: receiptInfo,
			validUntil: Date()
		)
		
		switch subscriptionResult {
		case .purchased(expiryDate: let expiryDate, items: let receiptItems):
			print("\(receiptItems)")
			UserService.sharedService.user.expiryDate = expiryDate
			purchased()
		case .notPurchased:
			break
		case .expired(expiryDate: let expiryDate, items: let receiptItems):
			print("\(receiptItems)")
			UserService.sharedService.user.expiryDate = expiryDate
		}
	}
	
}
