//
//  ChatParser.swift
//  ChatKepper
//
//  Created by Bogachev on 9/23/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class ChatMessage {
	var date: Date
	var sender: String
	var message: String
	var isFile: Bool = false
	
	init(date: Date, sender: String, message: String, isFile: Bool = false) {
		self.date = date
		self.sender = sender
		self.message = message
		self.isFile = isFile
	}
	
	convenience init?(message: MMessage) {
		guard let mobDate = message.mobDate, let mobSender = message.mobSender, let mobMessage = message.mobMessage else {
			return nil
		}
		self.init(date: mobDate as Date, sender: mobSender, message: mobMessage, isFile: message.rlsFile != nil)
	}
	
	convenience init?(json: [String : Any]) {
		guard let dateString = json["date"] as? String, let sender = json["sender"] as? String, let message = json["message"] as? String, let isFile = json["isFile"] as? Bool else { return nil }
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM/dd/yy, HH:mm:ss"
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		guard let date = dateFormatter.date(from: dateString) else { return nil }
		
		self.init(date: date, sender: sender, message: message, isFile: isFile)
	}
	
	func toJSON() -> [String : Any] {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM/dd/yy, HH:mm:ss"
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		let dateString = dateFormatter.string(from: self.date)
		
		let dictionary = ["date": dateString, "sender": self.sender, "message": self.message, "isFile": self.isFile] as [String : Any]
		return dictionary
	}
}

class ChatParser {
	
	static func parse(chatSourceFolderURL: URL, completion: (_ charName: String?,_ chatMessages: [ChatMessage]) -> ()) {
		
		var chatName: String?
		var chatMessages = [ChatMessage]()
		
		// chat name
		let fileName = chatSourceFolderURL.lastPathComponent
//		fileName.components(separatedBy: ".").first
		if let name = extracеChatNameFromFileName(fileName: fileName) {
			chatName = name
		} else {
			chatName = FileService.sharedInstance.verifyFileName(in: FileService.sharedInstance.mainDirectoryURL, name: "Untitled chat")
		}
		
		//conversation
		
		if let contentsOfDirectory = try? FileManager.default.contentsOfDirectory(at: chatSourceFolderURL, includingPropertiesForKeys: nil, options: []), let textURL =
			contentsOfDirectory.first(where: { $0.lastPathComponent.contains(".txt") }) {
			
			chatMessages = extractConversation(textSourceFileURL: textURL)

		}
		
		completion(chatName, chatMessages)
	}
	
	private static func extracеChatNameFromFileName(fileName: String) -> String? {
		var chatName = fileName.components(separatedBy: ".").first ?? fileName
		chatName = chatName.replacingOccurrences(of: "WhatsApp Chat - ", with: "")
		
		let pattern = "\\(([0-9]){1,2}\\)"
		if let regex = try? NSRegularExpression(pattern: pattern) {
			let stringLength = chatName.utf16.count
			if let lastMatch = regex.matches(in: chatName, range: NSMakeRange(0, stringLength)).last {
				if lastMatch.range.contains(stringLength - 1) {
					(chatName as NSString).replacingCharacters(in: lastMatch.range, with: "")
				}
			}
		}
			
		return chatName
	}
	
	private static func extractConversation(textSourceFileURL: URL) -> [ChatMessage] {
		guard let conversationSourceString = try? String(contentsOfFile: textSourceFileURL.path, encoding: String.Encoding.utf8) else { return []}
		print("\(conversationSourceString)")
		
		var postDate: Date?
		var sender: String?
		var message: String?
//		var isFile: Bool = false
		var chatMessage: ChatMessage?
		var chatMessages = [ChatMessage]()
		
		conversationSourceString.components(separatedBy: .newlines).forEach { (string) in
			var lineString = string
			
			let types: NSTextCheckingResult.CheckingType = [.date]
			let detector = try? NSDataDetector(types: types.rawValue)
			
			if let result = detector?.firstMatch(in: lineString, range: NSMakeRange(0, lineString.utf16.count)), result.range.contains(0) == true { //this is a new line
				
				//read date
				let dateString = (lineString as NSString).substring(with: result.range)
				let dateFormatter = DateFormatter()
				dateFormatter.dateFormat = "MM/dd/yy, HH:mm:ss"
				dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
				postDate = dateFormatter.date(from: dateString)
				
				if postDate == nil {
					dateFormatter.dateFormat = "MM/dd/yy, HH:mm:ss a"
					postDate = dateFormatter.date(from: dateString)
				}
				
				lineString = (lineString as NSString).replacingCharacters(in: result.range, with: "")
				
				//read nikname
				
				let components = lineString.components(separatedBy: ": ")
				
				if components.count >= 3 {
					if let first = components.first, first.isEmpty == true {
						
						//				let pattern = ":\\s([a-zA-Z0-9_]){1,}\\:\\s"
						//				if let regex = try? NSRegularExpression(pattern: pattern) {
						//
						//					let stringLength = lineString.utf16.count
						//					if let firstMatch = regex.firstMatch(in: lineString, range: NSMakeRange(0, stringLength)) {
						//						if firstMatch.range.contains(0) {
						
//						var senderNameString = components[1] // (lineString as NSString).substring(with: firstMatch.range)
						
//						let preffixRange = senderNameString.startIndex..<senderNameString.index(senderNameString.startIndex, offsetBy: 2)
//						senderNameString.removeSubrange(preffixRange)
//
//						let postfixRange = senderNameString.index(senderNameString.endIndex, offsetBy: -2)..<senderNameString.endIndex
//						senderNameString.removeSubrange(postfixRange)
						
						sender = components[1]
						
						lineString = (lineString as NSString).replacingOccurrences(of: ": \(components[1]): ", with: "")
						
						//						}
						//					}
						//
						//				}
						
					}
				}
				
				if sender == nil, chatMessage == nil { //first line
					return
				}
				
				//read message
				
				message = lineString
				
				if let postDate = postDate, let sender = sender, var message = message {
					
					if message.contains("<‎attached>") {
						message = message.replacingOccurrences(of: " <‎attached>", with: "")
//						isFile = true
					}
					
					if message.contains("<‎image omitted>") {
						message = "Sorry, image omitted."
//						isFile = true
					}
					
					let newMessage = ChatMessage(date: postDate, sender: sender, message: message)
					chatMessage = newMessage
					chatMessages.append(newMessage)
				}
				
			} else { //this is continuation
				
				guard !lineString.isEmpty else { return }
				
				if message != nil {
					message! += "/n\(lineString)"
				} else {
					message = lineString
				}
				
				if let chatMessage = chatMessage, let message = message { //update multi-line message
					chatMessage.message = message
				}
			}
		}
		
		return chatMessages
	}
}
