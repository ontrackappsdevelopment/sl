//
//  LocationManager.swift
//  SmartLock
//
//  Created by Bogachev on 3/9/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
  
  static let sharedInstance = LocationManager()
  
  var locationManager: CLLocationManager!
  
  var alert:UIAlertController!
  
  var compleation: ((_ coordinate: CLLocation?) -> ())?
  
  override init() {
    super.init()
    
    DispatchQueue.main.async {
      self.locationManager = CLLocationManager()
      self.locationManager?.delegate = self
    }
  }
  
//  func enableLocationService() {
//    self.locationManager.startUpdatingLocation()
//  }
//
//  func enableLocationService() {
//    CLLocationManager.disa
//  }
  
  func requestWhenInUseAuthorization() {
    
    switch CLLocationManager.authorizationStatus() {
    case .denied, .restricted:
      
      alert = UIAlertController(title: "", message: "Turn On Location Service to Allow ChatKeeper to Determine Your Location", preferredStyle: .alert)
      
      alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (_) in
        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
      }))
      
      alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
      }))
      
      DispatchQueue.main.async {
		
		if let visibleViewController = BaseViewController.visibleViewController {
			
			if UIDevice.current.isIpad() == true {
				
				if let popoverPresentationController = self.alert.popoverPresentationController {
					popoverPresentationController.sourceView = visibleViewController.view
					popoverPresentationController.sourceRect = CGRect(x: visibleViewController.view.bounds.size.width / 2.0,
					                                                  y: visibleViewController.view.bounds.size.height / 2.0,
					                                                  width: visibleViewController.view.bounds.size.width / 2.0,
					                                                  height: visibleViewController.view.bounds.size.height / 2.0)
				}
				
			}
			
			visibleViewController.present(self.alert, animated: true, completion: nil)
			
		}
		
//        BaseViewController.visibleViewController!.present(self.alert, animated: true, completion: nil)
		
      }
      
    case .notDetermined:
      DispatchQueue.main.async {
        self.locationManager.requestWhenInUseAuthorization()
      }
    default: break
      
    }
    
  }

  func currentLocation(compleation: ((_ coordinate: CLLocation?) -> ())?) {
    
//    locationManager.delegate = self
    
    if let location = locationManager.location {
      
      compleation!(location)
      
    } else {
      
      compleation!(nil)
      
    }
    
//    switch CLLocationManager.authorizationStatus() {
//    case .authorizedAlways, .authorizedWhenInUse:
//      
//      self.compleation = compleation
//      
//      DispatchQueue.main.async {
//       
//        self.locationManager.requestLocation()
//        
//      }
//      
//    default:
//      
//      if let action = compleation {
//        action(nil)
//      }
//      
//    }
  }
  
  //MARK: - CLLocationManagerDelegate
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
//    if let action = compleation, let location = locations.first {
//      action(location)
//    }
    
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("\(error)")
  }

}
