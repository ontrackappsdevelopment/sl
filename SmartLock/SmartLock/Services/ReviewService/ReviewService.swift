//
//  ReviewService.swift
//  SmartLock
//
//  Created by Bogachev on 6/10/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import StoreKit
import YandexMobileMetrica

class ReviewService {

	private static let instance = ReviewService()
	
	private var notShowReview = false
	
	private var alertIsShown = false
	
	init() {
		
	}
	
	enum AppVersionType {
		case noReview
		case new
		case update
	}
	
	enum ReviewUserType: String {
		case new = "newUser"
		case old = "oldUser"
	}
	
//	static let launchReviewAfter = 4
	
	static var reviewObject: ReviewObject?
	
	static func setupUserSession(type: AppVersionType) {
	
		switch type {
		case .new:
			reviewUserType = .new
			setupReviewLaunchCounter()
			
		case .update:
			reviewUserType = .old
			setupReviewLaunchCounter()
			
		case .noReview:
			instance.notShowReview = true
		}
		
	}
	
	static var reviewLaunchCounter: Int? {
		
		get {
			return UserDefaults.standard.integer(forKey: "ReviewLaunchCounter")
		}
		
		set(newValue) {
			
			if newValue == nil {
				UserDefaults.standard.removeObject(forKey: "ReviewLaunchCounter")
			} else {
				UserDefaults.standard.set(newValue, forKey: "ReviewLaunchCounter")
			}
			
			UserDefaults.standard.synchronize()
		}
		
	}

	static var reviewUserType: ReviewUserType? {
		
		get {
			if let value = UserDefaults.standard.object(forKey: "ReviewUserType") as? String, let type = ReviewUserType(rawValue: value) {
				return type
			} else {
				return nil
			}
		}
		
		set(newValue) {
			UserDefaults.standard.set(newValue?.rawValue, forKey: "ReviewUserType")
			UserDefaults.standard.synchronize()
		}
		
	}
	
	static func setupReviewLaunchCounter() {
		reviewLaunchCounter = 0
	}
	
	static func removeReviewLaunchCounter() {
		UserDefaults.standard.removeObject(forKey: "ReviewLaunchCounter")
		UserDefaults.standard.synchronize()
	}
	
	static var reviewCanceledDate: Date? {
		get {
			return UserDefaults.standard.value(forKey: "ReviewCanceledDate") as? Date
		}
		set(newValue) {
			UserDefaults.standard.set(newValue, forKey: "ReviewCanceledDate")
			UserDefaults.standard.synchronize()
		}
	}
	
	static func showReviewControllerIfNeeded(presenter: UIViewController) {
		
		var day: Int?
		let limitDays = 2 //User.daysOfFreeTrialPeriod == 0 ? 2 : User.daysOfFreeTrialPeriod
		
		guard reviewUserType != nil else {
			return
		}
		
		if let date = UserDefaults.standard.value(forKey: "AppVersionDate") as? Date {
			
			day = Calendar.current.dateComponents(Set([Calendar.Component.day]), from: date, to: Date()).day
			
			if let days = Calendar.current.dateComponents(Set([Calendar.Component.day]), from: date, to: Date()).day, (days < 0 || days >= limitDays) {
				return
			}
			
		} else {
			return
		}
		
		featchReviewData {
			
			guard let reviewObject = reviewObject, reviewObject.isActive == true  else {
				
				if let day = day, day >= 0 {
					
					if #available(iOS 10.3, *) {
						YMMYandexMetrica.reportEvent("Did Show Stars", parameters: nil, onFailure: nil)
						SKStoreReviewController.requestReview()
					} else {
						YMMYandexMetrica.reportEvent("Did Show Review", parameters: nil, onFailure: nil)
						UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=1288980595&action=write-review")!)
					}
					
					reviewLaunchCounter = nil
					reviewUserType  = nil
					
				}
				
				return
			}
			
			if let day = day {
				
				if day == 0 {
				
					if let counter = reviewLaunchCounter, counter < 1 {
						
						if let date = reviewCanceledDate, Calendar.current.isDateInToday(date) {
							return
						}
						
						if let message = reviewObject.message, let actionTitle = reviewObject.ok_button, let cancelTitle = reviewObject.cancel_button {
							
							if instance.alertIsShown == false, reviewUserType != nil {
							
								instance.alertIsShown = true
								
								_ = AlertView.shows(inViewController: presenter, text: message, actionTitle: actionTitle, cancelTitle: cancelTitle, action: {
									
									YMMYandexMetrica.reportEvent("Did Show Review", parameters: nil, onFailure: nil)
									UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=1288980595&action=write-review")!)
									
									reviewLaunchCounter = nil
									reviewUserType  = nil
									
								}, cancel: {
									
									YMMYandexMetrica.reportEvent("Did Press Cancel Review", parameters: nil, onFailure: nil)
									reviewCanceledDate = Date()
									
								})
									
							}
							
						}
						
					}
					
				}

				if day > 0 {
					
					if reviewUserType != nil {
					
						if #available(iOS 10.3, *) {
							YMMYandexMetrica.reportEvent("Did Show Stars", parameters: nil, onFailure: nil)
							SKStoreReviewController.requestReview()
						} else {
							YMMYandexMetrica.reportEvent("Did Show Review", parameters: nil, onFailure: nil)
							UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=1288980595&action=write-review")!)
						}
						
						reviewLaunchCounter = nil
						reviewUserType  = nil
						
					}
					
				}

			}
			
		}
	}
	
	static func featchReviewData(compleation: @escaping () -> ()) {
		
		guard let userType = reviewUserType else { return }
		
		reviewObject = nil
		
		NetworkManager.Requests.getReviewData(typeUser: userType.rawValue) { (object, errorMessage) in
			
			if let object = object {
			
				reviewObject = object
				
			}
			
			compleation()
			
		}
		
	}
	
}
