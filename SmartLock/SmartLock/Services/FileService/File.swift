//
//  File.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AVFoundation
import Accelerate
import CoreGraphics

enum FileType {
	case unknown
	case image
	case video
	case directory
	case archive
	case audio
}

class File {
	
	static var fileManager: FileManager {
		return FileManager.default
	}
	
	static let thambnailQueue = DispatchQueue(label: "ThambnailQueue")
	
	internal var url: URL
	
	internal var fileType = FileType.unknown
	
	internal var contentOfDirectory = [File]()
	
	var modificationDate: Date?
	
	var size: Int64?
	
	var level: Int = 0
	
	var isSecret: Bool {		
		if let album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: relativePath) {
			return LockType(rawValue: Int(album.mobLockType)) != .undefined
		}
		return false
	}
	
	internal var path: String {
		return url.path
	}
	
	internal var relativePath: String {
		let path = url.path.replacingOccurrences(of: "/private\(FileService.documentsDirectory.path)", with: "")
		return path.replacingOccurrences(of: FileService.documentsDirectory.path, with: "")
	}
	
	internal var name: String {
		return url.lastPathComponent
	}
	
	init(url: URL) {
		
		self.url = url
		
		if url.isDir {
			
			self.fileType = .directory
			
			calculateFileLavel()
			
		} else if url.isImage {
			
			self.fileType = .image
			
		} else if url.isVideo {
			
			self.fileType = .video
			
		} else if url.isArchive {
			
			self.fileType = .archive
			
		} else if url.isAudio {
			
			self.fileType = .audio
			
		}
		
		do {
			let fileAttributes = try FileManager.default.attributesOfItem(atPath: url.path)
			modificationDate = fileAttributes[FileAttributeKey.creationDate] as? Date
			size = fileAttributes[FileAttributeKey.size] as? Int64
		} catch let error {
			print("Error getting file modification attribute date: \(error.localizedDescription)")
		}
		
		calculateFileLavel()
		
	}
	
	func getSize() -> UInt64 {
		do {
			return try findSize(path: url.path)
		} catch  {			
			return 0
		}
	}
	
	convenience init(relativePath: String) {
		var path = relativePath
		_ = path.characters.removeFirst()
		let url = FileService.documentsDirectory.appendingPathComponent(path)
		self.init(url: url)
	}
	
}

//MARK: Directory

extension File {
	
	func calculateFileLavel() {
		level = url.path.components(separatedBy: "/").count
	}
	
	func fetchContent() {
		
		do {
			
			contentOfDirectory = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: []).map({ (itemURL) -> File in
				
				return File(url: itemURL)
				
			}).filter( { $0.url.lastPathComponent != ".DS_Store" } )
			
//			filter({ $0.fileType != .unknown }).sorted(by: { (file1, file2) -> Bool in
//
//				if file1.fileType == .directory && file2.fileType == .directory {
//
////					switch file1.modificationDate!.compare(file2.modificationDate!) {
////					case .orderedAscending: return true
////					case .orderedSame:
//						if let album1 = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: file1.relativePath),
//							let album2 = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: file2.relativePath) {
//							return album1.mobDate < album2.mobDate
//						}
//
////					default:
//						return false
////					}
//
////					return file1.modificationDate!.compare(file2.modificationDate!) == .orderedAscending
//
//				}
//
//				if file1.fileType == .directory && file2.fileType != .directory {
//
//					return true
//
//				}
//
//				if file1.fileType != .directory && file2.fileType != .directory {
//
////					switch file1.modificationDate!.compare(file2.modificationDate!) {
////					case .orderedAscending: return true
////					case .orderedSame:
//						if let file1 = MFile.mr_findFirst(byAttribute: "mobPath", withValue: file1.relativePath),
//							let file2 = MFile.mr_findFirst(byAttribute: "mobPath", withValue: file2.relativePath) {
//							return file1.mobDate < file2.mobDate
//						}
//
////					default:
//						return false
////					}
//
////					return file1.modificationDate!.compare(file2.modificationDate!) == .orderedAscending
//
//				}
//
//				return false
//
//			})
			
		} catch let error as NSError {
			
			print("\(error)")
			
		}
		
	}
		
	func getFiles() -> [File] {
		
		var files = [File]()
		
		if fileType != .directory {
			
			files.append(self)
			
		} else {
			
			let content = FileService.fetchAlbumContent(relativePath: relativePath)
			
			for item in content {
				
				let relativePath = item.value(forKey: "mobPath") as? String ?? ""
				let file = File(relativePath: relativePath)
				
				if file.fileType == .directory {
					
					files.append(contentsOf: file.getFiles())
					
				} else {
					
					files.append(file)
					
				}
				
			}
			
		}
		
		return files
		
	}
	
	func getFolders() -> [(File)] {
		
		var files = [File]()
		
		if fileType == .directory {
			
			files.append(self)
			
			let subAlbums = FileService.fetchSubAlbums(relativePath: relativePath)
			
			for album in subAlbums {
				
				let relativePath = album.value(forKey: "mobPath") as? String ?? ""
				let folder = File(relativePath: relativePath)
					
				files.append(contentsOf: folder.getFolders())
				
			}
			
		}
		
		return files
		
	}
	
}

extension File {
	
	static func createDirectory(path relativePath: String, compleation: (_ file: File?, _ error: NSError?) -> ()) {
		
		var file: File?
		var error: NSError?
		
		let url = FileService.documentsDirectory.appendingPathComponent(relativePath)
		
		if fileManager.fileExists(atPath: url.path) == true {
			
			file = File(url: url)
			
		} else {
			
			do {
				
				try fileManager.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
				
				file = File(url: url)
				
			} catch let caughtError as NSError {
				
				error = caughtError
				
			}
			
		}
		
		compleation(file, error)
		
	}
	
	static func createDirectory(atURL url: URL, compleation: (_ file: File?, _ error: NSError?) -> ()) {
		
		var file: File?
		var error: NSError?
		
		if fileManager.fileExists(atPath: url.path) == true {
			
			file = File(url: url)
			
		} else {
			
			do {
				
				try fileManager.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
				
				file = File(url: url)
				
			} catch let caughtError as NSError {
				
				error = caughtError
				
			}
			
		}
		
		compleation(file, error)
		
	}
	
	func moveTo(to toFile: File) {
		
		let context = NSManagedObjectContext.mr_default()
		
		let oldPath = relativePath
		
		do {
						
			let name = FileService.sharedInstance.verifyFileName(in: toFile, name: url.lastPathComponent)
			let newURL = toFile.url.appendingPathComponent(name)
			
			try File.fileManager.moveItem(at: url, to: newURL)
			
			url = newURL
			
			let newPath = relativePath
			
			if fileType == .directory {
				
				if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", oldPath), in: context) {
					
					context.mr_save(blockAndWait: { (localContext) in
					
						if let localAlbum = album.mr_(in: localContext) {
							localAlbum.moveTo(path: newPath, context: localContext)
						}
						
					})
					
				} else {
					
				}
			
			} else {
				
				if let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", oldPath), in: context) {

					context.mr_save(blockAndWait: { (localContext) in
						if let localFile = file.mr_(in: localContext) {
							localFile.moveTo(path: newPath, context: localContext)
						}
					})
					
				} else {
					
					context.mr_save(blockAndWait: { (localContext) in
						if let newFile = MFile.create(with: self.relativePath, context: localContext) {
							newFile.moveTo(path: newPath, context: localContext)
						}
						
					})

				}
			}
			
		} catch let error {
			
			print("\(error)")
			
		}
		
	}
	
	func moveWitoutLog(relativePath: String) {
		
		let newFile = File(relativePath: relativePath)
		let newURL = newFile.url
		let oldPath = self.relativePath
		
		do {
			try File.fileManager.moveItem(at: url, to: newURL)
		} catch let error {
			print("\(error)")
		}
		
		let context = NSManagedObjectContext.mr_default()
		
		if fileType == .directory {
			if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", oldPath), in: context) {
				context.mr_save(blockAndWait: { (localContext) in
					if let localAlbum = album.mr_(in: localContext) {
						localAlbum.moveToWitoutLog(path: relativePath, context: localContext)
					}
				})
			}
			
		} else {
			if let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", oldPath), in: context) {
				context.mr_save(blockAndWait: { (localContext) in
					if let localFile = file.mr_(in: localContext) {
						localFile.moveToWithouLog(path: relativePath, context: localContext)
					}
				})
			}
		}
		
	}
	
	func removeWitoutLog() {
		
		do {
			let fileType = self.fileType
			let relativePath = self.relativePath
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				if fileType == .directory {
					MAlbum.deleteWithoutLog(with: relativePath, context: localContext)
				} else {
					MFile.deleteWithouLog(with: relativePath, context: localContext)
				}
			})
			
			try File.fileManager.removeItem(at: url)
			
		} catch let error {
			
			print("\(error)")
			
		}
		
	}
	
	func remove() {
		
		do {
			let fileType = self.fileType
			let relativePath = self.relativePath

			removeThambnail()
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				if fileType == .directory {
					MAlbum.delete(with: relativePath, context: localContext)
				} else {
					MFile.delete(with: relativePath, context: localContext)
				}
			})
			
			try File.fileManager.removeItem(at: url)
			
		} catch let error {
			
			print("\(error)")
			
		}
		
	}
	
	func rename(name: String) {
		
		do {
			
			let newURL = url.deletingLastPathComponent().appendingPathComponent(name)
			
			try File.fileManager.moveItem(at: url, to: newURL)
			
			url = newURL
			
		} catch let error {
			
			print("\(error)")
			
		}
	}
	
}

extension File {
	
	var isBlocked: Bool {
		
		if let restoreLogs = MRestoreLog.mr_findAll() as? [MRestoreLog] {
			if restoreLogs.first(where: { (log) -> Bool in
				guard let path = log.mobPath else { return false }
				return "\(path)/".contains("\(self.relativePath)/") || "\(self.relativePath)/".contains("\(path)/")}) != nil {
				return true
			}
		}

		if let logs = MLog.mr_findAll(with: NSPredicate(format: "mobBackuping == true")) as? [MLog] {
			if logs.first(where: { (log) -> Bool in
				guard let path = log.mobPath else { return false }
				return "\(path)/".contains("\(self.relativePath)/") || "\(self.relativePath)/".contains("\(path)/")}) != nil {
				return true
			}
		}
		
		return false
		
	}
	
}
