//
//  FileService+Directory.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension FileService {
	
	static var documentsDirectory: URL {
		return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
	}
	
	var rootDirectory: URL {
		
		if isDecoyEnabled == true {
			
			return decoyInDirectoryURL
			
		} else {
			
			return FileService.documentsDirectory
			
		}
		
	}

	static var containerURL: URL? {
		return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.chkeeper.free")?.appendingPathComponent("Extension")
	}
	
	static var extensionDirectoryURL: URL {
		return FileService.sharedInstance.rootDirectory.appendingPathComponent(extensionDirectoryName)
	}
	
	static var archivesDirectoryURL: URL {
		return FileService.sharedInstance.rootDirectory.appendingPathComponent(archivesDirectoryName)
	}
	
	static var unpackedDirectoryURL: URL {
		return FileService.sharedInstance.rootDirectory.appendingPathComponent(unpackedDirectoryName)
	}
	
	static func getRelativePath(url: URL) -> String {
		let path = url.path.replacingOccurrences(of: "/private\(FileService.documentsDirectory.path)", with: "")
		return path.replacingOccurrences(of: FileService.documentsDirectory.path, with: "")
	}
	
	var mainDirectoryURL: URL {
		return rootDirectory.appendingPathComponent(mainDirectoryName)
	}
	
	var uploadServiceDirectoryURL: URL {
		return FileService.documentsDirectory.appendingPathComponent(uploadServiceDirectoryName)
	}
	
	var breakInDirectoryURL: URL {
		return FileService.documentsDirectory.appendingPathComponent(breakInDirectoryName)
	}
	
	var decoyInDirectoryURL: URL {
		return FileService.documentsDirectory.appendingPathComponent(decoyDirectoryName)
	}	
	
	var albumsDirectory: URL {
		return FileService.documentsDirectory.appendingPathComponent(mainDirectoryName)
	}

	static func getNewDirectoryName(inDirectory directoryURL: URL, name: String = "Untitled album", index: Int = 0) -> String {		
		let newName = index == 0 ? name : "\(name) (\(index))"
		let path = directoryURL.appendingPathComponent(newName).path
		if !FileService.sharedInstance.fileManager.fileExists(atPath: path) {
			return newName
		} else {
			return getNewDirectoryName(inDirectory: directoryURL, name: name, index: index + 1)
		}
	}
	
	func getNewDirectoryName(name: String = "Untitled album", index: Int = 0) -> String {
		
		let newName = index == 0 ? name : "\(name) (\(index))"
		
		let path = currentDirectory.url.appendingPathComponent(newName).path
		
		if !fileManager.fileExists(atPath: path) {
			
			return newName
			
		} else {
			
			return getNewDirectoryName(name: name, index: index + 1)
			
		}
		
	}
	
	func getNewFileURLInCurrentDirectory(extention: String) -> URL {
		return getNewFileURL(directory: currentDirectory, extention: extention)
	}
	
	func getNewFileURL(directory: File, extention: String) -> URL {
		return directory.url.appendingPathComponent("\(newFileName).\(extention)")
	}
	
	func clearBreakInFolder() {
		
		let file = File(url: breakInDirectoryURL)
		
		file.fetchContent()
		
		file.contentOfDirectory.forEach( { $0.remove() } )
		
	}
	
	func createUnpackedDirectoryIfNeeded() {
		File.createDirectory(atURL: FileService.unpackedDirectoryURL) { (file, error) in
			if let error = error {
				print("createUnpackedDirectoryIfNeeded: \(error)")
			}
		}
	}
	
	func createDirectory(name: String) -> File? {
		
		let newDirectoryURL = currentDirectory.url.appendingPathComponent(name)
		
		var directory: File?
		
		File.createDirectory(atURL: newDirectoryURL) { (file, error) in
			
			if error == nil {
				
				directory = file
				
			}
			
		}
		
//		directory?.removefolderThambnail()
		return directory
		
	}
	
	func createTmpDirectoryIfNeeded() {
		
		let tmpURL = FileService.documentsDirectory.appendingPathComponent("thumbnails")
		
		File.createDirectory(atURL: tmpURL) { (file, error) in }
		
	}
	
	func createMainDirectoryIfNeeded() {
		
		File.createDirectory(atURL: mainDirectoryURL) { (file, error) in
			
			if let error = error {
				print("createRootDirectoryIfNeeded: \(error)")
			}
			
			rootMediaDirectory = file
			
		}
		
	}
	
	func createUploadServiceDirectoryIfNeeded() {
		
		File.createDirectory(atURL: uploadServiceDirectoryURL) { (file, error) in
			
			if let error = error {
				print("createUploadServiceDirectoryIfNeeded: \(error)")
			}
			
			uploadServiceDirectory = file
			
		}
		
	}
	
	func createBreakInDirectoryIfNeeded() {
		
		File.createDirectory(atURL: breakInDirectoryURL) { (file, error) in
			
			if let error = error {
				print("createBreakInDirectoryIfNeeded: \(error)")
			}
			
		}
		
	}
	
	func createDecoyDirectoryIfNeeded() {
		
		File.createDirectory(atURL: decoyInDirectoryURL) { (file, error) in
			
			if let error = error {
				print("createDecoyDirectoryIfNeeded: \(error)")
			}
			
		}
		
	}
	
	func setCurrentDirectoryToParent() {
		
		if currentDirectory.url != rootMediaDirectory?.url {
			
			let parentURL = currentDirectory.url.deletingLastPathComponent()
			
			currentDirectory = File(url: parentURL)
			
		}
		
	}
	
	func fetchCurrentDirectoryContent() {
		currentDirectory.fetchContent()
	}
	
	func createFoldersForPath(path: String) {
		
		var components = path.components(separatedBy: "/")
		
		components.removeFirst() // "/"
		components.removeFirst() // "albums"
		components.removeLast()  // "file.extension"
		
		var parent = File(url: FileService.sharedInstance.albumsDirectory)
		components.forEach { (component) in
			parent = File(url: parent.url.appendingPathComponent(component))
			File.createDirectory(atURL: parent.url, compleation: { (file, error) in })
		}
		
		
	}
}
