//
//  File+Thumbnail.swift
//  SmartLock
//
//  Created by Bogachev on 9/1/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AVFoundation
import Accelerate
import CoreGraphics

extension File {
	
	static func getThumbnailPath(fileName: String) -> String {
		return FileService.documentsDirectory.appendingPathComponent("thumbnails/\(fileName)").path
	}

	func saveFileThumbnail() {
		if let fileObject = MFile.mr_findFirst(byAttribute: "mobPath", withValue: relativePath), let mobThumbnail = fileObject.mobThumbnail {
			_ = getFileThumbnailImage(name: mobThumbnail)
		}
	}
	
	func getFileThumbnailSourceImage() -> UIImage? {

		switch fileType {
		case .image:

			if let image = UIImage(contentsOfFile: self.path),
				let sourceImage = image.fixOrientationOfImage() {
				return sourceImage
			}
			
		case .video:
			
			let asset = AVURLAsset(url: url, options: nil)
			let imageGenerator = AVAssetImageGenerator(asset: asset)
			imageGenerator.appliesPreferredTrackTransform = true
			
			if let image = try? imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil) {
				let sourceImage = UIImage(cgImage: image)
				return sourceImage
			}
			
		default:
			return nil
		}
		
		return nil
	}

	func getFileThumbnailImage(name: String) -> UIImage? {
		let size = rint((UIScreen.main.bounds.width - (PhotosViewController.columnCount - 1)) / PhotosViewController.columnCount)
		return getPhotoThumbnailImage(name: name, size: size)
	}
	
	func getPhotoThumbnailImage(name: String, size: CGFloat) -> UIImage? {
		let thumbnailImageFileName = File.getThumbnailPath(fileName: name)
		if File.fileManager.fileExists(atPath: thumbnailImageFileName) {
			return UIImage(contentsOfFile: thumbnailImageFileName)
		}
		
		if let image = UIImage(contentsOfFile: self.path),
			let sourceImage = image.fixOrientationOfImage() {
			return generateThumbnailImage(sourceImage: sourceImage, thumbnailImageFileName: thumbnailImageFileName, size: size)
		}
		
		return nil
	}
	
	func getVideoThumbnailImage(name: String, size: CGFloat) -> UIImage? {
		let thumbnailImageFileName = File.getThumbnailPath(fileName: name)
		if File.fileManager.fileExists(atPath: thumbnailImageFileName) {
			return UIImage(contentsOfFile: thumbnailImageFileName)
		}
		
		let asset = AVURLAsset(url: url, options: nil)
		let imageGenerator = AVAssetImageGenerator(asset: asset)
		imageGenerator.appliesPreferredTrackTransform = true
		
		if let image = try? imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil) {
			let sourceImage = UIImage(cgImage: image)
			return generateThumbnailImage(sourceImage: sourceImage, thumbnailImageFileName: thumbnailImageFileName, size: size)
		}
		
		return nil
	}
	
	func generateThumbnailImage(sourceImage: UIImage, thumbnailImageFileName: String, size: CGFloat) -> UIImage? {
		
//		File.thambnailQueue.async {
//			autoreleasepool(invoking: {
		
				var contextBounds = CGRect.zero
				
				let width = size
				let height = size * sourceImage.size.height / sourceImage.size.width
				
				contextBounds.size = CGSize(width: width, height: height)
				
				var scaledImage:UIImage?
				UIGraphicsBeginImageContextWithOptions(contextBounds.size, false, 1.0)
				sourceImage.draw(in: contextBounds)
				scaledImage = UIGraphicsGetImageFromCurrentImageContext()
				UIGraphicsEndImageContext();
				
				if let scaledImage = scaledImage {
					let scaledImageJPEGRepresentation = UIImageJPEGRepresentation(scaledImage, 1.0);
					do {
						try scaledImageJPEGRepresentation?.write(to: URL(fileURLWithPath: thumbnailImageFileName), options: [Data.WritingOptions.atomic])
					} catch {
					}
					
					return scaledImage
				}
		
		return nil
				
//			})
//
//		}
		
	}
	
	func getAlbumThumbnailImage(name: String, size: CGSize, update: Bool = false) -> UIImage? {
		
		var imageSize = size
		var drawn = false
		
		let thumbnailImageFileName = File.getThumbnailPath(fileName: name)
		if File.fileManager.fileExists(atPath: thumbnailImageFileName) {
			if let image = UIImage(contentsOfFile: thumbnailImageFileName) {
				
				if size == CGSize.zero {
					imageSize = CGSize(width: 95, height: 67.5)
				}
				
				if update == false {
					return image
				}
			}
		}
		
		print("getAlbumThumbnailImage:\(relativePath)")
	
		let contentOfDirectory = FileService.fetchAlbumFiles(relativePath: relativePath)
		let count = contentOfDirectory.count
		guard count > 0 else {
			do {
				try File.fileManager.removeItem(atPath: thumbnailImageFileName)
			} catch  {
    
			}
			
			return nil
		}
		
		UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
		let context = UIGraphicsGetCurrentContext()
		UIColor.black.setFill()
		context?.fill(CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
		
		switch count {
		case 1,2,3:
			
			let object = contentOfDirectory[0]
			let relativePath = object.value(forKey: "mobPath") as? String ?? ""
			let file = File(relativePath: relativePath)
			
			if let image = file.getFileThumbnailSourceImage() {
				let imageBounds = CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height)
				if let image = image.imageWithSize(size: imageSize) {
					image.draw(in: imageBounds)
					
					drawn = true
				}
			}
			
		default:
			
			let object1 = contentOfDirectory[0]
			let object2 = contentOfDirectory[1]
			let object3 = contentOfDirectory[2]
			let object4 = contentOfDirectory[3]
			
			if	let relativePath1 = object1.value(forKey: "mobPath") as? String,
				let relativePath2 = object2.value(forKey: "mobPath") as? String,
				let relativePath3 = object3.value(forKey: "mobPath") as? String,
				let relativePath4 = object4.value(forKey: "mobPath") as? String {
				
				let file1 = File(relativePath: relativePath1)
				let file2 = File(relativePath: relativePath2)
				let file3 = File(relativePath: relativePath3)
				let file4 = File(relativePath: relativePath4)
				
				if	let image1 = file1.getFileThumbnailSourceImage(),
					let image2 = file2.getFileThumbnailSourceImage(),
					let image3 = file3.getFileThumbnailSourceImage(),
					let image4 = file4.getFileThumbnailSourceImage() {
					
					imageSize = CGSize(width: imageSize.width/2 - 1, height: imageSize.height/2 - 1)
					
					let imageBounds1 = CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height)
					let clippingPath = UIBezierPath(roundedRect: imageBounds1, cornerRadius: 3)
					
					let imageBounds2 = CGRect(x: imageSize.width + 1, y: 0, width: imageSize.width + 1, height: imageSize.height)
					clippingPath.append(UIBezierPath(roundedRect: imageBounds2, cornerRadius: 3))
					
					let imageBounds3 = CGRect(x: 0, y: imageSize.height + 1, width: imageSize.width, height: imageSize.height + 1)
					clippingPath.append(UIBezierPath(roundedRect: imageBounds3, cornerRadius: 3))
					
					let imageBounds4 = CGRect(x: imageSize.width + 1, y: imageSize.height + 1, width: imageSize.width + 1, height: imageSize.height + 1)
					clippingPath.append(UIBezierPath(roundedRect: imageBounds4, cornerRadius: 3))
					
					context?.addPath(clippingPath.cgPath)
					context?.clip()
					
					if let image = image1.imageWithSize(size: imageSize) {
						image.draw(in: imageBounds1)
					}

					if let image = image2.imageWithSize(size: imageSize) {
						image.draw(in: imageBounds2)
					}
					
					if let image = image3.imageWithSize(size: imageSize) {
						image.draw(in: imageBounds3)
					}

					if let image = image4.imageWithSize(size: imageSize) {
						image.draw(in: imageBounds4)
						drawn = true
					}
					
				}
				
			}
			
		}
		
		if let resultImage = UIGraphicsGetImageFromCurrentImageContext(), drawn == true {
			
			if let imageJPEGRepresentation = UIImageJPEGRepresentation(resultImage, 1.0) {
				
				do {
					try imageJPEGRepresentation.write(to: URL(fileURLWithPath: thumbnailImageFileName), options: [Data.WritingOptions.atomic])
				} catch let error as NSError {
					print("\(error)")
				}
				
				UIGraphicsEndImageContext()
				
				return resultImage
			}
			
		}

		return nil
		
	}
	
	func previewImageForVideo(rect: CGRect) -> UIImage? {
		let asset = AVURLAsset(url: url, options: nil)
		let imageGenerator = AVAssetImageGenerator(asset: asset)
		imageGenerator.appliesPreferredTrackTransform = true
		if let image = try? imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil) {
			let sourceImage = UIImage(cgImage: image)
			return sourceImage
		}
		return nil
	}
	
	func scaledImage(_ image: UIImage, maximumWidth: CGFloat) -> UIImage {
		let rect: CGRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
		let cgImage: CGImage = image.cgImage!.cropping(to: rect)!
		return UIImage(cgImage: cgImage, scale: image.size.width / maximumWidth, orientation: image.imageOrientation)
	}
	
	func removeThambnail() {
		
		var thumbnailImageFileName: String?
		
		if fileType == .image || fileType == .video {
			if let fileObject = MFile.mr_findFirst(byAttribute: "mobPath", withValue: relativePath), let thumbnail = fileObject.mobThumbnail  {
				thumbnailImageFileName = File.getThumbnailPath(fileName: thumbnail)
			}
		}
		
		if fileType == .directory {
			if let albumObject = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: relativePath), let thumbnail = albumObject.mobThumbnail  {
				thumbnailImageFileName = File.getThumbnailPath(fileName: thumbnail)
			}
		}
		
		if let thumbnailImageFileName = thumbnailImageFileName {
			do {
				try FileManager.default.removeItem(atPath: thumbnailImageFileName)
			} catch {
				
			}
		}
	}
	
}

extension UIImage {
	func squareImageFromImage() -> UIImage? {
		
		let imageSize = size
		
		if (imageSize.width == imageSize.height) {
			
			return self
			
		} else {
		
			var squareImage: UIImage?
			
			let smallerDimension = min(imageSize.width, imageSize.height)
			var cropRect = CGRect(x: 0, y: 0, width: smallerDimension, height: smallerDimension)
			
			if (imageSize.width <= imageSize.height) {
				cropRect.origin = CGPoint(x: 0, y: rint((imageSize.height - smallerDimension) / 2.0))
			} else {
				cropRect.origin = CGPoint(x: rint((imageSize.width - smallerDimension) / 2.0), y: 0)
			}
			
			if let cgImage = cgImage, let croppedImageRef = cgImage.cropping(to: cropRect) {
				squareImage = UIImage(cgImage: croppedImageRef)
			}

			return squareImage
			
		}
	}
}

extension UIImage {
	func imageWithSize(size:CGSize) -> UIImage? {
		var scaledImageRect = CGRect.zero
		var scaledImage: UIImage?
		
		let aspectWidth:CGFloat = size.width / self.size.width
		let aspectHeight:CGFloat = size.height / self.size.height
		let aspectRatio:CGFloat = max(aspectWidth, aspectHeight)
		
		scaledImageRect.size.width = self.size.width * aspectRatio
		scaledImageRect.size.height = self.size.height * aspectRatio
		scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
		scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
		
		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		self.draw(in: scaledImageRect)
		scaledImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return scaledImage
	}	
}
