//
//  FileService.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import Photos
import SSZipArchive
import YandexMobileMetrica

var rootFolderLevel: Int {
	return FileService.sharedInstance.rootMediaDirectory!.level
}

class FileService {
	
	//MARK: -

	static let extensionDirectoryName = "extension"
	static let archivesDirectoryName = "archives"
	static let unpackedDirectoryName = "unpacked"
	let mainDirectoryName = "albums"
	let uploadServiceDirectoryName = "uploadservice"
	let breakInDirectoryName = "BreakIn"
	let decoyDirectoryName = "Decoy"
	
	//MARK: -
	
	static let sharedInstance = FileService()
		
	//MARK: -
	
	var isDecoyEnabled = false {
		didSet {
			createMainDirectoryIfNeeded()
		}
	}
	
	var fileManager: FileManager {
		return FileManager.default
	}
	
	var rootMediaDirectory: File? {
		didSet {
			currentDirectory = rootMediaDirectory
		}
	}
	
	var uploadServiceDirectory: File?
	
	var currentDirectory: File!
	
	private init() {
		createDecoyDirectoryIfNeeded() // must be first
		createMainDirectoryIfNeeded()
		createTmpDirectoryIfNeeded()
		createUploadServiceDirectoryIfNeeded()
		createBreakInDirectoryIfNeeded()
	}	
	
	//MARK: -
	
	static func fetchAlbumContent(relativePath: String) -> [NSManagedObject] {
		
		var objects = [NSManagedObject]()
				
		objects.append(contentsOf: fetchSubAlbums(relativePath: relativePath) as [NSManagedObject])
		objects.append(contentsOf: fetchAlbumFiles(relativePath: relativePath) as [NSManagedObject])
		
//		if var albums = MAlbum.mr_findAll(with: NSPredicate(format: "mobAlbumPath = %@", "\(relativePath)")) as? [MAlbum] {
//			albums.sort(by: { $0.0.mobDate < $0.1.mobDate })
//			objects.append(contentsOf: albums as [NSManagedObject])
//		}
//		
//		if var files = MFile.mr_findAll(with: NSPredicate(format: "mobFilePath = %@", "\(relativePath)")) as? [MFile] {
//			files.sort(by: { $0.0.mobDate < $0.1.mobDate })
//			objects.append(contentsOf: files as [NSManagedObject])
//		}
		
		return objects
	}
	
	static func fetchAlbumFiles(relativePath: String) -> [MFile] {
		
		var objects = [MFile]()

		if var files = MFile.mr_findAll(with: NSPredicate(format: "mobFilePath = %@", "\(relativePath)")) as? [MFile] {
			files = files.filter({ (savedFile) -> Bool in
				if let mobPath = savedFile.mobPath {
					return mobPath.isImage || mobPath.isVideo
				}
				return false
			})
			
			files.sort(by: { $0.0.mobDate < $0.1.mobDate })
			objects.append(contentsOf: files)
		}
		
		return objects
	}
	
	static func fetchSubAlbums(relativePath: String) -> [MAlbum] {
		
		var objects = [MAlbum]()

		if var albums = MAlbum.mr_findAll(with: NSPredicate(format: "mobAlbumPath = %@", "\(relativePath)")) as? [MAlbum] {
			albums.sort(by: { $0.0.mobDate < $0.1.mobDate })
			objects.append(contentsOf: albums)
		}
		
		return objects
	}
	
	//MARK: - App Share Extention
	
	func checkAndUploadExtentionFiles(start: @escaping () -> (), progress: @escaping (_ progress: Float) -> (), competion: @escaping () -> ()) {
		let group = DispatchGroup()
//		var chatSourceURL = [URL]()
		
		DispatchQueue.global().async {
			guard let containerURL = FileService.containerURL else { return }
			
			let files = try? FileManager.default.contentsOfDirectory(at: containerURL, includingPropertiesForKeys: nil, options: [])
				.filter( { $0.lastPathComponent != ".DS_Store" } )
				.map({ (itemURL) -> File in return File(url: itemURL) })
				.filter({ $0.fileType == .archive })
			
			if let files = files {
			
				let count = files.count
				
				if count > 0 {
					DispatchQueue.main.async {
						start()
					}
				} else {
					BaseViewController.uploadExtentionInProgress = false
					return
				}
				
				FileService.sharedInstance.createUnpackedDirectoryIfNeeded()
				
				var index = 0
				files.forEach { (file) in
					
					let sourcePath = file.url.path
					if let sourceFileName = file.url.lastPathComponent.components(separatedBy: ".").first {
					
						let uniqueName = FileService.sharedInstance.verifyFileName(in: containerURL, name: sourceFileName)
						let newFileURL = FileService.unpackedDirectoryURL.appendingPathComponent(uniqueName)
						let destinationPath = newFileURL.path
						
						group.enter()
						SSZipArchive.unzipFile(atPath: sourcePath, toDestination: destinationPath, progressHandler: { (entry, zipInfo, entryNumber, total) in
							progress((Float(entryNumber) / Float(total) + Float(index)) / Float(count))
						}, completionHandler: { (path, succeeded, error) in
							try? FileManager.default.removeItem(atPath: path)
							group.leave()
						})
						group.wait()
						
						index += 1
					}
					
				}
				
				if count > 0 {
					DispatchQueue.main.async {
						competion()
					}
				}
			} else {
				BaseViewController.uploadExtentionInProgress = false
			}
		}
	}

}

enum FileErrors : Error {
	case BadEnumeration
	case BadResource
}

func findSize(path: String) throws -> UInt64 {
	
	let fullPath = (path as NSString).expandingTildeInPath
	let fileAttributes: NSDictionary = try FileManager.default.attributesOfItem(atPath: fullPath) as NSDictionary
	
	if let fileType = fileAttributes.fileType(), fileType == FileAttributeType.typeRegular.rawValue {
		return fileAttributes.fileSize()
	}
	
	let url = NSURL(fileURLWithPath: fullPath)
	guard let directoryEnumerator = FileManager.default.enumerator(at: url as URL, includingPropertiesForKeys: [URLResourceKey.fileSizeKey], options: [.skipsHiddenFiles], errorHandler: nil) else { throw FileErrors.BadEnumeration }
	
	var total: UInt64 = 0
	
	for (index, object) in directoryEnumerator.enumerated() {
		guard let fileURL = object as? NSURL else { throw FileErrors.BadResource }
		var fileSizeResource: AnyObject?
		try fileURL.getResourceValue(&fileSizeResource, forKey: URLResourceKey.fileSizeKey)
		guard let fileSize = fileSizeResource as? NSNumber else { continue }
		total += fileSize.uint64Value
		if index % 1000 == 0 {
			print(".", terminator: "")
		}
	}
	return total
}

