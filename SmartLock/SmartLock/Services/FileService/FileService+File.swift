//
//  FileService+File.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension FileService {
	
	func verifyFileName(name: String, index: Int = 0) -> String {
		
		let fileName = name.components(separatedBy: ".").first
		let fileExtension = name.components(separatedBy: ".").last
		
		var newName = index == 0 ? fileName! : "\(fileName!) (\(index))"
		
		if name.contains(".") {
			newName += "." + fileExtension!
		}
		
		let path = currentDirectory.url.appendingPathComponent(newName).path
		
		if !fileManager.fileExists(atPath: path) {
			
			return newName
			
		} else {
			
			return verifyFileName(name: name, index: index + 1)
			
		}
		
	}

	func verifyFileName(in directoryURL: URL, name: String, index: Int = 0) -> String {
		
		let fileName = name.components(separatedBy: ".").first
		let fileExtension = name.components(separatedBy: ".").last
		
		var newName = index == 0 ? fileName! : "\(fileName!) (\(index))"
		
		if name.contains(".") {
			newName += "." + fileExtension!
		}
		
		let path = directoryURL.appendingPathComponent(newName).path
		
		if !fileManager.fileExists(atPath: path) {
			
			return newName
			
		} else {
			
			return verifyFileName(in: directoryURL, name: name, index: index + 1)
			
		}
		
	}
	
	func verifyFileName(in directory: File, name: String, index: Int = 0) -> String {
		
		let fileName = name.components(separatedBy: ".").first
		let fileExtension = name.components(separatedBy: ".").last
		
		var newName = index == 0 ? fileName! : "\(fileName!) (\(index))"
		
		if name.contains(".") {
			newName += "." + fileExtension!
		}
		
		let path = directory.url.appendingPathComponent(newName).path
		
		if !fileManager.fileExists(atPath: path) {
			
			return newName
			
		} else {
			
			return verifyFileName(in: directory, name: name, index: index + 1)
			
		}
		
	}
	
	var newFileName: String {
		let dateFormater = DateFormatter()
		dateFormater.locale = Locale(identifier: "en_US")
		dateFormater.dateFormat = "dd-MM-yyyy, hh-mm-ss a"
		return "Camera \(dateFormater.string(from: Date()))"
	}
	
	var newImageURL: URL {
		return currentDirectory.url.appendingPathComponent("\(newFileName).jpg")
	}
	
	var newImageURLAlbums: URL {
		return albumsDirectory.appendingPathComponent("\(newFileName).jpg")
	}
	
	var newVideoURL: URL {
		return currentDirectory.url.appendingPathComponent("\(newFileName).MOV")
	}
	
	var newVideoURLAlbums: URL {
		return albumsDirectory.appendingPathComponent("\(newFileName).MOV")
	}
	
	func existsFileForPath(path: String) -> Bool {
		
		let fileUrl = FileService.documentsDirectory.appendingPathComponent(path)
		let filePath = fileUrl.path.replacingOccurrences(of: "//", with: "/")
		
		return fileManager.fileExists(atPath: filePath)
	}
	
	func deleteFiles(files: [File]) {
		files.forEach( { $0.remove() } )		
	}
	
	func move(files: [File], to folder: File) {
		
		files.sorted(by: { (file1, file2) -> Bool in
			
			var date1: Double?
			var date2: Double?
			
			if file1.fileType == .directory {
				
				if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", file1.relativePath)) {
					date1 = album.mobDate
				}
				
			} else {
				
				if let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", file1.relativePath)) {
					date1 = file.mobDate
				}
				
			}
			
			if file2.fileType == .directory {
				
				if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", file2.relativePath)) {
					date2 = album.mobDate
				}
				
			} else {
				
				if let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", file2.relativePath)) {
					date2 = file.mobDate
				}
				
			}
			
			guard date1 != nil, date2 != nil else { return false }
			
			return date1! < date2!
			
		}).forEach( { $0.moveTo(to: folder) } )
		
	}
	
}
