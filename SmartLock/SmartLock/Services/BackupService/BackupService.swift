//
//  BackupService.swift
//  SmartLock
//
//  Created by Bogachev on 4/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import Alamofire
import YandexMobileMetrica

enum BackupStatus {
	case inactive
	case backup
	case restore
}

enum BackupProcessStatus {
	case inactive
	case inProgress
	case waitingForWiFi
	case ready
	case hold
}

class BackupService: NSObject {
	
	static let sharedInstance = BackupService()
	
	private var fetchingRestore = false
	
	private var timer: Timer?
	
	var status = BackupStatus.inactive {
		didSet {
			print("BackupStatus:\(status)")
			
			if let controller = ChatListViewController.chatListViewController {
				controller.needSyncData = true
			}
			
			if let controller = ChatListViewController.chatListViewController, controller.isVisible {
				if let status = backupStatus {
					controller.showBackupStatus(text: status)
				} else {
					controller.hideBackupStatus()
				}
			}
		}
	}
	
	var backupStatus: String? {
		get {
			
			guard processStatus == .inProgress else {				
				if backupSpaceLimitException != .noException {
					return "25GB limit exceeded"
				} else {
					return nil
				}
			}
			
			if status == .inactive {
				
				if backupSpaceLimitException != .noException {
					return "25GB limit exceeded"
				}
				
			} else {
				
				if backupSpaceLimitException != .noException {
					return "25GB limit exceeded"
				} else {
					return ""
				}
			
			}
			
			return nil
		}
	}
	
	var processStatus = BackupProcessStatus.inactive {
		didSet {
			print("BackupStatus:\(status)")
			if let controller = ChatListViewController.chatListViewController, controller.isVisible {
				if let status = backupStatus {
					controller.showBackupStatus(text: status)
				} else {
					controller.hideBackupStatus()
				}
			}
		}
	}
	
	private var isReachableOnEthernetOrWiFi: Bool {
		
		if reachabilityManager?.isReachableOnEthernetOrWiFi == true {
			return true
		}
		
		if reachabilityManager?.isReachableOnWWAN == true {
			if let allowUseMobileInternet = LockService.sharedService.user?.mobAllowUseMobileInternet, allowUseMobileInternet == true {
				return true
			}
		}
		
		return false
	}
	
	private var reachabilityManager:NetworkReachabilityManager?
	
	private var allowBackup: Bool {
				
		if let user = MUser.mr_findFirst(), user.mobConfirmed == true {
			
			#if arch(i386) || arch(x86_64)
				return true
			#endif
			
			if UserService.sharedService.user.canBackup {
				return true

			}
			
			return false
			
		}
		
		return false
		
	}
	
	private var didFinishLaunching = true
	
	private var log: MLog?
	
	private var request: DataRequest?
	private var uploadingFile: Bool = false
	
	private var downloadRequest: DownloadRequest?
	
	var previewingPath: String?
	
	var syncDate: Date? = nil
	
	enum BackupSpaceLimitException {
		case noException
		case catchException
		case checkAndExit
		case waiting
	}
	
	var backupSpaceLimitException: BackupSpaceLimitException = .noException
//	private var checkBackupSpaceLimitAndExit = false
	
	override init() {
		super.init()
		
//		guard allowBackup == true else { return }
		
		self.prepareToBackup()
		
		NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
		
	}
	
	func tryBackupAgainWhenBackupSpaceLimitException() {
		
		if backupSpaceLimitException == .waiting {
			
			backupSpaceLimitException = .noException
			
		}
		
	}
	
	private func startTimer() {
		timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(launchBackup), userInfo: nil, repeats: true)
	}

	private func stopTimer() {
		guard timer != nil else {
			return
		}
		timer?.invalidate()
	}
	
	public func prepareToBackup() {
		
		startTimer()
		
		reachabilityManager = NetworkReachabilityManager()
		
		reachabilityManager?.listener = { reachabilityStatus in
			
			switch reachabilityStatus {
			case .reachable(.ethernetOrWiFi):
//				self.isReachableOnEthernetOrWiFi = true
				if self.processStatus == .waitingForWiFi {
					self.processStatus = .ready
				}
				case .reachable(.wwan):
				
				if let allowUseMobileInternet = LockService.sharedService.user?.mobAllowUseMobileInternet, allowUseMobileInternet == true {
//					self.isReachableOnEthernetOrWiFi = true
					if self.processStatus == .waitingForWiFi {
						self.processStatus = .ready
					}					
				}
				
			default:
//				self.isReachableOnEthernetOrWiFi = false
				break
			}
			
		}
		
		reachabilityManager!.startListening()
		
	}
	
	@objc private func didEnterBackground() {
		//    reachabilityManager!.stopListening()
		//    self.isReachableOnEthernetOrWiFi = false
	}
	
	@objc private func didBecomeActive() {
		
		guard syncDate != nil else { return }
		
		guard processStatus == .inactive else { return }
		
		stopTimer()
		
		status = .restore
		processStatus = .inProgress
		
		restoreFromBackup(compleation: { (upToDate) in
			
			if upToDate == false {
				self.status = .restore
				self.processStatus = .inactive
			} else {
				self.status = .backup
				self.processStatus = .inactive
			}
			
			self.startTimer()
		})
		
	}
	
	func switchToBackground() {
		processStatus = .inactive
		launchBackup()
	}
	
	@objc private func launchBackup() {

		guard allowBackup == true else { return }
		
		let context = NSManagedObjectContext.mr_default()
		
		if syncDate == nil {
			if let date = MDevice.mr_findFirst(in: context)!.mobSyncDate {
				syncDate = date as Date
			}
		}
		
		switch status {
		case .inactive:
		
			if syncDate == nil {
				
				status = .restore
				processStatus = .inProgress
				
				stopTimer()
				
				restoreFromBackup(compleation: { (upToDate) in
					
					if upToDate == false {
						
						self.status = .restore
						self.processStatus = .inactive
						
					} else {
						
						self.status = .backup
						self.processStatus = .inactive
						
					}
				
					self.startTimer()
					
				})
				
			} else if MRestoreLog.mr_findFirst(in: context) != nil { // check for not completed restore
			
				self.status = .restore
				self.processStatus = .inactive
				
			// check for not completed backup
			} else if MLog.mr_findFirst(with: backupSpaceLimitException == .checkAndExit || backupSpaceLimitException == .waiting ?
				NSPredicate(format: "mobDate <= %f AND mobAction != %@", syncDate!.timeIntervalSince1970, LogActionType.create.rawValue) :
				NSPredicate(format: "mobDate <= %f", syncDate!.timeIntervalSince1970), sortedBy: "mobDate", ascending: false) != nil {
				
				self.status = .backup
				self.processStatus = .inactive
				
			// start new beckap intent
			} else if MLog.mr_findFirst(with: backupSpaceLimitException == .checkAndExit || backupSpaceLimitException == .waiting ?
				NSPredicate(format: "mobAction != %@", LogActionType.create.rawValue) : nil, sortedBy: "mobDate", ascending: false) != nil {
				
//				status = .restore
//				processStatus = .inProgress
				
				stopTimer()
				
				restoreFromBackup(compleation: { (upToDate) in
					
					if upToDate == false {
					
						self.status = .restore
						self.processStatus = .inactive
						
					} else {
						
						self.status = .backup
						self.processStatus = .inactive
						
					}
					
					self.startTimer()
				})
				
			} else if didFinishLaunching == true {
				
				status = .restore
				processStatus = .inProgress
				
				stopTimer()
				
				restoreFromBackup(compleation: { (upToDate) in
					
					if upToDate == false {
						
						self.status = .restore
						self.processStatus = .inactive
						
					} else {
						
						self.status = .backup
						self.processStatus = .inactive
						
					}
					
					self.startTimer()

				})
				
			}
			
			didFinishLaunching = false
			
		case .restore:
			guard processStatus != .hold else { return }
			guard processStatus != .inProgress else { return }
			guard isReachableOnEthernetOrWiFi == true else {
				processStatus = .waitingForWiFi
				return
			}
			processStatus = .inProgress
			
			stopTimer()
			restore()
			
			//need resume
			
		case .backup:
			guard processStatus != .hold else { return }
			
//			if processStatus == .inProgress && request == nil {
//				processStatus = .inactive
//				backupSpaceLimitException = .noException
//				return
//			}
			
			guard processStatus != .inProgress else { return }
			guard isReachableOnEthernetOrWiFi == true else {
				processStatus = .waitingForWiFi
				return
			}
			
			if MLog.mr_findFirst(with: backupSpaceLimitException == .checkAndExit || backupSpaceLimitException == .waiting ?
				NSPredicate(format: "mobDate <= %f AND mobAction != %@", syncDate!.timeIntervalSince1970, LogActionType.create.rawValue) :
				NSPredicate(format: "mobDate <= %f", syncDate!.timeIntervalSince1970), sortedBy: "mobDate", ascending: false) != nil {

					processStatus = .inProgress
					stopTimer()
					backup()
				
			} else {
				
				if backupSpaceLimitException == .noException {
					
					self.status = .inactive
					self.processStatus = .inactive
					
				}
				
			}
			
		}
		
	}
	
	private func backup() {
		
		let context = NSManagedObjectContext.mr_default()
		
		if syncDate == nil {
			if let date = MDevice.mr_findFirst(in: context)!.mobSyncDate {
				syncDate = date as Date
			}
		}

		let predicate = backupSpaceLimitException == .checkAndExit || backupSpaceLimitException == .waiting ?
			NSPredicate(format: "mobDate <= %f AND mobAction != %@", syncDate!.timeIntervalSince1970, LogActionType.create.rawValue) :
			NSPredicate(format: "mobDate <= %f", syncDate!.timeIntervalSince1970)
		
		if let log = MLog.mr_findFirst(with: predicate, sortedBy: "mobDate", ascending: true, in: context) {
			
			context.mr_save({ (localContext) in
				if let localLog = log.mr_(in: localContext) {
					localLog.mobBackuping = true
				}
			}, completion: { (success, error) in
				print("backup:\(log.mobPath ?? "Log") locked")
				backupCompleteAction(log: log)
			})
			
			func backupCompleteAction(success: Bool) {
				if success == true {
					context.mr_save({ (localContext) in
						if let localLog = log.mr_(in: localContext) {
							if localLog.mobAction == LogActionType.create.rawValue {
								if let mobPath = localLog.mobPath {
									if mobPath.contains(".json") {
										var albumName = mobPath.removeLastComponent()
										albumName.removeLast()
										if let album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: albumName, in: localContext) {
											album.mobBackuped = true
										}
									}
								}
							}
							localLog.mr_deleteEntity()
						}
					}, completion: { (success, error) in
						self.backup()
					})
					
				} else {
					processStatus = .inactive
					startTimer()
				}
			}
			
			func backupCompleteAction(log: MLog) {

				guard let action = log.mobAction else {
					backupCompleteAction(success: true)
					return
				}
				
				switch log.mobAction! {
				case LogActionType.create.rawValue:
					
					//		Create for file only		//
					
					if log.mobType == LogFileType.directory.rawValue {
						
						if let hash = log.mobHash, hash.isEmpty == false {
							
							print("backupAddPasscode started")

							guard let mobPath = log.mobPath else {
								backupCompleteAction(success: true)
								return
							}

							if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
								backupCompleteAction(success: true)
								return
							}

							backupAddPasscode(log: log, compleation: { (success, errorMessage) in
								
								YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Add Passcode": "\(errorMessage)"], onFailure: nil)
								
								backupCompleteAction(success: success)
							})
							
						} else {
							
							context.mr_save({ (localContext) in
								if let localLog = log.mr_(in: localContext) {
									localLog.mr_deleteEntity()
								}
							}, completion: { (success, error) in
								self.backup()
							})
							
						}
						
					} else {
						print("upload started")
						
						guard let mobPath = log.mobPath else {
							backupCompleteAction(success: true)
							return
						}
						
						if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
							backupCompleteAction(success: true)
							return
						}
						
						upload(log: log, compleation: { (success, errorMessage) in
							
							YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Upload": "\(errorMessage)"], onFailure: nil)
							
							if let errorMessage = errorMessage {
								
								if errorMessage == "max_storage_limit" {
									if self.backupSpaceLimitException == .catchException {
										self.backupSpaceLimitException = .checkAndExit
									} else {
										self.backupSpaceLimitException = .catchException
									}
								} else if errorMessage.isEmpty {
									self.backupSpaceLimitException = .noException
								}
								
							} else {
								self.backupSpaceLimitException = .noException
							}
							
							backupCompleteAction(success: success)
							
						})
						
					}
					
					//		Delete for file only
					
				case LogActionType.delete.rawValue:
					
					if log.mobType == LogFileType.file.rawValue {
						print("deleteFile started")
						deleteFile(log: log, compleation: { (success, errorMessage) in
							YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Delete File": "\(errorMessage)"], onFailure: nil)
							backupCompleteAction(success: success)
						})
						
					} else {
						print("deleteAlbum started")
						deleteAlbum(log: log, compleation: { (success, errorMessage) in
							YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Delete Album": "\(errorMessage)"], onFailure: nil)
							backupCompleteAction(success: success)
						})
					}
					
				case LogActionType.move.rawValue:
					print("backupMove started")
					
					guard let mobPath = log.mobPath else {
						backupCompleteAction(success: true)
						return
					}
					
					if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
						backupCompleteAction(success: true)
						return
					}
					
					backupMove(log: log, compleation: { (success, errorMessage) in
						YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Move": "\(errorMessage)"], onFailure: nil)
						
						if errorMessage == "not_found_file_model" {
							backupCompleteAction(success: true)
						} else {
							backupCompleteAction(success: success)
						}
						
					})
					
				case LogActionType.rename.rawValue:
					print("rename started")
					
					guard let mobPath = log.mobPath else {
						backupCompleteAction(success: true)
						return
					}
					
					if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
						backupCompleteAction(success: true)
						return
					}
					
					rename(log: log, compleation: { (success, errorMessage) in
						YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Rename": "\(errorMessage)"], onFailure: nil)
						backupCompleteAction(success: success)
						
					})
					
				case LogActionType.addpasscode.rawValue:
					print("renabackupAddPasscodeme started")
					
					guard let mobPath = log.mobPath else {
						backupCompleteAction(success: true)
						return
					}
					
					if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
						backupCompleteAction(success: true)
						return
					}
					
					backupAddPasscode(log: log, compleation: { (success, errorMessage) in
						YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Add Passcode": "\(errorMessage)"], onFailure: nil)
						backupCompleteAction(success: success)
					})
					
				case LogActionType.deletepasscode.rawValue:
					print("backupDeletePasscode started")
					
					guard let mobPath = log.mobPath else {
						backupCompleteAction(success: true)
						return
					}
					
					if FileManager.default.fileExists(atPath: File(relativePath: mobPath).path) == false {
						backupCompleteAction(success: true)
						return
					}
					
					backupDeletePasscode(log: log, compleation: { (success, errorMessage) in
						YMMYandexMetrica.reportEvent("Error", parameters: ["Backup Delete Passcode": "\(errorMessage)"], onFailure: nil)
						backupCompleteAction(success: success)
					})
					
				default: break
					
				}
				
			}

			
		} else {
			
			if backupSpaceLimitException == .catchException {
				backup()
			} else if backupSpaceLimitException == .waiting {
				backupSpaceLimitException = .catchException
				backup()
			} else {
				status = .inactive
				processStatus = .inactive
				
				if backupSpaceLimitException != .noException {
					backupSpaceLimitException = .waiting
				}
				
				startTimer()
			}
			
		}
		
	}
	
	func resumeBackup() {
		
//		if processStatus == .waitingForWiFi {
//			return
//		}
//		
//		guard processStatus == .hold else { return }
//		
//		processStatus = .inactive
		
//		request?.resume()
		
//		launchBackup()
		
	}
	
	func suspendBackup() {
		
//		guard processStatus == .inProgress else { return }
		
//		processStatus = .hold
//		
//		request?.cancel()
		
	}
	
	func cancelBackup() {
		request?.cancel()
	}
	
	@objc private func restoreFromBackup(compleation: @escaping (_ upToDate: Bool) -> ()) {
		
		guard fetchingRestore == false else { return }
		
		fetchingRestore = true
		
		fetchRestoreData { (newSyncDate, newFiles, deletedFiles, moveLogs, passcodeLogs, errorMessage) in
		
			let context = NSManagedObjectContext.mr_default()

			newFiles?.forEach({ (backupFile) in
				
				let log = MRestoreLog.mr_createEntity(in: context)
				log?.mobUUID = backupFile.UUID
				log?.mobPath = backupFile.filePath
				log?.mobAction = LogActionType.create.rawValue
				log?.mobDate = backupFile.dateDevice!.dateFromISO8601! as NSDate
				log?.mobDateCreate = backupFile.dateCreated!.dateFromISO8601! as NSDate
				
			})

			deletedFiles?.forEach({ (backupFile) in
				
				let log = MRestoreLog.mr_createEntity(in: context)
				log?.mobUUID = backupFile.UUID
				log?.mobPath = backupFile.filePath
				log?.mobAction = LogActionType.delete.rawValue
				log?.mobDate = backupFile.dateDevice!.dateFromISO8601! as NSDate
				log?.mobDateCreate = backupFile.dateCreated!.dateFromISO8601! as NSDate
				
			})
			
			moveLogs?.forEach({ (movedFile) in
				
				if movedFile.type == "change_email", let email = movedFile.email {
					DataManager.registerUser(email: email)
				} else {
				
					let log = MRestoreLog.mr_createEntity(in: context)
					log?.mobPath = movedFile.newPath
					log?.mobOldPath = movedFile.oldPath
					log?.mobAction = LogActionType.move.rawValue
					log?.mobDate = movedFile.dateDevice!.dateFromISO8601! as NSDate
					log?.mobDateCreate = movedFile.dateCreated!.dateFromISO8601! as NSDate
					log?.mobType = movedFile.type == "file" ? LogFileType.file.rawValue : LogFileType.directory.rawValue
					
				}
				
			})

			passcodeLogs?.forEach({ (passcodeLog) in
				
				let log = MRestoreLog.mr_createEntity(in: context)
				log?.mobUUID = passcodeLog.uuid
				log?.mobPath = passcodeLog.path
				log?.mobAction = passcodeLog.removed == true ? LogActionType.deletepasscode.rawValue : LogActionType.addpasscode.rawValue
				log?.mobDate = passcodeLog.dateDevice!.dateFromISO8601! as NSDate
				log?.mobDateCreate = passcodeLog.dateModified!.dateFromISO8601! as NSDate
				log?.mobHash = passcodeLog.hash
				
			})
			
			let device = MDevice.mr_findFirst(in: context)
			
			if let log = MRestoreLog.mr_findFirst(with: nil, sortedBy: "mobDateCreate", ascending: false, in: context) {
				
				self.syncDate = (log.mobDateCreate! as Date) > newSyncDate ? log.mobDateCreate! as Date : newSyncDate
				
			} else {
				
				self.syncDate = newSyncDate
				
			}
			
//			device?.mobSyncDate = self.syncDate! as NSDate
			
			context.mr_save({ (localContext) in
				
				if let localDevice = device?.mr_(in: localContext) {
					localDevice.mobSyncDate = self.syncDate as NSDate?
				}
				
			}, completion: { (success, error) in				
				self.fetchingRestore = false
			})
			
			DispatchQueue.main.async {
				
				if let controller = ChatListViewController.chatListViewController {
					controller.needSyncData = true
				}
			}
			
			if MRestoreLog.mr_findFirst(in: context) != nil {
				compleation(false)
			} else {
				compleation(true)
			}
			
		}
		
	}
	
	private func fetchRestoreData(compleation: @escaping (_ newSyncDate: Date, _ newFilesLog: [BackupFile]?, _ deletedFilesLog: [BackupFile]?, _ moveFilesLog: [BackupMoveLog]?, _ passcodesLog: [BackupPasscode]?, _ errorMessage: String?) -> ()) {
		
		var newFilesLog: [BackupFile]?
		var deletedFilesLog: [BackupFile]?
		var moveFilesLog: [BackupMoveLog]?
		var passcodesLog: [BackupPasscode]?
		var errorMessage: String?
		
		let context = NSManagedObjectContext.mr_default()
		var syncDate: Date? = nil
			
		if syncDate == nil {
			if let date = MDevice.mr_findFirst(in: context)!.mobSyncDate {
				syncDate = date as Date
			}
		}
		
		let newSyncDate = Date()
		
		let dispatchGroup = DispatchGroup()
		
		dispatchGroup.enter()
		NetworkManager.Requests.getNewFiles(device_id: DataManager.device!.mobID!, fromDate: syncDate) { (files, message) in
			newFilesLog = files
			errorMessage = message
			dispatchGroup.leave()
		}
		
		if syncDate != nil {
			dispatchGroup.enter()
			NetworkManager.Requests.getDeletedFiles(device_id: DataManager.device!.mobID!, fromRemovedDate: syncDate) { (files, message) in
				deletedFilesLog = files
				errorMessage = message
				dispatchGroup.leave()
			}
		} else {
			deletedFilesLog = [BackupFile]()
		}
		
		if syncDate != nil {
			dispatchGroup.enter()
			NetworkManager.Requests.getMoveLog(device_id: DataManager.device!.mobID!, fromDate: syncDate) { (logs, message) in
				moveFilesLog = logs
				errorMessage = message
				dispatchGroup.leave()
			}
		} else {
			moveFilesLog = [BackupMoveLog]()
		}
		
		dispatchGroup.enter()
		NetworkManager.Requests.getListPasswords(device_id: DataManager.device!.mobID!, fromDate: syncDate) { (logs, message) in
			passcodesLog = logs
			errorMessage = message
			dispatchGroup.leave()
		}
		
		dispatchGroup.notify(queue: DispatchQueue.main) { 
			
			guard newFilesLog != nil, deletedFilesLog != nil, moveFilesLog != nil, passcodesLog != nil else {
				return
			}
			
			compleation(newSyncDate, newFilesLog, deletedFilesLog, moveFilesLog, passcodesLog, errorMessage)

		}
		
	}
	
	private func restore() {
		
		let context = NSManagedObjectContext.mr_default()
		
		if let log = MRestoreLog.mr_findFirstOrdered(byAttribute: "mobDate,mobDateCreate", ascending: true, in: context) {
			
			context.mr_save({ (localContext) in
				if let localLog = log.mr_(in: localContext) {
					localLog.mobBackuping = true
				}
			}, completion: { (success, error) in })
			
			func restoreCompleteAction(success: Bool) {
				
				if success == true {
					
					context.mr_save({ (localContext) in
						if let localLog = log.mr_(in: localContext) {
							localLog.mr_deleteEntity()
						}
					}, completion: { (success, error) in
						
						self.restore()
						
						DispatchQueue.main.async {
							
							if let controller = ChatListViewController.chatListViewController {
								controller.needSyncData = true
							}
							
						}
					
					})
					
				} else {
					startTimer()
				}
				
			}
			
			if previewingPath != nil {
			
				if log.mobPath!.contains(previewingPath!) || log.mobOldPath!.contains(previewingPath!) {
					
					return
					
				}
				
			}
			
			switch log.mobAction! {
			case LogActionType.create.rawValue:
				
				getFileURL(log: log, compleation: { (url, errorMessage) in
					
					YMMYandexMetrica.reportEvent("Error", parameters: ["Restore Get File URL": "\(errorMessage)"], onFailure: nil)
					
					if url != nil {
						
						self.download(log: log, url: url!, compleation: { (success, errorMessage) in
							
							YMMYandexMetrica.reportEvent("Error", parameters: ["Restore Download": "\(errorMessage)"], onFailure: nil)
							
							restoreCompleteAction(success: success)
							
						})
						
					}
					
				})
				
			case LogActionType.delete.rawValue: 

				let file = File(relativePath: log.mobPath!)
				file.removeWitoutLog()
				
				restoreCompleteAction(success: true)
				
			case LogActionType.move.rawValue:

				if log.mobOldPath == nil {
					restoreCompleteAction(success: true)
					return
				}
				
				if FileService.sharedInstance.existsFileForPath(path: log.mobOldPath!) == false {
					restoreCompleteAction(success: true)
					return
				}
				
				if log.mobPath == nil {
					
					let file = File(relativePath: log.mobOldPath!)
					file.removeWitoutLog()
					
					restoreCompleteAction(success: true)
					return
				}
				
				if let mobPath = log.mobPath, let mobOldPath = log.mobOldPath {
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						MAlbum.createAlbumsForPath(path: mobPath, context: localContext)
					})
					
					FileService.sharedInstance.createFoldersForPath(path: mobPath)
					let file = File(relativePath: mobOldPath)
					file.moveWitoutLog(relativePath: mobPath)
					
					restoreCompleteAction(success: true)
				}
				
			case LogActionType.addpasscode.rawValue, LogActionType.deletepasscode.rawValue:
			
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					var remove = false
					if let mobAction = log.mobAction, mobAction == LogActionType.deletepasscode.rawValue {
						remove = true
					}
					
					if let mobPath = log.mobPath, let mobHash = log.mobHash {
						MAlbum.restorePassword(with: mobPath, hash: mobHash, remove: remove, context: localContext)
					}
				})

				restoreCompleteAction(success: true)
								
			default: break
				
			}
			
		} else {
			
			status = .backup
			
			processStatus = .inProgress
			
			backup()
			
		}
		
	}
	
	private func download(log: MRestoreLog, url: URL, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		guard let fileURL = log.fileURL, let mobPath = log.mobPath, let mobUUID = log.mobUUID else { return }
		
		downloadRequest = NetworkManager.Requests.downloadFile(url: url, destinationURL: fileURL, compleation: { (success, errorMessage) in
			
			if success == true {
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					MFile.createFileWithoutLog(with: mobPath, uuid: mobUUID, context: localContext)
					MAlbum.createAlbumsForPath(path: mobPath, context: localContext)
					
					if (mobPath as NSString).lastPathComponent.contains(".json") {
						ChatService.readJSONFile(url: fileURL, completion: { (chatName, albumName, importedDate, messages) in
							if let chatName = chatName, let albumName = albumName, let importedDate = importedDate {
								ChatService.mergeChat(chatName: chatName, albumName: albumName, importedDate: importedDate, messages: messages, context: localContext)
							}
							
							if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath = %@", "/albums/\(albumName)")) {
								ChatService.updateChatJSONFile(album: album, backup: false)
							}
						})						
					}
				})
				
//				let file = File(relativePath: mobPath)
//				file.saveFileThumbnail()
			}
			
			compleation(success, errorMessage)
		})
		
	}
	
	private func getFileURL(log: MRestoreLog, compleation: @escaping (_ url: URL?, _ errorMessage: String?) -> ()) {
		
		let uuid = log.mobUUID
		
		NetworkManager.Requests.getFile(device_id: DataManager.device!.mobID!, uuid: uuid!) { (path, errorMessage) in
			
			if path != nil {
				
				if let url = URL(string: path!) {
					
					compleation(url, nil)
					
				} else {
					
					compleation(nil, "Error")
					
				}
				
			} else {
				
				compleation(nil, errorMessage)
				
			}
			
		}
		
	}
	
	private func upload(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		let url = FileService.documentsDirectory.appendingPathComponent(log.mobPath!)
		
		do {
			
			let data = try Data(contentsOf: url)
			
			uploadingFile = true

			NetworkManager.Requests.uploadFile(device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, data: data, fileURL: url, path: log.mobPath!, encodingCompletion: { (request) in
				
				if let request = request {
					self.request = request
				}
				
			}, compleation: { (uuid, errorMessage) in
				
				self.request = nil
				self.uploadingFile = false
				
				guard log.mobPath != nil else {
					compleation(true, nil)
					return
				}
				
				print("uploaded:\(log.mobPath ?? "")     \(errorMessage ?? "")")
				
				if uuid != nil {
					
					let content = NSManagedObjectContext.mr_default()
					
					if let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", log.mobPath!), in: content) {
						
						content.mr_save({ (localContext) in
							
							if let localFile = file.mr_(in: localContext) {
								localFile.mobUUID = uuid
							}
							
						}, completion: { (success, error) in
							compleation(true, nil)
						})
						
					} else {
						compleation(false, "UUID is NULL")
					}
					
				} else {
					
					compleation(false, errorMessage)
					
				}
				
			})
			
		} catch let error {
			compleation(false, error.localizedDescription)
		}
		
	}
	
	private func deleteFile(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		let url = FileService.documentsDirectory.appendingPathComponent(log.mobPath!)
		
		let uuid = log.mobUUID
		
		request = NetworkManager.Requests.deleteFile(device_id: DataManager.device!.mobID!, uuid: uuid!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, filename: url.lastPathComponent, path: log.mobPath!, compleation: { (success, errorMessage) in
			
			self.request = nil
			
			if success == true {
				
				compleation(true, nil)
				
			} else {
				
				if errorMessage == "not_found_file_model" {
					compleation(true, nil)
				} else {
					compleation(false, errorMessage)
				}
				
			}
			
		})
	}
	
	private func deleteAlbum(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
				
		request = NetworkManager.Requests.deleteAlbum(device_id: DataManager.device!.mobID!, path: log.mobPath!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (success, errorMessage) in
			
			self.request = nil
			
			if success == true {
				
				compleation(true, nil)
				
			} else {
				
				compleation(false, errorMessage)
				
			}
			
		})
	}
	
	private func backupMove(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		if log.mobType == LogFileType.file.rawValue {
			
			request = NetworkManager.Requests.moveFile(from: log.mobOldPath!, to: log.mobPath!, device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (success, errorMessage) in
				
				self.request = nil
				
				if success == true {
					
					compleation(true, nil)
					
				} else {
					
					compleation(false, errorMessage)
					
				}
				
			})
			
		} else if log.mobType == LogFileType.directory.rawValue {
			
			request = NetworkManager.Requests.moveFolder(from: log.mobOldPath!, to: log.mobPath!, device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (success, errorMessage) in
				
				self.request = nil
				
				if success == true {
					
					compleation(true, nil)
					
				} else {
					
					compleation(false, errorMessage)
					
				}
				
			})
			
		}
		
	}
	
	private func rename(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		request = NetworkManager.Requests.moveFolder(from: log.mobOldPath!, to: log.mobPath!, device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (success, errorMessage) in
			
			self.request = nil
			
			if success == true {
				
				compleation(true, nil)
				
			} else {
				
				compleation(false, errorMessage)
				
			}
			
		})
		
	}
	
	private func backupAddPasscode(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		guard let path = log.mobPath else {
			compleation(true, nil)
			return
		}
		
		request = NetworkManager.Requests.addPasscode(path: path, hash: log.mobHash!, device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (uuid, errorMessage) in
			
			self.request = nil
			
			if uuid != nil {
				
				let content = NSManagedObjectContext.mr_default()
				
				if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", path), in: content) {
					
					content.mr_save({ (localContext) in
					
						if let localAlbum = album.mr_(in: localContext) {
							localAlbum.mobUUID = uuid
						}
						
					}, completion: { (success, error) in
				
						compleation(true, nil)
						
					})
					
				} else {
					compleation(true, nil)
				}
				
			} else {
				
				compleation(false, errorMessage)
				
			}
			
		})
		
	}
	
	private func backupDeletePasscode(log: MLog, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
		
		request = NetworkManager.Requests.deletePasscode(uuid: log.mobUUID!, device_id: DataManager.device!.mobID!, date: Date(timeIntervalSince1970: log.mobDate).iso8601, compleation: { (success, errorMessage) in
			
			self.request = nil
			
			if success == true {
				
				compleation(true, nil)
				
			} else {
				
				compleation(false, errorMessage)
				
			}
			
		})
		
	}
	
}

extension Formatter {
	static let iso8601: DateFormatter = {
		let formatter = DateFormatter()
		formatter.calendar = Calendar(identifier: .iso8601)
		formatter.locale = Locale(identifier: "en_US_POSIX")
		formatter.timeZone = TimeZone(secondsFromGMT: 0)
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
		return formatter
	}()
}
extension Date {
	var iso8601: String {
		return Formatter.iso8601.string(from: self)
	}
}

extension String {
	var dateFromISO8601: Date? {
		return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
	}
}
