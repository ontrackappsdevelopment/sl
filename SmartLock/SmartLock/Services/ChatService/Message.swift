//
//  Message.swift
//  ChatKepper
//
//  Created by Bogachev on 9/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class Message {
	
	var sender: String
	var message: String
	var date: String
	var time: String
	var filePath: String?
	
	init(sender: String, message: String, date: Date, filePath: String? = nil) {
		self.sender = sender
		self.message = message
		self.filePath = filePath
		
		let dateFormatter = DateFormatter()
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		
		dateFormatter.dateStyle = .none
		dateFormatter.timeStyle = .short
		self.time = dateFormatter.string(from: date)
		
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .none
		self.date = dateFormatter.string(from: date)
	}
}
