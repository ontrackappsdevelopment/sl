//
//  Chat.swift
//  ChatKepper
//
//  Created by Bogachev on 9/25/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

enum ChatType {
	case single
	case multiple
}

class Chat {
	var name: String
	var albumName: String
	var type: ChatType
	var lastUpdateDate: Date
	var importedDate: Date
	var backuped: Bool
	var status: String
	
	init(name: String, albumName: String, type: ChatType, lastUpdateDate: Date, importedDate: Date, backuped: Bool, status: String) {
		self.name = name
		self.albumName = albumName
		self.type = type
		self.lastUpdateDate = lastUpdateDate
		self.importedDate = importedDate
		self.backuped = backuped
		self.status = status
	}
}
