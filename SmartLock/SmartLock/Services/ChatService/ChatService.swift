//
//  ChatService.swift
//  ChatKepper
//
//  Created by Bogachev on 9/23/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class ChatService {
	
	static func addUnackedChats(completion: @escaping () -> ()) {
		let files = try? FileManager.default.contentsOfDirectory(at: FileService.unpackedDirectoryURL, includingPropertiesForKeys: nil, options: [])
			.filter( { $0.lastPathComponent != ".DS_Store" } )
		
		files?.forEach({ (url) in
			ChatParser.parse(chatSourceFolderURL: url, completion: { (chatName, chatMessages) in
				guard let chatName = chatName else {
					completion()
					return
				}
				
				//				if let sameChat = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: chatName) {
				//					mergeChat(sameChat: sameChat, sourceFolderURL: url, name: chatName, messages: chatMessages, completion: completion)
				//				} else {
				
				createChat(sourceFolderURL: url, name: chatName, messages: chatMessages, completion: completion)
				
				//				}
				
			})
		})
		
	}
	
	static func createChat (sourceFolderURL: URL, name: String, messages: [ChatMessage], completion: @escaping () -> ()) {
		
		let mainDirectoryURL = FileService.sharedInstance.mainDirectoryURL
		let chatAlbumName = FileService.getNewDirectoryName(inDirectory: mainDirectoryURL, name: name)
		let chatFolderURL = mainDirectoryURL.appendingPathComponent(chatAlbumName)
		let chatFolderRelativePath = FileService.getRelativePath(url: chatFolderURL)
		let sourceFolder = File(url: sourceFolderURL)
		let destinationFolder = File(url: chatFolderURL)
		var nickNames = [String]()
		
		sourceFolder.fetchContent()
		
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in
			
			guard let album = MAlbum.mr_createEntity(in: localContext) else { return }
			album.mobThumbnail = "\(UUID().uuidString).jpg"
			album.mobDate = Date().timeIntervalSince1970
			album.mobChatName = name
			album.mobName = chatAlbumName
			album.mobPath = chatFolderRelativePath
			album.mobAlbumPath = chatFolderRelativePath.deletingLastPathComponent()
			album.mobLockType = Int16(LockType.undefined.rawValue)
			album.mobPasscode = ""
			album.mobImportedDate = Date().timeIntervalSince1970
			album.mobMyNick = ""
			
//			if let user = MUser.mr_findFirst(), let mobDefaultNick = user.mobDefaultNick, mobDefaultNick.isEmpty == false {
//				album.mobMyNick = user.mobDefaultNick
//			}
			
			_ = MLog.createDirectory(with: chatFolderRelativePath, context: localContext)
			_ = MLog.addAlbumPasscode(path: chatFolderRelativePath, lockType: .undefined, passcode: "", context: localContext)
			
			messages.sorted(by: {$0.0.date < $0.1.date}).forEach({ (chatMessage) in
				guard let message = MMessage.mr_createEntity(in: localContext) else { return }
				message.mobDate = chatMessage.date as NSDate
				message.mobMessage = chatMessage.message
				message.mobSender = chatMessage.sender
				message.mobDateCreate = Date().timeIntervalSince1970
				message.rlsAlbum = album
				album.rlsLastMessage = message
				
				if !nickNames.contains(chatMessage.sender) {
					nickNames.append(chatMessage.sender)
				}
			})
			
			album.mobParticipants = Int16(nickNames.count)
			
			for file in sourceFolder.contentOfDirectory {
				if file.url.pathExtension == "txt" { continue }
				
				let lastPathComponent = (file.relativePath as NSString).lastPathComponent
				let newRrelativePath = "/\(FileService.sharedInstance.mainDirectoryName)/\(chatAlbumName)/\(lastPathComponent)"
				let file = MFile.create(with: newRrelativePath, context: localContext)
				if let message = MMessage.mr_findFirst(with: NSPredicate(format: "rlsAlbum == %@ AND mobMessage == %@", album, lastPathComponent), in: localContext) {
					message.rlsFile = file
				}
			}
			
		}, completion: { (success, error) in
			
			try? FileManager.default.moveItem(at: sourceFolder.url, to: destinationFolder.url)
			
			//			destinationFolder.fetchContent()
			//			for file in destinationFolder.contentOfDirectory {
			//				if file.url.pathExtension == "txt" { continue }
			//				file.saveFileThumbnail()
			//			}
			
			if let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", chatFolderRelativePath)) {
				updateChatJSONFile(album: album)
			}
			
			completion()
			
		})
	}
	
	static func mergeChat (chatName: String, albumName: String, importedDate: Double, messages: [ChatMessage], context: NSManagedObjectContext) {
		guard let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath = %@", "/albums/\(albumName)"), in: context) else {
			return
		}
		
		album.mobName = albumName
		album.mobChatName = chatName
		album.mobImportedDate = importedDate
		album.mobBackuped = true
		
//		if let user = MUser.mr_findFirst(in: context), let mobDefaultNick = user.mobDefaultNick, mobDefaultNick.isEmpty == false {
//			album.mobMyNick = user.mobDefaultNick
//		}
		
		var nickNames = [String]()
		var newMessages = [ChatMessage]()
		let lastMessage = MMessage.mr_findAllSorted(by: "mobDate", ascending: false, with: NSPredicate(format: "rlsAlbum == %@", album))?.first as? MMessage
		if let lastMessage = lastMessage {
			if let lastMessageDate = lastMessage.mobDate as Date? {
				newMessages.append(contentsOf: messages.filter({ $0.date > lastMessageDate }))
			}
		} else {
			newMessages = messages
		}
		
		newMessages.sorted(by: {$0.0.date < $0.1.date}).forEach({ (chatMessage) in
			guard let message = MMessage.mr_createEntity(in: context) else { return }
			message.mobDate = chatMessage.date as NSDate
			message.mobMessage = chatMessage.message
			message.mobSender = chatMessage.sender
			message.mobDateCreate = Date().timeIntervalSince1970
			message.rlsAlbum = album
			
			if let mobPath = album.mobPath, let file = MFile.mr_findFirst(with: NSPredicate(format: "mobPath == %@", "\(mobPath)/\(chatMessage.message)"), in: context) {
				message.rlsFile = file
			}
			
			if let lastMessage = album.rlsLastMessage, let lastMessageDate = lastMessage.mobDate as Date? {
				if lastMessageDate < chatMessage.date {
					album.rlsLastMessage = message
				}
			} else {
				album.rlsLastMessage = message
			}
			
			if !nickNames.contains(chatMessage.sender) {
				nickNames.append(chatMessage.sender)
			}
		})
		
		album.mobParticipants = Int16(nickNames.count)
		
	}
	
	static func moveChat (movedAlbumName: String, destinationAlbumName: String, completion: @escaping () -> ()) {
		guard let movedAlbum = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: movedAlbumName),
			let destinationAlbum = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: destinationAlbumName),
			let movedAlbumPath = movedAlbum.mobPath,
			let destinationAlbumPath = destinationAlbum.mobPath else {
				return
		}
		
		var nickNames = [String]()
		let movedAlbumFolder = File(relativePath: movedAlbumPath)
		let movedAlbumFolderURL = movedAlbumFolder.url
		var movedURL = [(from:URL, to: URL)]()
		
		let destinationAlbumFolder = File(relativePath: destinationAlbumPath)
		let destinationAlbumFolderURL = destinationAlbumFolder.url
		
//		let lastMessage = MMessage.mr_findAllSorted(by: "mobDate", ascending: false, with: NSPredicate(format: "rlsAlbum == %@", destinationAlbum))?.first as? MMessage
//		let lastMessageDate = lastMessage?.mobDate
		
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in
			
			guard let localMovedAlbum = movedAlbum.mr_(in: localContext) else { return }
			guard let localDestinationAlbum = destinationAlbum.mr_(in: localContext) else { return }
			
			if let messages = MMessage.mr_findAll(with: NSPredicate(format: "rlsAlbum == %@", localMovedAlbum), in: localContext) as? [MMessage] {
				
				messages.forEach({ (message) in
					let savedFile = message.rlsFile
					var file: File?
					if let savedFilePath = savedFile?.mobPath {
						file = File(relativePath: savedFilePath)
					}
					
					if let date = message.mobDate, let mobSender = message.mobSender,
						let _ = MMessage.mr_findFirst(with: NSPredicate(format: "rlsAlbum == %@ AND mobDate == %@ AND mobSender == %@", destinationAlbum, date, mobSender), in: localContext) {
						
							if let savedFile = savedFile {
								if let mobPath = savedFile.mobPath {
									MFile.delete(with: mobPath, context: localContext)
								}
							}
							message.mr_deleteEntity(in: localContext)
						
					} else {
						
						message.rlsAlbum = localDestinationAlbum
						
						if let lastMessage = destinationAlbum.rlsLastMessage, let lastMessageDate = lastMessage.mobDate as Date?, let date = message.mobDate as Date? {
							if lastMessageDate < date {
								destinationAlbum.rlsLastMessage = message
							}
						} else {
							destinationAlbum.rlsLastMessage = message
						}
						
						if let mobSender = message.mobSender, nickNames.contains(mobSender) == false {
							nickNames.append(mobSender)
						}
						
						if let file = file {
							
							if file.url.pathExtension != "txt" && file.url.pathExtension != "json" {
								
								let name = FileService.sharedInstance.verifyFileName(in: destinationAlbumFolder, name: file.url.lastPathComponent)
								let newURL = destinationAlbumFolder.url.appendingPathComponent(name)
								
								movedURL.append((file.url, newURL))
								
								let newPath = "\(destinationAlbumFolder.relativePath)/\(name)"
								
								if let savedFile = savedFile {
									savedFile.moveTo(path: newPath, context: localContext)
								}
								
							}
							
						}
						
					}
					
				})
				
			}
			
			MAlbum.delete(with: movedAlbumPath, context: localContext)
			localDestinationAlbum.mobParticipants += localDestinationAlbum.mobParticipants + Int16(nickNames.count)
		
		}, completion: { (success, error) in
			
			movedURL.forEach({ (fromURL, toURL) in
				try? FileManager.default.moveItem(at: fromURL, to: toURL)
			})
			
			try? FileManager.default.removeItem(at: movedAlbumFolderURL)
			
			updateChatJSONFile(album: destinationAlbum)
			
			completion()
			
		})
		
	}
	
	static func updateChatJSONFile(album: MAlbum, backup: Bool = true) {
		guard let mobPath = album.mobPath, let mobChatName = album.mobChatName, let mobName = album.mobName else { return }
		
		let mobImportedDate = album.mobImportedDate
		let chatFolder = File(relativePath: mobPath)
		let textFileURL = chatFolder.url.appendingPathComponent("\(UUID().uuidString).json")
		let textFileRelativePath = FileService.getRelativePath(url: textFileURL)
		
		if let savedMessages = MMessage.mr_findAll(with: NSPredicate(format: "rlsAlbum == %@", album)) as? [MMessage] {
			
			var array = Array<[String: Any]>()
			savedMessages.forEach({ (saveMessages) in
				if let chatMessage = ChatMessage(message: saveMessages) {
					array.append(chatMessage.toJSON())
				}
			})
			
			if let data = try? JSONSerialization.data(withJSONObject: ["chat": mobChatName, "albumName": mobName, "data": array, "importedDate": mobImportedDate], options: []) {
				try? data.write(to: textFileURL, options: [])
			}
			
		}
		

		if FileManager.default.fileExists(atPath: textFileURL.path) {
			var files = [File]()
			if let savedFiles = MFile.mr_findAll(with: NSPredicate(format: "mobFilePath == %@ AND (mobPath contains[c] %@ OR mobPath contains[c] %@)", mobPath, "json", "txt")) as? [MFile] {
				
				savedFiles.forEach({ (savedFile) in
					if let mobPath = savedFile.mobPath {
						files.append(File(relativePath: mobPath))
					}
				})
				
				FileService.sharedInstance.deleteFiles(files: files)
				
				NSManagedObjectContext.mr_default().mr_save({ (localContext) in
					_ = MFile.create(with: textFileRelativePath, context: localContext)
				}, completion: { (success, error) in
					
				})
			}
			
		}
	}
	
	static func readJSONFile(url: URL, completion: (_ chatName: String?,_ albumName: String?,_ importedDate: Double?,_ messages: [ChatMessage]) -> ()) {
		var chatName: String?
		var albumName: String?
		var importedDate: Double?
		var messages = [ChatMessage]()
		
		if let data = try? Data(contentsOf: url) {
			if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []), let json = jsonObject as? [String: Any] {
				if let value = json["chat"] as? String {
					chatName = value
				}
				
				if let value = json["albumName"] as? String {
					albumName = value
				}
				
				if let value = json["importedDate"] as? Double {
					importedDate = value
				}
				
				if let array = json["data"] as? Array<[String: Any]> {
					array.forEach({ (json) in
						if let chatMessage = ChatMessage(json: json) {
							messages.append(chatMessage)
						}
					})
				}
			}
		}
		
		completion(chatName, albumName, importedDate, messages)
	}
	
	static func restoreFromCharJSONFile(savedFile: MFile) {
		guard let mobPath = savedFile.mobPath else { return }
		guard let mobFilePath = savedFile.mobFilePath else { return }
		
		let file = File(relativePath: mobPath)
		
		guard let data = try? Data(contentsOf: file.url, options: []) else { return }
		guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []), let json = jsonObject as? [String: Any] else { return }
		guard let array = json["data"] as? Array<[String: Any]> else { return }
		
		var messages = [ChatMessage]()
		var nickNames = [String]()
		array.forEach({ (json) in
			if let chatMessage = ChatMessage(json: json) {
				messages.append(chatMessage)
			}
		})
		
		if let album = MAlbum.mr_findFirst(byAttribute: "mobPath == %@", withValue: mobFilePath) {
			if let lastMessage = MMessage.mr_findAllSorted(by: "mobDate", ascending: false, with: NSPredicate(format: "rlsAlbum == %@", album))?.first as? MMessage {
				
				var newMessages = [ChatMessage]()
				
				if let lastMessageDate = lastMessage.mobDate as Date? {
					newMessages.append(contentsOf: messages.filter({ $0.date > lastMessageDate }))
				}
				
				NSManagedObjectContext.mr_default().mr_save({ (localContext) in
					
					guard let localAlbum = album.mr_(in: localContext) else { return }
					
					newMessages.sorted(by: {$0.0.date < $0.1.date}).forEach({ (chatMessage) in
						guard let message = MMessage.mr_createEntity(in: localContext) else { return }
						message.mobDate = chatMessage.date as NSDate
						message.mobMessage = chatMessage.message
						message.mobSender = chatMessage.sender
						message.mobDateCreate = Date().timeIntervalSince1970
						message.rlsAlbum = localAlbum
						
						if let lastMessage = localAlbum.rlsLastMessage, let lastMessageDate = lastMessage.mobDate as Date? {
							if lastMessageDate < chatMessage.date {
								localAlbum.rlsLastMessage = message
							}
						} else {
							localAlbum.rlsLastMessage = message
						}
						
						if !nickNames.contains(chatMessage.sender) {
							nickNames.append(chatMessage.sender)
						}
						
						localAlbum.mobParticipants = Int16(nickNames.count)
					})
					
				}, completion: { (success, error) in
					
				})
			}
		}
	}
	
	static func fetchChatList(completion: (_ list: [Chat]) -> ()) {
		var list = [Chat]()
		if let albums = MAlbum.mr_findAll() as? [MAlbum] {
			albums.forEach({ (album) in
				if let name = album.mobChatName, let albumName = album.mobName, let lastMessage = album.rlsLastMessage, let text = lastMessage.mobMessage, let lastUpdateDate = lastMessage.mobDate {
					let backuped = album.mobBackuped
					let type: ChatType = album.mobParticipants > 2 ? .multiple : .single
					list.append(Chat(name: name, albumName: albumName, type: type, lastUpdateDate: lastUpdateDate as Date, importedDate: Date(timeIntervalSince1970: album.mobImportedDate), backuped: backuped, status: lastMessage.rlsFile != nil ? "Attached file" : text))
				}
				
				list.sort(by: { (item1, item2) -> Bool in
					item1.lastUpdateDate > item2.lastUpdateDate
				})
			})
		}
		
		completion(list)
	}
	
	static func fetchChat(name: String, completion: (_ myNickName: String, _ list: [Message],_ dates: [String],_ paritcipants: [String]) -> ()) {
		var list = [Message]()
		var paritcipants = [String]()
		var dates = [String]()
		var myNickName = ""
		
		if let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: name) {
			if let mobMyNick = album.mobMyNick {
				myNickName = mobMyNick
			}
			if let savedMessages = MMessage.mr_findAllSorted(by: "mobDate", ascending: true, with: NSPredicate(format: "rlsAlbum == %@", album)) as? [MMessage] {
				savedMessages.forEach({ (savedMessage) in
					if let date = savedMessage.mobDate, let sender = savedMessage.mobSender, let message = savedMessage.mobMessage {
						var filePath: String?
						if let file = savedMessage.rlsFile {
							filePath = file.mobPath
						}
						
						let newMessage = Message(sender: sender, message: message, date: date as Date, filePath: filePath)
						list.append(newMessage)
						
						if !dates.contains(newMessage.date) {
							dates.append(newMessage.date)
						}
						
						if !paritcipants.contains(sender) {
							paritcipants.append(sender)
						}
					}
				})
			}
		}
		
		completion(myNickName, list, dates, paritcipants)
	}
	
	static func saveMyNickForChat(albumName: String, nickName: String, completion: @escaping () -> ()) {
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in
			if let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: albumName, in: localContext) {
				album.mobMyNick = nickName
			}
			
			if let localUser = MUser.mr_findFirst(in: localContext) {
				localUser.mobDefaultNick = nickName
			}
			
		}, completion: { (success, error) in
			completion()
		})
	}
	
	static func deleteChat(chatName: String, completion: @escaping () -> ()) {
		guard let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: chatName), let mobPath = album.mobPath else {
			completion()
			return
		}
		
		let file = File(relativePath: mobPath)
		file.removeThambnail()
		
		if let messages = MMessage.mr_find(byAttribute: "rlsAlbum", withValue: album) as? [MMessage] {
			messages.forEach({ (message) in
				if let file = message.rlsFile, let mobPath = file.mobPath {
					File(relativePath: mobPath).removeThambnail()
				}
			})
		}
		
		NSManagedObjectContext.mr_default().mr_save({ (localContext) in					
			if let album = MAlbum.mr_findFirst(byAttribute: "mobName", withValue: chatName, in: localContext), let mobPath = album.mobPath {
				MMessage.mr_deleteAll(matching: NSPredicate(format: "rlsAlbum == %@", album), in: localContext)
				MAlbum.delete(with: mobPath, context: localContext)
			}
			
		}, completion: { (success, error) in
			try? File.fileManager.removeItem(at: file.url)
			completion()
		})
	}
}
