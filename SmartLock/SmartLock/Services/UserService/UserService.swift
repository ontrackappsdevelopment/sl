//
//  UserService.swift
//  SmartLock
//
//  Created by Bogachev on 6/3/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class UserService {
	
	var user: User!
	
	static let sharedService = UserService()
	
	init() {
		user = User()
	}
	
}
