//
//  WebUploaderService.swift
//  SmartLock
//
//  Created by Bogachev on 2/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

enum TypeService {
	case download
	case upload
}

class WebUploaderService: NSObject, GCDWebUploaderDelegate {
	
	static let sharedInstance = WebUploaderService()
	
	var webServer: GCDWebUploader!
	
	var typeService: TypeService = .download
	
	func start(typeService: TypeService) -> String? {
		
		return nil
	}
	
	func stop() {
		
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didUploadFileAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didMoveItemFromPath fromPath: String!, toPath: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didCreateDirectoryAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didDownloadFileAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didDeleteItemAtPath path: String!) {
		
	}
}
