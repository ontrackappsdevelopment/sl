//
//  NetworkManager.swift
//  SmartLock
//
//  Created by Bogachev on 4/1/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class NetworkManager {
	
	static let BASE_URL = "https://api.chatkeepersave.me"
	
	private var sessionManager: SessionManager!
	private var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
	
	static let sharedInstance = NetworkManager()
	
	var backgroundCompletionHandler: (() -> Void)? {
		get {
			return sessionManager.backgroundCompletionHandler
		}
		set {
			sessionManager.backgroundCompletionHandler = newValue
		}
	}
	
	init() {
		//		let serverTrustPolicies: [String: ServerTrustPolicy] = [
		//			"api.smartphotolock.com": .pinCertificates(
		//				certificates: ServerTrustPolicy.certificates(),
		//				validateCertificateChain: true,
		//				validateHost: true
		//			)
		////			, "insecure.expired-apis.com": .DisableEvaluation
		//		]
		//
		//		let configuration = URLSessionConfiguration.default
		//		configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
		//
		//		sessionManager = SessionManager.default//SessionManager(configuration: URLSessionConfiguration.default, delegate: SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
		
		let configuration = URLSessionConfiguration.background(withIdentifier: "com.chatkeeper.app.background")
		configuration.timeoutIntervalForRequest = 900
		configuration.timeoutIntervalForResource = 900
		sessionManager = Alamofire.SessionManager(configuration: configuration)
		
//		sessionManager.delegate.downloadTaskDidFinishDownloadingToURL = { (session, task, url) in
//			print("asdfsdf")
//		}
//		
//		sessionManager.delegate.dataTaskDidReceiveData = { (session, task, data) in
//			print("aaaa")
//		}
//		
//		sessionManager.delegate.dataTaskDidReceiveResponseWithCompletion = { (session, task, response, _) in
//			print("asdasd")
//		}
//		
//		sessionManager.delegate.taskDidComplete = { (session, task, error) in
//			print("asdasd")
//		}
//		
//		sessionManager.delegate.dataTaskDidReceiveResponse = {(session:URLSession, dataTask:URLSessionDataTask, response:URLResponse) -> URLSession.ResponseDisposition in
//			return URLSession.ResponseDisposition.allow
//		}
	}
	
	func registerBackgroundTask() {
		backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
			self?.endBackgroundTask()
		}
		assert(backgroundTask != UIBackgroundTaskInvalid)
	}
	
	func endBackgroundTask() {
		print("Background task ended.")
		UIApplication.shared.endBackgroundTask(backgroundTask)
		backgroundTask = UIBackgroundTaskInvalid
	}
	
	func switchToBackground() {
		registerBackgroundTask()
		
		//		let configuration = URLSessionConfiguration.background(withIdentifier: "com.smartlock.app.background")
		//		configuration.timeoutIntervalForRequest = 200
		//		configuration.timeoutIntervalForResource = 200
		//		_ = Alamofire.SessionManager(configuration: configuration)
		
		//		sessionManager.session.getAllTasks { (tasks) in
		//
		//			print("\(tasks)")
		//
		//			tasks.forEach({ (task) in
		//				task.cancel()
		//			})
		//
		//			print("sdfghj")
		//
		//			BackupService.sharedInstance.switchToBackground()
		//
		////			(tasks as! [URLSessionDownloadTask]).forEach({ (task) in
		////
		////				task.cancel(byProducingResumeData: { (resumeData) in
		////
		////					var newTask: URLSessionDownloadTask!
		////
		////					if resumeData != nil {
		////
		////						newTask = self.session.downloadTask(withResumeData: resumeData!)
		////
		////					} else {
		////
		////						newTask = self.session.downloadTask(with: task.currentRequest!)
		////
		////					}
		////
		////					self.didChangeTaskIdentifier(old: task.taskIdentifier, new: newTask.taskIdentifier)
		////
		////					newTask.resume()
		////
		////				})
		////
		////			})
		//
		//		}
	}
	
	func switchToForeground() {
		
		endBackgroundTask()
		let configuration = URLSessionConfiguration.default
		_ = Alamofire.SessionManager(configuration: configuration)
	}
	
	struct Requests {
		
		static func zoo (compleation: @escaping (_ success: String?, _ errorMessage: String?) -> ()) {
			
			Alamofire.request("https://smartphotolock.com/zoo/hello.json").responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["hello"] as? String {
						
						compleation(result, nil)
						
					} else {
						
						compleation(nil, "Error")
						
					}
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func register (email: String, device_id: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
			
			let parameters = ["email": email, "device_id": device_id]
			
			let url = URL(string: "\(BASE_URL)/register")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .post, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func registerConfirm (deviceToken: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
			
			let parameters = ["device_register_token": deviceToken]
			
			let url = URL(string: "\(BASE_URL)/register/confirm_device")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .put, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						if errorMessage == "not_found_device" {
							compleation(false, "Confirmation code mismatch")
						} else {
							compleation(false, errorMessage)
						}
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func changeEmail (email: String, device_id: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
			
			let parameters = ["email": email]
			
			let url = URL(string: "\(BASE_URL)/register/change_email?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .post, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						if errorMessage == "this_is_your_email" {
							
							DataManager.registerUser(email: email)
							
						}
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func changeEmailConfirm (deviceToken: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) {
			
			let parameters = ["token": deviceToken]
			
			let url = URL(string: "\(BASE_URL)/register/change_email/confirm")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .put, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						if errorMessage == "not_found_device" {
							compleation(false, "Confirmation code mismatch")
						} else {
							compleation(false, errorMessage)
						}
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func changePassword (device_id: String, compleation: @escaping (_ code: String, _ success: Bool, _ errorMessage: String?) -> ()) {
			
			let restoreHash = String.randomNumeric(length: 4)
			
			let parameters = ["restore_hash": restoreHash]
			
			let url = URL(string: "\(BASE_URL)/register/change_password?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .put, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(restoreHash, true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(restoreHash, false, errorMessage)
						
					} else {
						
						compleation(restoreHash, false, "Error")
						
					}
					
				} else {
					
					compleation(restoreHash, false, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func uploadFile (device_id: String, date: String, data: Data, fileURL: URL, path: String, encodingCompletion: @escaping (_ request: UploadRequest?) -> (), compleation: @escaping (_ uuid: String?, _ errorMessage: String?) -> ()) {
			
			//			let multipartFormData = MultipartFormData()
			//			multipartFormData.append(data, withName: "image", fileName: fileURL.lastPathComponent, mimeType: fileURL.mimeType())
			//			multipartFormData.append(path.data(using: String.Encoding.utf8)!, withName: "file_path")
			//			multipartFormData.append(date.data(using: String.Encoding.utf8)!, withName: "date")
			
			//			guard let data = try? multipartFormData.encode() else {
			//				return nil
			//			}
			
			let url = URL(string: "\(BASE_URL)/file?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.upload(multipartFormData: { (multipartFormData) in
				
				multipartFormData.append(data, withName: "image", fileName: fileURL.lastPathComponent, mimeType: fileURL.mimeType())
				multipartFormData.append(path.data(using: String.Encoding.utf8)!, withName: "file_path")
				multipartFormData.append(date.data(using: String.Encoding.utf8)!, withName: "date")
				
			}, to: url!, encodingCompletion: { result in
				
				switch result {
				case .success(let uploadRequest, _, _):
					
					encodingCompletion(uploadRequest)
					
					uploadRequest.responseJSON { response in
						
						var errorMessage = ""
						
						var error = response.result.error
						if let responseError = error, let afError =  responseError as? AFError, afError.isResponseSerializationError == true {
							if let data = response.data {
								let responseString = String(data: data, encoding: String.Encoding.utf8)
								error = NSError(domain: "Network Error", code: (response.response?.statusCode)!, userInfo: [NSLocalizedDescriptionKey:responseString ?? ""]) as Error
							}
						}
						
						if let error = error {
							errorMessage = error.localizedDescription
						}
						
						if let JSON = response.result.value as? [String:Any] {
							
							if let uuid = JSON["uuid"] as? String {
								
								compleation(uuid, nil)
								
							} else if let errorMessage = JSON["err"] as? String {
								
								compleation(nil, errorMessage)
								
							} else {
								
								compleation(nil, "Error")
								
							}
							
						} else {
							
							compleation(nil, errorMessage)
							
						}
						
					}
					
				case .failure(let error):
					compleation(nil, error.localizedDescription)
				}
				
			})
			
			//			let uploadRequest = NetworkManager.sharedInstance.sessionManager.upload(data, to: url!, method: .post, headers: ["Content-Type" : multipartFormData.contentType])
			//
			//			uploadRequest.responseJSON { response in
			//
			//				var errorMessage = ""
			//
			//				var error = response.result.error
			//				if let responseError = error, let afError =  responseError as? AFError, afError.isResponseSerializationError == true {
			//					if let data = response.data {
			//						let responseString = String(data: data, encoding: String.Encoding.utf8)
			//						error = NSError(domain: "Network Error", code: (response.response?.statusCode)!, userInfo: [NSLocalizedDescriptionKey:responseString ?? ""]) as Error
			//					}
			//				}
			//
			//				if let error = error {
			//					errorMessage = error.localizedDescription
			//				}
			//
			//				if let JSON = response.result.value as? [String:Any] {
			//
			//					if let uuid = JSON["uuid"] as? String {
			//
			//						compleation(uuid, nil)
			//
			//					} else if let errorMessage = JSON["err"] as? String {
			//
			//						compleation(nil, errorMessage)
			//
			//					} else {
			//
			//						compleation(nil, "Error")
			//
			//					}
			//
			//				} else {
			//
			//					compleation(nil, response.result.error?.localizedDescription)
			//
			//				}
			//
			//			}
			//			return uploadRequest
			//
			
		}
		
		static func deleteFile (device_id: String, uuid: String, date: String, filename: String, path: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DataRequest? {
			
			let parameters = ["file_path": path, "file_name": filename, "date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/\(uuid)?device_id=\(device_id)")
			
			let deleteRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .delete, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return deleteRequest
			
		}
		
		static func deleteAlbum (device_id: String, path: String, date: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DataRequest? {
			
			let parameters = ["folder_path": path, "date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/remove_folder?device_id=\(device_id)")
			
			let deleteRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .delete, parameters: parameters, encoding: JSONEncoding(options: .prettyPrinted), headers: nil).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return deleteRequest
			
		}
		
		static func moveFile (from fromPath: String, to toPath: String, device_id: String, date: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DataRequest? {
			
			let parameters = ["file_path": fromPath, "new_file_path": toPath, "date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/move_file?device_id=\(device_id)")
			
			let moveRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .put, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return moveRequest
			
		}
		
		static func moveFolder (from fromPath: String, to toPath: String, device_id: String, date: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DataRequest?  {
			
			let parameters = ["folder_path": fromPath, "new_folder_path": toPath, "date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/move_folder?device_id=\(device_id)")
			
			let moveRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .put, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return moveRequest
			
		}
		
		static func addPasscode (path: String, hash: String, device_id: String, date: String, compleation: @escaping (_ uuid: String?, _ errorMessage: String?) -> ()) -> DataRequest?  {
			
			let parameters = ["folder_path": path, "password_hash": hash, "date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/add_password?device_id=\(device_id)")
			
			let addPasscodeRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .post, parameters: parameters).responseJSON { response in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let uuid = JSON["uuid"] as? String {
						
						compleation(uuid, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(nil, errorMessage)
						
					} else {
						
						compleation(nil, "Error")
						
					}
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return addPasscodeRequest
			
		}
		
		static func deletePasscode (uuid: String, device_id: String, date: String, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DataRequest?  {
			
			let parameters = ["date" : date]
			
			let url = URL(string: "\(BASE_URL)/file/remove_password/\(uuid)?device_id=\(device_id)")
			
			let addPasscodeRequest = NetworkManager.sharedInstance.sessionManager.request(url!, method: .delete, parameters: parameters).responseJSON { (response) in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let result = JSON["result"] as? String, result == "ok" {
						
						compleation(true, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(false, errorMessage)
						
					} else {
						
						compleation(false, "Error")
						
					}
					
				} else {
					
					compleation(false, response.result.error?.localizedDescription)
					
				}
				
			}
			
			return addPasscodeRequest
			
		}
		
		static func getNewFiles (device_id: String, fromDate: Date? = nil, compleation: @escaping (_ backupFiles: [BackupFile]?, _ errorMessage: String?) -> ()) {
			
			var parameters: [String:Any] = ["device_id" : device_id]
			
			if let value = fromDate {
				parameters ["from_date"] = value.iso8601
			}
			
			getFilesList(parameters: parameters) { (backupFile, errorMessage) in
				compleation(backupFile, errorMessage)
			}
			
		}
		
		static func getDeletedFiles (device_id: String, fromDate: Date? = nil, fromRemovedDate: Date? = nil, removed: Bool? = nil, compleation: @escaping (_ backupFiles: [BackupFile]?, _ errorMessage: String?) -> ()) {
			
			var parameters: [String:Any] = ["device_id" : device_id]
			
			if let value = fromRemovedDate {
				parameters["from_removed_date"] = value.iso8601
			}
			
			parameters["removed"] = "true"
			
			getFilesList(parameters: parameters) { (backupFile, errorMessage) in
				compleation(backupFile, errorMessage)
			}
			
		}
		
		static func getFilesList (parameters: [String:Any]?, compleation: @escaping (_ backupFiles: [BackupFile]?, _ errorMessage: String?) -> ()) {
			
			let url = URL(string: "\(BASE_URL)/file/list")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .get, parameters: parameters).responseArray(queue: nil, keyPath: "files", context: nil) { (response: DataResponse<[BackupFile]>) in
				
				if let backupFiles = response.result.value {
					
					compleation(backupFiles, nil)
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func getFile (device_id: String, uuid: String, compleation: @escaping (_ url: String?, _ errorMessage: String?) -> ()) {
			
			let url = URL(string: "\(BASE_URL)/file/\(uuid)?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .get, parameters: nil).responseJSON { response in
				
				if let JSON = response.result.value as? [String:Any] {
					
					if let url = JSON["url"] as? String {
						
						compleation(url, nil)
						
					} else if let errorMessage = JSON["err"] as? String {
						
						compleation(nil, errorMessage)
						
					} else {
						
						compleation(nil, "Error")
						
					}
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func getMoveLog (device_id: String, fromDate: Date? = nil, compleation: @escaping (_ backupFiles: [BackupMoveLog]?, _ errorMessage: String?) -> ()) {
			
			var parameters: [String:Any]?
			
			if let value = fromDate {
				parameters = ["from_date" : value.iso8601]
			}
						
			let url = URL(string: "\(BASE_URL)/file/move_logs?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .get, parameters: parameters).responseArray(queue: nil, keyPath: "logs", context: nil) { (response: DataResponse<[BackupMoveLog]>) in
				
				if let moveLogs = response.result.value {
					
					compleation(moveLogs, nil)
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func getListPasswords (device_id: String, fromDate: Date? = nil, compleation: @escaping (_ backupFiles: [BackupPasscode]?, _ errorMessage: String?) -> ()) {
			
			var parameters: [String:Any]?
			
			if let value = fromDate {
				parameters = ["from_date" : value.iso8601]
			} else {
				parameters = ["removed" : "false"]
			}
			
			let url = URL(string: "\(BASE_URL)/file/list_passwords?device_id=\(device_id)")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .get, parameters: parameters).responseArray(queue: nil, keyPath: "passwords_list", context: nil) { (response: DataResponse<[BackupPasscode]>) in
				
				if let passwords = response.result.value {
					
					compleation(passwords, nil)
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			}
			
		}
		
		static func downloadFile (url: URL, destinationURL: URL, compleation: @escaping (_ success: Bool, _ errorMessage: String?) -> ()) -> DownloadRequest? {
			
			let destination: DownloadRequest.DownloadFileDestination = { _, _ in
				return (destinationURL, [.removePreviousFile, .createIntermediateDirectories])
			}
			
			let downloadRequest = NetworkManager.sharedInstance.sessionManager.download(url, to: destination).response { response in
				
				if response.error == nil {
					
					compleation(true, nil)
					
				} else {
					
					compleation(false, response.error?.localizedDescription)
					
				}
				
			}
			
			return downloadRequest
			
		}
		
		static func getReviewData (typeUser: String, compleation: @escaping (_ reviewObject: ReviewObject?, _ errorMessage: String?) -> ()) {
			
			guard let languageCode = (Locale.current as NSLocale).object(forKey: .languageCode) as? String else { return }
			
			let parameters = ["typeUser" : typeUser, "locale" : languageCode, "app" : Bundle.main.bundleIdentifier ?? ""]
			
			let url = URL(string: "https://api.smartphotolock.com/api2/review")
			
			NetworkManager.sharedInstance.sessionManager.request(url!, method: .get, parameters: parameters).responseObject(completionHandler: { (response: DataResponse<ReviewObject>) in
				
				if let reviewObject = response.result.value {
					
					compleation(reviewObject, nil)
					
				} else {
					
					compleation(nil, response.result.error?.localizedDescription)
					
				}
				
			})
			
		}
		
	}
	
}
