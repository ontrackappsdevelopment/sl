//
//  KeychainService.swift
//  SmartLock
//
//  Created by Bogachev on 2/12/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import Security

let userAccount = "com.chatkeeper.app.AuthenticatedUser"
let accessGroup = "com.chatkeeper.app.SecuritySerivice"
let passwordKey = "com.chatkeeper.app.KeyForPassword"
let firstLaunchDateKey = "com.chatkeeper.app.firstLaunchDateKey"

let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)
//let kSecAttrAccessibleAlwaysValue = NSString(format: kSecAttrAccessibleAlways)

class KeychainService: NSObject {
	
	static var _firstLaunchDate: Date?
	
	static func saveFirstLaunchDate() {
		if firstLaunchDate == nil {
			self.save(service: firstLaunchDateKey as NSString, string: DateFormatter.defaultFormatter.string(from: Date()) as NSString)
			
//			NetworkManager.Requests.zoo(compleation: { (zoo, errorMessage) in
//
				var days = 3
//				if let zoo = zoo {
//					switch zoo {
//					case "cat": days = 3
//					case "dog": days = 5
//					case "pig": days = 7
//					default: break
//					}
			
					UserDefaults.standard.setValue(days, forKey: "FreeTrialPeriod")
			
//				}
			
//			})
		}
		
		_firstLaunchDate = firstLaunchDate
	}
	
	static var firstLaunchDate: Date? {
		
		if let dateString = UserDefaults.standard.string(forKey: "firstLaunchDateKey") {
			return DateFormatter.defaultFormatter.date(from: dateString as String)
		} else {
		
			if let dateString = self.load(service: firstLaunchDateKey as NSString), dateString.length > 0 {
				UserDefaults.standard.set(dateString, forKey: "firstLaunchDateKey")
				UserDefaults.standard.synchronize()
				return DateFormatter.defaultFormatter.date(from: dateString as String)
			} else {
				return nil
			}
			
		}
	}
	
	static func savePassword(token: NSString) {
		self.save(service: passwordKey as NSString, string: token)
	}
	
	static func loadPassword() -> NSString? {
		return self.load(service: passwordKey as NSString)
	}
	
	static func deletePassword(token: NSString) {
		self.delete(service: passwordKey as NSString, data: token)
	}
	
	static func save(service: NSString, string: NSString) {
		let dataFromString = string.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!
		
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
		
		SecItemDelete(keychainQuery as CFDictionary)
		SecItemAdd(keychainQuery as CFDictionary, nil)
	}
	
	static func save(service: NSString, data: Data) {
		
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, data], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
		
		SecItemDelete(keychainQuery as CFDictionary)
		SecItemAdd(keychainQuery as CFDictionary, nil)
	}
	
	static func load(service: NSString) -> NSString? {
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
		
		var dataTypeRef :AnyObject?
		let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
		var contentsOfKeychain: NSString? = nil
		
		if status == errSecSuccess {
			if let retrievedData = dataTypeRef as? NSData {
				contentsOfKeychain = String(data: retrievedData as Data, encoding: String.Encoding.utf8) as NSString?
			}
		} else {
			print("Nothing was retrieved from the keychain. Status code \(status)")
		}
		
		return contentsOfKeychain
	}
	
	static func loadData(service: NSString) -> Data? {
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
		
		var dataTypeRef :AnyObject?
		let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
		
		if status == errSecSuccess {
			if let retrievedData = dataTypeRef as? NSData {
				return retrievedData as Data
			}
		} else {
			print("Nothing was retrieved from the keychain. Status code \(status)")
		}
		
		return nil
	}
	
	static func delete(service: NSString, data: NSString) {
		let dataFromString = data.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!
		
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
		
		SecItemDelete(keychainQuery as CFDictionary)
	}
	
	static func delete(service: NSString, data1: Data) {
		
		let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, data1], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
		
		SecItemDelete(keychainQuery as CFDictionary)
	}

}
