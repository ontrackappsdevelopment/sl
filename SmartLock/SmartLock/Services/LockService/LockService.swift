//
//  LockService.swift
//  SmartLock
//
//  Created by Bogachev on 3/4/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AudioToolbox
import LocalAuthentication
import CoreLocation
import YandexMobileMetrica

enum LockType: Int {
	case undefined
	case numpad4
	case numpad6
	case dotlock
	case aplhanum
	
	var metricaName: String {
		switch self {
		case .numpad4:
			return "4-pin"
		case .numpad6:
			return "6-pin"
		case .aplhanum:
			return "Alfanum"
		case .dotlock:
			return "Dotlock"
		default: return ""
		}
	}
	
}

enum DecoyType: Int {
	case decoyOff
	case after3attepms
	case after5attepms
	case after10attepms
}

class LockService {
	
	static let sharedService = LockService()
	
	var lockAlbum: MAlbum?
	var lockAlbumFile: File?
	
	var firstLaunch: Bool {
		return !UserDefaults.standard.bool(forKey: "firstLaunch")
	}
	
	var fifthLaunch: Bool {
		return !UserDefaults.standard.bool(forKey: "fifthLaunch")
	}
	
	var tenthLaunch: Bool {
		return !UserDefaults.standard.bool(forKey: "tenthLaunch")
	}
	
	var launhes: Int {
		get {
			return UserDefaults.standard.integer(forKey: "launches")
		}
		
		set (newValue) {
			
			if firstLaunch == false {
				UserDefaults.standard.set(newValue, forKey: "launches")
				UserDefaults.standard.synchronize()
			}
		}
	}
	
	var user: MUser? {
		
		let context = NSManagedObjectContext.mr_default()
		
		if let objects = MUser.mr_findAll(in: context), let object = objects.first, let user = object as? MUser {
			
			return user
			
		}
		
		return nil
		
	}
	
	var isDecoyEnabled = false
	
	func firstLaunchCompleted() {
		UserDefaults.standard.set(true, forKey: "firstLaunch")
		UserDefaults.standard.synchronize()
	}
	
	func fifthLaunchCompleted() {
		UserDefaults.standard.set(true, forKey: "fifthLaunch")
		UserDefaults.standard.synchronize()
	}
	
	func tenthLaunchCompleted() {
		UserDefaults.standard.set(true, forKey: "tenthLaunch")
		UserDefaults.standard.synchronize()
	}
	
	func createUser(lockType: LockType, passcode: String?) -> MUser? {
		YMMYandexMetrica.reportEvent("Create User", parameters: ["lockType": lockType.metricaName], onFailure: nil)
		let context = NSManagedObjectContext.mr_default()
		context.mr_save(blockAndWait: { (localContext) in
			if let user = MUser.mr_createEntity(in: localContext) {
				user.mobLockType = Int16(lockType.hashValue)
				user.mobPasscode = passcode
				user.mobSlideShowTransition = Int16(SlideShowTransition.dissolve.rawValue)
				user.mobSlideShowInterval = Int16(SlideShowTimeInterval.three.rawValue)
				user.mobDefaultNick = ""
			}
		})
		return MUser.mr_findFirst(in: context)
	}
		
	func saveBreakInAttempt(imageFileName: String?, location: CLLocation?, time: Date?)  {
		
		let context = NSManagedObjectContext.mr_default()
		
		context.mr_save(blockAndWait: { (localContext) in
		
			if let attemp = MBreakIn.mr_createEntity(in: localContext) {
				
				if let fileName = imageFileName {
					attemp.mobImageFileName = fileName
				}
				
				if location != nil {
					attemp.mobLatitude = location!.coordinate.latitude.roundTo(places: 5)
					attemp.mobLongitude = location!.coordinate.longitude.roundTo(places: 5)
				}
				
				if let time = time {
					attemp.mobTime = time as NSDate?
				}
				
			}
			
		})
	}
	
	func deleteBreakInAttempt(attemp: MBreakIn) {
		let context = NSManagedObjectContext.mr_default()
		context.mr_save(blockAndWait: { (localContext) in
			if let localAttemp = attemp.mr_(in: localContext) {
				localAttemp.mr_deleteEntity()
			}
		})
	}
	
	func deleteAllBreakInAttempt() {
		let context = NSManagedObjectContext.mr_default()
		context.mr_save(blockAndWait: { (localContext) in
			MBreakIn.mr_truncateAll(in: localContext)
		})
	}

	func setLockScreen(lockType: LockType, presenter: UIViewController, canCancel: Bool = true, canOpenDecoy: Bool = false, showAnimated: Bool = false, alpha: CGFloat = 1.0, remove: Bool = false,
	                   completion: @escaping (String) -> ()) {

		var lockViewController: LockViewController?
		
		switch lockType {
		case .numpad4, .numpad6:
			lockViewController = StoryboardScene.instantiateNumpadLockScene(modal: alpha != 1.0)
			(lockViewController as! NumpadLockViewController).pinLenght = lockType == .numpad4 ? 4 : 6
			lockViewController?.automaticallyAdjustsScrollViewInsets = true
		case .dotlock:
			lockViewController = StoryboardScene.instantiatePatternLockScene(modal: alpha != 1.0)
		case .aplhanum:
			lockViewController = StoryboardScene.instantiateAlphanumericLockScene(modal: alpha != 1.0)
		default:
			break
		}

		if let lockViewController = lockViewController {
			lockViewController.canCancel = canCancel
			lockViewController.canOpenDecoy = canOpenDecoy
			
			lockViewController.success = { (pin: String) in
				
				if remove == true {
					UIView.animate(withDuration: 0.3, animations: {
						lockViewController.view.alpha = 0
					}, completion: { (_) in
						lockViewController.willMove(toParentViewController: nil)
						lockViewController.view.removeFromSuperview()
						lockViewController.removeFromParentViewController()
						completion(pin)
					})
					
				} else {
					completion(pin)
				}
			}
			
			lockViewController.failure = { }
			
			lockViewController.cancel = { }
		
			presenter.addViewControllerAsSubView(viewController: lockViewController, showAnimated: showAnimated)
		}
		
		
	}
	
	static func lockWithPadLockScreen() {
		
		ABPadLockScreenView.appearance().backgroundColor = UIColor(hue:0.61, saturation:0.55, brightness:0.64, alpha:1)
		
		ABPadLockScreenView.appearance().labelColor = UIColor.white
		
		let buttonLineColor = UIColor.white
		ABPadButton.appearance().backgroundColor = UIColor.clear
		ABPadButton.appearance().borderColor = UIColor(red: 76/255, green: 72/255, blue: 239/255, alpha: 1)
		ABPadButton.appearance().selectedColor = buttonLineColor
		ABPinSelectionView.appearance().selectedColor = buttonLineColor
		ABPinSelectionView.appearance().borderColor = UIColor(red: 76/255, green: 72/255, blue: 239/255, alpha: 1)
		
	}
	
	func checkForEmail(compleate: () -> ()) {
		if user?.mobEmail == nil {
			compleate()
		} else {
			if user?.mobEmail == "" {
				compleate()
			}
		}
	}
	
}
