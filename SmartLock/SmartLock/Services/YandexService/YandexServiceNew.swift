//
//  YandexServiceNew.swift
//  SmartLock
//
//  Created by Bogachev on 2/16/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class YandexServiceNew {
  
  private var auth2ViewController: YOAuth2ViewController?
  
  var autorizationCompleation: (() -> ())?
  
  public var token : String?
  public let baseURL = "https://cloud-api.yandex.net:443"
  public let clientId = "bdfb621d6c214f7ba090f8d9ae9ec6d9"
  
  private var oauthURL: String {
    return "https://oauth.yandex.ru/authorize?response_type=token&client_id=\(clientId)"
  }
  
  static let sharedInstance = YandexServiceNew()
  
  init() {
    
    
    
  }

  
  func fetchAuthorization(presenter: UIViewController, compleation: @escaping () -> () ) {
    
    autorizationCompleation = compleation
    
//    if session.oAuthToken == nil {
    
      auth2ViewController = YOAuth2ViewController(delegate: nil)
      auth2ViewController?.pushFrom(viewController: presenter)
      
//    } else {
//
//      compleation()
//      
//    }
    
  }
  
  func handleURL(url: NSURL) -> Bool {
    var querydict : [String: String] = [:]
    
    if let fragment = url.fragment {
      for tuple in fragment.components(separatedBy: "&") {
        let nv = tuple.components(separatedBy: "=") as [NSString]
        
        switch (nv[0], nv[1]) {
        case (let name, let value):
          querydict[name as String] = value as String
        }
      }
      token = querydict["access_token"]
      if let _ = token {
        saveToken(token!)
      }
      return true
    }
    return false
  }
  
  
//  let response_type = "token"
//
//  let urlstr = "https://oauth.yandex.ru/authorize?response_type=\(response_type)&client_id=\(clientId)"

  fileprivate func saveToken(_ token: String) {
    KeychainService.savePassword(token: token as NSString)
  }
  
  fileprivate func loadToken() -> String? {
    return KeychainService.loadPassword() as String?
  }
  
  fileprivate func deleteToken() {
//    KeychainService.deletePassword(token: session.oAuthToken as NSString)
  }
  
}
