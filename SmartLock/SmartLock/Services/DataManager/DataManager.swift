//
//  DataManager.swift
//  SmartLock
//
//  Created by Bogachev on 4/1/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class DataManager {
	
	static func createDeviceIfNeeded() {
		
		guard DataManager.device == nil else { return }
		
		NSManagedObjectContext.mr_default().mr_save(blockAndWait: { (localContext) in
			if let device = MDevice.mr_createEntity(in: localContext) {
				device.mobID = UUID().uuidString
			}
		})
	}
	
	static var device: MDevice? {
		let context = NSManagedObjectContext.mr_default()
		if let objects = MDevice.mr_findAll(in: context), let object = objects.first, let device = object as? MDevice {
			return device
		}
		return nil
	}
	
	static func registerUser(email: String) {
		let context = NSManagedObjectContext.mr_default()
		context.mr_save(blockAndWait: { (localContext) in
			if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
				localUser.mobEmail = email
				localUser.mobConfirmed = true
			}
		})
	}
	
}
