//
//  AppDelegate.swift
//  SmartLock
//
//  Created by Bogachev on 2/3/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import MagicalRecord
import Fabric
import Crashlytics
import SwiftyStoreKit
import YandexMobileMetrica
import FacebookCore
import Firebase

let applicationDidUnlocked = NSNotification.Name(rawValue: "ApplicationDidUnlocked")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CrashlyticsDelegate {
	
	var window: UIWindow?
	var inactiveView: UIView?
	var lockViewController: LockViewController?
	
	static var appIsLocked: Bool = false {
		didSet {
			if appIsLocked == false && oldValue == true {
				NotificationCenter.default.post(name: applicationDidUnlocked, object: nil)
			}
		}
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
//		print("FileService.containerURL: \(FileService.containerURL)")
		
		UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "SF UI Display Regular", size: 14.0)
		
		UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
				
//		SDKSettings.enableLoggingBehavior(.appEvents)
		
		FirebaseApp.configure()
		
//		if let receiptData = KeychainService.loadData(service: "SmartLockAppReceipt") {
//			KeychainService.delete(service: "SmartLockAppReceipt", data1: receiptData)
//		}
//		
//		if let receiptData = KeychainService.loadData(service: "firstLaunchDateKey") {
//			KeychainService.delete(service: "firstLaunchDateKey", data1: receiptData)
//		}

		Fabric.with([Crashlytics.self])
		
		YMMYandexMetrica.activate(withApiKey: "4a81c7e5-3e11-4a44-8c9e-f3f8e500ef33")

		MagicalRecord.setupAutoMigratingCoreDataStack()
		
		let navigationBarApearance = UINavigationBar.appearance()
		let font = UIFont(name: "SFUIDisplay-Regular", size: 18)
		let color = UIColor.white
		navigationBarApearance.titleTextAttributes = [NSFontAttributeName: font!, NSForegroundColorAttributeName:color]
		
		application.applicationSupportsShakeToEdit = true
		
		completeIAPTransactions()
		
		setup()
		
		return true
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		NetworkManager.sharedInstance.switchToBackground()
		lockScreen()
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		
		AppEventsLogger.activate(application)
		
		if inactiveView != nil {
			inactiveView?.removeFromSuperview()
			inactiveView = nil
		}
		
		LockService.sharedService.launhes += 1
		
		NetworkManager.sharedInstance.switchToForeground()
	}

	func applicationWillResignActive(_ application: UIApplication) {
		
		guard AppDelegate.appIsLocked == false else { return }
		guard inactiveView == nil else { return }
		
		guard BaseViewController.avoidHidingWhenResignActive == false else {
			BaseViewController.avoidHidingWhenResignActive = true
			return
		}
		
		if let window = window {
			window.endEditing(true)
		}
		
		inactiveView = UIView(frame: UIScreen.main.bounds)
		
		inactiveView?.backgroundColor = UIColor.black
		
		UIApplication.shared.keyWindow!.addSubview(inactiveView!)
	}
	
	func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
		NetworkManager.sharedInstance.backgroundCompletionHandler = completionHandler
	}

	//MARK: - Internal
	
	private func setup() {
		
//		User.verifyReceipt { (result) in }
		
		print("\(NSPersistentStore.mr_url(forStoreName: MagicalRecord.defaultStoreName())!.path)")
		
		if let deviceID = MDevice.mr_findFirst()?.mobID {
			print("deviceID: \(deviceID)")
		}
		
		if let email = MUser.mr_findFirst()?.mobEmail {
			print("deviceID: \(email)")
		}
		
		KeychainService.saveFirstLaunchDate()
		
		_ = FileService.sharedInstance
		_ = LocationManager.sharedInstance
		_ = BackupService.sharedInstance
		
		DataManager.createDeviceIfNeeded()
		
		if let bundleVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
			if let userDefaultsVersion = UserDefaults.standard.string(forKey: "AppVersion") {
				
				if userDefaultsVersion != bundleVersion {
					ReviewService.setupUserSession(type: .update)
					UserDefaults.standard.set(Date(), forKey: "AppVersionDate")
					UserDefaults.standard.removeObject(forKey: "canShowSwipeHint")
					UserDefaults.standard.removeObject(forKey: "canShowDrageHint")
					UserDefaults.standard.removeObject(forKey: "canShowBackupException")
				}
				
			} else {
				
				ReviewService.setupUserSession(type: .new)
				UserDefaults.standard.set(Date(), forKey: "AppVersionDate")
			}
			
			UserDefaults.standard.set(bundleVersion, forKey: "AppVersion")
		}
		
		UserDefaults.standard.synchronize()
		
	}
	
	private func completeIAPTransactions() {
		
		SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
			
			for purchase in purchases {
				
				if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
					
					if purchase.needsFinishTransaction {
						
						SwiftyStoreKit.finishTransaction(purchase.transaction)
					}
					
					print("purchased: \(purchase.productId)")
				}
			}
		}
	}	
	
	func pushMainViewControler(animated : Bool = true) {
		if let window = window, let navigationController = window.rootViewController as? UINavigationController {
			let controller = StoryboardScene.Chat.instantiateChatListScene()
			navigationController.setViewControllers([controller], animated: animated)
		}
	}

	func purchaseViewControler(animated : Bool = true) {
		if let window = window, let navigationController = window.rootViewController as? UINavigationController {
			let controller = StoryboardScene.instantiatePurchasesScene()
			navigationController.setViewControllers([controller], animated: animated)
		}
	}
	
	func setEmailViewControler(animated : Bool = true) {
		if let window = window, let navigationController = window.rootViewController as? UINavigationController {
			let mainController = StoryboardScene.Chat.instantiateChatListScene()
			let emailController = EmailViewController.register()
			navigationController.setViewControllers([mainController, emailController], animated: animated)
		}
	}
	
	//MARK: - Public
	
	func lockScreen() {
		
		guard AppDelegate.appIsLocked == false else { return }
		
		guard let user = LockService.sharedService.user, let lockTypeValue = LockType(rawValue: Int(user.mobLockType)) else { return }
		
		guard let lockViewController = LockViewController.getLockControllerBy(lockType: lockTypeValue) else { return }
		
		self.lockViewController = lockViewController
		
		lockViewController.lockMode = .unlock
		lockViewController.passcode = user.mobPasscode
		lockViewController.canCancel = false
		
		lockViewController.success = { (pin: String) in
			
			if let window = self.window, let rootViewController = window.rootViewController {
				if let navigationController = rootViewController as? UINavigationController,
					let _ = navigationController.viewControllers.first as? SplashViewController,
					navigationController.viewControllers.count == 2 {
					
					self.removeLock()
					
					if UserService.sharedService.user.appUsingType == .locked {
						self.purchaseViewControler()
					} else {
						self.pushMainViewControler()
					}
					
					AppDelegate.appIsLocked = false
					
				} else {
					
					DispatchQueue.main.async {
						UIView.animate(withDuration: 0.7, animations: {
							lockViewController.view.alpha = 0
						}, completion: { (_) in
							AppDelegate.appIsLocked = false
							self.removeLock()
						})
					}
					
				}
				
			}
		}
		
		lockViewController.failure = {
			
			guard let decoyType = DecoyType(rawValue: Int(LockService.sharedService.user!.mobDecoyPasscodeType)) else {
				return
			}
			
			if decoyType != .decoyOff {
				
				AppDelegate.appIsLocked = false
				
				LockService.sharedService.isDecoyEnabled = true
				FileService.sharedInstance.isDecoyEnabled = true
				
				self.removeLock()
				
				self.pushMainViewControler(animated: false)
			}
			
		}
		
		if let window = window, let rootViewController = window.rootViewController {
			window.endEditing(true)
			
			AppDelegate.appIsLocked = true
			rootViewController.addChildViewController(lockViewController)
			lockViewController.view.backgroundColor = UIColor.black
			lockViewController.view.frame = UIScreen.main.bounds;
			window.addSubview(lockViewController.view)
			rootViewController.didMove(toParentViewController: lockViewController)
			lockViewController.viewWillAppear()
		}
	}
	
	func removeLock() {
		guard let lockViewController = lockViewController else { return }
		
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidUnlockNotification"), object: nil)
		
		lockViewController.remove()
		self.lockViewController = nil
	}
		
	func removeLockAndPushPhotos() {
		if	let window = self.window,
			let rootViewController = window.rootViewController,
			let _ = rootViewController as? UINavigationController {
			
			self.removeLock()
			self.pushMainViewControler()
			AppDelegate.appIsLocked = false
			
		}
	}
}
