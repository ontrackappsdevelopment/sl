//
//  MLog.swift
//  SmartLock
//
//  Created by Bogachev on 4/22/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension MLog {
	
	static func createFile(with path: String, context: NSManagedObjectContext) -> MLog? {
		
		if let log = MLog.mr_createEntity(in: context) {
		
			log.mobType = LogFileType.file.rawValue
			log.mobPath = path
			log.mobAction = LogActionType.create.rawValue
			log.mobDate = NSDate().timeIntervalSince1970
		
			return log
		}
		
		return nil
	}
	
	static func createDirectory(with path: String, context: NSManagedObjectContext) -> MLog? {
		
		if let log = MLog.mr_createEntity(in: context) {
		
			log.mobType = LogFileType.directory.rawValue
			log.mobPath = path
			log.mobAction = LogActionType.create.rawValue
			log.mobDate = NSDate().timeIntervalSince1970
		
			return log
		}
		
		return nil
		
	}
	
	static func deleteFile(with path: String, uuid: String?, context: NSManagedObjectContext) -> MLog? {
		
		var predicate = NSPredicate(format: "mobPath == %@ AND mobAction == %@ AND mobType == %d AND mobBackuping == false", path, LogActionType.create.rawValue, LogFileType.file.rawValue)
		
		if MLog.mr_findFirst(with: predicate, in: context) != nil {
			
			predicate = NSPredicate(format: "mobPath == %@ AND mobBackuping == false", path)
			
			MLog.mr_deleteAll(matching: predicate, in: context)
			
		} else {
			
			predicate = NSPredicate(format: "mobPath == %@ AND mobBackuping == false", path)
			
			MLog.mr_deleteAll(matching: predicate, in: context)
			
			if let log = MLog.mr_createEntity(in: context) {
				log.mobType = LogFileType.file.rawValue
				log.mobPath = path
				log.mobAction = LogActionType.delete.rawValue
				log.mobDate = NSDate().timeIntervalSince1970
				log.mobUUID = uuid
				return log
			}
			
		}
		
		return nil
		
	}

	static func deleteDirectoryWithoutLog(with path: String, context: NSManagedObjectContext) {
		
		var predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@ AND mobBackuping == false", "\(path)/")
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
		predicate = NSPredicate(format: "mobPath == %@ AND mobBackuping == false", path)
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
	}
	
	static func deleteDirectory(with path: String, context: NSManagedObjectContext) {
		
		if MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND mobAction == %@ AND mobBackuping == false", path, LogActionType.create.rawValue), in: context) == nil {
			let log = MLog.mr_createEntity(in: context)
			log?.mobDate = NSDate().timeIntervalSince1970
			log?.mobPath = path
			log?.mobType = LogFileType.directory.rawValue
			log?.mobAction = LogActionType.delete.rawValue
		}
		
		var predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@ AND mobBackuping == false", "\(path)/")
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
		predicate = NSPredicate(format: "mobPath == %@ AND mobAction != %@ AND mobBackuping == false", path, LogActionType.delete.rawValue)
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
	}
	
	static func renameAlbum(oldPath: String, newPath: String, context: NSManagedObjectContext) -> MLog? {
		
		let predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@ AND mobBackuping == false", "\(oldPath)/")
		
		(MLog.mr_findAll(with: predicate, in: context) as! [MLog]).forEach { (log) in
			
			log.mobPath = log.mobPath?.replacingOccurrences(of: oldPath, with: newPath)
			
		}
		
		if let log = MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND (mobAction == %@ OR mobAction == %@ OR mobAction == %@) AND mobBackuping == false",
		                                                 oldPath, LogActionType.create.rawValue, LogActionType.rename.rawValue, LogActionType.move.rawValue), in: context) {
			log.mobPath = newPath
			
			return log
			
		} else {
			
			if let log = MLog.mr_createEntity(in: context) {
			
				log.mobType = LogFileType.directory.rawValue
				
				log.mobOldPath = oldPath
				
				log.mobPath = newPath
				
				log.mobAction = LogActionType.rename.rawValue
				
				log.mobDate = NSDate().timeIntervalSince1970
				
				return log
				
			}
			
		}
		
		return nil
	}
	
	static func moveAlbum(from fromPath: String, to toPath: String, context: NSManagedObjectContext) -> MLog? {
		
		let predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@ AND mobBackuping == false", "\(fromPath)/")

		let newDateCreate = NSDate().timeIntervalSince1970
		
		(MLog.mr_findAll(with: predicate, in: context) as! [MLog]).sorted(by: { $0.0.mobDate > $0.1.mobDate }).forEach { (log) in
			
			log.mobPath = log.mobPath?.replacingOccurrences(of: fromPath, with: toPath)
			log.mobDate = NSDate().timeIntervalSince1970

		}
		
		if let log = MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND (mobAction == %@ OR mobAction == %@ OR mobAction == %@ OR mobAction == %@ OR mobAction == %@) AND mobBackuping == false",
		                                                 fromPath, LogActionType.create.rawValue, LogActionType.rename.rawValue, LogActionType.move.rawValue, LogActionType.addpasscode.rawValue, LogActionType.deletepasscode.rawValue), in: context) {
			log.mobPath = toPath
			log.mobDate = newDateCreate
			
			return log
			
		} else {
			
			if let log = MLog.mr_createEntity(in: context) {
	
				log.mobType = LogFileType.directory.rawValue
				log.mobOldPath = fromPath
				log.mobPath = toPath
				log.mobAction = LogActionType.move.rawValue
				log.mobDate = newDateCreate
				
				return log
			
			}
		}
		
		return nil
		
	}
	
	static func moveFile(from fromPath: String, to toPath: String, context: NSManagedObjectContext) -> MLog? {
				
		if let log = MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND (mobAction == %@ OR mobAction == %@ OR mobAction == %@) AND mobBackuping == false",
		                                                 fromPath, LogActionType.create.rawValue, LogActionType.rename.rawValue, LogActionType.move.rawValue), in: context) {
			log.mobPath = toPath
			log.mobDate = NSDate().timeIntervalSince1970

			return log
			
		} else {
			
			if let log = MLog.mr_createEntity(in: context) {
			
				log.mobType = LogFileType.file.rawValue
				log.mobOldPath = fromPath
				log.mobPath = toPath
				log.mobAction = LogActionType.move.rawValue
				log.mobDate = NSDate().timeIntervalSince1970
				return log
				
			}
			
		}
		
		return nil
	}
	
	static func addAlbumPasscode(path: String, lockType: LockType, passcode: String, context: NSManagedObjectContext) -> MLog? {
		
		let predicate = NSPredicate(format: "mobPath == %@ AND (mobAction == %@ OR mobAction == %@) AND mobBackuping == false", path, LogActionType.deletepasscode.rawValue, LogActionType.addpasscode.rawValue)
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
		if let log = MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND mobAction == %@ AND mobBackuping == false", path, LogActionType.create.rawValue), in: context) {
			
			let string = "\(lockType.hashValue)/t/t/t\(passcode)"
			log.mobHash = try! string.aesEncrypt(key: "smartlocksmartlo", iv: "kcoltramskcoltra")
			return log
			
		} else {
			
			if let log = MLog.mr_createEntity(in: context) {
				log.mobType = LogFileType.directory.rawValue
				log.mobPath = path
				log.mobAction = LogActionType.addpasscode.rawValue
				log.mobDate = NSDate().timeIntervalSince1970
				let string = "\(lockType.hashValue)/t/t/t\(passcode)"
				log.mobHash = try! string.aesEncrypt(key: "smartlocksmartlo", iv: "kcoltramskcoltra")
				return log
			}
		}
		
		return nil
	}
	
	static func deleteAlbumPasscode(path: String, uuid: String?, context: NSManagedObjectContext) -> MLog? {
		
		let predicate = NSPredicate(format: "mobPath == %@ AND (mobAction == %@ OR mobAction == %@) AND mobBackuping == false", path, LogActionType.deletepasscode.rawValue, LogActionType.addpasscode.rawValue)
		
		MLog.mr_deleteAll(matching: predicate, in: context)
		
		if let log = MLog.mr_findFirst(with: NSPredicate(format: "mobPath == %@ AND mobAction == %@ AND mobBackuping == false", path, LogActionType.create.rawValue), in: context) {
			log.mobHash = nil
			return log
			
		} else {
			
			if let log = MLog.mr_createEntity(in: context) {
				log.mobType = LogFileType.directory.rawValue
				log.mobPath = path
				log.mobAction = LogActionType.deletepasscode.rawValue
				log.mobUUID = uuid
				log.mobDate = NSDate().timeIntervalSince1970
				return log
			}
		}
		
		return nil
	}
	
}
