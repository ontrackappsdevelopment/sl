//
//  MAlbum.swift
//  SmartLock
//
//  Created by Bogachev on 3/19/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension MAlbum {
	
	static func create(with path: String, name: String, lockType: LockType, passcode: String, context: NSManagedObjectContext) {
		
		if MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: path, in: context) != nil {
			return
		}
		
		let album = MAlbum.mr_createEntity(in: context)!
		
		album.mobThumbnail = "\(UUID().uuidString).jpg"		
		album.mobName = name
		album.mobPath = path
		album.mobAlbumPath = path.deletingLastPathComponent()
		album.mobLockType = Int16(lockType.hashValue)
		album.mobPasscode = passcode
		album.mobEnableTouchID = false
		album.mobDate = Date().timeIntervalSince1970
		album.mobMyNick = ""
		
		_ = MLog.createDirectory(with: path, context: context)
		
	}

	static func createFileWithoutLog(with path: String, name: String, context: NSManagedObjectContext) {
		if MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: path, in: context) != nil {
			return
		}
		
		if let album = MAlbum.mr_createEntity(in: context) {
			album.mobThumbnail = "\(UUID().uuidString).jpg"
			album.mobName = name
			album.mobPath = path
			album.mobAlbumPath = path.deletingLastPathComponent()
			album.mobLockType = Int16(LockType.undefined.hashValue)
			album.mobEnableTouchID = false
			album.mobDate = Date().timeIntervalSince1970
			album.mobMyNick = ""
		}
	}

	static func deleteWithoutLog(with path: String, context: NSManagedObjectContext) {
		
		var predicate = NSPredicate(format: "mobPath == %@", "\(path)")
		MAlbum.mr_findAll(with: predicate, in: context)?.forEach({ (object) in
			object.mr_deleteEntity(in: context)
			
			MMessage.mr_deleteAll(matching: NSPredicate(format: "rlsAlbum == %@", object), in: context)
		})
		
		predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@", path)
		MFile.mr_deleteAll(matching: predicate, in: context)
		
	}
	
	static func delete(with path: String, context: NSManagedObjectContext) {
		
		var predicate = NSPredicate(format: "mobPath == %@", "\(path)")
		MAlbum.mr_findAll(with: predicate, in: context)?.forEach({ (object) in
			MLog.deleteDirectory(with: (object as! MAlbum).mobPath!, context: context)
			object.mr_deleteEntity(in: context)
		})
		
		predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@", path)
		MFile.mr_deleteAll(matching: predicate, in: context)
		
	}
	
	func rename(newPath: String, context: NSManagedObjectContext) {
		
		let oldPath = mobPath
		
		mobPath = newPath
		mobAlbumPath = newPath.deletingLastPathComponent()
		
		_ = MLog.renameAlbum(oldPath: oldPath!, newPath: newPath, context: context)
		
		let predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@", "\(oldPath!)/")
		
		(MAlbum.mr_findAll(with: predicate, in: context) as! [MAlbum]).sorted(by: { $0.0.mobDate > $0.1.mobDate }).forEach { (album) in
			
			album.mobPath = album.mobPath?.replacingOccurrences(of: oldPath!, with: newPath)
			
			if let path = album.mobPath {
				album.mobAlbumPath = path.deletingLastPathComponent()
			}
	
		}
		
		(MFile.mr_findAll(with: predicate, in: context) as! [MFile]).sorted(by: { $0.0.mobDate > $0.1.mobDate }).forEach { (file) in
			
			file.mobPath = file.mobPath?.replacingOccurrences(of: oldPath!, with: newPath)
			
			if let path = file.mobPath {
				file.mobFilePath = path.deletingLastPathComponent()
			}
			
		}
		
	}
	
	func moveTo(path: String, context: NSManagedObjectContext) {
		
		let oldPath = mobPath!
		let newPath = path
		
		let predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@", "\(oldPath)/")
		
		mobDate = Date().timeIntervalSince1970
				
		(MAlbum.mr_findAll(with: predicate, in: context) as! [MAlbum]).sorted(by: { $0.0.mobDate < $0.1.mobDate }).forEach { (album) in
			
			album.mobPath = album.mobPath?.replacingOccurrences(of: oldPath, with: newPath)
			if let path = album.mobPath {
				album.mobAlbumPath = path.deletingLastPathComponent()
			}
			
			album.mobDate = Date().timeIntervalSince1970
			
		}
		
		(MFile.mr_findAll(with: predicate, in: context) as! [MFile]).sorted(by: { $0.0.mobDate < $0.1.mobDate }).forEach { (file) in
			
			file.mobPath = file.mobPath?.replacingOccurrences(of: oldPath, with: newPath)
			if let path = file.mobPath {
				file.mobFilePath = path.deletingLastPathComponent()
			}
			
			file.mobDate = Date().timeIntervalSince1970
			
		}
		
		_ = MLog.moveAlbum(from: oldPath, to: newPath, context: context)
		
		mobPath = path
		mobAlbumPath = path.deletingLastPathComponent()
		
	}
	
	func moveToWitoutLog(path: String, context: NSManagedObjectContext) {
		
		let oldPath = mobPath!
		let newPath = path
		
		let predicate = NSPredicate(format: "mobPath BEGINSWITH[c] %@", "\(oldPath)/")
		
		mobDate = Date().timeIntervalSince1970
		
		let album = MAlbum.mr_findFirst(with: NSPredicate(format: "mobPath == %@", oldPath), in: context)
		album?.mobPath = newPath
		album?.mobAlbumPath = newPath.deletingLastPathComponent()
		album?.mobDate = Date().timeIntervalSince1970
		
		(MAlbum.mr_findAll(with: predicate, in: context) as! [MAlbum]).sorted(by: { $0.0.mobDate < $0.1.mobDate }).forEach { (album) in
			
			album.mobPath = album.mobPath?.replacingOccurrences(of: oldPath, with: newPath)
			if let path = album.mobPath {
				album.mobAlbumPath = path.deletingLastPathComponent()
			}
			album.mobDate = Date().timeIntervalSince1970
			
		}
		
		(MFile.mr_findAll(with: predicate, in: context) as! [MFile]).sorted(by: { $0.0.mobDate < $0.1.mobDate }).forEach { (file) in
			
			file.mobPath = file.mobPath?.replacingOccurrences(of: oldPath, with: newPath)
			if let path = file.mobPath {
				file.mobFilePath = path.deletingLastPathComponent()
			}
			file.mobDate = Date().timeIntervalSince1970

		}
		
		mobPath = path
		mobAlbumPath = path.deletingLastPathComponent()		
	}
	
	static func createAlbumsForPath(path: String, context: NSManagedObjectContext) {
		
		var components = path.components(separatedBy: "/")
		
		components.removeFirst() // "/"
		components.removeFirst() // "albums"
		components.removeLast()  // "file.extension"
		
		var parent = File(url: FileService.sharedInstance.albumsDirectory)
		
		components.forEach { (component) in
			_ = MAlbum.createFileWithoutLog(with: "\(parent.relativePath)/\(component)", name: component, context: context)
			parent = File(url: parent.url.appendingPathComponent(component))
		}
		
	}
	
	static func secretPath() -> [String] {
		
		if let albums = MAlbum.mr_findAll() {
			
			return (albums as! [MAlbum]).filter( { LockType(rawValue: Int($0.mobLockType)) != .undefined } ).map( { $0.mobPath! } )
			
		}
		
		return [String]()
		
	}
	
	static func restorePassword(with path: String, hash: String, remove: Bool = false, context: NSManagedObjectContext) {
				
		var album: MAlbum?
		
		album = MAlbum.mr_findFirst(byAttribute: "mobPath", withValue: path, in: context)
		
		if album == nil {
			if let newAlbum = MAlbum.mr_createEntity(in: context) {
				newAlbum.mobThumbnail = "\(UUID().uuidString).jpg"
				newAlbum.mobPath = path
				newAlbum.mobAlbumPath = path.deletingLastPathComponent()
				newAlbum.mobDate = Date().timeIntervalSince1970
				newAlbum.mobMyNick = ""
				album = newAlbum
			}
		}
		
		let string = try! hash.aesDecrypt(key: "smartlocksmartlo", iv: "kcoltramskcoltra")
		let components = string.components(separatedBy: "/t/t/t")
		
		if let component = Int(components.first!), let lockType = LockType(rawValue: component), remove == false {
			album?.mobLockType = Int16(lockType.rawValue)
			album?.mobPasscode = components.last
		} else {
			album?.mobLockType = Int16(LockType.undefined.rawValue)
			album?.mobPasscode = ""
		}
		
		File.createDirectory(path: path, compleation: { (file, error) in })		
	}
}
