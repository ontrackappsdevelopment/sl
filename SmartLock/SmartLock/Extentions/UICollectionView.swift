//
//  UICollectionView.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UICollectionView {
	
	func register(_ nib: UINib?, forCellWithReuseIdentifier identifier: CellIdentifier) {
		register(nib, forCellWithReuseIdentifier: identifier.rawValue)
	}
	
	func dequeueReusableCell(withReuseIdentifier identifier: CellIdentifier, for indexPath: IndexPath) -> UICollectionViewCell {
		return dequeueReusableCell(withReuseIdentifier: identifier.rawValue, for: indexPath)
	}
	
}
