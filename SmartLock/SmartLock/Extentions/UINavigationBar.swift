//
//  UINavigationBar.swift
//  SmartLock
//
//  Created by Bogachev on 3/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UINavigationBar {

  func configurateTransparentNavigationBar() {
    
    setBackgroundImage(UIImage(), for: .default)
    shadowImage = UIImage()
    backgroundColor = UIColor.clear
    isTranslucent = true
    tintColor = UIColor.white
    barTintColor = UIColor.clear
    
  }

}
