//
//  UINavigationController.swift
//  SmartLock
//
//  Created by Bogachev on 4/6/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UINavigationController {
	
	var canLandscape: Bool {
		return viewControllers.contains(where: { $0 is SlideViewController })
	}
	
	override open func viewDidLoad() {
		super.viewDidLoad()
		setNeedsStatusBarAppearanceUpdate()
	}
	
	override open var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	open override var shouldAutorotate: Bool {
		return true
	}
	
	open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return canLandscape ? .all : .portrait
	}
	
}

extension UINavigationController {
	
	func setViewControllers(viewControllers: [UIViewController], animated: Bool, completion: @escaping () -> ()) {
		setViewControllers(viewControllers, animated: animated)
		
		if let coordinator = transitionCoordinator, animated {
			coordinator.animate(alongsideTransition: nil) { _ in
				completion()
			}
		} else {
			completion()
		}
		
		
	}
	
	func pushViewController(viewController: UIViewController, animated: Bool, completion: @escaping () -> ()) {
		pushViewController(viewController, animated: animated)
		
		if let coordinator = transitionCoordinator, animated {
			coordinator.animate(alongsideTransition: nil) { _ in
				completion()
			}
		} else {
			completion()
		}
	}
	
	func popViewController(animated: Bool, completion: @escaping () -> ()) {
		popViewController(animated: animated)
		
		if let coordinator = transitionCoordinator, animated {
			coordinator.animate(alongsideTransition: nil) { _ in
				completion()
			}
		} else {
			completion()
		}
	}
}
