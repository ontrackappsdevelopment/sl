//
//  UIColor.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UIColor {
  
  static var textColor: UIColor {
    return UIColor(red: 138/255, green: 146/255, blue: 168/255, alpha: 1.0)
  }
  
  static var blueBackgroundColor: UIColor {
    return UIColor(colorLiteralRed: 66/255, green: 18/255, blue: 138/255, alpha: 1)
  }
  
}
