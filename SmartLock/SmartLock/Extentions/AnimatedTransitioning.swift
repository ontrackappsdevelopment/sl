//
//  AnimatedTransitioning.swift
//  SmartLock
//
//  Created by Bogachev on 3/14/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class FadeInAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1.0
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

    let containerView = transitionContext.containerView
    _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
    let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
    
    containerView.addSubview(toVC!.view)
    toVC!.view.alpha = 0.0
    
    let duration = transitionDuration(using: transitionContext)
    UIView.animate(withDuration: duration, animations: {
      toVC!.view.alpha = 1.0
    }, completion: { finished in
      let cancelled = transitionContext.transitionWasCancelled
      transitionContext.completeTransition(!cancelled)
    })
    
  }
  
}

class FadeOutAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1.0
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    
    let containerView = transitionContext.containerView
    let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
    let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
    
    containerView.addSubview(toVC!.view)
    containerView.addSubview(fromVC!.view)
    fromVC!.view.alpha = 1.0
    
    let duration = transitionDuration(using: transitionContext)
    UIView.animate(withDuration: duration, animations: {
      fromVC!.view.alpha = 0.0
    }, completion: { finished in
      let cancelled = transitionContext.transitionWasCancelled
      transitionContext.completeTransition(!cancelled)
    })
    
  }
  
}

class DissolveAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1.5
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    
    let containerView = transitionContext.containerView
    let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
    let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
    
    containerView.addSubview(toVC!.view)
    containerView.addSubview(fromVC!.view)
    fromVC!.view.alpha = 1.0
    toVC!.view.alpha = 0.0
    
    let duration = transitionDuration(using: transitionContext)
    UIView.animate(withDuration: duration, animations: {
      toVC!.view.alpha = 1.0
      fromVC!.view.alpha = 0.0
    }, completion: { finished in
      let cancelled = transitionContext.transitionWasCancelled
      transitionContext.completeTransition(!cancelled)
    })
    
  }
  
}
