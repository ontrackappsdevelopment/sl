//
//  String.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
	
	static func randomNumeric(length: Int) -> String {
		let letters = "0123456789"
		let randomLength = UInt32(letters.characters.count)
		
		let randomString: String = (0 ..< length).reduce(String()) { accum, _ in
			let randomOffset = arc4random_uniform(randomLength)
			let randomIndex = letters.index(letters.startIndex, offsetBy: Int(randomOffset))
			return accum.appending(String(letters[randomIndex]))
		}
		
		return randomString
	}
	
	static func random(length: Int) -> String {
		let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		let randomLength = UInt32(letters.characters.count)
		
		let randomString: String = (0 ..< length).reduce(String()) { accum, _ in
			let randomOffset = arc4random_uniform(randomLength)
			let randomIndex = letters.index(letters.startIndex, offsetBy: Int(randomOffset))
			return accum.appending(String(letters[randomIndex]))
		}
		
		return randomString
	}
	
	var isEmail: Bool {
		
		let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
		return emailPredicate.evaluate(with: self)
		
	}
	
	var lastPathComponent: String {
		
		return NSString(string: self).lastPathComponent as String
		
	}
	
	func removeLastComponent() -> String {
		let lastComponent = self.lastPathComponent
		return self.replacingOccurrences(of: lastComponent, with: "")
	}
	
	var isVideo: Bool {
		
		if self.lastPathComponent.lowercased().hasSuffix("mov") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("mp4") {
			return true
		}
		
		return false
	}
	
	var isImage: Bool {
		
		if self.lastPathComponent.lowercased().hasSuffix("png") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("tiff") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("tif") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("jpeg") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("jpg") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("gif") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("bmp") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("bmpf") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("ico") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("cur") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("xbm") {
			return true
		}
		
		return false
	}
	
}

extension String {
	
	var MD5: String? {
		let length = Int(CC_MD5_DIGEST_LENGTH)
		
		guard let data = self.data(using: String.Encoding.utf8) else { return nil }
		
		let hash = data.withUnsafeBytes { (bytes: UnsafePointer<Data>) -> [UInt8] in
			var hash: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
			CC_MD5(bytes, CC_LONG(data.count), &hash)
			return hash
		}
		
		return (0..<length).map { String(format: "%02x", hash[$0]) }.joined()
	}
	
}

extension String {
	
	func aesEncrypt(key: String, iv: String) throws -> String {
		let data = self.data(using: .utf8)!
		let encrypted = try! AES(key: key, iv: iv, blockMode: .CBC, padding: PKCS7()).encrypt([UInt8](data))
		let encryptedData = Data(encrypted)
		return encryptedData.base64EncodedString()
	}
	
	func aesDecrypt(key: String, iv: String) throws -> String {
		let data = Data(base64Encoded: self)!
		let decrypted = try! AES(key: key, iv: iv, blockMode: .CBC, padding: PKCS7()).decrypt([UInt8](data))
		let decryptedData = Data(decrypted)
		return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
	}
	
}

extension String {
	func deletingLastPathComponent() -> String {
		return NSString(string: self).deletingLastPathComponent
	}
}
