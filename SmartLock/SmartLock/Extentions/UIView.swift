//
//  UIView.swift
//  Place2b
//
//  Created by Bogachev on 11/25/16.
//  Copyright © 2016 Pixelmate. All rights reserved.
//

import UIKit

@IBDesignable extension UIView {
  
  @IBInspectable var cornerRadius:CGFloat {
    
    get { return layer.cornerRadius }
    
    set { layer.cornerRadius = newValue }
    
  }
  
  @IBInspectable var borderWidth:CGFloat {
    
    get { return layer.borderWidth }
    
    set { layer.borderWidth = newValue }
    
  }

  @IBInspectable var borderColor:UIColor? {
    
    get {
      
      if let _ = layer.borderColor {
        
        return UIColor(cgColor: layer.shadowColor!)
        
      } else { return nil }
    }
    
    set {
      
      if let _ = newValue {
        
        layer.borderColor = newValue!.cgColor
        
      }
    }
    
  }
  
  @IBInspectable var shadowColor:UIColor? {
    
    get {
      
      if let _ = layer.shadowColor {
        
        return UIColor(cgColor: layer.shadowColor!)
        
      } else { return nil }
    }
    
    set {
      
      if let _ = newValue {
        
        layer.shadowColor = newValue!.cgColor
        
      }
    }
    
  }
  
  @IBInspectable var shadowRadius: CGFloat {
    
    get { return layer.shadowRadius }
    
    set { layer.shadowRadius = newValue }
    
  }
  
  @IBInspectable var shadowOffset: CGSize {
    
    get { return layer.shadowOffset }
    
    set { layer.shadowOffset = newValue }
    
  }
  
  @IBInspectable var masksToBounds: Bool {
    
    get { return layer.masksToBounds }
    
    set { layer.masksToBounds = newValue }
    
  }
  
  @IBInspectable var shadowOpacity: Float {
    
    get { return layer.shadowOpacity }
    
    set { layer.shadowOpacity = newValue }
    
  }
  
}

internal extension UIView {
  
  internal func roundCorners(corners:UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
  }
  
}

extension UIView {
  
  func animatedRemoveFromSuperView() {
    
    UIView.animate(withDuration: 0.3, animations: { 
      
      self.alpha = 0.0
      
    }) { (success) in
      
      self.removeFromSuperview()
      
    }
    
  }
  
}

extension UIView {
  
  func removeAllSubviews() {
    
    self.subviews.forEach( { $0.removeFromSuperview() } )
    
  }
  
}
