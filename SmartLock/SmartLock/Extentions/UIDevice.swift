//
//  UIDevice.swift
//  SmartLock
//
//  Created by Bogachev on 3/21/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UIDevice {

  enum DeviceType {
    case iPhone35
    case iPhone40
    case iPhone47
    case iPhone55
    case iPad
    case TV
	
    var isPhone: Bool {
      return UIDevice.current.userInterfaceIdiom == .phone
    }
	}
	
	func isIpad() -> Bool {
		return UIDevice.current.userInterfaceIdiom == .pad
	}
//
//  }
//
  var deviceType: DeviceType? {
    switch UIDevice.current.userInterfaceIdiom {
    case .tv:
      return .TV

    case .pad:
      return .iPad

    case .phone:
      let screenSize = UIScreen.main.bounds.size
      let height = max(screenSize.width, screenSize.height)

      switch height {
      case 480:
        return .iPhone35
      case 568:
        return .iPhone40
      case 667:
        return .iPhone47
      case 736:
        return .iPhone55
      default:
        return nil
      }

    default:
      return nil
    }
  }
}

