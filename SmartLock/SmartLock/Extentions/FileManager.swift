//
//  FileManager.swift
//  SmartLock
//
//  Created by Bogachev on 2/19/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension FileManager {

  func delete(paths: [String]) {
    
    for path in paths {
      
      do {
        
        try FileManager.default.removeItem(atPath: path)
        
      } catch let error as NSError {
        
        print("\(error.localizedDescription)")
        
      }
      
    }
    
  }
  
  func delete(urls: [URL]) {
    
    for ulr in urls {
      
      do {
        
        try FileManager.default.removeItem(at: ulr)
        
      } catch let error {
        
        print("\(error.localizedDescription)")
        
      }
      
    }
    
  }
  
}
