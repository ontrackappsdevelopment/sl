//
//  UIImageView.swift
//  CameraTest
//
//  Created by Bogachev on 1/30/17.
//  Copyright © 2017 SBogachev. All rights reserved.
//

import Foundation
import AVFoundation

extension UIImageView {
  
  func setImageAsVideoThumbnail(videoURL: URL) {
    
    do {
      let asset = AVURLAsset(url: videoURL, options: nil)
      let imageGenerator = AVAssetImageGenerator(asset: asset)
      imageGenerator.appliesPreferredTrackTransform = true
      let image = try imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
      let thumbnailImage = UIImage(cgImage: image)
      
      DispatchQueue.main.async {
        self.image = thumbnailImage
      }
      
    } catch let error {
      print("*** Error generating thumbnail: \(error.localizedDescription)")
    }
    
  }


  
}
