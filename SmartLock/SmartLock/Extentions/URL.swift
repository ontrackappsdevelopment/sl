//
//  URL.swift
//  CameraTest
//
//  Created by Bogachev on 1/30/17.
//  Copyright © 2017 SBogachev. All rights reserved.
//

import Foundation

extension URL {
	
	var isDir: Bool {
		
		var isDir: ObjCBool = false;
		
		FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
		
		return isDir.boolValue
		
	}
	
	var isVideo: Bool {
		
		if self.lastPathComponent.lowercased().hasSuffix("mov") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("mp4") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("m4v") {
			return true
		}
		
		return false
	}
	
	var isImage: Bool {
		
		if self.lastPathComponent.lowercased().hasSuffix("png") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("tiff") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("tif") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("jpeg") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("jpg") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("gif") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("bmp") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("bmpf") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("ico") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("cur") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("xbm") {
			return true
		}
		
		if self.lastPathComponent.lowercased().hasSuffix("ig") {
			return true
		}
		
		return false
	}
	
	var isArchive: Bool {
		return self.lastPathComponent.lowercased().hasSuffix("zip")
	}
	
	var isAudio: Bool {
		
		if self.lastPathComponent.lowercased().hasSuffix("opus") {
			return true
		}

		if self.lastPathComponent.lowercased().hasSuffix("mp3") {
			return true
		}
		
		return false
	}
}
