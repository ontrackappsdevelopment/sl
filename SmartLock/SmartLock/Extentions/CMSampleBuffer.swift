//
//  CMSampleBuffer.swift
//  SmartLock
//
//  Created by Bogachev on 3/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AVFoundation

extension CMSampleBuffer {
  func image(orientation: UIImageOrientation = .up, scale: CGFloat = 1.0) -> UIImage? {
    guard let buffer = CMSampleBufferGetImageBuffer(self) else { return nil }
    
    let ciImage = CIImage(cvPixelBuffer: buffer)
    
    let image = UIImage(ciImage: ciImage)
    
    return image
  }
  
  func imageFromSampleBuffer() -> UIImage {
    
    let imageBuffer = CMSampleBufferGetImageBuffer(self)!
    
    CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))
    
    let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
    let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
    let width = CVPixelBufferGetWidth(imageBuffer)
    let height = CVPixelBufferGetHeight(imageBuffer)
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    
    let bitmapInfo = CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
    let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
    let quartzImage = context!.makeImage()
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))
    
    let image = UIImage(cgImage: quartzImage!, scale: 1.0, orientation: UIImageOrientation.right)

    return image
    
  }
}
