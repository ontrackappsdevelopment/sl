//
//  UIFont.swift
//  SmartLock
//
//  Created by Bogachev on 8/12/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UIFont {

	static var titleMedium: UIFont? {
		return UIFont(name: "SFUIDisplay-Medium", size: 16)
	}
	
	static var titleBold: UIFont? {
		return UIFont(name: "SFUIDisplay-Bold", size: 16)
	}
	
}
