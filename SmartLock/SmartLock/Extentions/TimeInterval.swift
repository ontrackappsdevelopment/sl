//
//  TimeInterval.swift
//  SmartLock
//
//  Created by Bogachev on 3/26/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension TimeInterval {
  
  func toString() -> String {
    
    let ti = NSInteger(self)
    
    let seconds = ti % 60
    let minutes = (ti / 60) % 60
    let hours = (ti / 3600)
    
    return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
  }
}

