//
//  StoreKitService.swift
//  ChatKepper
//
//  Created by Bogachev on 10/24/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import SwiftyStoreKit

class StoreKitService {
	
	func completeIAPTransactions() {
		
		SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
			
			for purchase in purchases {
				if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
					
					if purchase.needsFinishTransaction {
						// Deliver content from server, then:
						SwiftyStoreKit.finishTransaction(purchase.transaction)
					}
					print("purchased: \(purchase.productId)")
				}
			}
		}
	}
}
