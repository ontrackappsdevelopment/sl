//
//  UIAlertController.swift
//  Eventizer
//
//  Created by Bogachev on 11/4/16.
//  Copyright © 2016. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardSceneType {
	static var storyboardName: String { get }
}

extension StoryboardSceneType {
	
	static func storyboard() -> UIStoryboard {
		return UIStoryboard(name: self.storyboardName, bundle: nil)
	}
	
	static func initialViewController<T>(type: T.Type) -> T {
		guard let vc = storyboard().instantiateInitialViewController() as? T else {
			fatalError("Failed to instantiate initialViewController for \(storyboardName)")
		}
		print("\(vc)")
		return vc
	}
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
	func viewController() -> UIViewController {
		return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
	}
	
	static func viewController(identifier: Self) -> UIViewController {
		return identifier.viewController()
	}
	
	func instantiateViewController<T>(type: T.Type) -> T {
		guard let vc = self.viewController() as? T else {
			fatalError("ViewController 'confirm' is not of the expected class \(type).")
		}
		return vc
	}
}

struct Storyboards {
	static var main: UIStoryboard {
		return UIStoryboard(name: "Main", bundle: nil)
	}
}

struct StoryboardScene {
	
	static func instantiateChoosePasscodeTypeScene() -> ChoosePasscodeTypeViewController {				
		return UIDevice.current.isIpad() ? LockPad.instantiateChoosePasscodeTypeScene() : Lock.instantiateChoosePasscodeTypeScene()
	}
	
	static func instantiateSettingsScene() -> SettingsViewController {
		return UIDevice.current.isIpad() ? MainPad.instantiateSettingsScene() : Main.instantiateSettingsScene()
	}
	
	static func instantiatePurchasesScene() -> PurchasesViewController {
		return UIDevice.current.isIpad() ? LockPad.instantiatePurchasesScene() : Lock.instantiatePurchasesScene()
	}
	
	static func instantiateAlbumSettingsScene() -> AlbumViewController {
		return UIDevice.current.isIpad() ? MainPad.instantiateAlbumSettingsScene() : Main.instantiateAlbumSettingsScene()
	}
	
	static func instantiateBreakInLogScene() -> BreakInLogViewController {
		return UIDevice.current.isIpad() ? MainPad.instantiateBreakInLogScene() : Main.instantiateBreakInLogScene()
	}
	
	static func instantiateMoveToScene() -> MoveToViewController {
		return UIDevice.current.isIpad() ? MainPad.instantiateMoveToScene() : Main.instantiateMoveToScene()
	}
	
	static func instantiateNumpadLockScene(modal: Bool) -> NumpadLockViewController {
		return UIDevice.current.isIpad() ? (modal == true ? LockPad.instantiateNumpadLockModelScene() : LockPad.instantiateNumpadLockPadScene()) : Lock.instantiateNumpadLockPadScene()
	}
	
	static func instantiatePatternLockScene(modal: Bool) -> PatternLockViewController {
		return UIDevice.current.isIpad() ? (modal == true ? LockPad.instantiatePatternLockModalScene() : LockPad.instantiatePatternLockPadScene()) : Lock.instantiatePatternLockScene()
	}
	
	static func instantiateAlphanumericLockScene(modal: Bool) -> AlphanumericLockViewController {
		return UIDevice.current.isIpad() ? (modal == true ? LockPad.instantiateAlphanumericLockModalScene() : LockPad.instantiateAlphanumericLockPadScene()) : Lock.instantiateAlphanumericLockScene()
	}
	
	static func instantiateEmailScene() -> EmailViewController {
		return UIDevice.current.isIpad() ? LockPad.instantiateEmailScene() : Lock.instantiateEmailScene()
	}
	
	static func instantiateChangeEmailScene() -> EmailViewController {
		return UIDevice.current.isIpad() ? LockPad.instantiateChangeEmailScene() : Lock.instantiateChangeEmailScene()
	}

	static func instantiateConfirmCodeScene() -> ConfirmCodeViewController {
		return UIDevice.current.isIpad() ? LockPad.instantiateConfirmCodeScene() : Lock.instantiateConfirmCodeScene()
	}
	
	static func instantiatePasscodeRecoveryScene() -> ConfirmCodeViewController {
		return UIDevice.current.isIpad() ? LockPad.instantiatePasscodeRecoveryScene() : Lock.instantiatePasscodeRecoveryScene()
	}

	enum Lock: String, StoryboardSceneType {
		static var storyboardName: String = "Lock"
		
		case purchasesScene = "PurchasesViewController"
		static func instantiatePurchasesScene() -> PurchasesViewController {
			return StoryboardScene.Lock.purchasesScene.instantiateViewController(type: PurchasesViewController.self)
		}
		
		case choosePasscodeTypeScene = "ChoosePasscodeTypeViewController"
		static func instantiateChoosePasscodeTypeScene() -> ChoosePasscodeTypeViewController {
			return StoryboardScene.Lock.choosePasscodeTypeScene.instantiateViewController(type: ChoosePasscodeTypeViewController.self)
		}
		
		case numpadLockScene = "NumpadLockViewController"
		static func instantiateNumpadLockPadScene() -> NumpadLockViewController {
			return StoryboardScene.Lock.numpadLockScene.instantiateViewController(type: NumpadLockViewController.self)
		}

		case patternLockScene = "PatternLockViewController"
		static func instantiatePatternLockScene() -> PatternLockViewController {
			return StoryboardScene.Lock.patternLockScene.instantiateViewController(type: PatternLockViewController.self)
		}
		
		case alphanumericLockScene = "AlphanumericLockViewController"
		static func instantiateAlphanumericLockScene() -> AlphanumericLockViewController {
			return StoryboardScene.Lock.alphanumericLockScene.instantiateViewController(type: AlphanumericLockViewController.self)
		}
		
		case emailScene = "EmailViewController"
		static func instantiateEmailScene() -> EmailViewController {
			return StoryboardScene.Lock.emailScene.instantiateViewController(type: EmailViewController.self)
		}
		
		case changeEmailScene = "ChangeEmailViewController"
		static func instantiateChangeEmailScene() -> EmailViewController {
			return StoryboardScene.Lock.changeEmailScene.instantiateViewController(type: EmailViewController.self)
		}
		
		case confirmCodeScene = "ConfirmCodeViewController"
		static func instantiateConfirmCodeScene() -> ConfirmCodeViewController {
			return StoryboardScene.Lock.confirmCodeScene.instantiateViewController(type: ConfirmCodeViewController.self)
		}

		case passcodeRecoveryScene = "PasscodeRecoveryViewController"
		static func instantiatePasscodeRecoveryScene() -> ConfirmCodeViewController {
			return StoryboardScene.Lock.passcodeRecoveryScene.instantiateViewController(type: ConfirmCodeViewController.self)
		}
		
	}

	enum LockPad: String, StoryboardSceneType {
		static var storyboardName: String = "Lock_Pad"
		
		case purchasesScene = "PurchasesViewController"
		static func instantiatePurchasesScene() -> PurchasesViewController {
			return StoryboardScene.LockPad.purchasesScene.instantiateViewController(type: PurchasesViewController.self)
		}
		
		case choosePasscodeTypeScene = "ChoosePasscodeTypeViewController"
		static func instantiateChoosePasscodeTypeScene() -> ChoosePasscodeTypeViewController {
			return StoryboardScene.LockPad.choosePasscodeTypeScene.instantiateViewController(type: ChoosePasscodeTypeViewController.self)
		}
		
		case numpadLockModalScene = "NumpadLockModalViewController"
		static func instantiateNumpadLockModelScene() -> NumpadLockViewController {
			return StoryboardScene.LockPad.numpadLockModalScene.instantiateViewController(type: NumpadLockViewController.self)
		}
		
		case numpadLockPadScene = "NumpadLockPadViewController"
		static func instantiateNumpadLockPadScene() -> NumpadLockViewController {
			return StoryboardScene.LockPad.numpadLockPadScene.instantiateViewController(type: NumpadLockViewController.self)
		}
		
		case patternLockModalScene = "PatternLockModalViewController"
		static func instantiatePatternLockModalScene() -> PatternLockViewController {
			return StoryboardScene.LockPad.patternLockModalScene.instantiateViewController(type: PatternLockViewController.self)
		}
		
		case patternLockPadScene = "PatternLockPadViewController"
		static func instantiatePatternLockPadScene() -> PatternLockViewController {
			return StoryboardScene.LockPad.patternLockPadScene.instantiateViewController(type: PatternLockViewController.self)
		}
		
		case alphanumericLockModalScene = "AlphanumericLockModalViewController"
		static func instantiateAlphanumericLockModalScene() -> AlphanumericLockViewController {
			return StoryboardScene.LockPad.alphanumericLockModalScene.instantiateViewController(type: AlphanumericLockViewController.self)
		}
		
		case alphanumericLockPadScene = "AlphanumericLockPackViewController"
		static func instantiateAlphanumericLockPadScene() -> AlphanumericLockViewController {
			return StoryboardScene.LockPad.alphanumericLockPadScene.instantiateViewController(type: AlphanumericLockViewController.self)
		}
		
		case emailScene = "EmailPadViewController"
		static func instantiateEmailScene() -> EmailViewController {
			return StoryboardScene.LockPad.emailScene.instantiateViewController(type: EmailViewController.self)
		}
		
		case changeEmailScene = "ChangeEmailPadViewController"
		static func instantiateChangeEmailScene() -> EmailViewController {
			return StoryboardScene.LockPad.changeEmailScene.instantiateViewController(type: EmailViewController.self)
		}
		
		case confirmCodeScene = "ConfirmCodePadViewController"
		static func instantiateConfirmCodeScene() -> ConfirmCodeViewController {
			return StoryboardScene.LockPad.confirmCodeScene.instantiateViewController(type: ConfirmCodeViewController.self)
		}

		case passcodeRecoveryScene = "PasscodeRecoveryPadViewController"
		static func instantiatePasscodeRecoveryScene() -> ConfirmCodeViewController {
			return StoryboardScene.LockPad.passcodeRecoveryScene.instantiateViewController(type: ConfirmCodeViewController.self)
		}

	}
	
	enum Main: String, StoryboardSceneType {
		static var storyboardName: String = "Main"
		
		case photosScene = "PhotosViewController"
		static func instantiatePhotosScene() -> PhotosViewController {
			return StoryboardScene.Main.photosScene.instantiateViewController(type: PhotosViewController.self)
		}
		
		case settingsScene = "SettingsViewController"
		static func instantiateSettingsScene() -> SettingsViewController {
			return StoryboardScene.Main.settingsScene.instantiateViewController(type: SettingsViewController.self)
		}
		
		case albumSettingsScene = "AlbumViewController"
		static func instantiateAlbumSettingsScene() -> AlbumViewController {
			return StoryboardScene.Main.albumSettingsScene.instantiateViewController(type: AlbumViewController.self)
		}
		
		case breakInLogScene = "BreakInLogViewController"
		static func instantiateBreakInLogScene() -> BreakInLogViewController {
			return StoryboardScene.Main.breakInLogScene.instantiateViewController(type: BreakInLogViewController.self)
		}
		
		case moveToScene = "MoveToViewController"
		static func instantiateMoveToScene() -> MoveToViewController {
			return StoryboardScene.Main.moveToScene.instantiateViewController(type: MoveToViewController.self)
		}
		
		case slideScene = "SlideViewController"
		static func instantiateSlideScene() -> SlideViewController {
			return StoryboardScene.Main.slideScene.instantiateViewController(type: SlideViewController.self)
		}
		
		case tutorialScene = "TutorialViewController"
		static func instantiateTutorialScene() -> TutorialViewController {
			return StoryboardScene.Main.tutorialScene.instantiateViewController(type: TutorialViewController.self)
		}
	}
	
	enum MainPad: String, StoryboardSceneType {
		static var storyboardName: String = "Main_Pad"
		
		case settingsScene = "SettingsViewController"
		static func instantiateSettingsScene() -> SettingsViewController {
			return StoryboardScene.MainPad.settingsScene.instantiateViewController(type: SettingsViewController.self)
		}
		
		case purchasesScene = "PurchasesViewController"
		static func instantiatePurchasesScene() -> PurchasesViewController {
			return StoryboardScene.MainPad.purchasesScene.instantiateViewController(type: PurchasesViewController.self)
		}
		
		case albumSettingsScene = "AlbumViewController"
		static func instantiateAlbumSettingsScene() -> AlbumViewController {
			return StoryboardScene.MainPad.albumSettingsScene.instantiateViewController(type: AlbumViewController.self)
		}
		
		case breakInLogScene = "BreakInLogViewController"
		static func instantiateBreakInLogScene() -> BreakInLogViewController {
			return StoryboardScene.MainPad.breakInLogScene.instantiateViewController(type: BreakInLogViewController.self)
		}
		
		case moveToScene = "MoveToViewController"
		static func instantiateMoveToScene() -> MoveToViewController {
			return StoryboardScene.MainPad.moveToScene.instantiateViewController(type: MoveToViewController.self)
		}
	}
	
	enum Chat: String, StoryboardSceneType {
		static var storyboardName: String = "Chat"
		
		case chatListScene = "ChatListViewController"
		static func instantiateChatListScene() -> ChatListViewController {
			return StoryboardScene.Chat.chatListScene.instantiateViewController(type: ChatListViewController.self)
		}

		case chatScene = "ChatViewController"
		static func instantiateChatScene() -> ChatViewController {
			return StoryboardScene.Chat.chatScene.instantiateViewController(type: ChatViewController.self)
		}
		
		case chooseNickNameScene = "ChooseNickNameViewController"
		static func instantiateChooseNickName() -> ChooseNickNameViewController {
			return StoryboardScene.Chat.chooseNickNameScene.instantiateViewController(type: ChooseNickNameViewController.self)
		}

	}
}

extension UIViewController {
	
    func push(from viewController: UIViewController, animated: Bool = true) {
		
		viewController.navigationController?.pushViewController(self, animated: animated)
	}
	
	func presentBy(_ viewController: UIViewController) {
		
		viewController.present(self, animated: true, completion: nil)
		
	}
    
    func popToRoot() {
        navigationController?.popToRootViewController(animated: true)
    }
}

