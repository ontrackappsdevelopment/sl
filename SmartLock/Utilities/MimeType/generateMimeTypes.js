var fs = require('fs');

var text = fs.readFileSync('/usr/local/etc/nginx/mime.types').toString('utf8');

var lines = text
.replace(/[\s\n]*types\s*{/, '')
.replace(/}[\s\n]*/, '')
.replace(/\n\s+/g, '\n')
.split('\n')
.filter(function(l) { return /^[^\s]/.test(l) })
.map(function(l) {
  var m = l.match(/^([^\s]+)\s+([^;]+);/);
  return [m[1], m[2].split(' ')];
});

var map = {};

lines.forEach(function(l) {
  l[1].forEach(function(ext) {
    map[ext] = l[0];
  })
});

console.info(JSON.stringify(map, null, 2));
