//
//  TutorialViewController.swift
//  ChatKepper
//
//  Created by Bogachev on 10/2/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class TutorialViewController: BaseViewController, UIScrollViewDelegate {

	var leftSwipeGestureRecognizer: UISwipeGestureRecognizer?
	var rightSwipeGestureRecognizer: UISwipeGestureRecognizer?
	
	@IBOutlet weak var actionButton: UIButton!
	@IBOutlet weak var tutorialScrollView: UIScrollView! {
		didSet {
			tutorialScrollView.isScrollEnabled = false
			tutorialScrollView.delegate = self
			
			leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(onLeftSwipe(sender:)))
			if let leftSwipeGestureRecognizer = leftSwipeGestureRecognizer {
				leftSwipeGestureRecognizer.direction = .left
				tutorialScrollView.addGestureRecognizer(leftSwipeGestureRecognizer)
			}
			
			rightSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(onRightSwipe(sender:)))
			if let rightSwipeGestureRecognizer = rightSwipeGestureRecognizer {
				rightSwipeGestureRecognizer.direction = .right
				tutorialScrollView.addGestureRecognizer(rightSwipeGestureRecognizer)
			}
		}
	}
	
	@IBOutlet weak var tutorialPageControll: UIPageControl!
	
	override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		fillScrollView()
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
	}
	
	private func fillScrollView() {
		tutorialScrollView.removeAllSubviews()
		
		var x: CGFloat = 0.0
		let y: CGFloat = 0.0
		let width = UIScreen.main.bounds.width
		let height = UIScreen.main.bounds.height
		
		for i in 1...4 {
			let frame = CGRect(x: x, y: y, width: width, height: height)
			let imageView = UIImageView(frame: frame)
			
			imageView.contentMode = .scaleAspectFill
			if UIDevice.current.isIpad() == true {
				imageView.contentMode = .scaleAspectFit
			}
			
			imageView.image = UIImage(named: "tutorial\(i)")
			x += width
			
			tutorialScrollView.addSubview(imageView)
		}
		
		tutorialScrollView.contentSize = CGSize(width: width * 4, height: height)
		tutorialPageControll.numberOfPages = 4
	}
	
	@objc private func onLeftSwipe(sender: UISwipeGestureRecognizer) {
		var x:CGFloat = 0
		if tutorialPageControll.currentPage == 3 {
			x = CGFloat(tutorialPageControll.currentPage) * tutorialScrollView.frame.size.width
		} else {
			let page = tutorialPageControll.currentPage + 1
			x = CGFloat(page) * tutorialScrollView.frame.size.width
		}
		tutorialScrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
	}
	
	@objc private func onRightSwipe(sender: UISwipeGestureRecognizer) {
		var x:CGFloat = 0
		if tutorialPageControll.currentPage == 0 {
			x = 0.0
		} else {
			let page = tutorialPageControll.currentPage - 1
			x = CGFloat(page) * tutorialScrollView.frame.size.width
		}
		tutorialScrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		guard tutorialPageControll.numberOfPages > 0 else { return }		
		let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
		tutorialPageControll.currentPage = Int(pageNumber)
		
		if tutorialPageControll.currentPage < 3 {
			actionButton.setTitle("Skip", for: .normal)
			actionButton.setTitleColor(UIColor(red: 132.0/255.0, green: 132.0/255.0, blue: 132.0/255.0, alpha: 1.0), for: .normal)
			actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
		} else {
			actionButton.setTitle("Close", for: .normal)
			actionButton.setTitleColor(UIColor.red, for: .normal)
			actionButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
		}
	}

	@IBAction func onAction(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
}


