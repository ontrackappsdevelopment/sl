//
//  CollectionPushAndPoppable.swift
//  PopInAndOutCollectionViewTransition
//
//  Created by Stefano Vettor on 15/12/15.
//  Copyright © 2015 Stefano Vettor. All rights reserved.
//

import Foundation
import UIKit
import DragDropiOS

protocol CollectionPushAndPoppable {
    weak var collectionView: DragDropCollectionView!  { get }
    var sourceCell: UICollectionViewCell? { get }
//    var collectionView: UICollectionView? { get }
    var view: UIView! { get }
}
