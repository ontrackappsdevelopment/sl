//
//  ShareViewController.swift
//  SmartLockShare
//
//  Created by Bogachev on 6/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices

class ShareViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var statusLabel: UILabel!
	
	private var sourceURL: URL?
	private var targetURL: URL?
	
	var containerURL: URL? {
		return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.chkeeper.free")?.appendingPathComponent("Extension")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.statusLabel.text = ""
		
		SVProgressHUD.setViewForExtension(self.view)
		
		guard let containerURL = containerURL else { return }
		
		if !FileManager.default.fileExists(atPath: containerURL.path) {
			try? FileManager.default.createDirectory(at: containerURL, withIntermediateDirectories: true, attributes: nil)
		}
		
		let extensionItem = extensionContext?.inputItems.first as? NSExtensionItem
		let itemProvider = extensionItem?.attachments?.first as? NSItemProvider
		
		if let itemProvider = itemProvider {
			let contentType = kUTTypeZipArchive as String
			if itemProvider.hasItemConformingToTypeIdentifier(contentType) {
				
				SVProgressHUD.show()
				itemProvider.loadItem(forTypeIdentifier: contentType, options: nil) { url, error in
					
					if let url = url as? URL {
						self.sourceURL = url
						let fileName = url.lastPathComponent
						self.targetURL = containerURL.appendingPathComponent(fileName)
						
						DispatchQueue.main.async {
							SVProgressHUD.dismiss()
							self.statusLabel.text = fileName
						}						
					}
				}
			}
		}
	}
	
	func verifyFileName(in directoryURL: URL, name: String, index: Int = 0) -> String? {
		
		let fileName = name.components(separatedBy: ".").first
		let fileExtension = name.components(separatedBy: ".").last
		var newName = index == 0 ? fileName! : "\(fileName!) (\(index))"
		
		if name.contains(".") {
			newName += "." + fileExtension!
		}
		
		let path = directoryURL.appendingPathComponent(newName).path
		
		if !FileManager.default.fileExists(atPath: path) {
			return newName
		} else {
			if let containerURL = containerURL {
				return verifyFileName(in: containerURL, name: name, index: index + 1)
			} else {
				return nil
			}
		}
		
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
		let cancelError = NSError(domain: NSCocoaErrorDomain, code: -1, userInfo: nil)
		self.extensionContext!.cancelRequest(withError: cancelError)
	}
	
	@IBAction func onAdd(_ sender: UIButton) {
		guard let sourceURL = sourceURL, let targetURL = targetURL else { return }
		
		if FileManager.default.fileExists(atPath: targetURL.path) {
			try? FileManager.default.removeItem(atPath: targetURL.path)
		}
		
		do {
			try FileManager.default.copyItem(at: sourceURL, to: targetURL)
		} catch let error as NSError {
			print("\(error)")
		}
		
		self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
	}
}
