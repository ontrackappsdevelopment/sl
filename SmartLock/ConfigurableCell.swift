//
//  ConfigurableCell.swift
//  UniGuide
//
//  Created by Bogachev on 4/26/17.
//  Copyright © 2017 VIT Art LLC. All rights reserved.
//

import Foundation

protocol ConfigurableCell {
	associatedtype T
	var viewModel: T? { get set }
	func configureWithViewModel(_: T)
	static var identifier: String { get }
}
